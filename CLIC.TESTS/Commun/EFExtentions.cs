﻿using Clic.DAL.Entities.Base;
using Clic.Data.DbContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLIC.TESTS.Commun
{
    public static class EFExtentions
    {
        public static void AddOrUpdate<T>(
            Func<T, object> propertyToMatch, IEnumerable<T> entities)
            where T : EntityBase
        {
            List<T> existingData;
            using (AppDbContext db = new AppDbContext())
            {
                existingData = db.Set<T>().ToList();
                foreach (T item in entities)
                {
                    bool exists = existingData.Any(g => propertyToMatch(g).Equals(propertyToMatch(item)));
                    if (!exists)
                    {
                        db.Entry(item).State = EntityState.Added;
                    }
                    else
                    {
                        T entity = existingData.First(g => propertyToMatch(g).Equals(propertyToMatch(item)));
                        db.Entry(entity).State = EntityState.Detached;

                        item.Id = entity.Id;
                        try
                        {
                            db.Entry(item).State = EntityState.Modified;

                        }
                        catch
                        {

                        }
                    }

                }

                db.SaveChanges();
            }
        }

    }
}
