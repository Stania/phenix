﻿using Clic.BLL.IServices;
using Clic.BLL.Services.FicheZep;
using Clic.DAL.Entities.FicheZep;
using Clic.Data.DbContext;
using CLic.BLL.Services.FicheZep;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PHNIX.BLL.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Clic.DAL.Persistance.Commun;
using System.Linq;
using Clic.BLL.Services.Commun;
using Clic.DAL.Entities.Profils;
using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Entities.Profil;
using Clic.BLL.Services.Profil;

namespace CLIC.TESTS.Commun
{
    [TestClass]
    public class SeedTest
    {
        public SeedTest()
        {
        }

        [TestMethod]
        public void Seed()
        {
            using (AppDbContext context = new AppDbContext())
            {
                //secteur
                var secteurMante = new Secteur() { Nom = Constantes.Secteurs.Mantes };
                EFExtentions.AddOrUpdate<Secteur>(t => t.Nom, new List<Secteur>() { secteurMante });

                //types de mesures de protection
                List<MesureProtectionType> mesureTypes = new List<MesureProtectionType>()
                {
                    new MesureProtectionType(){ Nom = Constantes.MesureProtectionTypes.ProtectionEtVerification },
                    new MesureProtectionType(){ Nom = Constantes.MesureProtectionTypes.ItinérairesAdetruirePRCI },
                    new MesureProtectionType(){ Nom = Constantes.MesureProtectionTypes.ItinérairesAdetruireEtAMunirDunDA },
                    new MesureProtectionType(){ Nom = Constantes.MesureProtectionTypes.OrganeDeCommandeAplacerEtAmunirDeDA },
                    new MesureProtectionType(){ Nom = Constantes.MesureProtectionTypes.DialogueDeProtection },
                    new MesureProtectionType(){ Nom = Constantes.MesureProtectionTypes.AutorisationADetruire },
                    new MesureProtectionType(){ Nom = Constantes.MesureProtectionTypes.AiguilleImmobiliseeDansPositionIndiquee },
                    new MesureProtectionType(){ Nom = Constantes.MesureProtectionTypes.ConditonParticuliereVerificationLiberation },
                    new MesureProtectionType(){ Nom = Constantes.MesureProtectionTypes.JalonDarretAplacerSurLeTerrain },
                };
                EFExtentions.AddOrUpdate<MesureProtectionType>(t => t.Nom, mesureTypes);

                //Types zeps
                List<ZepType> zeptypes = new List<ZepType>()
                {
                    new ZepType(){ Nom = Constantes.ZepTypes.TypeG },
                    new ZepType(){ Nom = Constantes.ZepTypes.TypeL },
                    new ZepType(){ Nom = Constantes.ZepTypes.TypeLG }
                };
                EFExtentions.AddOrUpdate<ZepType>(t => t.Nom, zeptypes);

                //Types zeps
                List<AutreDepecheType> autreDepecheType = new List<AutreDepecheType>()
                {
                    new AutreDepecheType(){ Propriete = Constantes.AutreDepecheTypes.Point_Remarquable, Nom = "Point Remarquable" },
                    new AutreDepecheType(){ Propriete = Constantes.AutreDepecheTypes.RPTX , Nom = "RPTX" },
                    new AutreDepecheType(){ Propriete = Constantes.AutreDepecheTypes.Telephone , Nom = "N° téléphone" },
                };
                EFExtentions.AddOrUpdate<AutreDepecheType>(t => t.Nom, autreDepecheType);

                //types particularites
                List<ParticulariteType> particularitetypes = new List<ParticulariteType>()
                {
                    new ParticulariteType(){ Nom = Constantes.ParticulariteTypes.ApplicableDansCondition },
                    new ParticulariteType(){ Nom = Constantes.ParticulariteTypes.DFVResititueeOccupee },
                    new ParticulariteType(){ Nom = Constantes.ParticulariteTypes.TTXStationne },
                    new ParticulariteType(){ Nom = Constantes.ParticulariteTypes.AutresDispositionsParticulieresTTX },
                    new ParticulariteType(){ Nom = Constantes.ParticulariteTypes.AiguillesAPositionObligee },
                    new ParticulariteType(){ Nom = Constantes.ParticulariteTypes.AiguillesADisposerDansPositionPrevue },
                    new ParticulariteType(){ Nom = Constantes.ParticulariteTypes.DispositionsParticulieresParExploitation },
                    new ParticulariteType(){ Nom = Constantes.ParticulariteTypes.OutilDeBouclageParLEquipement },
                    new ParticulariteType(){ Nom = Constantes.ParticulariteTypes.DispositionsParticulieresParLEquipement },
                    new ParticulariteType(){ Nom = Constantes.ParticulariteTypes.AutresMesuresEventiellesParLEquipement },
                };
                EFExtentions.AddOrUpdate<ParticulariteType>(t => t.Nom, particularitetypes);

                //points remarquable
                List<PointRemarquableType> pointRemarquableTypes = new List<PointRemarquableType>()
                {
                    new PointRemarquableType(){ Nom = Constantes.PointRemarquableTypes.PointEngagement },
                    new PointRemarquableType(){ Nom = Constantes.PointRemarquableTypes.PointDegagement },
                    new PointRemarquableType(){ Nom = Constantes.PointRemarquableTypes.SignauxIntermediaires }
                };
                EFExtentions.AddOrUpdate<PointRemarquableType>(t => t.Nom, pointRemarquableTypes);

                //manoeuvre types
                List<ManoeuvreType> manoeuvreTypes = new List<ManoeuvreType>()
                {
                    new ManoeuvreType(){ Nom = Constantes.ManoeuvreTypes.Engagement },
                    new ManoeuvreType(){ Nom = Constantes.ManoeuvreTypes.Degagement },
                    new ManoeuvreType(){ Nom = Constantes.ManoeuvreTypes.Franchissement },
                };
                EFExtentions.AddOrUpdate<ManoeuvreType>(t => t.Nom, manoeuvreTypes);

                //occupation types
                List<OccupationType> occupationTypes = new List<OccupationType>()
                {
                    new OccupationType(){ Nom = Constantes.OccupationTypes.OccupeeALAccord },
                    new OccupationType(){ Nom = Constantes.OccupationTypes.OccupeeALaRestitution },
                };
                EFExtentions.AddOrUpdate<OccupationType>(t => t.Nom, occupationTypes);

                //postes
                List<Poste> postes = new List<Poste>();
                foreach (var item in Constantes.Postes.PosteList)
                {
                    postes.Add(new Poste() { Nom = item, SecteurId = secteurMante.Id });
                }
                EFExtentions.AddOrUpdate<Poste>(t => t.Nom, postes);

                //Statuts zep
                EFExtentions.AddOrUpdate<StatutZep>(t => t.Statut, new List<StatutZep>()
                {
                    new StatutZep(){Statut = Constantes.ZepStatuts.EnCreation},
                    new StatutZep(){Statut = Constantes.ZepStatuts.EnAccord},
                    new StatutZep(){Statut = Constantes.ZepStatuts.Accordee},
                    new StatutZep(){Statut = Constantes.ZepStatuts.Restituee},
                    new StatutZep(){Statut = Constantes.ZepStatuts.Annulee},
                    new StatutZep(){Statut = Constantes.ZepStatuts.LeveeMesure},
                    new StatutZep(){Statut = Constantes.ZepStatuts.Refusee}
                });

                //Statuts zep
                EFExtentions.AddOrUpdate<DepecheType>(t => t.Nom, new List<DepecheType>()
                {
                    new DepecheType(){Nom = Constantes.DepecheType.Engagement},
                    new DepecheType(){Nom = Constantes.DepecheType.AutreDepeche},
                    new DepecheType(){Nom = Constantes.DepecheType.Degagement},

                    new DepecheType(){Nom = Constantes.DepecheType.Notification},
                    new DepecheType(){Nom = Constantes.DepecheType.Restitution},

                    new DepecheType(){Nom = Constantes.DepecheType.RestitutionEngagement},
                    new DepecheType(){Nom = Constantes.DepecheType.RestitutionDegagement},

                    new DepecheType(){Nom = Constantes.DepecheType.MesureProtection},
                    new DepecheType(){Nom = Constantes.DepecheType.VerificationZepLibre_Ligne},
                    new DepecheType(){Nom = Constantes.DepecheType.VerificationZepLibre_IPCS},
                    new DepecheType(){Nom = Constantes.DepecheType.AvisRestitutionDFV},
                    new DepecheType(){Nom = Constantes.DepecheType.LeveeDesMesures},
                });

                // profils
                List<Profil> profils = new List<Profil>()
                {
                    new Profil(){ Nom = Constantes.Profils.Administrateur },
                    new Profil(){ Nom = Constantes.Profils.AgentCirculation },
                    new Profil(){ Nom = Constantes.Profils.Aiguilleur },
                    new Profil(){ Nom = Constantes.Profils.CCL },
                    new Profil(){ Nom = Constantes.Profils.CelluleTravaux },
                    new Profil(){ Nom = Constantes.Profils.DPx },
                    new Profil(){ Nom = Constantes.Profils.PoleTravaux },
                };
                EFExtentions.AddOrUpdate<Profil>(t => t.Nom, profils);


                Utilisateur user = new Utilisateur();
                UtilisateurService userService = new UtilisateurService();
                user.Login = "dupond";
                user.HasshedPassword = userService.HashPassword("azerty1234");
                Utilisateur user2 = new Utilisateur();
                user2.Login = "martin";
                user2.HasshedPassword = userService.HashPassword("azerty1234");
                List<Utilisateur> users = new List<Utilisateur>() { user, user2 };
                EFExtentions.AddOrUpdate<Utilisateur>(t => t.Login, users);

                Profil agentCirculation;
                Profil aiguilleur;
                Poste PRCI;
                Poste MantePRS;
                Poste Poste2;
                Secteur mantes;
                using (AppDbContext db = new AppDbContext())
                {
                    agentCirculation = db.Profil.FirstOrDefault(a => a.Nom == Constantes.Profils.AgentCirculation);
                    aiguilleur = db.Profil.FirstOrDefault(a => a.Nom == Constantes.Profils.Aiguilleur);
                    mantes = db.Secteur.FirstOrDefault(a => a.Nom == Constantes.Secteurs.Mantes);
                    PRCI = db.Poste.FirstOrDefault(a => a.Nom == "Mantes PRCI");
                    MantePRS = db.Poste.FirstOrDefault(a => a.Nom == "Mantes PRS");
                    Poste2 = db.Poste.FirstOrDefault(a => a.Nom == "Mantes Poste 2");
                    user = db.Utilisateur.FirstOrDefault(a => a.Login == "dupond");
                    user2 = db.Utilisateur.FirstOrDefault(a => a.Login == "martin");
                }

                List<UserProfil> listeUserProfil = new List<UserProfil>()
                {
                    new UserProfil() {SecteurId = mantes.Id, PosteId = PRCI.Id, ProfilId = agentCirculation.Id, UtilisateurId = user.Id},
                    new UserProfil() {SecteurId = mantes.Id, PosteId = PRCI.Id, ProfilId = aiguilleur.Id, UtilisateurId = user.Id},
                    new UserProfil() {SecteurId = mantes.Id,PosteId = MantePRS.Id, ProfilId = agentCirculation.Id, UtilisateurId = user.Id},
                    new UserProfil() {SecteurId = mantes.Id,PosteId = MantePRS.Id, ProfilId = aiguilleur.Id, UtilisateurId = user.Id},
                    new UserProfil() {SecteurId = mantes.Id,PosteId = Poste2.Id, ProfilId = aiguilleur.Id, UtilisateurId = user.Id},
                    new UserProfil() {SecteurId = mantes.Id,PosteId = PRCI.Id, ProfilId = agentCirculation.Id, UtilisateurId = user2.Id},
                    new UserProfil() {SecteurId = mantes.Id,PosteId = PRCI.Id, ProfilId = aiguilleur.Id, UtilisateurId = user2.Id},
                    new UserProfil() {SecteurId = mantes.Id,PosteId = MantePRS.Id, ProfilId = agentCirculation.Id, UtilisateurId = user2.Id},
                    new UserProfil() {SecteurId = mantes.Id,PosteId = MantePRS.Id, ProfilId = aiguilleur.Id, UtilisateurId = user2.Id},
                    new UserProfil() {SecteurId = mantes.Id,PosteId = Poste2.Id, ProfilId = aiguilleur.Id, UtilisateurId = user2.Id}
                };
                EFExtentions.AddOrUpdate<UserProfil>(t => new { t.PosteId, t.ProfilId , t.UtilisateurId, t.SecteurId} , listeUserProfil);





            }
        }
    }
}
