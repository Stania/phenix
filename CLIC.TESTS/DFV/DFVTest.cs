﻿using Clic.BLL.IServices;
using Clic.BLL.Services.Commun;
using Clic.BLL.Services.FicheDFV;
using Clic.BLL.Services.FicheZep;
using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Entities.FicheZep;
using CLic.BLL.Services.FicheZep;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PHENIX.BLL.IServices;
using PHNIX.BLL.IServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CLIC.TESTS.FicheDFV
{
    [TestClass]
    public class DFVTest
    {
        private IDFVService _DFVService;
        private IZepService _ZepService;
        private IManoeuvreService _ManoeuvreService;
        private IOccupationService _OccupationService;
        private IMesureService _MesureService;
        private IDepecheService _DepecheService;
        public DFVTest()
        {
            _DepecheService = new DepecheService();
            _ZepService = new ZepService();
            _OccupationService = new OccupationService();
            _MesureService = new MesureService();

            _ManoeuvreService = new ManoeuvreService(_DepecheService);
            _DFVService = new DFVService(_ZepService, _ManoeuvreService,_DepecheService, _OccupationService, _MesureService);

        }

        [TestMethod]
        public void AddOrUpdateDfvTest()
        {
            Zep zep = _ZepService.GetRandomZep();
            Poste poste = _ZepService.GetRandomPoste();

            ManoeuvreType engagement = _ZepService.GetManoeuvreTypeByName(Constantes.ManoeuvreTypes.Engagement);
            ManoeuvreType degagement = _ZepService.GetManoeuvreTypeByName(Constantes.ManoeuvreTypes.Degagement);
            ManoeuvreType franchissement = _ZepService.GetManoeuvreTypeByName(Constantes.ManoeuvreTypes.Franchissement);
            OccupationType alaccord = _OccupationService.GetOccupationTypeByName(Constantes.OccupationTypes.OccupeeALAccord);
            OccupationType alarestitution = _OccupationService.GetOccupationTypeByName(Constantes.OccupationTypes.OccupeeALaRestitution);
            List<StatutZep> statutzepList = _ZepService.GetAllStatutZep();
            StatutZep zepEnDemande = statutzepList.FirstOrDefault(t => t.Statut == Constantes.ZepStatuts.EnCreation);
            StatutZep zepEnAccord = statutzepList.FirstOrDefault(t => t.Statut == Constantes.ZepStatuts.EnAccord);
            StatutZep zepAccordee = statutzepList.FirstOrDefault(t => t.Statut == Constantes.ZepStatuts.Accordee);

            if (zepEnDemande == null) Assert.Fail();

            Random rnd = new Random();
            string numero = rnd.Next(1, 100000).ToString();
            if (poste ==null) Assert.Fail();
            if (zep.PosteDemandeDFV == null) Assert.Fail();
            if (zep == null) Assert.Fail();
            if (zep.PointRemarquable_ZepList == null || zep.PointRemarquable_ZepList.Count < 1) Assert.Fail();
            List<PointRemarquable_Zep> prs = new List<PointRemarquable_Zep>(zep.PointRemarquable_ZepList);
            numero = "003";
            DFV dfvItem = new DFV
            {
                DateCreation = DateTime.Now,
                ZepId = zep.Id,//liste
                ResponsableDFVId = zep.PosteDemandeDFV.Id,//liste
                AvecVerificationDeLiberation = zep.DFVAvecVerificationLiberation??false,
                NumeroTelephone = "0000000000",
                RPTX = "BOB",
                Numero = numero,//liste
                SansTTX = false,
                DateDebutApplicabilite = new DateTime(2018, 4, 1, 23, 0, 0),
                DateFinApplicabilite = new DateTime(2018, 4, 2, 5, 0, 0),
                OperationNumero = "111",
                ContratTravauxNumero = "CT1",
                AHTNumero = "AHT1",
                Avis = "A1",
                AvisNumero = "A11",
                CCTTNumero = "CCTT1",
                DateApplicabilite = new DateTime(2018, 4, 1, 21, 35, 0),
                StatusZepId =zepEnDemande.Id 
            };

            _DFVService.AddOrUpdateDFV(dfvItem);
            dfvItem = _DFVService.GetDFVByNumero(numero);
            //valider la demande
            dfvItem = _DFVService.ValiderDemandeDFV(dfvItem);
            Assert.IsTrue(dfvItem.StatusZep.Id == zepEnAccord.Id,"Le statut de la zep doit être 'En accord' après la validation de la demande DFV");

            //TTX
            var eng = new Manoeuvre()
            {
                DFVId = dfvItem.Id,
                ManoeuvreTypeId = engagement.Id,//liste
                PointRemarquable_ZepId = prs[0].Id,//liste
                ParEcrit = true,
                TTXNature = "Draisine",
                TTXNumero = "TTX1",
            };

            var deg = new Manoeuvre()
            {
                DFVId = dfvItem.Id,
                ManoeuvreTypeId = degagement.Id,//liste
                PointRemarquable_ZepId = prs[0].Id,//liste
                ParEcrit = true,
                TTXNature = "Draisine",
                TTXNumero = "TTX1",
            };

            _ManoeuvreService.AddManoeuvre(deg);
            _ManoeuvreService.AddManoeuvre(eng);
            eng.Commentaire = "update";
            _ManoeuvreService.UpdateManoeuvre(eng);
            //_ManoeuvreService.DeleteManoeuvre(eng);

            var occ = new Occupation()
            {
                DFVId = dfvItem.Id,
                OccupationTypeId = alaccord.Id,//liste
                TTXNumero = "ad",
                TTXNature = "ddz",
            };
            _OccupationService.AddOccupation(occ);
            occ.Commentaire = "update";
            occ.OccupationTypeId = alarestitution.Id;
            _OccupationService.UpdateOccupation(occ);
            _OccupationService.DeleteOccupation(occ);

            dfvItem.DateDebutAccord = DateTime.Now;
            dfvItem.DateFinAccord = DateTime.Now.AddDays(1);
            dfvItem.AccordeParEcrit = true;
            dfvItem = _DFVService.AccorderDFV(dfvItem);
            Assert.IsTrue(dfvItem.StatusZep.Id == zepAccordee.Id);
        }

        [TestMethod]
        public void MesuresPrises()
        {
            string numero = "48629";
            DFV dfvItem = _DFVService.GetDFVByNumero(numero);
            Poste poste = _ZepService.GetRandomPoste();
            if (poste == null) Assert.Fail();

            MesureProtectionPrise mesure = new MesureProtectionPrise()
            {
                DFVId = dfvItem.Id,
                PosteDemandeId = poste.Id,
                VerificationLiberationVoieprevue = false,
            };

            _MesureService.AddMesureProtectionPrise(mesure);
            dfvItem = _DFVService.GetDFVByNumero(numero);

            Assert.IsTrue(dfvItem.MesureProtectionPriseList != null && dfvItem.MesureProtectionPriseList.Count > 0);

            MesureProtectionPrise mpp = dfvItem.MesureProtectionPriseList.FirstOrDefault();
            mpp.VerificationLiberationVoieprevue = true;

            _MesureService.UpdateMesureProtectionPrise(mpp);
            _MesureService.DeleteMesureProtectionPrise(mpp);

        }

        [TestMethod]
        public void Depeche()
        {
            string numero = "48629";
            DFV dfvItem = _DFVService.GetDFVByNumero(numero);
            List<DepecheType> depecheTypes = _DepecheService.GetAllDepecheType();
            var typeDepcheEngagement = depecheTypes.FirstOrDefault(t => t.Nom == Constantes.DepecheType.Engagement);
            //Zep zep = _ZepService.GetRandomZep();
            var engagement = dfvItem.ManoeuvreList?.FirstOrDefault();

            engagement.ParDepeche = true;
            engagement.ParEcrit = false;
            
            //engagement.Depeche.NumeroDonne = "5";
            //_ManoeuvreService.UpdateManoeuvre(engagement);
            //if (dfvItem.ManoeuvreList != null && dfvItem.ManoeuvreList.Count > 1)
            //{
            //    var manoeuvre = dfvItem.ManoeuvreList.Last();
            //    if(manoeuvre.Depeche == null)
            //    {
            //        manoeuvre.Depeche = new Depeche()
            //        {
            //            NumeroDonne = "6",
            //            NumeroRecu = "7",
            //            DepecheTypeId = typeDepcheEngagement.Id,
            //            DateDepeche = DateTime.Now
            //        };
            //        _ManoeuvreService.UpdateManoeuvre(manoeuvre);
            //    }
            //}
        }

        [TestMethod]
        public void VerificationZepLibre()
        {
            string numero = "48629";
            DFV dfv = _DFVService.GetDFVByNumero(numero);
            Poste poste = _ZepService.GetRandomPoste();
            List<DepecheType> depecheTypes = _DepecheService.GetAllDepecheType();
            var typeDepcheVerifLigne = depecheTypes.FirstOrDefault(t => t.Nom == Constantes.DepecheType.VerificationZepLibre_Ligne);
            if (poste == null) Assert.Fail();
            if(dfv.VerificationZepLibre_Ligne == null)
            {
                Depeche depeche = new Clic.DAL.Entities.FicheDFV.Depeche()
                {
                    NumeroDonne = "1",
                    NumeroRecu = "3",
                    DateDepeche = DateTime.Now,
                    DepecheTypeId = typeDepcheVerifLigne.Id
                };
                
                dfv.VerificationZepLibre_Ligne = new VerificationZepLibre_Ligne()
                {
                    DFVId = dfv.Id,
                    Nom = "",
                    PosteDemandeId = poste.Id,
                    HeureArrivee = DateTime.Now,
                    NumeroTrainRecu = "T22",
                    Depeche = depeche
                };

                _DFVService.AddOrUpdateDFV(dfv);
            }
            else
            {
                dfv.VerificationZepLibre_Ligne.Depeche.NumeroRecu = "update2";
                dfv.VerificationZepLibre_Ligne.Nom = "nom";

                //dfv = _DFVService.AddOrUpdateDFV(dfv);
                _MesureService.UpdateVerificationZepLibre_Ligne(dfv.VerificationZepLibre_Ligne);

                Assert.IsTrue(dfv.VerificationZepLibre_Ligne.Nom == "nom");
            }
        }

    }
}