﻿using Clic.BLL.IServices;
using Clic.BLL.Services.FicheZep;
using Clic.DAL.Entities.FicheZep;
using CLic.BLL.Services.FicheZep;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PHNIX.BLL.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace CLIC.TESTS.FicheZep
{
    [TestClass]
    public class ZepTest
    {
        private IZepService _ZepService;
        private List<Zep> listeZeps = new List<Zep> {
                new Zep() { Numero = "ZEP 799", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 800", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 801", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 802", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 803", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 804", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 805", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 807", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 809", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 810", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 811", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 812", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 814", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 815", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 816", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 817", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 818", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 819", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 820", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 821", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 822", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 823", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 824", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 825", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 826", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 827", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 828", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 829", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 830", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 832", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 833", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 834", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 835", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 836", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 837", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 838", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 839", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 840", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 841", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 843", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 844", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 845", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 846", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 848", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 849", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 851", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 855", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 856", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 859", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 860", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 861", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 862", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 863", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 865", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 867", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 869", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 875", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 876", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 877", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 881", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 882", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 885", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 887", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 890", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 891", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 892", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 893", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 894", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 895", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 896", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 897", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 898", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 901", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 903", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 905", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 907", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 909", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 911", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 913", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 915", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 917", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 919", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 921", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 800 + 802", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 801 + 803", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 802 + 829", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 804 + 807", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 804 + 812", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 804 + 810", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 805 + 806", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 805 + 809", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 805 + 830", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 810 + 814", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 811 + 816", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 811 + 895", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 812 + 819", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 812 + 836", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 815 + 818", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 820 + 822", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 821 + 823", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 826 + 828", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 827 + 885", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 830 + 833", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 832 + 835", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 833 + 863", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 834 + 892", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 835 + 865", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 838 + 840", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 839 + 841", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 840 + 841", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 841 + 843", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 843 + 845", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 844 + 846", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 845 + 875", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 848 + 849", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 849 + 875", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 855 + 893", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 875 + 876", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 893 + 895", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 893 + 896", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 895 + 897", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 897 + 915", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 903 + 909", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 917 + 919", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 806 + 808 + 810", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 815 + 897 + 915", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 831 + 833 + 867", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 833 + 835 + 837", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 834 + 837 + 892", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 844 + 846 + 848", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 851 + 1801 + 1803", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 856 + 860 + 862", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 876 + 877 + 917", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 891 + 893 + 895", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 892 + 894 + 899", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 894 + 896 + 898", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 801 + 803 + 813 + 829", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 802 + 806 + 808 +810", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 803 + 831 + 833 + 867", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 809 + 811 + 815 + 817", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 814 + 816 + 818 + 820", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 818 + 820 + 826 + 828", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 820 + 821 + 822 + 823", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 825 + 827 + 897 + 915", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 827 + 882 + 885 + 887", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 830 + 832 + 834 + 892", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 843 + 844 + 845 + 846", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 843 + 845 + 849 + 851", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 875 + 876 + 877 + 881", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 801 + 803 + 805 + 813 + 829", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 804 + 806 + 807 + 808 + 810", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 820 + 822 + 824 + 826 + 828", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 821 + 823 + 825 + 827 + 885", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 827 + 881 + 882 + 885 + 887", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 2001", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 763", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1009", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1818", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1801 + 1818", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1814 + 1816 + 1818 + 848", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 110", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1114", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP MANTES – GARGENVILLE Voie 2", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP MANTES - MEULAN Voie 2", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP MANTES – CONFLANS Voie 2", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP GARGENVILLE - MANTES VOIE 1", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP MEULAN - MANTES VOIE 1", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP CONFLANS - MANTES VOIE 1", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "GROUPEMENT MAME 02", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "GROUPEMENT MACO 02", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "Groupement COMA 01", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "Groupement MEMA 01", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "Groupement MEMA 05", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "Intervalle 7 - 194", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 701", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 702", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 703", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 704", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 705", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 706", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 707", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 709", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 710", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 712", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 713", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 714", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 716", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 717", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 718", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 719", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 720", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 721", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 722", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 723", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 724", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 725", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 726", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 727", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 728", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 729", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 731", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 732", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 733", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 734", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 735", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 736", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 737", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 738", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 739", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 741", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 743", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 747", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 749", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 751", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 755", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 757", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 759", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 761", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 701 + 724", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 702 + 759", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 706 + 712", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 707 + 709", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 707 + 721", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 709 + 713", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 712 + 739", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 713 + 721", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 713 + 722", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 720 + 728", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 731 + 757", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 734 + 736", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 739 + 755", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 741 + 755", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 717 + 751", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 759 + 761", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 734 + 717 + 749", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 749 + 717 + 751", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1005", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1010", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1007", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1008", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1011", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1004", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1006", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1001", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1003", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1020", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1021", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1022 (ZEP 712 + 714 + 723 + 727 + 729 + 739 + 747)", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1002 (ZEP 723 + 727 + 729 + 735 + 755 + 757)", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1801", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1802", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1803", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1804", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1805", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1806", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1807", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1808", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1809", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1810", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1811", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1812", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1813", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1814", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1816", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1817", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1819", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1801 + 1816", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1802 + 1819", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP 1805 + 1810", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1805 + 1812", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1806 + 1811", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1809 + 1811", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1803 + 1805 + 1807", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1801 + 1803 + 1805 + 1807", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP 1030", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP MANTES – LES MUREAUX Voie 2", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP MANTES - PLAISIR Voie 2", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP MANTES – EVREUX Voie 1", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP EVREUX - MANTES VOIE 2", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "ZEP LES MUREAUX - MANTES VOIE 1", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "ZEP PLAISIR - MANTES VOIE 1", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "GROUPEMENT MP 02", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "GROUPEMENT MAMT 01", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "Groupement MUMA 01", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "Groupement PM 02", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "Groupement EM 02", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "Groupement PM 03", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "Groupement MTMA 02", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "Intervalle 192 - 15", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "Intervalle 402 - 501", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "Intervalle 502 - 611", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "Intervalle 622 - 71", ZepTypeId = 1 , PosteDemandeDFVId = 1},
                new Zep() { Numero = "Intervalle 503 - 404", ZepTypeId = 1, PosteDemandeDFVId = 1 },
                new Zep() { Numero = "Intervalle 613 - 504", ZepTypeId = 1 , PosteDemandeDFVId = 1}};
        public ZepTest()
        {
            _ZepService = new ZepService();
        }

        [TestMethod]
        public void CreateZepTest()
        {
            _ZepService = new ZepService();
            _ZepService.AddRange(listeZeps);
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void CreateGroupe()
        {
            _ZepService = new ZepService();
            List<CompositionZep> listCompo = new List<CompositionZep>();
            foreach (var zep in listeZeps)
            {
                string[] numSpit = zep.Numero.Split('+');
                if (numSpit.Length > 1)
                {
                    foreach (var num in numSpit)
                    {
                        string newNum;
                        if (num.Contains("("))
                        {
                             newNum = num.Substring(0, 14);
                        }
                        else if (num.Contains("ZEP "))
                        {
                             newNum = num.Substring(4);
                        }
                        else {
                            newNum = num;
                        }
                        var zepGroup = _ZepService.GetZepByNumero(zep.Numero);
                        var zepEnfant = _ZepService.GetZepByNumero("ZEP " + newNum.Trim());
                        if(zepGroup != null && zepEnfant != null) listCompo.Add(new CompositionZep()
                        {
                            GroupeZepId = zepGroup.Id,
                            ZepEnfantId = zepEnfant.Id,
                        });
                    }
                }
            }
            _ZepService.AddGroupRange(listCompo);
            Assert.IsTrue(true);
        }


        [TestMethod]
        public void GetAllZep()
        {
            var zeps = _ZepService.GetAllZeps();
            Assert.IsTrue(zeps != null && zeps.Count > 0);
        }

        [TestMethod]
        public void GetZepById ()
        {
            _ZepService.GetZepById(1);
        }


        [TestMethod]
        public void GezNotDrawedZeps()
        {
           var coco = _ZepService.GetNotDrawedZepList();
        }

        [TestMethod]
        public void GetEtatOperationnel()
        {
            var coco = _ZepService.GetEtatOperationnel();
        }
    }

}
