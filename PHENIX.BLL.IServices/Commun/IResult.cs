﻿
using System.Linq;
using System.Threading.Tasks;

namespace PHENIX.BLL.IServices.Commun
{
    public interface IResult<T>
    {
        T Value { get; set; }
        ResultStatus Status { get; set; }
        string SuccessMessage { get; set; }
        string ErrorMessage { get; set; }
    }

    public enum ResultStatus
    {
        Success = 1,
        Error = 2,
        Warning = 3
    }
}
