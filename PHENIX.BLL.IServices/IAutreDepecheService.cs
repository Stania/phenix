﻿using Clic.DAL.Entities.FicheDFV;
using System;
using System.Collections.Generic;
using System.Text;

namespace PHENIX.BLL.IServices
{
    public interface IAutreDepecheService
    {
        AutreDepeche AddAutreDepecheManoeuvre(AutreDepeche autreDepeche);
        AutreDepeche UpdateAutreDepeche(AutreDepeche autreDepeche);
        void DeleteAutreDepeche(AutreDepeche manoeuvre);
        AutreDepeche GetAutreDepecheById(int id);
        List<AutreDepecheType> GetAllAutreDepecheType();
        List<AutreDepeche> GetAllAutreDepecheByIdDFV(int IdDfv);
        AutreDepeche CreateAutreDepecheDFV(AutreDepeche dfv);
    }
}
