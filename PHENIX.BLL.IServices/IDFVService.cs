﻿using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profil;
using System;
using System.Collections.Generic;
using System.Text;

namespace PHENIX.BLL.IServices
{
    public interface IDFVService
    {
        DFV AddOrUpdateDFV(DFV demande);
        DFV GetDFVById(int id);
        DFV GetDFVByNumero(string numero);
        List<DFV> GetDfvList(Poste pConnectedPoste, Clic.DAL.Entities.Profil.Profil pConnectedProfil);
        DFV ValiderDemandeDFV(DFV dfv);
        /// <summary>
        /// Accorder la DFV et passage de son statut à "Accordée"
        /// </summary>
        DFV AccorderDFV(DFV dfv);

       

        VerificationZepLibre_Ligne ValiderConfirmationVerification(VerificationZepLibre_Ligne verif);
        VerificationZepLibre_Ligne ValiderDemandeVerification(VerificationZepLibre_Ligne verfif);
        VerificationZepLibre_Ligne UpdateVerificationZepLibre_Ligne(VerificationZepLibre_Ligne v);
        VerificationZepLibre_Ligne AddVerificationZepLibre_Ligne(VerificationZepLibre_Ligne v);
        VerificationZepLibre_Ligne GetVerificationZepLibre_LigneById(int id);
     
        List<Aiguille_DFV> UpdateAiguilleDFV(List<Aiguille_DFV> pListe);
        DFV RestituerDFV(DFV pDFV);
        List<DFV> GetDfvListPrevisionnelle(Profil profilBdd);
        DFV CheckOrUpdateDfvStatutLeveeMesure(int id);
    }
}
