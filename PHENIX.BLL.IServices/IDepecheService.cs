﻿using Clic.DAL.Entities.FicheDFV;
using System;
using System.Collections.Generic;
using System.Text;

namespace PHENIX.BLL.IServices
{
    public interface IDepecheService
    {
        Depeche AddDepeche(Depeche depeche);
        Depeche AddDepecheToMesureProtection(Depeche depeche, int mesureId);
        Depeche AddDepecheToManoeuvre(Depeche depeche, int manoeuvreId);

        void DeleteDepeche(Depeche depeche);
        List<DepecheType> GetAllDepecheType();
        DepecheType GetDepecheTypeByName(string pNom);
        List<Manoeuvre_Depeche> GetListManoeuvreDepecheByIdDfv(int id);
        Depeche AddDepecheNotification(Depeche depeche, int manoeuvreId);
    }
}
