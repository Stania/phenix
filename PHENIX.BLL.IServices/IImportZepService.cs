﻿

using PHENIX.BLL.IServices.Commun;
using System.IO;

namespace PHNIX.BLL.IServices
{
    public interface IImportZepService
    {
        string ImportFichesZepsExcel(string path);
        IResult<int> ImportFichesZepsExcel(Stream fileStream);
    }
}
