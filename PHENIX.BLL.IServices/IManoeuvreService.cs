﻿using Clic.DAL.Entities.FicheDFV;
using System;
using System.Collections.Generic;
using System.Text;

namespace PHENIX.BLL.IServices
{
    public interface IManoeuvreService
    {
        Manoeuvre AddManoeuvre(Manoeuvre manoeuvre);
        Manoeuvre UpdateManoeuvre(Manoeuvre manoeuvre);
        void DeleteManoeuvre(Manoeuvre manoeuvre);
        Manoeuvre GetManoeuvreById(int id);
        List<ManoeuvreType> GetAllManoeuvreType();
        Manoeuvre EffectueManoeuvre(Manoeuvre manoeuvre);
        ManoeuvreType GetManoeuvreTypeById(int Id);
        ManoeuvreType GetManoeuvreTypeByName(string Nom);
    }
}
