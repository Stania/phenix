﻿using Clic.DAL.Entities.FicheDFV;
using System;
using System.Collections.Generic;
using System.Text;

namespace PHENIX.BLL.IServices
{
    public interface IMesureService
    {
        void AddDepecheToMesure(Depeche depeche, MesureProtectionPrise mesure);

        MesureProtectionPrise GetMesureProtectionPriseById(int Id);
        MesureProtectionPrise AddMesureProtectionPrise(MesureProtectionPrise mesure);
        MesureProtectionPrise UpdateMesureProtectionPrise(MesureProtectionPrise mesure);
        int DeleteMesureProtectionPrise(MesureProtectionPrise mesure);
        VerificationZepLibre_Ligne GetVerificationZepLibre_LigneById(int Id);
        void AddVerificationZepLibre_Ligne(VerificationZepLibre_Ligne verif);
        void UpdateVerificationZepLibre_Ligne(VerificationZepLibre_Ligne verif);
        void DeleteVerificationZepLibre_Ligne(VerificationZepLibre_Ligne verif);
        List<MesureProtectionPrise> GetMesuresProtectionByDfvId(int idDfv);
        VerificationZepLibre_IPCS GetVerificationZepLibre_IPCSById(int Id);
        void AddVerificationZepLibre_IPCS(VerificationZepLibre_IPCS verif);
        void UpdateVerificationZepLibre_IPCS(VerificationZepLibre_IPCS verif);
        void DeleteVerificationZepLibre_IPCS(VerificationZepLibre_IPCS verif);
    }
}
