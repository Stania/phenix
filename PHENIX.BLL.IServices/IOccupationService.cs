﻿using Clic.DAL.Entities.FicheDFV;
using System;
using System.Collections.Generic;
using System.Text;

namespace PHENIX.BLL.IServices
{
    public interface IOccupationService
    {
        Occupation AddOccupation(Occupation Occupation);
        Occupation UpdateOccupation(Occupation Occupation);
        void DeleteOccupation(Occupation Occupation);
        Occupation GetOccupationById(Int32 Id);
        List<OccupationType> GetAllOccupationType();
        OccupationType GetOccupationTypeByName(string nom);
    }
}
