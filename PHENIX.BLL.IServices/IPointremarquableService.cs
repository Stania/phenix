﻿using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Entities.FicheZep;
using System;
using System.Collections.Generic;
using System.Text;

namespace PHENIX.BLL.IServices
{
    public interface IPointRemarquableService
    {
        List<PointRemarquable_Zep> GetListPointRemarquableByIdZep(int idZep);
        PointRemarquable_Zep GetPointRemarquable_ZepById(int Id);
    }
}
