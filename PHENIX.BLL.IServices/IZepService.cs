﻿using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Entities.FicheZep;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.BLL.IServices
{
    public interface IZepService
    {
        Zep GetZepById(int Id);

        List<ZepType> GetAllZepTypes();

        List<Zep> GetAllZeps();

        Zep GetRandomZep();

        Poste GetPosteById(int Id);

        Poste GetRandomPoste();

        ManoeuvreType GetManoeuvreTypeByName(string nom);


        List<string> GetAllZepNumeros();
        Zep GetZepByNumero(string numero);
        List<ParticulariteType> GetAllParticulariteTypes();
        List<Poste> GetAllPostes();
        List<Poste> GetAllPostesByIdZep(int idZep);
        List<Poste> GetAllPostesConditionDeProtectionIdZep(int idZep);
        List<MesureProtectionType> GetAllMesureProtectionTypes();
        List<StatutZep> GetAllStatutZep();
        StatutZep GetStatutZepByName(string nom);

        List<PointRemarquableType> GetAllPointRemarquableTypes();
        List<Zep> GetNotDrawedZepList();
        List<Zep> GetDrawedZepList();
        List<Zep> GetEtatOperationnel();
        Zep UpdateObjetGraphique(Zep pZep);

        void AddRange(List<Zep> pListe);
        void AddGroupRange(List<CompositionZep> pListe);
        Poste GetPosteByName(string pPosteName);
        List<Zep> GetZepsAutorises(string poste,string profil = null);
    }
}
