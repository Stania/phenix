﻿using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profil;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.BLL.IServices.Profil
{
    public interface IPasswordHasher
    {
        String HashPassword(String pPassword);
        PasswordVerificationResult VerifyHashedPassword(String pHashedPassword, String oProvidedPassword);
    }

    public enum PasswordVerificationResult
    {
        Failed,
        Success,
        SuccessRehashNeeded
    }
}
