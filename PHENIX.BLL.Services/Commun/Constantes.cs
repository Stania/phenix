﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.BLL.Services.Commun
{
    public static class Constantes
    {
        public static class UserClaims
        {
            public static readonly string Profil = "Profil";
            public static readonly string Secteur = "Secteur";
            public static readonly string Poste = "Poste";
        }
        /// <summary>
        /// Profils présents dans la SPEC
        /// </summary>
        public static class Profils
        {
            public static readonly string Administrateur = "Administrateur";
            public static readonly string AgentCirculation = "Agent Circulation";
            public static readonly string Aiguilleur = "Aiguilleur";
            public static readonly string CCL = "CCL";
            public static readonly string CelluleTravaux = "Cellule Travaux";
            public static readonly string DPx = "DPx";
            public static readonly string PoleTravaux = "Pôle Travaux";
        }

        public static class MesureProtectionTypes
        {
            public static readonly string ConditonParticuliereVerificationLiberation = "Conditions particulières de vérification de libération de la ZEP";
            public static readonly string ProtectionEtVerification = "Conditions de protection et vérifications des postes";
            public static readonly string ItinérairesAdetruirePRCI = "Itinéraires à détruire(PRCI)";
            public static readonly string ItinérairesAdetruireEtAMunirDunDA = "Itinéraires à détruire et à munir de DA";
            public static readonly string OrganeDeCommandeAplacerEtAmunirDeDA = "Organe de commande à placer dans la position indiquée et munis d'un DA";
            public static readonly string DialogueDeProtection = "Dialogue de protection";
            public static readonly string AutorisationADetruire = "Autorisation à détruire";
            public static readonly string AiguilleImmobiliseeDansPositionIndiquee = "Aiguille immobilisée dans la position indiquée";
            public static readonly string JalonDarretAplacerSurLeTerrain = "Jalon d'arrêt à placer sur le terrain";
        }

        public static class ParticulariteTypes
        {
            //Applicable dans les conditions(Poste ouvert / Poste fermé)
            public static readonly string ApplicableDansCondition = "Applicable dans les conditions(Poste ouvert / Poste fermé)";
            //Dispositions particulières
            public static readonly string DFVResititueeOccupee = "DFV restituée occupée";
            public static readonly string TTXStationne = "TTX Stationné";
            public static readonly string AutresDispositionsParticulieresTTX = "Autre dispositions particulières TTX";
            //Procédés DFV
            public static readonly string AiguillesAPositionObligee = "Aiguilles à position obligée";
            public static readonly string AiguillesADisposerDansPositionPrevue = "Aiguilles à disposer dans la position prévue avant accord";
            public static readonly string DispositionsParticulieresParExploitation = "Dispositions particulières par l'Exploitation";
            //Mesure à prendre par l'équipements
            public static readonly string OutilDeBouclageParLEquipement = "Outil de bouclage par l'Equipement";
            public static readonly string DispositionsParticulieresParLEquipement = "Dispositions particulières par l'Equipement";
            public static readonly string AutresMesuresEventiellesParLEquipement = "Autres mesures éventielles par l'Equipement";
        }

        public static class ZepTypes
        {
            public static readonly string TypeL = "Type L";
            public static readonly string TypeG = "Type G";
            public static readonly string TypeLG = "Type L + G";
        }

        public static class AutreDepecheTypes
        {
            public static readonly string Point_Remarquable = "PointRemarquable_ZepId";
            public static readonly string RPTX = "RPTX";
            public static readonly string Telephone = "NumeroTelephone";

        }

        public static class PointRemarquableTypes
        {
            public static readonly string PointEngagement = "Point d'engagement";
            public static readonly string PointDegagement = "Point de dégagement";
            public static readonly string SignauxIntermediaires = "Signaux intermédiaires";
        }

        public static class ManoeuvreTypes
        {
            public static readonly string Engagement = "Engagement";
            public static readonly string Degagement = "Dégagement";
            public static readonly string Franchissement = "Franchissement";
        }

        public static class OccupationTypes
        {
            public static readonly string OccupeeALAccord = "ZEP occupée par TTx à l'accord de la DFV";
            public static readonly string OccupeeALaRestitution = "ZEP occupée par TTx à la restitution";
        }


        public static class ProtectionVerificationPoste
        {
            public static readonly string Verification = "Vérification";
            public static readonly string Protection = "Protection";
            public static readonly string Verification_Protection = "Vérification et Protection";
        }

        public static class ZepStatuts
        {
            public static readonly string EnCreation = "En création";
            public static readonly string EnAccord = "En attente d'accord";
            public static readonly string Accordee = "Accordée";
            public static readonly string Restituee = "Restituée";
            public static readonly string Annulee = "Annulée";
            public static readonly string Refusee = "Refusée";
            public static readonly string LeveeMesure = "Levee de mesure à faire";

        }

        public static class ZepEtats
        {
            public static readonly string Applicable = "Applicable";
            public static readonly string Valide = "Validée";
            public static readonly string EnTravail = "En travail";
            public static readonly string Archive = "Archivée";
        }

        public static class Secteurs
        {
            public static readonly string Mantes = "Mantes";
        }

        public static class DepecheType
        {
            public static readonly string Engagement = "Engagement";
            public static readonly string Degagement = "Degagement";
            public static readonly string AutreDepeche = "Autre Depeche";

            public static readonly string Notification = "Notification";
            public static readonly string Restitution = "Restitution";

            public static readonly string RestitutionEngagement = "Réstitution engagement";
            public static readonly string RestitutionDegagement = "Réstitution dégagement";

            public static readonly string MesureProtection = "Mesure de protection";

            public static readonly string VerificationZepLibre_Ligne = "Verification Zep Libre Ligne";
            public static readonly string VerificationZepLibre_IPCS = "Verification Zep Libre IPCS";

            public static readonly string AvisRestitutionDFV = "Avis de restitution de la DFV";
            public static readonly string LeveeDesMesures = "Levee des mesures";
        }

        public static class TypeTTX
        {
            public static readonly List<string> ListAllType = new List<string>()
            {
                "Autre Train Travaux",
                "Bigrue","Bourreuse 1er Niveau (BML 1N)",
                "Bourreuse 2ème Niveau (BML 2N)",
                "Bourreuse 3ème Niveau (BML 3N)",
                "Carotteuse",
                "Combi",
                "Dérouleur CAT",
                "Draisine",
                "Draisine + Remorques",
                "Draisine + Wagons",
                "Draisine CAT : ESC",
                "Engin Diesel + Wagons",
                "Engin Electrique + Wagons",
                "Engin Maintenance Cat. (EMC)",
                "Engin Multif. Maint. Voie -EMV",
                "Locotracteur + Wagons",
                "Lorry Automoteur",
                "Régaleuse",
                "Stabilisateur",
                "Suite MRT-Méca. Remplact Trav.",
                "Train Auto-Lavant",
                "Train Désherbeur",
                "Train Meuleur",
                "Train Travaux : Ballast",
                "Train Travaux Entreprise",
                "Train Travaux Terre",
                "TTx : Appro./Ramassage",
                "TTx : Autres engins spéciaux",
                "TTx : Curage/Assainissement",
                "TTx : Déchargt/Rechargt LRS",
                "TTx : Finitions",
                "TTx : Pose/Dépose",
                "TTx : Surveillance",
                "TTx CAT : Plateformes (APMC,.)",
                "TTx Coupe",
                "TTx OA : Plateforme (PF)",
                "Véhicule Rail/Route CAT :VRRSC",
                "Wagons d'Inspection Tunnel-WIT"

            };
            public static readonly string AutreTrainTravaux = "Autre Train Travaux";
            public static readonly string Bigrue = "Bigrue";
            public static readonly string Bourreuse1erNiveau = "Bourreuse 1er Niveau (BML 1N)";
            public static readonly string Bourreuse2erNiveau = "Bourreuse 2ème Niveau (BML 2N)";
            public static readonly string Bourreuse3erNiveau = "Bourreuse 3ème Niveau (BML 3N)";
            public static readonly string Carotteuse = "Carotteuse";
            public static readonly string Combi = "Combi";
            public static readonly string DerouleurCAT = "Dérouleur CAT";
            public static readonly string Draisine = "Draisine";
            public static readonly string DraisineRemorques = "Draisine + Remorques";
            public static readonly string DraisineWagons = "Draisine + Wagons";
            public static readonly string DraisineCATESC = "Draisine CAT : ESC";
            public static readonly string EnginDieselWagons = "Engin Diesel + Wagons";
            public static readonly string EnginElectriqueWagons = "Engin Electrique + Wagons";
            public static readonly string EnginMaintenanceCat = "Engin Maintenance Cat. (EMC)";
            public static readonly string EnginMultiMaintVoie = "Engin Multif. Maint. Voie -EMV";
            public static readonly string LocotracteurWagons = "Locotracteur + Wagons";
            public static readonly string LorryAutomoteur = "Lorry Automoteur";
            public static readonly string Regaleuse = "Régaleuse";
            public static readonly string Stabilisateur = "Stabilisateur";
            public static readonly string SuiteMRTMecaRemplactTrav = "Suite MRT-Méca. Remplact Trav.";
            public static readonly string TrainAutoLavant = "Train Auto-Lavant";
            public static readonly string TrainDesherbeur = "Train Désherbeur";
            public static readonly string TrainMeuleur = "Train Meuleur";
            public static readonly string TrainTravauxBallast = "Train Travaux : Ballast";
            public static readonly string TrainTravauxEntreprise = "Train Travaux Entreprise";
            public static readonly string TrainTravauxTerre = "Train Travaux Terre";
            public static readonly string TTxApproRamassage = "TTx : Appro./Ramassage";
            public static readonly string TTxAutresEnginsspéciaux = "TTx : Autres engins spéciaux";
            public static readonly string TTxCurageAssainissement = "TTx : Curage/Assainissement";
            public static readonly string TTxDéchargtRechargtLRS = "TTx : Déchargt/Rechargt LRS";
            public static readonly string TTxFinitions = "TTx : Finitions";
            public static readonly string TTxPoseDepose = "TTx : Pose/Dépose";
            public static readonly string TTxSurveillance = "TTx : Surveillance";
            public static readonly string TTxCATPlateformesAPMC = "TTx CAT : Plateformes (APMC,.)";
            public static readonly string TTxCoupe = "TTx Coupe";
            public static readonly string TTxOAPlateformePF = "TTx OA : Plateforme (PF)";
            public static readonly string VehiculeRailRouteCATVRRSC = "Véhicule Rail/Route CAT :VRRSC";
            public static readonly string WagonsInspectionTunnelWIT = "Wagons d'Inspection Tunnel-WIT";
        }
        public static class Postes
        {
            public static readonly List<string> ListTypes = new List<string>()
            {
                "ZepTypes",
                "OccupationTypes",
                "PointRemarquableTypes",
                "MesuresTypes",
                "ManoeuvreTypes",
                "DepecheTypes"
            };

            public static readonly List<string> PosteList = new List<string>()
            {
                "Mantes PRS",
                "Mantes PRCI",
                "Mantes Poste 2",
                "Mantes Plaine Paris",
                "EP Port de Paris / Limay",
                "Conflans",
                "Evreux",
                "Gargenville",
                "Les Mureaux",
                "Meulan",
                "Plaisir",
                "Vernon",
                "RPTx"
            };
        }
    }
}
