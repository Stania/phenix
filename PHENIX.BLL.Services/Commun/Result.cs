﻿using PHENIX.BLL.IServices.Commun;
using System.Linq;
using System.Threading.Tasks;

namespace Clic.BLL.Services.Commun
{
      
    public class Result<T> : IResult<T>
    {
        public T Value { get; set; }
        public ResultStatus Status { get; set; }
        public string Message { get; set; }
        public string SuccessMessage { get; set; }
        public string ErrorMessage { get; set; }
    }
}
