﻿using Clic.BLL.Services.Commun;
using Clic.BLL.Services.FicheZep;
using Clic.DAL.Entities.FicheDFV;
using Clic.Data.DbContext;
using Microsoft.EntityFrameworkCore;
using PHENIX.BLL.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clic.BLL.Services.FicheDFV
{
    public class AutreDepecheService : IAutreDepecheService
    {
        IManoeuvreService _manoeuvreSrv;
        IPointRemarquableService _pointRemarquableSrv;
        IDepecheService _depecheSrv;
        IDFVService _DfvSrv;
        public AutreDepecheService(IManoeuvreService manoeuvreSrv, IPointRemarquableService pointRemarquableSrv, IDepecheService depecheSrv, IDFVService dfvSrv)
        {
            this._manoeuvreSrv = manoeuvreSrv;
            this._pointRemarquableSrv = pointRemarquableSrv;
            this._depecheSrv = depecheSrv;
            this._DfvSrv = dfvSrv;

        }
        private void ValidateAutreDepeche(AutreDepeche autreDepeche, AppDbContext context)
        {
            if (autreDepeche == null || !context.AutreDepeche.Any(t => t.Id == autreDepeche.Id))
                throw new ArgumentException("Argument invalide : " + nameof(autreDepeche.Id));
        }

        public AutreDepeche GetAutreDepecheById(int Id)
        {
            using (AppDbContext context = new AppDbContext())
            {
                if (!context.AutreDepeche.Any(t => t.Id == Id))
                    throw new ArgumentException("Argument invalide : " + nameof(Id));

                return context.AutreDepeche
                   .Include(t => t.AutreDepecheType)
                    .Include(t => t.Depeche)
                        .ThenInclude(x => x.DepecheType)
                    .Include(t => t.Manoeuvre)
                        .ThenInclude(x => x.ManoeuvreType)
                    .Where(t => t.Id == Id)
                    .FirstOrDefault();
            }
        }
        public AutreDepecheType GetAutreDepecheTypeByProperty(string pProperty)
        {
            using (AppDbContext context = new AppDbContext())
            {
                if (!context.AutreDepecheType.Any(t => t.Propriete == pProperty))
                    throw new ArgumentException("Argument invalide : " + nameof(pProperty));

                return context.AutreDepecheType
                    .Where(t => t.Propriete == pProperty)
                    .FirstOrDefault();
            }
        }

        public AutreDepeche AddAutreDepecheManoeuvre(AutreDepeche autreDepeche)
        {
            using (AppDbContext context = new AppDbContext())
            {
                var manoeuvreExistante = _manoeuvreSrv.GetManoeuvreById(autreDepeche.Manoeuvre.Id);
                if (manoeuvreExistante == null)
                    throw new Exception("Impossible de retrouver la manoeuvre liée.");
                if (manoeuvreExistante.PointRemarquable_ZepId != autreDepeche.Manoeuvre.PointRemarquable_ZepId) // Le point remarquable à changé
                {
                    var pointRemarquable = _pointRemarquableSrv.GetPointRemarquable_ZepById(autreDepeche.Manoeuvre.PointRemarquable_ZepId.Value);
                    var typeAutreDepeche = this.GetAutreDepecheTypeByProperty(nameof(Manoeuvre.PointRemarquable_ZepId));
                    var typeDepeche = _depecheSrv.GetDepecheTypeByName(Constantes.DepecheType.AutreDepeche);
                    var transac = context.Database.BeginTransaction();
                    try
                    {
                        autreDepeche.AncienneValeur = manoeuvreExistante.PointRemarquable_Zep.Valeur;

                        autreDepeche.NouvelleValeur = pointRemarquable.Valeur;
                        autreDepeche.ManoeuvreId = manoeuvreExistante.Id;
                        autreDepeche.AutreDepecheTypeId = typeAutreDepeche.Id;

                        autreDepeche.Depeche.DepecheTypeId = typeDepeche.Id;
                        _manoeuvreSrv.UpdateManoeuvre(autreDepeche.Manoeuvre);
                        var depeche = _depecheSrv.AddDepeche(autreDepeche.Depeche);
                        autreDepeche.Depeche = null;
                        autreDepeche.DepecheId = depeche.Id;
                        autreDepeche.Manoeuvre = null;
                        autreDepeche.DFV = null;
                        var newAutreDepeche = context.AutreDepeche.Add(autreDepeche).Entity;
                        context.SaveChanges();
                        transac.Commit();
                        return this.GetAutreDepecheById(newAutreDepeche.Id);
                    }
                    catch (Exception e)
                    {
                        transac.Rollback();
                        throw e;
                    }
                }
                //var newAutreDepeche = context.AutreDepeche.Add(autreDepeche).Entity;
                //context.SaveChanges();

                return null;
            }
        }

        public AutreDepeche UpdateAutreDepeche(AutreDepeche autreDepeche)
        {
            using (AppDbContext context = new AppDbContext())
            {
                ValidateAutreDepeche(autreDepeche, context);
                context.AutreDepeche.Attach(autreDepeche);
                context.Entry(autreDepeche).State = EntityState.Modified;
                context.SaveChanges();
                return this.GetAutreDepecheById(autreDepeche.Id);
            }
        }

        public void DeleteAutreDepeche(AutreDepeche autreDepeche)
        {
            using (AppDbContext context = new AppDbContext())
            {
                ValidateAutreDepeche(autreDepeche, context);
                AutreDepeche man = GetAutreDepecheById(autreDepeche.Id);
                context.AutreDepeche.Remove(man);
                context.SaveChanges();
            }
        }
        public List<AutreDepecheType> GetAllAutreDepecheType()
        {
            using (AppDbContext context = new AppDbContext())
            {
                return context.AutreDepecheType.ToList();
            }
        }
        public List<AutreDepeche> GetAllAutreDepecheByIdDFV(int IdDfv)
        {
            using (AppDbContext context = new AppDbContext())
            {


                return context.AutreDepeche
                    .Include(t => t.AutreDepecheType)
                    .Include(t => t.Depeche)
                        .ThenInclude(x => x.DepecheType)
                    .Include(t => t.Manoeuvre)
                        .ThenInclude(x => x.ManoeuvreType)
                    .Include(t => t.DFV)
                        .ThenInclude(x => x.StatusZep)
                    .Where(t => t.Manoeuvre.DFVId == IdDfv || t.DFVId == IdDfv)
                    .ToList();
            }
        }

        public AutreDepeche CreateAutreDepecheDFV(AutreDepeche autreDepeche)
        {
            using (AppDbContext context = new AppDbContext())
            {
                var dfvExistante = _DfvSrv.GetDFVById(autreDepeche.DFV.Id);
                if (dfvExistante == null)
                    throw new Exception("Impossible de retrouver la dfv liée.");
                if (dfvExistante.RPTX != autreDepeche.DFV.RPTX || dfvExistante.NumeroTelephone != autreDepeche.DFV.NumeroTelephone)
                {
                    var typeDepeche = _depecheSrv.GetDepecheTypeByName(Constantes.DepecheType.AutreDepeche);
                    var transac = context.Database.BeginTransaction();
                    try
                    {
                        autreDepeche.Depeche.DepecheTypeId = typeDepeche.Id;
                        var depeche = _depecheSrv.AddDepeche(autreDepeche.Depeche);
                        var dfv = autreDepeche.DFV;

                        var nouveauNumeroTel = autreDepeche.DFV.NumeroTelephone;
                        var idToReturn = 0;
                        if (dfvExistante.RPTX != autreDepeche.DFV.RPTX)
                        {
                            var typeAutreDepeche = this.GetAutreDepecheTypeByProperty(nameof(DFV.RPTX));
                            var newAutreDepecheRptx = new AutreDepeche()
                            {
                                AncienneValeur = dfvExistante.RPTX,
                                NouvelleValeur = dfv.RPTX,
                                AutreDepecheTypeId = typeAutreDepeche.Id,
                                DFVId = dfvExistante.Id,
                                DepecheId = depeche.Id,
                                ManoeuvreId = null,
                                DFV = null,
                                Manoeuvre = null,
                            };
                            newAutreDepecheRptx = context.AutreDepeche.Add(newAutreDepecheRptx).Entity;
                            context.SaveChanges();
                            idToReturn = newAutreDepecheRptx.Id;
                        }
                        if (dfvExistante.NumeroTelephone!= nouveauNumeroTel)
                        {
                            var typeAutreDepeche = this.GetAutreDepecheTypeByProperty(nameof(DFV.NumeroTelephone));

                            var newAutreDepecheTelephone = new AutreDepeche()
                            {
                                AncienneValeur = dfvExistante.NumeroTelephone,
                                NouvelleValeur = dfv.NumeroTelephone,
                                AutreDepecheTypeId = typeAutreDepeche.Id,
                                DFVId = dfvExistante.Id,
                                DepecheId = depeche.Id,
                                ManoeuvreId = null,
                                DFV = null,
                                Manoeuvre = null,
                            };

                            newAutreDepecheTelephone  = context.AutreDepeche.Add(newAutreDepecheTelephone).Entity;
                            context.SaveChanges();
                            idToReturn = newAutreDepecheTelephone.Id;
                        }
                        _DfvSrv.AddOrUpdateDFV(dfv);
                        context.SaveChanges();
                        transac.Commit();
                        return this.GetAutreDepecheById(idToReturn);
                    }
                    catch (Exception e)
                    {
                        transac.Rollback();
                        throw e;
                    }
                }
                return null;
            }
        }
    }
}
