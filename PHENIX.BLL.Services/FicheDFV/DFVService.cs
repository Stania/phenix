﻿using Clic.BLL.IServices;
using Clic.BLL.Services.Commun;
using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profil;
using Clic.DAL.Persistance.ExtentionMethods;
using Clic.Data.DbContext;
using CLic.BLL.Services.FicheZep;
using Microsoft.EntityFrameworkCore;
using PHENIX.BLL.IServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Clic.BLL.Services.FicheDFV
{
    public partial class DFVService : IDFVService
    {
        private IZepService _ZepService;
        private IManoeuvreService _ManoeuvreService;
        private IOccupationService _OccupationService;
        private IDepecheService _DepecheService;
        private IMesureService _MesureService;
        public DFVService(IZepService ZepService, IManoeuvreService ManoeuvreService, IDepecheService DepecheService, IOccupationService OccupationService, IMesureService MesureService)
        {
            _ZepService = ZepService;
            _ManoeuvreService = ManoeuvreService;
            _DepecheService = DepecheService;
            _OccupationService = OccupationService;
            _MesureService = MesureService;

        }

        public DFV ValiderDemandeDFV(DFV dfv)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity<DFV>(dfv);
                if (dfv.DateDebutApplicabilite == null) throw new ArgumentException("Argument null : " + nameof(dfv.DateDebutApplicabilite));
                if (dfv.DateApplicabilite == null) throw new ArgumentException("Argument null : " + nameof(dfv.DateApplicabilite));
                if (string.IsNullOrWhiteSpace(dfv.Numero)) throw new ArgumentException("Veuillez renseigner le numéro de la DFV");
                if (string.IsNullOrWhiteSpace(dfv.RPTX)) throw new ArgumentException("Veuillez renseigner le RPTX");
                if (string.IsNullOrWhiteSpace(dfv.NumeroTelephone)) throw new ArgumentException("Veuillez renseigner le N° du RPTX");
                if (dfv.ZepId == null) throw new ArgumentException("Veuillez renseigner la Zep concerner par la DFV.");

                if (dfv.DateFinApplicabilite == null) throw new ArgumentException("Argument null : " + nameof(dfv.DateFinApplicabilite));
                if (dfv.DateDebutApplicabilite > dfv.DateDebutApplicabilite) throw new ArgumentException("La date de début de l'applicabilité doit être antérieur à la date de fin.");
                if (!dfv.RecuParDepeche.HasValue
                    || dfv.RecuParDepeche.Value == false
                    || string.IsNullOrEmpty(dfv.DepecheReceptionNumero))
                    throw new ArgumentException("Veuillez chocher la case reçu par déêche !");
                StatutZep enAccord = _ZepService.GetStatutZepByName(Constantes.ZepStatuts.EnAccord);
                dfv.StatusZepId = enAccord.Id;
                dfv.StatusZep = enAccord;
                if (!string.IsNullOrWhiteSpace(dfv.DerriereTrainOuvrantNumero))
                {
                    if (dfv.OccupationList == null)
                        throw new ArgumentException("Il doit y avoir au moins une occupation dans la DFV dans le cas où l'option 'derrière TTx déclencheur' est coché");
                    if (dfv.OccupationList != null && dfv.OccupationList.Count < 1)
                    {
                        throw new ArgumentException("Il doit y avoir au moins une occupation dans la DFV dans le cas où l'option 'derrière TTx déclencheur' est coché");
                    }
                }

                if (dfv.OccupationList != null && dfv.OccupationList.Count > 0)
                {
                    if (dfv.OccupationList.Any(x => string.IsNullOrWhiteSpace(x.TTXNumero)))
                        throw new Exception("Veuillez renseigner tous les numéros d'occupations.");
                }
                if (dfv.ManoeuvreList != null && dfv.ManoeuvreList.Count > 0)
                {
                    if (dfv.ManoeuvreList.Any(x => string.IsNullOrWhiteSpace(x.TTXNumero)))
                        throw new Exception("Veuillez renseigner tous les numéros d'engagements et de dégagements.");
                }

                if (dfv.DerriereTTXDeclencheur.HasValue)
                {
                    if (dfv.DerriereTTXDeclencheur == true && string.IsNullOrWhiteSpace(dfv.DerriereTrainOuvrantNumero))
                        throw new Exception("Pour un TTx déclencheur veuillez renseigner le N° du TTx.");
                }

                var posteZep = _ZepService.GetAllPostesConditionDeProtectionIdZep(dfv.ZepId);
                foreach (var p in posteZep)
                {
                    if (p.Id != dfv.ResponsableDFVId)
                    {
                        MesureProtectionPrise mesure = new MesureProtectionPrise()
                        {
                            DFVId = dfv.Id,
                            PosteDemandeId = p.Id
                        };
                        _MesureService.AddMesureProtectionPrise(mesure);
                    }
                }
                return AddOrUpdateDFV(dfv);
            }
        }

        public DFV AddOrUpdateDFV(DFV dfv)
        {
            using (AppDbContext context = new AppDbContext())
            {
                dfv.VerificationZepLibre_Ligne = null;
                dfv.Zep = null;
                dfv.VerificationZepLibre_IPCS = null;
                dfv.StatusZep = null;
                dfv.ResponsableDFV = null;
                dfv.MesureProtectionPriseList = null;
                dfv.ManoeuvreList = null;
                dfv.Aiguille_DFVList = null;
                dfv.OccupationList = null;
                if (context.DFV.Any(t => t.Id == dfv.Id))
                {
                    if (context.Entry(dfv).State == EntityState.Detached)
                    {
                        try
                        {
                            context.DFV.Attach(dfv);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    context.Entry(dfv).State = EntityState.Modified;
                }
                else
                {
                    if (context.DFV.Any(t => t.Numero == dfv.Numero && !string.IsNullOrWhiteSpace(t.Numero))) throw new Exception("Le numéro de DFV a déjà été utilisé");
                    if (context.DFV.Any(x => x.ZepId == dfv.ZepId)) // Une DFV est déjà créée pour cette ZEP
                    {
                        var listDFV = context.DFV.Include(t => t.StatusZep).Where(x => x.ZepId == dfv.ZepId &&
                        x.StatusZep.Statut != Constantes.ZepStatuts.Refusee &&
                        x.StatusZep.Statut != Constantes.ZepStatuts.Annulee).ToList();
                        if (listDFV.Any(x =>
                        x.DateDebutApplicabilite <= dfv.DateFinApplicabilite &&
                        dfv.DateDebutApplicabilite <= x.DateFinApplicabilite &&
                        (x.StatusZep.Statut == Constantes.ZepStatuts.EnAccord ||
                            x.StatusZep.Statut == Constantes.ZepStatuts.Accordee)))
                            throw new Exception("Une autre DFV existe pour cette période pour la ZEP sélectionnée.");
                    }
                    dfv.StatusZepId = this._ZepService.GetStatutZepByName(Constantes.ZepStatuts.EnCreation).Id;
                    dfv = context.DFV.Add(dfv).Entity;
                }
                context.SaveChanges();
                return GetDFVById(dfv.Id);
            }
        }

        public DFV GetDFVById(int Id)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateId<DFV>(Id);
                var dfv = context.DFV
                   .Include(t => t.StatusZep)
                    .Include(t => t.OccupationList)
                        .ThenInclude(t => t.OccupationType)
                    .Include(t => t.ResponsableDFV)
                    .Include(t => t.VerificationZepLibre_IPCS)
                        .ThenInclude(t => t.PosteDemande)
                    .Include(t => t.VerificationZepLibre_IPCS)
                        .ThenInclude(t => t.Depeche)
                    .Include(t => t.VerificationZepLibre_Ligne)
                        .ThenInclude(t => t.PosteDemande)
                     .Include(t => t.VerificationZepLibre_Ligne)
                        .ThenInclude(t => t.Depeche)
                        .ThenInclude(t => t.DepecheType)
                     .Include(t => t.VerificationZepLibre_Ligne)
                        .ThenInclude(t => t.Depeche2)
                        .ThenInclude(t => t.DepecheType)
                    .Include(t => t.Zep)
                        .ThenInclude(t => t.ZepType)
                    .Include(t => t.Zep)
                        .ThenInclude(t => t.MesureProtection_ZepList)
                        .ThenInclude(t => t.Poste)
                    .Include(t => t.Zep)
                        .ThenInclude(t => t.MesureProtection_ZepList)
                        .ThenInclude(t => t.MesureProtectionType)
                    .Include(t => t.Zep)
                        .ThenInclude(t => t.AiguilleList)
                    .Include(t => t.MesureProtectionPriseList)
                        .ThenInclude(t => t.MesureProtectionPrise_DepecheList)
                        .ThenInclude(t => t.Depeche)
                        .ThenInclude(t => t.DepecheType)
                    .Include(t => t.MesureProtectionPriseList)
                        .ThenInclude(t => t.PosteDemande)
                    .Include(t => t.ManoeuvreList)
                        .ThenInclude(t => t.ManoeuvreType)
                    .Include(t => t.ManoeuvreList)
                        .ThenInclude(t => t.Manoeuvre_DepecheList)
                        .ThenInclude(t => t.Depeche)
                        .ThenInclude(t => t.DepecheType)
                    .Include(t => t.ManoeuvreList)
                        .ThenInclude(t => t.Manoeuvre_DepecheList)
                        .ThenInclude(t => t.Manoeuvre)
                        .ThenInclude(t => t.ManoeuvreType)
                    .Include(t => t.ManoeuvreList)
                        .ThenInclude(t => t.PointRemarquable_Zep)
                        .ThenInclude(t => t.PosteConcerne)
                    .Include(x => x.MaterielRoulantList)
                    .Include(t => t.Aiguille_DFVList)
                        .ThenInclude(t => t.Aiguille)
                   .Where(t => t.Id == Id)
                    .FirstOrDefault();
                return dfv;
            }
        }

        public DFV GetDFVByNumero(string numero)
        {
            using (AppDbContext context = new AppDbContext())
            {
                var dfv = context.DFV
                    .Include(t => t.StatusZep)
                    .Include(t => t.OccupationList)
                        .ThenInclude(t => t.OccupationType)
                    .Include(t => t.ResponsableDFV)
                    .Include(t => t.VerificationZepLibre_IPCS)
                        .ThenInclude(t => t.PosteDemande)
                    .Include(t => t.VerificationZepLibre_IPCS)
                        .ThenInclude(t => t.Depeche)
                    .Include(t => t.VerificationZepLibre_Ligne)
                        .ThenInclude(t => t.PosteDemande)
                     .Include(t => t.VerificationZepLibre_Ligne)
                        .ThenInclude(t => t.Depeche)
                        .ThenInclude(t => t.DepecheType)
                     .Include(t => t.VerificationZepLibre_Ligne)
                        .ThenInclude(t => t.Depeche2)
                        .ThenInclude(t => t.DepecheType)
                    .Include(t => t.Zep)
                        .ThenInclude(t => t.ZepType)
                    .Include(t => t.Zep)
                        .ThenInclude(t => t.MesureProtection_ZepList)
                        .ThenInclude(t => t.Poste)
                     .Include(t => t.Zep)
                        .ThenInclude(t => t.MesureProtection_ZepList)
                        .ThenInclude(t => t.MesureProtectionType)
                    .Include(t => t.Zep)
                        .ThenInclude(t => t.AiguilleList)
                    .Include(t => t.MesureProtectionPriseList)
                        .ThenInclude(t => t.MesureProtectionPrise_DepecheList)
                        .ThenInclude(t => t.Depeche)
                        .ThenInclude(t => t.DepecheType)
                    .Include(t => t.MesureProtectionPriseList)
                        .ThenInclude(t => t.PosteDemande)
                    .Include(t => t.ManoeuvreList)
                        .ThenInclude(t => t.ManoeuvreType)
                    .Include(t => t.ManoeuvreList)
                        .ThenInclude(t => t.Manoeuvre_DepecheList)
                        .ThenInclude(t => t.Depeche)
                        .ThenInclude(t => t.DepecheType)
                    .Include(t => t.ManoeuvreList)
                        .ThenInclude(t => t.Manoeuvre_DepecheList)
                        .ThenInclude(t => t.Manoeuvre)
                        .ThenInclude(t => t.ManoeuvreType)
                    .Include(t => t.ManoeuvreList)
                        .ThenInclude(t => t.PointRemarquable_Zep)
                        .ThenInclude(t => t.PosteConcerne)
                    .Include(t => t.Aiguille_DFVList)
                        .ThenInclude(t => t.Aiguille)
                    .Include(x => x.MaterielRoulantList)
                        .Where(t => t.Numero == numero)
                    .FirstOrDefault();

                return dfv;
            }
        }

        public DFV AccorderDFV(DFV dfv)
        {
            using (AppDbContext context = new AppDbContext())
            {
                var transac = context.Database.BeginTransaction();
                try
                {
                    context.ValidateEntity<DFV>(dfv);
                    //vérifier les mesures de protection
                    var mesureDfv = _MesureService.GetMesuresProtectionByDfvId(dfv.Id);

                    if (mesureDfv != null)
                    {
                        foreach (var mesure in mesureDfv)
                        {
                            if (!(mesure.MesureProtectionPrise_DepecheList != null && mesure.MesureProtectionPrise_DepecheList.Count() > 0))
                            {
                                throw new Exception("Certaines mesures de protection n'ont pas été prises.");
                            }
                        }
                    }
                    //vérifier les mesures de protection

                    //plus de vérification avant accord
                    if (dfv.StatusZep == null || dfv.StatusZep.Statut != Constantes.ZepStatuts.EnAccord)
                    {
                        throw new ArgumentException("Le status de la zep doit être 'En accord' avant de l'accorder : " + nameof(dfv.StatusZep));
                    }


                    //if (!string.IsNullOrWhiteSpace(dfv.DerriereTrainOuvrantNumero))
                    //{
                    //    if (dfv.OccupationList != null && dfv.OccupationList.Count < 1)
                    //    {
                    //        throw new ArgumentException("Il doit y avoir au moins une occupation dans la DFV dans le cas où l'option 'DerriereTrainOuvrantNumero' est coché");
                    //    }
                    //    else
                    //    {
                    //        if (dfv.OccupationList.ToList().Any(t => 
                    //        string.IsNullOrWhiteSpace(t.TTXLieuStationnement)
                    //        || t.OccupationType.Nom != Constantes.OccupationTypes.OccupeeALAccord))
                    //        {
                    //            throw new ArgumentException("Toutes les occupation à l'accord doivent avoir un lieu de stationnement.");
                    //        }
                    //        else
                    //        {
                    //            dfv.OccupationList.ToList().ForEach(x => _OccupationService.UpdateOccupation(x));

                    //        }
                    //    }
                    //}

                    if (dfv.DateDebutAccord == null) throw new ArgumentException("Argument null : " + nameof(dfv.DateDebutAccord));
                    if (dfv.DateFinAccord == null) throw new ArgumentException("Argument null : " + nameof(dfv.DateFinAccord));
                    if (dfv.DateDebutAccord > dfv.DateFinAccord) throw new ArgumentException("La date de début de l'accord doit être antérieur à la date de fin.");


                    if (dfv.AccordeParEcrit == null && string.IsNullOrWhiteSpace(dfv.TransmisParDepecheNumero))
                    {
                        throw new ArgumentException("L'accord doit être donné par écrit ou transmis par dépêche : " + nameof(dfv.AccordeParEcrit));
                    }

                    if (dfv.ManoeuvreList != null && dfv.ManoeuvreList.ToArray().Length > 0)
                    {
                        var typeManoeuvre = _ManoeuvreService.GetManoeuvreTypeByName(Constantes.ManoeuvreTypes.Franchissement);
                        var listManoeuvre = dfv.ManoeuvreList.Where(x => x.ManoeuvreTypeId == typeManoeuvre.Id).ToList();
                        listManoeuvre.ForEach(x => _ManoeuvreService.UpdateManoeuvre(x));
                    }
                    if (dfv.MaterielRoulantList.Any())
                    {
                        if (dfv.MaterielRoulantList.Where(x => (string.IsNullOrEmpty(x.TTXLieuStationnement) || string.IsNullOrEmpty(x.TTXNumero)) ||
                                                               (string.IsNullOrWhiteSpace(x.TTXLieuStationnement) || string.IsNullOrWhiteSpace(x.TTXNumero))).Any())
                            throw new Exception("Veuillez remplir tous les champs concernant le matériel roulant");
                        context.MaterielRoulant.AddRange(dfv.MaterielRoulantList);
                        context.SaveChanges();
                    }
                    StatutZep statut = context.StatutZep.FirstOrDefault(t => t.Statut == Constantes.ZepStatuts.Accordee);
                    if (statut == null) throw new Exception("Le statut Accordée n'existe pas dans la BDD");
                    dfv.StatusZepId = statut.Id;
                    dfv.StatusZep = statut;
                    dfv = AddOrUpdateDFV(dfv);
                    transac.Commit();
                    return dfv;
                }
                catch (Exception e)
                {
                    transac.Rollback();
                    throw e;
                }

            }
        }
        public List<DFV> GetDfvListPrevisionnelle(Clic.DAL.Entities.Profil.Profil pProfil)
        {
            using (AppDbContext context = new AppDbContext())
            {
                var dfvlist = context.DFV
                    .Include(t => t.StatusZep)
                    .Include(t => t.OccupationList)
                        .ThenInclude(t => t.OccupationType)
                    .Include(t => t.ResponsableDFV)
                    .Include(t => t.VerificationZepLibre_IPCS)
                        .ThenInclude(t => t.PosteDemande)
                    .Include(t => t.VerificationZepLibre_IPCS)
                        .ThenInclude(t => t.Depeche)
                    .Include(t => t.VerificationZepLibre_Ligne)
                        .ThenInclude(t => t.PosteDemande)
                     .Include(t => t.VerificationZepLibre_Ligne)
                        .ThenInclude(t => t.Depeche)
                    .Include(t => t.Zep)
                        .ThenInclude(t => t.ZepType)
                    .Include(t => t.MesureProtectionPriseList)
                        .ThenInclude(t => t.PosteDemande)
                    .Include(t => t.ManoeuvreList)
                        .ThenInclude(t => t.Manoeuvre_DepecheList)
                    .Include(t => t.Aiguille_DFVList)
                        .ThenInclude(t => t.Aiguille)
                    .Include(x => x.MaterielRoulantList)
                    .Where(a => (a.StatusZep.Statut == Constantes.ZepStatuts.EnCreation && a.IsPrevisonelle == true) &&
                                (pProfil.Nom == Constantes.Profils.PoleTravaux))
                    .ToList();

                dfvlist.ForEach(t =>
                {
                    t.ManoeuvreList.ToList().ForEach(x =>
                    {
                        x.DFV = null;
                    });
                    t.MesureProtectionPriseList.ToList().ForEach(x =>
                    {
                        x.DFV = null;
                        x.PosteDemande = null;

                    });
                    t.OccupationList.ToList().ForEach(x =>
                    {
                        x.DFV = null;
                    });
                    t.Aiguille_DFVList.ToList().ForEach(x =>
                    {
                        x.DFV = null;
                        x.Aiguille.Aiguille_DFVList = null;
                    });
                    t.Zep.DFVList = null;
                    t.Zep.ZepType.ZepList = null;
                    t.StatusZep.DFVList = null;

                });
                return dfvlist;
            }
        }
        public List<DFV> GetDfvList(Poste pPoste, Clic.DAL.Entities.Profil.Profil pProfil)
        {
            using (AppDbContext context = new AppDbContext())
            {
                var dfvlist = context.DFV
                    .Include(t => t.StatusZep)
                    .Include(t => t.OccupationList)
                        .ThenInclude(t => t.OccupationType)
                    .Include(t => t.ResponsableDFV)
                    .Include(t => t.VerificationZepLibre_IPCS)
                        .ThenInclude(t => t.PosteDemande)
                    .Include(t => t.VerificationZepLibre_IPCS)
                        .ThenInclude(t => t.Depeche)
                    .Include(t => t.VerificationZepLibre_Ligne)
                        .ThenInclude(t => t.PosteDemande)
                     .Include(t => t.VerificationZepLibre_Ligne)
                        .ThenInclude(t => t.Depeche)
                    .Include(t => t.Zep)
                        .ThenInclude(t => t.ZepType)
                    .Include(t => t.MesureProtectionPriseList)
                        .ThenInclude(t => t.PosteDemande)
                    .Include(t => t.ManoeuvreList)
                        .ThenInclude(t => t.Manoeuvre_DepecheList)
                    .Include(t => t.Aiguille_DFVList)
                        .ThenInclude(t => t.Aiguille)
                    .Include(x => x.MaterielRoulantList)
                    .Where(a => (a.ResponsableDFVId == pPoste.Id && pProfil.Nom == Constantes.Profils.AgentCirculation) ||
                        (a.Zep.MesureProtection_ZepList.Any(b => b.PosteId == pPoste.Id)
                        || (a.ManoeuvreList.Any(b => b.PointRemarquable_Zep.PosteConcerneId == pPoste.Id)))
                        )
                    .ToList();

                dfvlist.ForEach(t =>
                {
                    t.ManoeuvreList.ToList().ForEach(x =>
                    {
                        x.DFV = null;
                    });
                    t.MesureProtectionPriseList.ToList().ForEach(x =>
                    {
                        x.DFV = null;
                        x.PosteDemande = null;
                    });
                    t.OccupationList.ToList().ForEach(x =>
                    {
                        x.DFV = null;
                    });
                    t.Aiguille_DFVList.ToList().ForEach(x =>
                    {
                        x.DFV = null;
                        x.Aiguille.Aiguille_DFVList = null;
                    });
                    t.Zep.DFVList = null;
                    t.Zep.ZepType.ZepList = null;
                    t.StatusZep.DFVList = null;

                });
                return dfvlist;
            }
        }

        public List<Aiguille_DFV> UpdateAiguilleDFV(List<Aiguille_DFV> pListe)
        {
            using (AppDbContext context = new AppDbContext())
            {
                foreach (var item in pListe)
                {
                    if (item.Id > 0)
                        context.ValidateEntity(item);
                    else
                        context.ValidateEntity(item, true);
                    context.ValidateId<DFV>(item.DFVId);
                    context.ValidateId<Aiguille>(item.AiguilleId);
                    item.DFV = null;
                    item.Aiguille = null;
                    if (item.Id > 0)
                    {
                        context.Aiguille_DFV.Remove(context.Aiguille_DFV.First(a => a.Id == item.Id));
                        item.Id = 0;
                    }
                    item.Id = context.Aiguille_DFV.Add(item).Entity.Id;
                }
                context.SaveChanges();
                List<Aiguille_DFV> toDelete = context.Aiguille_DFV.Where(a => a.DFVId == pListe.First().DFVId && !pListe.Any(b => b.Id == a.Id)).ToList();
                foreach (var item in toDelete)
                {
                    context.Remove(item);
                }
                context.SaveChanges();
            }
            return pListe;
        }

        public VerificationZepLibre_Ligne ValiderConfirmationVerification(VerificationZepLibre_Ligne verif)
        {
            using (AppDbContext context = new AppDbContext())
            {
                if (verif.Id < 1) AddVerificationZepLibre_Ligne(verif);
                else UpdateVerificationZepLibre_Ligne(verif);
                var result = GetVerificationZepLibre_LigneById(verif.Id);
                return result;
            }
        }

        public VerificationZepLibre_Ligne GetVerificationZepLibre_LigneById(int id)
        {
            using (AppDbContext context = new AppDbContext())
            {
                var verif = context.VerificationZepLibre_Ligne
                    .Include(t => t.Depeche)
                        .ThenInclude(t => t.DepecheType)
                    .Include(t => t.Depeche2)
                        .ThenInclude(t => t.DepecheType)
                    .FirstOrDefault(t => t.Id == id);
                return verif;
            }
        }

        public VerificationZepLibre_Ligne AddVerificationZepLibre_Ligne(VerificationZepLibre_Ligne v)
        {
            using (AppDbContext context = new AppDbContext())
            {
                DepecheType depecheType = _DepecheService.GetDepecheTypeByName(Constantes.DepecheType.VerificationZepLibre_Ligne);
                Depeche newDepeche = new Depeche()
                {
                    NumeroDonne = v.Depeche.NumeroDonne,
                    NumeroRecu = v.Depeche.NumeroRecu,
                    DateDepeche = v.Depeche.DateDepeche,
                    DepecheTypeId = depecheType.Id
                };
                newDepeche = _DepecheService.AddDepeche(newDepeche);
                VerificationZepLibre_Ligne newV = new VerificationZepLibre_Ligne()
                {
                    DestinationTrain = v.DestinationTrain,
                    HeureArrivee = v.HeureArrivee,
                    Nom = v.Nom,
                    NumeroTrainRecu = v.NumeroTrainRecu,
                    OrigineTrain = v.OrigineTrain,
                    PosteDemandeId = v.PosteDemandeId,
                    Depeche2 = null,
                    DepecheId = newDepeche.Id,
                    DFVId = v.DFVId,
                };
                context.VerificationZepLibre_Ligne.Add(newV);

                //if (v.Depeche != null)
                //{
                //    v.Depeche.DepecheTypeId = depecheType.Id;
                //}

                //if (v.Depeche2 != null) context.Entry(v.Depeche2).State = EntityState.Detached;

                //context.Entry(v).State = EntityState.Added;
                //context.VerificationZepLibre_Ligne.Add(v);
                context.SaveChanges();
                var result = GetVerificationZepLibre_LigneById(newV.Id);
                return result;
            }
        }

        public VerificationZepLibre_Ligne UpdateVerificationZepLibre_Ligne(VerificationZepLibre_Ligne v)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.Attach(v);
                DepecheType depecheType = _DepecheService.GetDepecheTypeByName(Constantes.DepecheType.VerificationZepLibre_Ligne);

                if (v.Depeche2 != null)
                {
                    v.Depeche2.DepecheTypeId = depecheType.Id;
                }

                if (v.Depeche != null)
                {
                    try
                    {
                        context.Entry(v.Depeche).State = EntityState.Unchanged;
                    }
                    catch (Exception ex) { throw ex; }
                }
                context.Entry(v).State = EntityState.Modified;
                context.SaveChanges();

                var result = GetVerificationZepLibre_LigneById(v.Id);
                return result;
            }
        }
        public DFV RestituerDFV(DFV pDFV)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity(pDFV);
                if (pDFV.IsResitutionParDepeche == null)
                    throw new ArgumentNullException(nameof(pDFV.IsResitutionParDepeche) + " Veuillez choisir le type de réstitution !");
                if (pDFV.DateRestitution == null)
                    throw new ArgumentNullException(nameof(pDFV.DateRestitution) + " Veuillez donner la date de réstitution !");
                if (pDFV.IsResitutionParDepeche == true)
                    if (pDFV.NumeroDepecheRecuRestitution == null)
                        throw new ArgumentNullException(nameof(pDFV.NumeroDepecheRecuRestitution) + " Veuillez donner numéro de dépêche reçu !");
                if (pDFV.StatusZep == null || pDFV.StatusZep.Statut != Constantes.ZepStatuts.Accordee)
                {
                    throw new ArgumentException("Le status de la zep doit être 'Accordée' avant de la restituer : " + nameof(pDFV.StatusZep));
                }
                var maDFV = context.DFV.First(a => a.Id == pDFV.Id);
                maDFV.IsResitutionParDepeche = pDFV.IsResitutionParDepeche;
                maDFV.DateRestitution = pDFV.DateRestitution;
                maDFV.NumeroDepecheRecuRestitution = pDFV.NumeroDepecheRecuRestitution;
                context.Entry(maDFV).Property(a => a.IsResitutionParDepeche).IsModified = true;
                context.Entry(maDFV).Property(a => a.DateRestitution).IsModified = true;
                context.Entry(maDFV).Property(a => a.NumeroDepecheRecuRestitution).IsModified = true;
                context.SaveChanges();
                return CheckOrUpdateDfvStatutLeveeMesure(maDFV.Id);
            }
        }

        public VerificationZepLibre_Ligne ValiderDemandeVerification(VerificationZepLibre_Ligne verif)
        {
            using (AppDbContext context = new AppDbContext())
            {
                if (verif.Id < 1) AddVerificationZepLibre_Ligne(verif);
                else UpdateVerificationZepLibre_Ligne(verif);

                var result = GetVerificationZepLibre_LigneById(verif.Id);
                return result;
            }
        }

        public DFV CheckOrUpdateDfvStatutLeveeMesure(int id)
        {
            using (AppDbContext context = new AppDbContext())
            {
                var dfvEntity = this.GetDFVById(id);
                if (dfvEntity == null)
                    throw new Exception("Impossible de retrouver la DFV sélectionnée");
                // L'AC avait des mesure de protection à prendre
                // Il faut vérifier si il à levée ses mesures de protection
                StatutZep leveeMesure = _ZepService.GetStatutZepByName(Constantes.ZepStatuts.LeveeMesure);
                if (dfvEntity.MesureProtectionPiseParAC.HasValue && dfvEntity.MesureProtectionPiseParAC == true)
                {
                    if (!dfvEntity.LeveeMesuresParAC.HasValue || dfvEntity.LeveeMesuresParAC == false)
                    {
                        dfvEntity.StatusZepId = leveeMesure.Id;
                        context.Entry(dfvEntity).Property(a => a.StatusZepId).IsModified = true;
                        context.SaveChanges();
                        return this.GetDFVById(id);
                    }
                }
                if (dfvEntity.MesureProtectionPriseList.Any())
                {
                    //On vérifie que toutes les mesures de protection prise ont une dépêche de type Levée de mesure.
                    if (!dfvEntity.MesureProtectionPriseList.ToList().TrueForAll(x => x.MesureProtectionPrise_DepecheList.Any(t => t.Depeche.DepecheType.Nom == Constantes.DepecheType.LeveeDesMesures)))
                    {
                        //Les levées de mesures n'ont pas été prises.
                        dfvEntity.StatusZepId = leveeMesure.Id;
                        context.Entry(dfvEntity).Property(a => a.StatusZepId).IsModified = true;
                        context.SaveChanges();
                        return this.GetDFVById(id);
                    }
                }
                // Si tous les tests sont passé, les levées de mesures ont été prises, la DFV est restituée.
                var statutZepRestitue = context.StatutZep.Where(x => x.Statut == Constantes.ZepStatuts.Restituee).FirstOrDefault();
                if (statutZepRestitue == null)
                    throw new Exception("Il manque des status de DFV, veuillez contacter l'administrateur.");
                dfvEntity.StatusZepId = statutZepRestitue.Id;
                context.Entry(dfvEntity).Property(a => a.StatusZepId).IsModified = true;
                context.SaveChanges();
                return this.GetDFVById(id);
            }
        }
    }
}
