﻿using Clic.BLL.IServices;
using Clic.BLL.Services.Commun;
using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Persistance.ExtentionMethods;
using Clic.Data.DbContext;
using CLic.BLL.Services.FicheZep;
using Microsoft.EntityFrameworkCore;
using PHENIX.BLL.IServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Clic.BLL.Services.FicheDFV
{
    public class DepecheService : IDepecheService
    {
        public DepecheService()
        {

        }
        private void ValidateDepeche(Depeche depeche, AppDbContext context)
        {
            if (depeche == null || !context.Depeche.Any(t => t.Id == depeche.Id))
                throw new ArgumentException("Argument invalide : " + nameof(depeche.Id));
        }

        public List<DepecheType> GetAllDepecheType()
        {
            using (AppDbContext context = new AppDbContext())
            {
                return context.DepecheType.ToList();
            }

        }

        public DepecheType GetDepecheTypeByName(string depecheTypeName)
        {
            using (AppDbContext context = new AppDbContext())
            {
                return context.DepecheType.FirstOrDefault(t => t.Nom.Equals(depecheTypeName));
            }
        }


        public DepecheType GetDepecheTypeByNom(string pNom)
        {
            using (AppDbContext context = new AppDbContext())
            {
                if (!context.DepecheType.Any(t => t.Nom == pNom))
                    throw new ArgumentException("Argument invalide : " + nameof(pNom));

                return context.DepecheType
                    .Where(t => t.Nom == pNom)
                    .FirstOrDefault();
            }
        }

        public Depeche GetDepecheById(int Id)
        {
            using (AppDbContext context = new AppDbContext())
            {
                var dep = context.Depeche
                    .Include(t => t.DepecheType)
                    .FirstOrDefault(t => t.Id == Id);
                return dep;
            }
        }

        public Depeche AddDepeche(Depeche depeche)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.Entry(depeche).State = EntityState.Added;
                context.Depeche.Add(depeche);
                context.SaveChanges();
                return GetDepecheById(depeche.Id);
            }
        }

        public Depeche AddDepecheToMesureProtection(Depeche depeche, int mesureId)
        {
            using (AppDbContext context = new AppDbContext())
            {
                //créer une transaction
                context.Entry(depeche).State = EntityState.Added;
                var newDepeche = context.Depeche.Add(depeche).Entity;
                context.SaveChanges();
                MesureProtectionPrise_Depeche mpd = new MesureProtectionPrise_Depeche()
                {
                    MesureProtectionPriseId = mesureId,
                    DepecheId = depeche.Id
                };
                context.MesureProtectionPrise_Depeche.Add(mpd);
                context.SaveChanges();
                return GetDepecheById(depeche.Id);
            }
        }

        public Depeche AddDepecheToManoeuvre(Depeche depeche, int manoeuvreId)
        {
            using (AppDbContext context = new AppDbContext())
            {
                //créer une transaction
                Manoeuvre_Depeche mdep = new Manoeuvre_Depeche()
                {
                    ManoeuvreId = manoeuvreId,
                    DepecheId = depeche.Id
                };
                context.Manoeuvre_Depeche.Add(mdep);
                context.SaveChanges();
                return GetDepecheById(depeche.Id);
            }
        }

        public void UpdateDepche(Depeche depeche)
        {
            using (AppDbContext context = new AppDbContext())
            {
                ValidateDepeche(depeche, context);
                context.Depeche.Attach(depeche);
                context.Entry(depeche).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteDepeche(Depeche depeche)
        {
            using (AppDbContext context = new AppDbContext())
            {
                ValidateDepeche(depeche, context);
                context.Entry(depeche).State = EntityState.Deleted;
                context.Depeche.Remove(depeche);
                context.SaveChanges();
            }
        }

        public List<Manoeuvre_Depeche> GetListManoeuvreDepecheByIdDfv(int IdDfv)
        {
            using (AppDbContext context = new AppDbContext())
            {


                return context.Manoeuvre_Depeche
                    .Include(t => t.Depeche)
                        .ThenInclude(x => x.DepecheType)
                    .Include(t => t.Manoeuvre)
                        .ThenInclude(x => x.ManoeuvreType)
                    .Where(t => t.Manoeuvre.DFVId == IdDfv)
                    .ToList();
            }
        }

        public Depeche AddDepecheNotification(Depeche depeche, int manoeuvreId)
        {
            using (AppDbContext context = new AppDbContext())
            {
                var transac = context.Database.BeginTransaction();
                //Récuperer toutes les manoeuvres pour le poste de la manoeuvre, et attribuer la depeche.
                try
                {
                    var manoeuvre = context.Manoeuvre.Where(x => x.Id == manoeuvreId).Include(x => x.PointRemarquable_Zep).FirstOrDefault();
                    if (manoeuvre == null)
                        throw new Exception("Impossible de retrouver la manoeuvre sélectionnée.");
                    var allManoeuvrePoste = context.Manoeuvre.Where(x => x.DFVId == manoeuvre.DFVId && 
                                                                            x.PointRemarquable_Zep.PosteConcerneId == manoeuvre.PointRemarquable_Zep.PosteConcerneId && 
                                                                            (x.ManoeuvreType.Nom == Constantes.ManoeuvreTypes.Engagement || 
                                                                             x.ManoeuvreType.Nom == Constantes.ManoeuvreTypes.Degagement)).ToList();
                    if (!allManoeuvrePoste.Any())
                        throw new Exception("Aucune manoeuvre prévue pour le poste sélectionné.");
                    var newDepeche = this.AddDepeche(depeche);
                    foreach(var manoeouvre in allManoeuvrePoste)
                    {
                       this.AddDepecheToManoeuvre(newDepeche, manoeouvre.Id);
                    }
                    transac.Commit();
                    return GetDepecheById(depeche.Id);
                }
                catch (Exception e)
                {
                    transac.Rollback();
                    throw e;
                }

            }
        }
    }
}
