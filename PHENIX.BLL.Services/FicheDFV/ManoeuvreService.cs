﻿using Clic.BLL.Services.Commun;
using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Persistance.ExtentionMethods;
using Clic.Data.DbContext;
using Microsoft.EntityFrameworkCore;
using PHENIX.BLL.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clic.BLL.Services.FicheDFV
{
    public class ManoeuvreService : IManoeuvreService
    {
        public IDepecheService _depecheSrv;
        public ManoeuvreService(IDepecheService depecheSrv)
        {
            this._depecheSrv = depecheSrv;
        }
        private void ValidateManoeuvre(Manoeuvre manoeuvre, AppDbContext context)
        {
            if (manoeuvre == null || !context.Manoeuvre.Any(t => t.Id == manoeuvre.Id))
                throw new ArgumentException("Argument invalide : " + nameof(manoeuvre.Id));
        }

        public Manoeuvre GetManoeuvreById(int Id)
        {
            using (AppDbContext context = new AppDbContext())
            {
                if (!context.Manoeuvre.Any(t => t.Id == Id))
                    throw new ArgumentException("Argument invalide : " + nameof(Id));

                return context.Manoeuvre
                    .Include(t => t.ManoeuvreType)
                    .Include(t => t.PointRemarquable_Zep)
                        .ThenInclude(t => t.PosteConcerne)
                    .Include(t => t.Manoeuvre_DepecheList)
                        .ThenInclude(t => t.Manoeuvre)
                    .Include(t => t.Manoeuvre_DepecheList)
                        .ThenInclude(t => t.Depeche)
                    .Where(t => t.Id == Id)
                    .FirstOrDefault();
            }
        }

        public ManoeuvreType GetManoeuvreTypeById(int Id)
        {
            using (AppDbContext context = new AppDbContext())
            {
                if (!context.ManoeuvreType.Any(t => t.Id == Id))
                    throw new ArgumentException("Argument invalide : " + nameof(Id));

                return context.ManoeuvreType
                    .Where(t => t.Id == Id)
                    .FirstOrDefault();
            }
        }

        public ManoeuvreType GetManoeuvreTypeByName(string Nom)
        {
            using (AppDbContext context = new AppDbContext())
            {
                if (!context.ManoeuvreType.Any(t => t.Nom == Nom))
                    throw new ArgumentException("Argument invalide : " + nameof(Nom));

                return context.ManoeuvreType
                    .Where(t => t.Nom == Nom)
                    .FirstOrDefault();
            }
        }
        public Manoeuvre AddManoeuvre(Manoeuvre manoeuvre)
        {
            using (AppDbContext context = new AppDbContext())
            {
                var newManoeuvre = context.Manoeuvre.Add(manoeuvre).Entity;
                context.SaveChanges();
                return this.GetManoeuvreById(newManoeuvre.Id);
            }
        }

        public Manoeuvre UpdateManoeuvre(Manoeuvre manoeuvre)
        {
            manoeuvre.DFV = null;
            manoeuvre.ManoeuvreType = null;
            manoeuvre.Manoeuvre_DepecheList = null;
            manoeuvre.AutreDepecheList = null;
            manoeuvre.PointRemarquable_Zep = null;
            using (AppDbContext context = new AppDbContext())
            {
                ValidateManoeuvre(manoeuvre, context);
                context.Manoeuvre.Attach(manoeuvre);
                context.Entry(manoeuvre).State = EntityState.Modified;
                context.SaveChanges();
                return this.GetManoeuvreById(manoeuvre.Id);
            }
        }

        public void DeleteManoeuvre(Manoeuvre manoeuvre)
        {
            using (AppDbContext context = new AppDbContext())
            {
                ValidateManoeuvre(manoeuvre, context);
                Manoeuvre man = GetManoeuvreById(manoeuvre.Id);
                context.Manoeuvre.Remove(man);
                context.SaveChanges();
            }
        }
        public List<ManoeuvreType> GetAllManoeuvreType()
        {
            using (AppDbContext context = new AppDbContext())
            {
                return context.ManoeuvreType.ToList();
            }
        }

        public Manoeuvre EffectueManoeuvre(Manoeuvre manoeuvre)
        {
            manoeuvre.DFV = null;
            manoeuvre.ManoeuvreType = null;
            manoeuvre.PointRemarquable_Zep = null;

            using (AppDbContext context = new AppDbContext())
            {
                var transac = context.Database.BeginTransaction();
                try
                {
                    context.ValidateEntity(manoeuvre);
                    if (manoeuvre.ParDepeche == false && manoeuvre.ParEcrit == false && manoeuvre.Verbale == false)
                        throw new Exception("Veuillez sélectionner un type d'autorisation.");
                    if (manoeuvre.ParDepeche == true)
                    {
                        var typeManoeuvre = this.GetManoeuvreTypeById(manoeuvre.ManoeuvreTypeId);
                        var typeDepeche = typeManoeuvre.Nom == Constantes.ManoeuvreTypes.Degagement ?
                                                                _depecheSrv.GetDepecheTypeByName(Constantes.DepecheType.Degagement) :
                                                                _depecheSrv.GetDepecheTypeByName(Constantes.DepecheType.Engagement);
                        //var manoeuvre_depecheManoeuvre = manoeuvre.Manoeuvre_DepecheList.Where(x => x.Depeche.DepecheTypeId == typeDepeche.Id).FirstOrDefault();
                        //if (manoeuvre_depecheManoeuvre == null)
                        //    throw new Exception("Pas de dépêche.");

                        var depecheManoeuvre = manoeuvre.Manoeuvre_DepecheList.First().Depeche;
                        depecheManoeuvre.DepecheTypeId = typeDepeche.Id;
                        var newDepeche = _depecheSrv.AddDepeche(depecheManoeuvre);
                        manoeuvre.Manoeuvre_DepecheList = null;
                        var newDepeche_Manoeuvre = new Manoeuvre_Depeche();
                        newDepeche_Manoeuvre.DepecheId = newDepeche.Id;
                        newDepeche_Manoeuvre.ManoeuvreId = manoeuvre.Id;
                        context.Manoeuvre_Depeche.Add(newDepeche_Manoeuvre);
                        context.SaveChanges();
                    }
                    else if (manoeuvre.ParEcrit == true)
                    {
                        manoeuvre.Manoeuvre_DepecheList = null;
                        if (manoeuvre.SignatureRptx == false)
                            throw new Exception("La signature du Rptx est requise pour une signature par écrit.");

                    }else
                    {
                        manoeuvre.Manoeuvre_DepecheList = null;
                    }
                    manoeuvre = this.UpdateManoeuvre(manoeuvre);
                    transac.Commit();
                    return this.GetManoeuvreById(manoeuvre.Id);
                }catch(Exception e)
                {
                    transac.Rollback();
                    throw e;
                }


            }
        }
    }
}
