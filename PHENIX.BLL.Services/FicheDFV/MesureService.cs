﻿using Clic.DAL.Entities.FicheDFV;
using Clic.Data.DbContext;
using Microsoft.EntityFrameworkCore;
using PHENIX.BLL.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Clic.DAL.Persistance.ExtentionMethods;
using Clic.DAL.Entities.FicheZep;

namespace Clic.BLL.Services.FicheDFV
{
    public class MesureService : IMesureService
    {
        public MesureService()
        {
        }

        public void AddDepecheToMesure(Depeche depeche, MesureProtectionPrise mesure)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity<MesureProtectionPrise>(mesure);
                context.ValidateDepeche(depeche);
                
                if (depeche.Id < 1)
                {
                    context.Depeche.Attach(depeche);
                    context.Entry(depeche).State = EntityState.Added;
                }
                MesureProtectionPrise_Depeche mppd = new MesureProtectionPrise_Depeche()
                {
                    DepecheId = depeche.Id,
                    MesureProtectionPriseId = mesure.Id
                };
                context.MesureProtectionPrise_Depeche.Add(mppd);
                context.SaveChanges();
            }
        }

        #region Mesures de protection
        
        public List<MesureProtectionPrise> GetAllMesuresProtection()
        {
            using (AppDbContext context = new AppDbContext())
            {
                var liste = context.MesureProtectionPrise
                    .Include(t => t.PosteDemande)
                    .ToList();
                return liste;
            }
        }

        public List<MesureProtectionPrise> GetMesuresProtectionByDfvId(int dfvId)
        {
            using (AppDbContext context = new AppDbContext())
            {
                var liste = context.MesureProtectionPrise
                    .Include(t => t.MesureProtectionPrise_DepecheList)
                        .ThenInclude(t => t.Depeche)
                    .Include(t => t.PosteDemande)
                    .Where(t => t.DFVId == dfvId)
                    .ToList();
                return liste;
            }
        }

        public MesureProtectionPrise GetMesureProtectionPriseById(int Id)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateId<MesureProtectionPrise>(Id);
                return context.MesureProtectionPrise
                    .Include(t => t.PosteDemande)
                    .Include(t => t.MesureProtectionPrise_DepecheList)
                        .ThenInclude(t => t.Depeche)
                    .Where(t => t.Id == Id)
                    .FirstOrDefault();
            }
        }

        public MesureProtectionPrise AddMesureProtectionPrise(MesureProtectionPrise mesure)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity<MesureProtectionPrise>(mesure, true);
                context.MesureProtectionPrise.Add(mesure);
                context.SaveChanges();
                return GetMesureProtectionPriseById(mesure.Id);
            }
        }

        public MesureProtectionPrise UpdateMesureProtectionPrise(MesureProtectionPrise mesure)
        {
            mesure.DFV = null;
            mesure.MesureProtectionPrise_DepecheList = null;
            mesure.PosteDemande = null;

            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity<MesureProtectionPrise>(mesure);
                context.MesureProtectionPrise.Attach(mesure);
                context.Entry(mesure).State = EntityState.Modified;
                context.SaveChanges();
                var result = GetMesureProtectionPriseById(mesure.Id);
                return result;
            }
        }

        public int DeleteMesureProtectionPrise(MesureProtectionPrise mesure)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity<MesureProtectionPrise>(mesure);
                MesureProtectionPrise mesur = GetMesureProtectionPriseById(mesure.Id);
                context.MesureProtectionPrise.Remove(mesur);
                context.SaveChanges();
                return mesur.Id;
            }
        }
        #endregion

        #region Verification Zep libre par L'AC Ligne

        public VerificationZepLibre_Ligne GetVerificationZepLibre_LigneById(int Id)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateId<VerificationZepLibre_Ligne>(Id);
                return context.VerificationZepLibre_Ligne
                    .Where(t => t.Id == Id)
                    .FirstOrDefault();
            }
        }

        public void AddVerificationZepLibre_Ligne(VerificationZepLibre_Ligne verif)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity<VerificationZepLibre_Ligne>(verif);
                context.VerificationZepLibre_Ligne.Add(verif);
                context.SaveChanges();
            }
        }

        public void UpdateVerificationZepLibre_Ligne(VerificationZepLibre_Ligne verif)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity<VerificationZepLibre_Ligne>(verif);
                context.VerificationZepLibre_Ligne.Attach(verif);
                context.Entry(verif).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteVerificationZepLibre_Ligne(VerificationZepLibre_Ligne verif)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity<VerificationZepLibre_Ligne>(verif);
                VerificationZepLibre_Ligne ver = GetVerificationZepLibre_LigneById(verif.Id);
                context.VerificationZepLibre_Ligne.Remove(ver);
                context.SaveChanges();
            }
        }
        #endregion

        #region Verification Zep libre par L'AC IPCS
        
        public VerificationZepLibre_IPCS GetVerificationZepLibre_IPCSById(int Id)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateId<VerificationZepLibre_IPCS>(Id);
                return context.VerificationZepLibre_IPCS
                    .Where(t => t.Id == Id)
                    .FirstOrDefault();
            }
        }

        public void AddVerificationZepLibre_IPCS(VerificationZepLibre_IPCS verif)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity<VerificationZepLibre_IPCS>(verif);
                context.VerificationZepLibre_IPCS.Add(verif);
                context.SaveChanges();
            }
        }

        public void UpdateVerificationZepLibre_IPCS(VerificationZepLibre_IPCS verif)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity<VerificationZepLibre_IPCS>(verif);
                context.VerificationZepLibre_IPCS.Attach(verif);
                context.Entry(verif).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteVerificationZepLibre_IPCS(VerificationZepLibre_IPCS verif)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity<VerificationZepLibre_IPCS>(verif);
                VerificationZepLibre_IPCS ver = GetVerificationZepLibre_IPCSById(verif.Id);
                context.VerificationZepLibre_IPCS.Remove(ver);
                context.SaveChanges();
            }
        }

        #endregion
    }
}
