﻿using Clic.BLL.IServices;
using Clic.BLL.Services.Commun;
using Clic.DAL.Entities.FicheZep;
using Clic.Data.DbContext;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using PHENIX.BLL.IServices.Commun;
using PHNIX.BLL.IServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Clic.BLL.Services.FicheZep
{
    /// <summary>
    /// Classe contenant des services relatifs à l'import des ZEPs à partir de sources externes (Fichiers Excel)
    /// </summary>
    public class ImportZepService : IImportZepService
    {
        private IZepService _ZepService;
        private List<ZepType> zepTypes;
        private List<ParticulariteType> particulatiteTypes;
        private List<Poste> postes;
        private List<PointRemarquableType> pointRemarquableTypes;
        private List<string> numerosZeps;

        public ImportZepService(IZepService zepService)
        {
            this._ZepService = zepService;
            zepTypes = _ZepService.GetAllZepTypes();
            particulatiteTypes = _ZepService.GetAllParticulariteTypes();
            postes = _ZepService.GetAllPostes();
            pointRemarquableTypes = _ZepService.GetAllPointRemarquableTypes();
            numerosZeps = _ZepService.GetAllZepNumeros();
        }

        /// <summary>
        /// Import toutes les zeps se trouvant dans les onglets du fichier passé en paramètres
        /// </summary>
        /// <param name="path">Chemin absolut vers le fichier à importer</param>
        public string ImportFichesZepsExcel(string path)
        {
            FileInfo file = new FileInfo(path);
            if (zepTypes.Count < 1) throw new Exception("Aucun type zep n'est défini dans la base de données.");
            string result = string.Empty;
            using (AppDbContext context = new AppDbContext())
            {
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    for(int tab=1; tab < package.Workbook.Worksheets.Count; tab++)
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets[tab];
                        if (! IsTabZep(worksheet)) continue;
                        try
                        {
                            ImportFicheZepExcel(worksheet, context);
                        }catch(Exception ex)
                        {
                            var msg = ex.Message;
                            result += msg;
                        }
                    }
                }
            }
            return result;
        }

        public IResult<int> ImportFichesZepsExcel(Stream fileStream)
        {
            if (zepTypes.Count < 1) throw new Exception("Aucun type zep n'est défini dans la base de données.");
            int nbImportes = 0;
            int nbTab =0;
            string result = string.Empty;
            using (AppDbContext context = new AppDbContext())
            {
                using (ExcelPackage package = new ExcelPackage(fileStream))
                {
                    nbTab = package.Workbook.Worksheets.Count;
                    for (int tab = 1; tab <= package.Workbook.Worksheets.Count; tab++)
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets[tab];
                        if (!IsTabZep(worksheet)) continue;
                        try
                        {
                            if (worksheet.Hidden == eWorkSheetHidden.Visible)
                            {
                                ImportFicheZepExcel(worksheet, context);
                                ++nbImportes;
                            }
                        }
                        catch (Exception ex)
                        {
                            var msg = ex.Message;
                            result += "\r \n" + msg;
                        }
                    }
                }
            }
            var r = result;
            var res = new Result<int>()
            {
                Value = nbImportes,
                ErrorMessage = r,
                SuccessMessage = "",
                Status = ResultStatus.Success,
            };

            return res;
        }

        /// <summary>
        /// Import la Zep se trouvant dans l'onglet Excel passé en paramètre
        /// </summary>
        /// <param name="worksheet">Feuille Excel</param>
        /// <param name="context">Contexte Entity</param>
        private void ImportFicheZepExcel(ExcelWorksheet worksheet, AppDbContext context)
        {
            #region Information ZEP
            //numéro
            string numero = worksheet.Cells["E2"].Value?.ToString() as string;
            if (string.IsNullOrWhiteSpace(numero))
            {
                throw new Exception(String.Format("Fiche {0} : Aucun numéro Zep n'est renseigné. ", worksheet.Name));
            }

            Zep zep = _ZepService.GetZepByNumero(numero);
            if (zep == null)
            {
                zep = new Zep()
                {
                    Numero = numero
                };
                context.Zep.Add(zep);
            }

            //type zep
            string typeZep = worksheet.Cells["E7"].Value as string;
            int idZepType = zepTypes.Where(t => t.Nom == typeZep).Select(t => t.Id).FirstOrDefault();

            if (idZepType == 0)
            {
                throw new Exception(String.Format("Fiche {0} : Aucun Type Zep n'est renseigné. ", worksheet.Name));
            }
            zep.ZepTypeId = idZepType;

            //dates applicabilité
            zep.DateDebutApplicabilite = worksheet.Cells["F8"].Value as DateTime?;
            zep.DateFinApplicabilite = worksheet.Cells["I8"].Value as DateTime?;

            //image
            zep.FichierImage = worksheet.Cells["E9"].Value as string;

            //composition de la zep
            var rgComposition = worksheet.Cells["E10:Q10"].Value as object[,];

            //supprimer compositions zep
            if (zep.CompositionGroupeZepList != null)
            {
                zep.CompositionGroupeZepList.ToList().ForEach(t => {
                    t.ZepEnfant = null;
                    t.GroupeZep = null;
                    context.Entry(t).State = EntityState.Deleted;
                });
            }

            for (int i = 0; i < rgComposition.Length; i++)
            {
                if(rgComposition[0, i]!= null)
                {
                    string numZep = rgComposition[0, i].ToString();
                    numZep = "ZEP " + numZep;
                    var zepLiee = _ZepService.GetZepByNumero(numZep);
                    if (zepLiee != null)
                    {
                        CompositionZep composition = new CompositionZep()
                        {
                            GroupeZepId = zep.Id,
                            ZepEnfantId = zepLiee.Id
                        };

                        context.CompositionZep.Add(composition);
                    }
                }
            }

            //Applicable dans conditions
            var rgConditionsApplicables = worksheet.Cells["E12:Q12"].Value as object[,];

            //supprimer 
            if (zep.ParticulariteList != null)
            {
                var toDelete = zep.ParticulariteList.Where(t => t.ParticulariteType.Nom == Constantes.ParticulariteTypes.ApplicableDansCondition).ToList();
                toDelete.ToList().ForEach(t => {
                    t.Zep = null;
                    context.Entry(t).State = EntityState.Deleted;
                });
            }

            InsertParticularite(12, Constantes.ParticulariteTypes.ApplicableDansCondition, zep, worksheet, context);

            //Poste à demander pour la DFV
            string ACposteDFV = worksheet.Cells["E13"].Value as string;
            if (!string.IsNullOrEmpty(ACposteDFV))
            {
                string posteDFV = String.Join(" ", ACposteDFV.Split(" ").Skip(1));
                Poste p = postes.FirstOrDefault(t => t.Nom == posteDFV);
                if (p != null)
                {
                    zep.PosteDemandeDFVId = p.Id;
                }
                else
                {
                    throw new Exception(String.Format("Fiche {0} : le 'poste demandé' renseigné n'existe pas. ", numero));
                }
            }
            else
            {
                throw new Exception(String.Format("Fiche {0} : Aucun poste demandé n'est renseigné. ", numero));
            }

            //supprimer aiguilles zep
            if (zep.AiguilleList != null)
            {
                zep.AiguilleList.ToList().ForEach(t => {
                    t.Zep = null;
                    context.Entry(t).State = EntityState.Deleted;
                });
            }

            //Liste des aiguilles de la ZEP
            var rgAiguilles = worksheet.Cells["E23:Q23"].Value as object[,];
            for (int i = 0; i < rgAiguilles.Length; i++)
            {
                string nomAig = rgAiguilles[0, i] as string;
                if (!string.IsNullOrWhiteSpace(nomAig))
                {
                    Aiguille aiguille = new Aiguille()
                    {
                        Nom = nomAig,
                        ZepId = zep.Id
                    };
                    context.Aiguille.Add(aiguille);
                }
            }

            //Conditions de protection et vérifications des postes
            string[] typeMesures = { Constantes.MesureProtectionTypes.ProtectionEtVerification };
            InsertMesureProtectionParPoste(15, 1, 2, typeMesures, zep, worksheet, postes, context);

            #endregion

            #region Train de travaux
            string ttxAutorise = worksheet.Cells["G25"].Value as string;
            zep.AutorisationTTX = ttxAutorise != null && ttxAutorise.ToLower() == "oui" ? true : false;

            string lamAutorise = worksheet.Cells["I25"].Value as string;
            zep.AutorisationLAM = lamAutorise != null && lamAutorise.ToLower() == "oui" ? true : false;

            //points d'engagements
            InsertPointRemarquableParPoste(27, Constantes.PointRemarquableTypes.PointEngagement, zep, worksheet, postes, context);
            InsertPointRemarquableParPoste(35, Constantes.PointRemarquableTypes.PointDegagement, zep, worksheet, postes, context);
            InsertPointRemarquableParPoste(43, Constantes.PointRemarquableTypes.SignauxIntermediaires, zep, worksheet, postes, context);

            //dispositions particulières
            InsertParticularite(51, Constantes.ParticulariteTypes.DFVResititueeOccupee, zep, worksheet, context);
            InsertParticularite(52, Constantes.ParticulariteTypes.TTXStationne, zep, worksheet, context);
            InsertParticularite(53, Constantes.ParticulariteTypes.AutresDispositionsParticulieresTTX, zep, worksheet, context);

            #endregion

            #region Procédés DFV
            //Mesures à prendre par l'exploitant
            InsertParticularite(67, Constantes.ParticulariteTypes.AiguillesAPositionObligee, zep, worksheet, context);
            InsertParticularite(68, Constantes.ParticulariteTypes.AiguillesADisposerDansPositionPrevue, zep, worksheet, context);
            InsertParticularite(115, Constantes.ParticulariteTypes.DispositionsParticulieresParExploitation, zep, worksheet, context);

            //Mesures de protection par poste procédés DFV
            string[] typeMesuresDFV = {
                        Constantes.MesureProtectionTypes.ItinérairesAdetruirePRCI,
                        Constantes.MesureProtectionTypes.ItinérairesAdetruireEtAMunirDunDA,
                        Constantes.MesureProtectionTypes.OrganeDeCommandeAplacerEtAmunirDeDA,
                        Constantes.MesureProtectionTypes.DialogueDeProtection,
                        Constantes.MesureProtectionTypes.AutorisationADetruire,
                        Constantes.MesureProtectionTypes.AiguilleImmobiliseeDansPositionIndiquee,
                        Constantes.MesureProtectionTypes.JalonDarretAplacerSurLeTerrain
                    };

            InsertMesureProtectionParPoste(70, 7, 2, typeMesuresDFV, zep, worksheet, postes, context);

            string[] typeMesure = {
                 Constantes.MesureProtectionTypes.ConditonParticuliereVerificationLiberation
                    };

            InsertMesureProtectionParPoste(108, 1, 2, typeMesure, zep, worksheet, postes, context);

            //Mesure à prendre par l'équipement
            InsertParticularite(117, Constantes.ParticulariteTypes.OutilDeBouclageParLEquipement, zep, worksheet, context);
            InsertParticularite(118, Constantes.ParticulariteTypes.DispositionsParticulieresParLEquipement, zep, worksheet, context);
            InsertParticularite(119, Constantes.ParticulariteTypes.AutresMesuresEventiellesParLEquipement, zep, worksheet, context);


            #endregion

            #region Procédés et options applicable
            int ligneOption = 56;
            bool? nullValue = null;
            string verifLiberation = worksheet.Cells["E" + ligneOption++].Value as string;
       
            zep.DFVAvecVerificationLiberation = string.IsNullOrEmpty(verifLiberation) ? nullValue : (verifLiberation.ToLower()) == "oui" ? true : false;

            string DFVSansVerificationLiberationDerriereTrainOuvrant = worksheet.Cells["E" + ligneOption++].Value as string;
            zep.DFVSansVerificationLiberationDerriereTrainOuvrant = DFVSansVerificationLiberationDerriereTrainOuvrant == null ? nullValue : DFVSansVerificationLiberationDerriereTrainOuvrant.ToLower() == "oui" ? true : false;

            string DFVSansVerificationLiberationTTxDeclencheur = worksheet.Cells["E" + ligneOption++].Value as string;
            zep.DFVSansVerificationLiberationTTxDeclencheur = DFVSansVerificationLiberationTTxDeclencheur == null ? nullValue : DFVSansVerificationLiberationTTxDeclencheur.ToLower() == "oui" ? true : false;

            string DFVSansVerificationLiberationTTxStationne = worksheet.Cells["E" + ligneOption++].Value as string;
            zep.DFVSansVerificationLiberationTTxStationne = DFVSansVerificationLiberationTTxStationne == null ? nullValue : DFVSansVerificationLiberationTTxStationne.ToLower() == "oui" ? true : false;

            string DFVRestitueeOccupee = worksheet.Cells["E" + ligneOption++].Value as string;
            zep.DFVRestitueeOccupee = DFVRestitueeOccupee == null ? nullValue : DFVRestitueeOccupee.ToLower() == "oui" ? true : false;

            string DFVSansVerificationLiberationTTxStationnePartieG = worksheet.Cells["E" + ligneOption++].Value as string;
            zep.DFVSansVerificationLiberationTTxStationnePartieG = DFVSansVerificationLiberationTTxStationnePartieG == null ? nullValue : DFVSansVerificationLiberationTTxStationnePartieG.ToLower() == "oui" ? true : false;

            string GeqSansTTx = worksheet.Cells["E" + ligneOption++].Value as string;
            zep.GeqSansTTx = GeqSansTTx == null ? nullValue : GeqSansTTx.ToLower() == "oui" ? true : false;

            string GeqAvecTTx = worksheet.Cells["E" + ligneOption++].Value as string;
            zep.GeqAvecTTx = GeqAvecTTx == null ? nullValue : GeqAvecTTx.ToLower() == "oui" ? true : false;

            #endregion
            context.Entry(zep).State = EntityState.Modified;
            context.SaveChanges();

            //incompatibilité zep
            var rgIncompatibilite = worksheet.Cells["E11:Q11"].Value as object[,];

            //supprimer incompatibilites zep
            if (zep.IncompatibiliteZepList != null && zep.IncompatibiliteZepList.Count > 0)
            {
                zep.IncompatibiliteZepList.ToList().ForEach(t =>
                {
                    t.Zep = null;
                    t.ZepIncompatible = null;
                    context.Entry(t).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                });
               
                context.SaveChanges();
            }

            for (int i = 0; i < rgIncompatibilite.Length; i++)
            {
                string numZep = rgIncompatibilite[0, i] as string;
                var zepLiee = _ZepService.GetZepByNumero(numZep);
                if (zepLiee != null)
                {
                    IncompatibiliteZep incompatibilite = new IncompatibiliteZep()
                    {
                        ZepId = zep.Id,
                        ZepIncompatibleId = zepLiee.Id
                    };
                    context.IncompatibiliteZep.Add(incompatibilite);
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Vérifie si l'onglet du fichier Excel est une Zep
        /// </summary>
        /// <param name="ws">Feuille Excel</param>
        /// <returns></returns>
        private bool IsTabZep(ExcelWorksheet ws)
        {
            string tabNom = ws.Cells["C2"].Value as string;
            string tabinfo = ws.Cells["C4"].Value as string;
            string tabTTX = ws.Cells["C25"].Value as string;
            string tabOptions = ws.Cells["C55"].Value as string;
            string tabDFV = ws.Cells["C65"].Value as string;

            if (string.IsNullOrWhiteSpace(tabNom)
                || string.IsNullOrWhiteSpace(tabinfo)
                || string.IsNullOrWhiteSpace(tabTTX)
                || string.IsNullOrWhiteSpace(tabOptions)
                || string.IsNullOrWhiteSpace(tabDFV))
            {
                return false;
            }

            if(tabNom.Trim().ToLower() != "ZEP / Groupement / CCTT :".ToLower()
                || tabinfo.Trim().ToLower() != "INFOS".ToLower()
                || tabTTX.Trim().ToLower() != "Train-Travaux".ToLower()
                || tabOptions.Trim().ToLower() != "Procédés et options applicables".ToLower()
                || tabDFV.Trim().ToLower() != "Procédés DFV".ToLower()
                )
            {
                return false;
            }

            return true;
        }

        private void InsertParticularite(int ligneParticularite, string ParticulariteType, Zep zep, ExcelWorksheet worksheet, AppDbContext context)
        {
            List<ParticulariteType> particularitesTypes = _ZepService.GetAllParticulariteTypes();
            int ligne = ligneParticularite;
            var rgParticularites = worksheet.Cells["E" + ligne + ":" + "Q" + ligne].Value as object[,];
            ParticulariteType particulariteType = particularitesTypes.FirstOrDefault(t => t.Nom == ParticulariteType);
            if (zep.ParticulariteList != null && zep.ParticulariteList.Count > 0)
            {
                var PToDelete = zep.ParticulariteList.Where(t => t.ParticulariteType != null && t.ParticulariteType.Nom == ParticulariteType).ToList();
                PToDelete.ForEach(t => {
                    t.Zep = null;
                    context.Entry(t).State = EntityState.Deleted;
                });
            }

            //récupérer valeurs
            var rgvaleurs = worksheet.Cells["E" + ligne + ":" + "Q" + ligne].Value as object[,];
            for (int j = 0; j < rgvaleurs.Length; j++)
            {
                string valeur = rgvaleurs[0, j] as string;
                if (!string.IsNullOrWhiteSpace(valeur))
                {
                    Particularite particul = new Particularite()
                    {
                        ParticulariteTypeId = particulariteType.Id,
                        ZepId = zep.Id,
                        Valeur = valeur,
                    };
                    context.Particularite.Add(particul);
                }
            }
        }

        private void InsertMesureProtectionParPoste(int lignePostes, int nbLignes, int decalage, string[] MesureProtectionTypes, Zep zep, ExcelWorksheet worksheet, List<Poste> postes, AppDbContext context)
        {
            List<MesureProtectionType> mesureProtectionTypes = _ZepService.GetAllMesureProtectionTypes();
            
            int ligneCP = lignePostes;
            var rgPostes = worksheet.Cells["E" + ligneCP + ":" + "Q" + ligneCP].Value as object[,];
            
            int ligne = 0;
            for (int i = 0; i < rgPostes.Length; i++)
            {
                string strPoste = rgPostes[0, i] as string;
                Poste poste = null;
                //int ligne = ligneCP + i + 3;
                if (!String.IsNullOrWhiteSpace(strPoste) && strPoste.Split(" ").Length > 0)
                {
                   // string nomPoste = "Poste " + string.Join(" ", strPoste.Split(" ").Skip(1));
                    poste = postes.FirstOrDefault(t => t.Nom == strPoste);
                }

                if (poste != null)
                {
                    for(int l = 0; l < nbLignes; l++)
                    {
                        MesureProtectionType mesureType = mesureProtectionTypes.FirstOrDefault(t => t.Nom == MesureProtectionTypes[l]);
                        if (mesureType == null)
                            throw new Exception("Type mesure de protection n'existe pas dans la BDD.");

                        if (zep.MesureProtection_ZepList != null && zep.MesureProtection_ZepList.Count > 0)
                        {
                            var MPToDelete = zep.MesureProtection_ZepList.Where(t => t.Id>0 && t.MesureProtectionType != null && t.MesureProtectionType.Nom == MesureProtectionTypes[l]).ToList();
                            MPToDelete.ForEach(t => t.Zep = null);
                            foreach(var elem in MPToDelete)
                            {
                                elem.Zep = null;
                                context.Entry(elem).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                            }
                        }
                        int positionPoste = i;

                        ligne = lignePostes + decalage  + (positionPoste * nbLignes)  + l ;
                        //récupérer les mesures à prendre
                        var rgMesuresAPrendre = worksheet.Cells["E" + ligne + ":" + "Q" + ligne].Value as object[,];
                        for (int j = 0; j < rgMesuresAPrendre.Length; j++)
                        {
                            string mesure = rgMesuresAPrendre[0, j] as string;
                            if (!string.IsNullOrWhiteSpace(mesure))
                            {
                                MesureProtection_Zep mp = new MesureProtection_Zep()
                                {
                                    MesureProtectionTypeId = mesureType.Id,
                                    PosteId = poste.Id,
                                    ZepId = zep.Id,
                                    Valeur = mesure
                                };
                                context.MesureProtection_Zep.Add(mp);
                            }
                        }
                    }
                }
            }
        }
        
        private void InsertPointRemarquableParPoste(int lignePostes,string PRType, Zep zep, ExcelWorksheet worksheet, List<Poste> postes, AppDbContext context)
        {
            List<PointRemarquableType> pointRemarquableTypes = _ZepService.GetAllPointRemarquableTypes();

            //points d'engagements
            int lignePE = lignePostes;
            var rgPostes = worksheet.Cells["E" + lignePE + ":" + "M" + lignePE].Value as object[,];
            PointRemarquableType engagementType = pointRemarquableTypes.FirstOrDefault(t => t.Nom == PRType);
            if (engagementType == null) throw new Exception("Le type point remarquable n'existe pas dans la BDD.");

            if (zep.PointRemarquable_ZepList != null && zep.PointRemarquable_ZepList.Count > 0)
            {
                var PRToDelete = zep.PointRemarquable_ZepList.Where(t => t.PointRemarquableType !=null && t.PointRemarquableType.Nom == PRType && (t.ManoeuvreList == null || t.ManoeuvreList.Count <1)).ToList();

                PRToDelete.ForEach(t => {
                    t.Zep = null;
                    context.Entry(t).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                });
            }

            for (int i = 0; i < rgPostes.Length; i++)
            {
                string strPoste = rgPostes[0, i] as string;
                Poste poste = null;
                int ligne = lignePE + i + 2;
                if (!String.IsNullOrWhiteSpace(strPoste) && strPoste.Split(" ").Length > 0)
                {
                    //string nomPoste = "Poste " + string.Join(" ", strPoste.Split(" ").Skip(1));
                    poste = postes.FirstOrDefault(t => t.Nom == strPoste);
                }

                if (poste != null)
                {
                    //récupérer les mesures à prendre
                    var rgPointRemarquables = worksheet.Cells["E" + ligne + ":" + "Q" + ligne].Value as object[,];
                    for (int j = 0; j < rgPointRemarquables.Length; j++)
                    {
                        string pr = rgPointRemarquables[0, j] as string;
                        if (!string.IsNullOrWhiteSpace(pr))
                        {
                            PointRemarquable_Zep prZep = new PointRemarquable_Zep()
                            {
                                PointRemarquableTypeId = engagementType.Id,
                                PosteConcerneId = poste.Id,
                                ZepId = zep.Id,
                                Valeur = pr
                            };
                            context.PointRemarquable_Zep.Add(prZep);
                        }
                    }
                }
            }
        }

      
    }
}
