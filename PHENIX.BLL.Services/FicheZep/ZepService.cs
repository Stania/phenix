﻿using Clic.DAL.Entities.FicheZep;
using Clic.Data.DbContext;
using Microsoft.EntityFrameworkCore;
using Clic.BLL.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Persistance.ExtentionMethods;
using Clic.BLL.Services.Commun;

namespace CLic.BLL.Services.FicheZep
{
    public class ZepService : IZepService
    {

        public Zep GetZepByNumero(string numero)
        {
            using (AppDbContext context = new AppDbContext())
            {
                return context.Zep
                    .Include(t => t.ZepType)
                    .Include(t => t.CompositionZepEnfantList)
                    .Include(t => t.CompositionGroupeZepList)
                        .ThenInclude(r => r.ZepEnfant)
                    .Include(t => t.IncompatibiliteZepList)
                        .ThenInclude(r => r.ZepIncompatible)
                    .Include(t => t.IncompatibiliteAutresZepList)
                    .Include(t => t.MesureProtection_ZepList)
                        .ThenInclude(v => v.MesureProtectionType)
                    .Include(t => t.MesureProtection_ZepList)
                        .ThenInclude(r => r.Poste)
                    .Include(t => t.PointRemarquable_ZepList)
                        .ThenInclude(v => v.PointRemarquableType)
                    .Include(t => t.PointRemarquable_ZepList)
                       .ThenInclude(r => r.PosteConcerne)
                    .Include(t => t.PointRemarquable_ZepList)
                        .ThenInclude(t => t.ManoeuvreList)
                        .ThenInclude(t => t.ManoeuvreType)
                    .Include(t => t.ParticulariteList)
                        .ThenInclude(r => r.ParticulariteType)
                    .Include(t => t.AiguilleList)
                        .ThenInclude(r => r.Aiguille_DFVList)
                    .Include(t => t.DFVList)
                    .Include(t => t.VerificationList)
                        .ThenInclude(r => r.VerificationType)
                    .Include(t => t.PosteDemandeDFV)
                    .FirstOrDefault(t => t.Numero.Equals(numero))
                    ;
            }
        }

        public Zep GetZepById(int Id)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateId<Zep>(Id);
                var zep = context.Zep
                    .Include(t => t.ZepType)
                    .Include(t => t.CompositionZepEnfantList)
                    .Include(t => t.CompositionGroupeZepList)
                        .ThenInclude(r => r.ZepEnfant)
                    .Include(t => t.IncompatibiliteZepList)
                        .ThenInclude(r => r.ZepIncompatible)
                    .Include(t => t.IncompatibiliteAutresZepList)
                    .Include(t => t.MesureProtection_ZepList)
                        .ThenInclude(v => v.MesureProtectionType)
                    .Include(t => t.MesureProtection_ZepList)
                        .ThenInclude(r => r.Poste)
                    .Include(t => t.PointRemarquable_ZepList)
                        .ThenInclude(v => v.PointRemarquableType)
                    .Include(t => t.PointRemarquable_ZepList)
                       .ThenInclude(r => r.PosteConcerne)
                    .Include(t => t.PointRemarquable_ZepList)
                        .ThenInclude(t => t.ManoeuvreList)
                        .ThenInclude(t => t.ManoeuvreType)
                    .Include(t => t.ParticulariteList)
                        .ThenInclude(r => r.ParticulariteType)
                    .Include(t => t.AiguilleList)
                        .ThenInclude(r => r.Aiguille_DFVList)
                    .Include(t => t.DFVList)
                    .Include(t => t.VerificationList)
                        .ThenInclude(r => r.VerificationType)
                    .Include(t => t.PosteDemandeDFV)
                    .Where(a => a.Id == Id)
                   .FirstOrDefault();
                return zep;
            }
        }

        public List<Zep> GetAllZeps()
        {
            using (AppDbContext context = new AppDbContext())
            {
                List<Zep> zeps = context.Zep
                            .Include(a => a.ZepType)
                            //.Include(t => t.CompositionZepEnfantList)
                            //    .ThenInclude(t => t.ZepEnfant)
                            //.Include(t => t.CompositionGroupeZepList)
                            //    .ThenInclude(t => t.ZepEnfant)
                            //.Include(t => t.IncompatibiliteZepList)
                            //.ThenInclude(t => t.ZepIncompatible)
                            //    .Include(t => t.IncompatibiliteAutresZepList)
                            //.ThenInclude(t => t.ZepIncompatible)
                            //.Include(t => t.ParticulariteList)
                            //    .ThenInclude(t => t.ParticulariteType)
                            //.Include(t => t.MesureProtection_ZepList)
                            //    .ThenInclude(t => t.MesureProtectionType)
                            //.Include(t => t.PointRemarquable_ZepList)
                            //.ThenInclude(t => t.PointRemarquableType)
                            .Include(t => t.PosteDemandeDFV)

                            .ToList();
                foreach (var item in zeps)
                {
                    if (item.PosteDemandeDFV != null)
                    {
                        item.PosteDemandeDFV.DFVList = null;
                        item.PosteDemandeDFV.ZepList = null;
                    }

                    item.ZepType.ZepList = null;
                }
                return zeps;
            }
        }

        public List<ZepType> GetAllZepTypes()
        {
            using (AppDbContext context = new AppDbContext())
            {
                var listetypesZep = context.ZepType.ToList();
                return listetypesZep;
            }
        }

        public List<string> GetAllZepNumeros()
        {
            List<string> numeros = new List<string>();
            using (AppDbContext context = new AppDbContext())
            {
                numeros = context.Zep.Select(t => t.Numero).ToList();
            }
            return numeros;
        }



        public List<ParticulariteType> GetAllParticulariteTypes()
        {
            using (AppDbContext context = new AppDbContext())
            {
                return context.ParticulariteType.ToList();
            }
        }

        public List<Poste> GetAllPostes()
        {
            using (AppDbContext context = new AppDbContext())
            {
                var postes = context.Poste
                    .Include(t => t.Secteur)
                    .ToList();

                postes.ForEach(t => t.Secteur.PosteList = null);
                return postes;
            }
        }

        public List<MesureProtectionType> GetAllMesureProtectionTypes()
        {
            using (AppDbContext context = new AppDbContext())
            {
                var mpt = context.MesureProtectionType
                    .ToList();
                return mpt;
            }
        }



        public List<PointRemarquableType> GetAllPointRemarquableTypes()
        {
            using (AppDbContext context = new AppDbContext())
            {
                var prt = context.PointRemarquableType.ToList();
                return prt;
            }
        }

        public List<Zep> GetNotDrawedZepList()
        {
            using (AppDbContext context = new AppDbContext())
            {
                var listeZep = context.Zep
                    .Where(t => (t.ObjetGraphiqueList == null || t.ObjetGraphiqueList.Count == 0)
                        && (t.CompositionGroupeZepList == null || t.CompositionGroupeZepList.Count == 0))
                        .OrderBy(a => a.Numero)// celles qui ne sont pas parentes d'un groupe
                    .ToList();
                return listeZep;
            }
        }

        public List<Zep> GetDrawedZepList()
        {
            using (AppDbContext context = new AppDbContext())
            {
                DateTime dateNow = DateTime.Now;
                var listeZep = context.Zep
                    .Include(a => a.ObjetGraphiqueList)
                    .Where(t => t.ObjetGraphiqueList.Count > 0
                        && (t.CompositionGroupeZepList == null || t.CompositionGroupeZepList.Count == 0)) // celles qui ne sont pas parentes d'un groupe
                    .ToList();
                return listeZep;
            }
        }
        public List<Zep> GetEtatOperationnel()
        {
            using (AppDbContext context = new AppDbContext())
            {
                DateTime dateNow = DateTime.Now;
                var listZepTemp = context.Zep
                    .Where(t => (t.ObjetGraphiqueList.Count > 0
                                || (t.CompositionGroupeZepList.Count > 0
                                    && t.CompositionGroupeZepList.Any(c => c.ZepEnfant.ObjetGraphiqueList.Count > 0)))
                                    )
                    .Include(a => a.ObjetGraphiqueList)
                    .Include(a => a.CompositionGroupeZepList)
                        .ThenInclude(a => a.ZepEnfant.ObjetGraphiqueList)
                    .Include(a => a.DFVList)
                        .ThenInclude(b => b.StatusZep)
                    .Include(a => a.DFVList)
                        .ThenInclude(b => b.OccupationList)
                    .Include(a => a.DFVList)
                        .ThenInclude(b => b.ManoeuvreList)
                        .ThenInclude(c => c.ManoeuvreType)
                    .Select(b => new
                    {
                        b,
                        DFVList = b.DFVList.Where(t =>
                        (dateNow >= t.DateDebutApplicabilite && dateNow < t.DateFinApplicabilite)
                        && (t.StatusZep.Statut == Constantes.ZepStatuts.EnAccord || t.StatusZep.Statut == Constantes.ZepStatuts.Accordee))
                    })
                    .ToList();
                foreach (var x in listZepTemp)
                {
                    x.b.DFVList = x.DFVList.ToList();
                }
                return listZepTemp.Select(x => x.b).ToList();

                //var listdfv = context.DFV
                //    .Include(a=>a.StatusZep)
                //    .Include(a=>a.Zep.ObjetGraphiqueList)
                //    .Include(a => a.Zep.CompositionGroupeZepList)
                //        .ThenInclude(a => a.ZepEnfant.ObjetGraphiqueList)
                //    .Where(t => (dateNow >= t.DateDebutApplicabilite && dateNow < t.DateFinApplicabilite)
                //            && (t.Zep.ObjetGraphiqueList.Count > 0 
                //                || (t.Zep.CompositionGroupeZepList.Count > 0    
                //                    && t.Zep.CompositionGroupeZepList.Any(c=>c.ZepEnfant.ObjetGraphiqueList.Count > 0)))) 
                //    .ToList();
            }
        }

        public Zep UpdateObjetGraphique(Zep pZep)
        {
            if (pZep == null)
                throw new ArgumentNullException(nameof(pZep));
            else if (pZep.Id < 1)
                throw new ArgumentNullException(nameof(pZep.Id));
            else if (!pZep.ObjetGraphiqueList.Any())
                throw new ArgumentNullException(nameof(pZep.ObjetGraphiqueList));
            using (AppDbContext context = new AppDbContext())
            {
                Zep zep = context.Zep
                    .Include(a => a.ObjetGraphiqueList)
                    .Where(a => a.Id == pZep.Id)
                    .FirstOrDefault();
                if (zep == null) throw new Exception("Impossible de trouver la zep séléctionnée !");
                foreach (ObjetGraphique obj in zep.ObjetGraphiqueList)
                {
                    context.ObjetGraphique.Remove(obj);
                }
                foreach (ObjetGraphique obj in pZep.ObjetGraphiqueList)
                {
                    obj.ZepId = zep.Id;
                    context.ObjetGraphique.Add(obj);
                }
                context.SaveChanges();
                return zep;
            }

        }

        public void AddRange(List<Zep> pListe)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.Zep.AddRange(pListe);
                context.SaveChanges();
            }
        }
        public void AddGroupRange(List<CompositionZep> pListe)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.CompositionZep.AddRange(pListe);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Retourne une Zep au hazard ou Null si aucune zep n'a été créée.
        /// </summary>
        /// <returns></returns>
        public Zep GetRandomZep()
        {
            using (AppDbContext context = new AppDbContext())
            {
                int nbZeps = context.Zep.Count();
                Random rnd = new Random();
                var id = rnd.Next(1, nbZeps);
                var r = GetZepById(id);
                return r;
            }
        }

        public Poste GetRandomPoste()
        {
            using (AppDbContext context = new AppDbContext())
            {
                List<int> ids = context.Poste.Select(t => t.Id).ToList();
                Random rnd = new Random();
                var index = rnd.Next(1, ids.Count);
                var r = GetPosteById(ids[index]);
                return r;
            }
        }

        public Poste GetPosteById(int Id)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateId<Poste>(Id);
                Poste poste = context.Poste.FirstOrDefault(t => t.Id == Id);
                return poste;
            }
        }

        public ManoeuvreType GetManoeuvreTypeByName(string nom)
        {
            using (AppDbContext context = new AppDbContext())
            {
                return context.ManoeuvreType.FirstOrDefault(t => t.Nom == nom);
            }
        }



        public List<StatutZep> GetAllStatutZep()
        {
            using (AppDbContext context = new AppDbContext())
            {
                return context.StatutZep.ToList();
            }
        }

        public StatutZep GetStatutZepByName(string nom)
        {
            using (AppDbContext context = new AppDbContext())
            {
                if (context.StatutZep.Any(t => t.Statut == nom))
                {
                    return context.StatutZep.FirstOrDefault(t => t.Statut == nom);
                }
                else
                {
                    throw new ArgumentException("Le statut " + nom + " n'a pas été créé.");
                }
            }
        }

        public Poste GetPosteByName(string pPosteName)
        {
            using (AppDbContext context = new AppDbContext())
            {
                var poste = context.Poste.FirstOrDefault(t => t.Nom == pPosteName);
                if (poste == null)
                {
                    throw new ArgumentException("Le poste " + pPosteName + " n'a pas été créé.");
                }
                return poste;
            }
        }

        public List<Zep> GetZepsAutorises(string nomPoste,  string profil = null)
        {
            using (AppDbContext context = new AppDbContext())
            {
                Poste poste = GetPosteByName(nomPoste);
                List<Zep> zeps = new List<Zep>();
                if(profil == Constantes.Profils.PoleTravaux)
                {
                    zeps = context.Zep.ToList();
                }
                else
                {
                    zeps = context.Zep.Where(t => t.PosteDemandeDFVId == poste.Id).ToList();
                }
                return zeps;
            }
        }

        public List<Poste> GetAllPostesByIdZep(int idZep)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateId<Zep>(idZep);
                var poste = context.MesureProtection_Zep.Where(x => x.ZepId == idZep).Select(x => x.Poste).Distinct().ToList();
                return poste;
            }
        }

        public List<Poste> GetAllPostesConditionDeProtectionIdZep(int idZep)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateId<Zep>(idZep);
                var poste = context.MesureProtection_Zep.Where(x => x.ZepId == idZep && x.MesureProtectionType.Nom == Constantes.MesureProtectionTypes.ProtectionEtVerification).Select(x => x.Poste).Distinct().ToList();
                return poste;
            }
        }
    }
}
