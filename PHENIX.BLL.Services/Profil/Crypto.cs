﻿using System;
using System.Security.Cryptography;
using System.Text;
using BCrypt.Net;

namespace Clic.BLL.Services.Profil
{
    public static class Crypto
    {
        private const int SaltSize = 256 / 8;
        private const int HashByteSize = 20;

        public static string HashPassword(string password, int iterations) 
        {
            var saltBytes = GenerateSalt();
            var hashBytes = HashWithPbkdf2(password, saltBytes, iterations);
            var combinedBytes = new byte[hashBytes.Length + saltBytes.Length];
            Buffer.BlockCopy(saltBytes, 0, combinedBytes, 0, saltBytes.Length);
            Buffer.BlockCopy(hashBytes, 0, combinedBytes, saltBytes.Length, hashBytes.Length);
            var saltAndHash = Convert.ToBase64String(combinedBytes);
            return saltAndHash;
        }

        public static byte[] HashWithPbkdf2(string password, byte[] saltBytes, int iterations)
        {
            var pbkdf2 = new Rfc2898DeriveBytes(password, saltBytes, iterations, HashAlgorithmName.SHA512);
            return pbkdf2.GetBytes(HashByteSize);
        }

        public static byte[] GenerateSalt()
        {
            var salt = new byte[SaltSize];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(salt);
            }
            return salt;
        }

        public static bool VerifyHashedPassword(string hashedPassword, string password, int iterations) 
        {
            if (hashedPassword == null)
            {
                throw new ArgumentNullException("hashedPassword");
            }
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            var hashedPasswordBytes = Convert.FromBase64String(hashedPassword);
     
            if (hashedPasswordBytes.Length != (SaltSize + HashByteSize))
            {
                return false;
            }

            var salt = new byte[SaltSize];
            Buffer.BlockCopy(hashedPasswordBytes, 0, salt, 0, SaltSize);
            var storedSubkey = new byte[HashByteSize];
            Buffer.BlockCopy(hashedPasswordBytes, SaltSize, storedSubkey, 0, HashByteSize);

            var generatedSubkey = HashWithPbkdf2(password, salt, iterations);
            return ByteArraysEqual(storedSubkey, generatedSubkey);
        }

        /// <summary>
        /// Compares two byte arrays without short circuiting.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private static bool ByteArraysEqual(byte[] a, byte[] b)
        {
            if (ReferenceEquals(a, b))
            {
                return true;
            }
            if (a == null || b == null || a.Length != b.Length)
            {
                return false;
            }
            bool areSame = true;
            for (int i = 0; i < a.Length; i++)
            {
                areSame &= (a[i] == b[i]);
            }
            return areSame;
        }
    }
}
