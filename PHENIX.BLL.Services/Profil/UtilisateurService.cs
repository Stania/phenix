﻿using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profil;
using Clic.Data.DbContext;
using Microsoft.EntityFrameworkCore;
using Clic.BLL.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Clic.BLL.IServices.Profil;
using Clic.DAL.Persistance.ExtentionMethods;

namespace Clic.BLL.Services.Profil
{
    public class UtilisateurService : PasswordHasher, IUtilisateurService
    {
        
        public Utilisateur Authenticate(string pLogin, string pPassword)
        {
            Utilisateur u = this.GetUser(pLogin);
            PasswordVerificationResult result = this.VerifyHashedPassword(u.HasshedPassword, pPassword);
            if (result == PasswordVerificationResult.Failed) return null;
            else return u;
        }

        public Utilisateur CreateUser(Utilisateur pUtilisateur, string pPassword)
        {
            using (AppDbContext context = new AppDbContext())
            {
                if (context.Utilisateur.Any(a => a.Login == pUtilisateur.Login))
                    throw new Exception("Un utilisateur avec la même login email éxiste déjà.");
                pUtilisateur.HasshedPassword = this.HashPassword(pPassword);
                Utilisateur u = context.Utilisateur.Add(pUtilisateur).Entity;
                context.SaveChanges();
                return u;
            }

        }

        public Utilisateur GetUser(string pUsername)
        {
            using (AppDbContext context = new AppDbContext())
            {
                Utilisateur u = context.Utilisateur.Where(x => x.Login == pUsername)
                            .FirstOrDefault();
                if (u == null) throw new Exception("L'utilisateur " + pUsername + " n'éxiste pas.");
                return u;
            }
        }

        public ICollection<UserProfil> GetUserProfils(string pUserName)
        {
            using (AppDbContext context = new AppDbContext())
            {
                return context.UserProfil
                    .Include(a=>a.Profil)
                    .Include(a=>a.Secteur)
                    .Include(a=>a.Poste)
                    .Where(a => a.Utilisateur.Login == pUserName).ToList();
            }
        }

        public bool IsInRole(UserProfil pUserProfil)
        {
            using (AppDbContext context = new AppDbContext())
            {
                context.ValidateEntity<UserProfil>(pUserProfil,false,true);
                int profilId = pUserProfil.Profil != null ? pUserProfil.Profil.Id : pUserProfil.ProfilId;
                int sercteurId = pUserProfil.Secteur != null ? pUserProfil.Secteur.Id : pUserProfil.SecteurId;
                int? posteId = pUserProfil.Poste != null ? pUserProfil.Poste.Id : pUserProfil.PosteId;
                int userId = pUserProfil.Utilisateur != null ? pUserProfil.Utilisateur.Id : pUserProfil.UtilisateurId;
                return context.UserProfil.Any(a=>a.PosteId == posteId && 
                                                a.ProfilId == profilId && 
                                                a.SecteurId == sercteurId && 
                                                a.UtilisateurId == userId);
            }
        }

        public Utilisateur UpdateUser(Utilisateur pUtilisateur)
        {
            using (AppDbContext context = new AppDbContext())
            {
                Utilisateur u = context.Utilisateur.Where(w => w.Id == pUtilisateur.Id).FirstOrDefault();
                if (u == null) throw new Exception("L'utilisateur n'éxiste pas.");
                u.Login = pUtilisateur.Login;
                u.Nom = pUtilisateur.Nom;
                context.SaveChanges();
                return u;
            }
        }

        public Utilisateur UpdateUser(Utilisateur pUtilisateur, string pPassword)
        {
            using (AppDbContext context = new AppDbContext())
            {
                Utilisateur u = context.Utilisateur.Where(w => w.Id == pUtilisateur.Id).FirstOrDefault();
                if (u == null) throw new Exception("L'utilisateur n'éxiste pas.");
                u.Login = pUtilisateur.Login;
                u.Nom = pUtilisateur.Nom;
                context.SaveChanges();
                return u;
            }
        }

        public Clic.DAL.Entities.Profil.Profil GetProfilByName(String pProfilName)
        {
            using (AppDbContext context = new AppDbContext())
            {
                if (!context.Profil.Any(a => a.Nom == pProfilName))
                    throw new Exception("Ce profil n'éxiste pas !");
                var profil = context.Profil.Where(a => a.Nom == pProfilName).FirstOrDefault();
                return profil;
            }
        }
    }
}
