﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.Base
{
    public class EntityBase
    {
        public int Id { get; set; }

        public void ValidateData()
        {
            OnValidateData();
        }

        protected virtual void OnValidateData()
        {
           
        }
    }
}
