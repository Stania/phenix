﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheZep;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    public class Aiguille_DFV : EntityBase
    {
        public int AiguilleId { get; set; }
        public Aiguille Aiguille { get; set; }

        public int DFVId { get; set; }
        public DFV DFV { get; set; }

        public string Commentaire { get; set; }
    }
}
