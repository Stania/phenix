﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheZep;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    public class AutreDepeche : EntityBase
    {

        public int AutreDepecheTypeId { get; set; }
        public AutreDepecheType AutreDepecheType { get; set; }
        public int DepecheId { get; set; }
        public Depeche Depeche { get; set; }
        public Nullable<int> ManoeuvreId { get; set; }
        public Manoeuvre Manoeuvre { get; set; }
        public Nullable<int> DFVId { get; set; }
        public DFV DFV { get; set; }
        public string AncienneValeur { get; set; }
        public string NouvelleValeur { get; set; }
    }
}
