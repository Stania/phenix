﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheZep;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    public class AutreDepecheType : EntityBase
    {
        public string Nom { get; set; }
        public string Propriete { get; set; }
        public ICollection<AutreDepeche> AutreDepecheList { get; set; }
    }
}
