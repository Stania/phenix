﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profil;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    /// <summary>
    /// Classe représentant la Demande de Fermeture de Voie
    /// </summary>
    public class DFV : EntityBase
    {
        /// <summary>
        /// Numéro de la DFV
        /// </summary>
        public string Numero { get; set; }
        /// <summary>
        /// En demande, En accord, Accordée, Restituée
        /// </summary>
        public StatutZep StatusZep { get; set; }
        public int StatusZepId { get; set; }

        public Nullable<DateTime> DateCreation { get; set; }
        public string NumeroTelephone { get; set; }
        public string RPTX { get; set; }
        public bool AvecVerificationDeLiberation { get; set; }
        public DateTime? DateDebutApplicabilite { get; set; }
        public DateTime? DateFinApplicabilite { get; set; }
        public Nullable<Boolean> IsPrevisonelle { get; set; }
        public Nullable<Boolean> DerriereTrainOuvrant { get; set; }
        public string DerriereTrainOuvrantNumero { get; set; }

        public Nullable<Boolean> DerriereTTXDeclencheur { get; set; }
        public string DerriereTTXDeclencheurNumero { get; set; }

        public Nullable<bool> SansTTX { get; set; }

        #region Application
        public string OperationNumero { get; set; }
        public string AHTNumero { get; set; }
        public string Avis { get; set; }
        public string AvisNumero { get; set; }
        public string ContratTravauxNumero { get; set; }
        public string CCTTNumero { get; set; }
        public Nullable<DateTime> DateApplicabilite { get; set; }
        public Nullable<Boolean> RecuParDepeche { get; set; }
        public string DepecheReceptionNumero { get; set; }
        public Nullable<bool> SignatureRPTX { get; set; }
        #endregion

        public int ZepId { get; set; }
        /// <summary>
        /// Zone de protection élémentaire sur laquelle la demande de fermeture s'applique
        /// </summary>
        public Zep Zep { get; set; }
        public int ResponsableDFVId { get; set; }
        /// <summary>
        /// Agent de circulation responsable de la DFV
        /// </summary>
        public Poste ResponsableDFV { get; set; }

        #region Verification Zep Libre
        public Nullable<bool> ObservationControles { get; set; }
        public Nullable<bool> ObservationDirecteVoie { get; set; }
        public Nullable<bool> ObservationParAutreMoyen { get; set; }
        public string ObservationParAutreMoyenDescription { get; set; }
        public Nullable<bool> ObservationParEchangeDepeches { get; set; }
        //public int VerificationZepLibre_IPCSId { get; set; }
        public VerificationZepLibre_IPCS VerificationZepLibre_IPCS { get; set; }
        //public int VerificationZepLibre_LigneId { get; set; }
        public VerificationZepLibre_Ligne VerificationZepLibre_Ligne { get; set; }
        #endregion

        #region Mesures de protection
        /// <summary>
        /// Mesures de protection prises par l'AC
        /// </summary>
        public Nullable<bool> MesureProtectionPiseParAC { get; set; }
        public Nullable<DateTime> DateMesureProtectionPiseParAC { get; set; }
        /// <summary>
        /// Levé de mesure par l'AC
        /// </summary>
        public Nullable<bool> LeveeMesuresParAC { get; set; }
        public Nullable<DateTime> DateLeveeMesuresParAC { get; set; }

        #endregion

        #region Bloc Accord de la Zep
        public Nullable<DateTime> DateDebutAccord { get; set; }
        public Nullable<DateTime> DateFinAccord { get; set; }
        public Nullable<bool> AccordeParEcrit { get; set; }
        public string TransmisParDepecheNumero { get; set; }

        #endregion

        #region Bloc Restitution de la DFV
        public DateTime? DateRestitution { get; set; }
        public Boolean? IsResitutionParDepeche { get; set; }
        public int? NumeroDepecheRecuRestitution { get; set; }
        #endregion

        public ICollection<Manoeuvre> ManoeuvreList { get; set; }
        public ICollection<Occupation> OccupationList { get; set; }
        public ICollection<MesureProtectionPrise> MesureProtectionPriseList { get; set; }
        public ICollection<Aiguille_DFV> Aiguille_DFVList { get; set; }
        public ICollection<AutreDepeche> AutreDepecheList { get; set; }
        public ICollection<MaterielRoulant> MaterielRoulantList { get; set; }

        #region calculé
        public string Nuit { get
            {
                string res = "";
                if (this.DateDebutApplicabilite == null) return res;
                var j1 = this.DateDebutApplicabilite.Value;
                if (j1 != null)
                {
                    string mois = j1.Month.ToString();
                    string du = j1.ToString("dd/MM");
                    string au = j1.AddDays(1).ToString("dd/MM");

                    string annee = j1.Year.ToString();
                    return du + " au " + au + "  " + annee;
                }
                return res;
            }
        }

        public string HeureTheoriqueDebut
        {
            get
            {
                string res = "";
                if (this.DateDebutApplicabilite == null) return res;
                var j1 = this.DateDebutApplicabilite.Value;
                if (j1 != null)
                {
                    return j1.ToShortTimeString();
                }
                return "";
            }
        }
        public string HeureTheoriqueFin
        {
            get
            {
                string res = "";
                if (this.DateFinApplicabilite == null) return res;
                var j1 = this.DateFinApplicabilite.Value;
                if (j1 != null)
                {
                    return j1.ToShortTimeString();
                }
                return "";
            }
        }
        #endregion

        protected override void OnValidateData()
        {
            if (this.DateDebutApplicabilite == null) throw new ArgumentException("Argument null: " + nameof(this.DateDebutApplicabilite));
            if (this.DateFinApplicabilite == null) throw new ArgumentException("Argument null: " + nameof(this.DateFinApplicabilite));
        }
    }
}
