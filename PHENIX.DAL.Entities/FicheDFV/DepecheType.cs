﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    public class DepecheType : EntityBase
    {
        public string Nom { get; set; }
        public ICollection<Depeche> DepecheList { get; set; }
    }
}
