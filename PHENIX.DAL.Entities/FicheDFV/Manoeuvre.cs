﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheZep;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    /// <summary>
    /// Mouvement (engagement, dégagement ..) effectué par un train de travaux
    /// </summary>
    public class Manoeuvre : EntityBase
    {
        /// <summary>
        /// Numéro du train de travaux qui va effectuer la manoeuvre
        /// </summary>
        public string TTXNumero { get; set; }
        /// <summary>
        /// Type du train de travaux
        /// </summary>
        public string TTXNature { get; set; }
        /// <summary>
        /// Voie ou se situe le TTX avant dégamement
        /// </summary>
        public string TTXVoie { get; set; }
        /// <summary>
        /// Point ou se situe le TTX avant dégagement
        /// </summary>
        public string TTXPointInitial { get; set; }
        /// <summary>
        /// Direction du train après le dégamenent
        /// </summary>
        public string Direction { get; set; }

        public Nullable<bool> Verbale { get; set; }
        public Nullable<bool> ParEcrit { get; set; }
        public Nullable<bool> ParDepeche { get; set; }
        public Nullable<bool> FranchissementEffectue { get; set; }

        public Nullable<bool> SignatureRptx { get; set; }
        public Nullable<DateTime> DateDemandeDegagement { get; set; }
        public string PointTTX { get; set; }

        public int ManoeuvreTypeId { get; set; }

        public string Commentaire { get; set; }

        /// <summary>
        /// Type de manoeuvre (Engagement, dégagement ou franchissement
        /// </summary>
        public ManoeuvreType ManoeuvreType { get; set; }

        public Nullable<int> PointRemarquable_ZepId { get; set; }
        /// <summary>
        /// Point d'engagement ou de dégagement
        /// </summary>
        public PointRemarquable_Zep PointRemarquable_Zep { get; set; }

        public int DFVId { get; set; }
        public DFV DFV { get; set; }
       
        public ICollection<AutreDepeche> AutreDepecheList { get; set; }
        public ICollection<Manoeuvre_Depeche> Manoeuvre_DepecheList { get; set; }
    }
}
