﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    public class ManoeuvreType : EntityBase
    {
        public string Nom { get; set; }
        public ICollection<Manoeuvre> ManoeuvreList { get; set; }
    }
}
