﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    public class MaterielRoulant : EntityBase
    {
        public string TTXNumero { get; set; }
         public string TTXLieuStationnement { get; set; }
        public int DFVId { get; set; }
         public DFV DFV { get; set; }
    }
}
