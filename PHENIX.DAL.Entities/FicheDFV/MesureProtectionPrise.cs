﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profil;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    public class MesureProtectionPrise : EntityBase
    {
        /// <summary>
        /// J'ai pris les mesures de protection prévues
        /// </summary>
        public Nullable<bool> PriseDeMesure { get; set; }
        /// <summary>
        /// J'ai vérifié 
        /// </summary>
        public Nullable<bool> VerificationLiberationVoieprevue { get; set; }
        public Nullable<DateTime> DateDePrise { get; set; }

        public int PosteDemandeId { get; set; }
        public Poste PosteDemande { get; set; }

        public int DFVId { get; set; }
        public DFV DFV { get; set; }
        public IEnumerable<MesureProtectionPrise_Depeche> MesureProtectionPrise_DepecheList { get; set; }
    }
}
