﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profil;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    public class MesureProtectionPrise_Depeche : EntityBase
    {
        public int MesureProtectionPriseId { get; set; }
        public int DepecheId { get; set; }
        public MesureProtectionPrise MesureProtectionPrise { get; set; }
        public Depeche Depeche { get; set; }
    }
}
