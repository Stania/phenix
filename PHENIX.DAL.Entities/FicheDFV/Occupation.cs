﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    public class Occupation : EntityBase
    {
        public string TTXNumero { get; set; }
        public string TTXNature { get; set; }
        public string TTXPosition { get; set; }
        public string Commentaire { get; set; }
        public int OccupationTypeId { get; set; }
        public OccupationType OccupationType { get; set; }
        public int DFVId { get; set; }
        public DFV DFV { get; set; }
    }
}
