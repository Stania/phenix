﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    public class OccupationType : EntityBase
    {
        public string Nom { get; set; }
        public ICollection<Occupation> OccupationList { get; set; }
    }
}
