﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    public class StatutZep : EntityBase
    {
        public string Statut { get; set; }
        public ICollection<DFV> DFVList { get; set; }
    }
}
