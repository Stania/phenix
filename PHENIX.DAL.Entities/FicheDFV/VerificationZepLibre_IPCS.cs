﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profil;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    public class VerificationZepLibre_IPCS : EntityBase
    {
        public Nullable<DateTime> HeureDemande { get; set; }

        public int PosteDemandeId { get; set; }
        public Poste PosteDemande { get; set; }

        public int DFVId { get; set; }
        public DFV DFV { get; set; }

        public int DepecheId { get; set; }
        public Depeche Depeche { get; set; }
    }
}
