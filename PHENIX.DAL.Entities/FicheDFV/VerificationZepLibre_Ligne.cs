﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profil;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Clic.DAL.Entities.FicheDFV
{
    public class VerificationZepLibre_Ligne : EntityBase
    {
        public string Nom { get; set; }

        public DateTime? HeureArrivee { get; set; }
        public string NumeroTrainRecu { get; set; }

        public string OrigineTrain { get; set; }
        public string DestinationTrain { get; set; }

        public int PosteDemandeId { get; set; }
        public virtual Poste PosteDemande { get; set; }

        public int DFVId { get; set; }
        public virtual DFV DFV { get; set; }

        public Nullable<int> DepecheId { get; set; }
        [ForeignKey(nameof(DepecheId))]
        public virtual Depeche Depeche { get; set; }

        public Nullable<int> Depeche2Id { get; set; }
        [ForeignKey(nameof(Depeche2Id))]
        public virtual Depeche Depeche2 { get; set; }
    }
}
