﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class CompositionZep : EntityBase    
    {
        /// <summary>
        /// Id d'une zep de type zep
        /// </summary>
        public int ZepEnfantId { get; set; }
        [ForeignKey(nameof(ZepEnfantId))]
        public virtual Zep ZepEnfant { get; set; }

        /// <summary>
        /// Id d'une zep de type groupement de zep
        /// </summary>
        public int GroupeZepId { get; set; }
        [ForeignKey(nameof(GroupeZepId))]
        public virtual Zep GroupeZep { get; set; }

    }
}
