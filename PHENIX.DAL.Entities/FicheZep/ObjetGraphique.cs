﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class ObjetGraphique : EntityBase
    {
        public string Coordonnees { get; set; }
        public string Type { get; set; }
        public string Color { get; set; }

        public int ZepId { get; set; }
        public Zep Zep { get; set; }

    }
}
