﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class Particularite : EntityBase
    {
        public string Valeur { get; set; }

        public int ZepId { get; set; }
        public Zep Zep { get; set; }

        public int ParticulariteTypeId { get; set; }
        public ParticulariteType ParticulariteType { get; set; }
       
    }
}
