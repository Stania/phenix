﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class PointRemarquableType : EntityBase
    {
        public string Nom { get; set; }
        public virtual ICollection<PointRemarquable_Zep> PointRemarquable_ZepList { get; set; }
    }
}
