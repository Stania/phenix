﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Entities.Profil;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class PointRemarquable_Zep : EntityBase
    {
        public string Valeur { get; set; }
        public int ZepId { get; set; }
        public Zep Zep { get; set; }

        public int PosteConcerneId { get; set; }
        public Poste PosteConcerne { get; set; }

        public int PointRemarquableTypeId { get; set; }
        public PointRemarquableType PointRemarquableType { get; set; }

        public ICollection<Manoeuvre> ManoeuvreList { get; set; }
    }
}
