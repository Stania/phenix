﻿using Clic.DAL.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class VerificationType : EntityBase
    {
        public string Nom { get; set; }
        public ICollection<Verification> VerificationList { get; set; }
    }
}
