﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Entities.Profil;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Clic.DAL.Entities.FicheZep
{
    public class Zep : EntityBase
    {
        #region Informations
        public string Numero { get; set; }
        /// <summary>
        /// Type de la ZEP (Ligne, Gare ou les deux dans le cas d'un groupement de ZEP)
        /// </summary>
        public int ZepTypeId { get; set; }
        public ZepType ZepType { get; set; }

        public string Indice { get; set; }
        public DateTime? DateDebutApplicabilite { get; set; }
        public DateTime? DateFinApplicabilite { get; set; }
        public string FichierImage { get; set; }
        
        /// <summary>
        /// Poste à demander pour une DFV (Ex. AC du poste PRCI)
        /// </summary>
        public Poste PosteDemandeDFV { get; set; }
        public Nullable<int> PosteDemandeDFVId { get; set; }

        #endregion

        #region Train de travaux
        public Nullable<bool> AutorisationTTX { get; set; }
        public Nullable<bool> AutorisationLAM { get; set; }
        public string TTxStationne { get; set; }
        public string AutresDispositionsParticulieres { get; set; }
        #endregion

        #region Procédés et options applicables
        public Nullable<bool> DFVAvecVerificationLiberation { get; set; }
        public Nullable<bool> DFVSansVerificationLiberationDerriereTrainOuvrant { get; set; }
        public Nullable<bool> DFVSansVerificationLiberationTTxDeclencheur { get; set; }
        public Nullable<bool> DFVSansVerificationLiberationTTxStationne { get; set; }
        public Nullable<bool> DFVRestitueeOccupee { get; set; }
        public Nullable<bool> DFVSansVerificationLiberationTTxStationnePartieG { get; set; }
        public Nullable<bool> GeqSansTTx { get; set; }
        public Nullable<bool> GeqAvecTTx { get; set; }
        #endregion

        [InverseProperty(nameof(CompositionZep.ZepEnfant))]
        public virtual ICollection<CompositionZep> CompositionZepEnfantList { get; set; }

        [InverseProperty(nameof(CompositionZep.GroupeZep))]
        public virtual ICollection<CompositionZep> CompositionGroupeZepList { get; set; }

        [InverseProperty(nameof(IncompatibiliteZep.Zep))]
        public ICollection<IncompatibiliteZep> IncompatibiliteZepList { get; set; }

        [InverseProperty(nameof(IncompatibiliteZep.ZepIncompatible))]
        public ICollection<IncompatibiliteZep> IncompatibiliteAutresZepList { get; set; }
        
        public ICollection<MesureProtection_Zep> MesureProtection_ZepList { get; set; }
        public ICollection<PointRemarquable_Zep> PointRemarquable_ZepList { get; set; }
        public ICollection<Particularite> ParticulariteList { get; set; }
        public ICollection<Aiguille> AiguilleList { get; set; }
        public ICollection<FicheDFV.DFV> DFVList { get; set; }
        public ICollection<Verification> VerificationList { get; set; }
        public ICollection<ObjetGraphique> ObjetGraphiqueList { get; set; }
    }
}
