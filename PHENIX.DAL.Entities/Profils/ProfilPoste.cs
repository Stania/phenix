﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheZep;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.Profil
{
    public class ProfilPoste : EntityBase
    {
        public int ProfilId { get; set; }
        public Profil Profil { get; set; }

        public int PosteId { get; set; }
        public Poste Poste { get; set; }

    }
}
