﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profil;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.Profils
{
    public class Secteur : EntityBase
    {
        public string Nom { get; set; }
        public ICollection<Poste> PosteList { get; set; }
        public ICollection<UserProfil> UserProfilList { get; set; }

    }
}
