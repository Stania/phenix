﻿using Clic.DAL.Entities.Base;
using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Entities.Profil
{
    public class UserProfil : EntityBase
    {
        public int ProfilId { get; set; }
        public Profil Profil { get; set; }

        public int? PosteId { get; set; }
        public Poste Poste { get; set; }

        public int SecteurId { get; set; }
        public Secteur Secteur { get; set; }

        public int UtilisateurId { get; set; }
        public Utilisateur Utilisateur { get; set; }


        protected override void OnValidateData()
        {
            if (this.Profil == null || this.Profil.Id < 1)
            {
                if (ProfilId < 1) throw new ArgumentException("Argument null : " + nameof(this.Profil));
            }

            if (this.Secteur == null || this.Secteur.Id < 1)
            {
                if (SecteurId < 1) throw new ArgumentException("Argument null : " + nameof(this.Secteur));
            }

            if (this.Utilisateur == null || this.Utilisateur.Id < 1)
            {
                if (UtilisateurId < 1) throw new ArgumentException("Argument null : " + nameof(this.Utilisateur));
            } 
        }
    }
}
