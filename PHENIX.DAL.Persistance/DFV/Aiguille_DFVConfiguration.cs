﻿using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Entities.FicheZep;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class Aiguille_DFVConfiguration : IEntityTypeConfiguration<Aiguille_DFV>
    {
        void IEntityTypeConfiguration<Aiguille_DFV>.Configure(EntityTypeBuilder<Aiguille_DFV> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.DFV)
                .WithMany(t => t.Aiguille_DFVList)
                .HasForeignKey(p => p.DFVId);
            
            builder.HasOne(p => p.Aiguille)
               .WithMany(t => t.Aiguille_DFVList)
               .HasForeignKey(p => p.AiguilleId);

            builder.Property(p => p.Commentaire)
                .HasMaxLength(1024);
               
        }
    }
}
