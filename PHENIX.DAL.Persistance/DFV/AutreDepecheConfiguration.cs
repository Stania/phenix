﻿using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class AutreDepecheConfiguration : IEntityTypeConfiguration<AutreDepeche>
    {
        void IEntityTypeConfiguration<AutreDepeche>.Configure(EntityTypeBuilder<AutreDepeche> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.AncienneValeur)
               .IsRequired()
               .HasMaxLength(1024);

            builder.Property(p => p.NouvelleValeur)
               .IsRequired()
               .HasMaxLength(1024);

            builder.HasOne(p => p.AutreDepecheType)
                .WithMany(t => t.AutreDepecheList)
                .HasForeignKey(t => t.AutreDepecheTypeId)
                .IsRequired();

            builder.HasOne(p => p.Manoeuvre)
               .WithMany(t => t.AutreDepecheList)
               .HasForeignKey(t => t.ManoeuvreId);

            builder.HasOne(p => p.DFV)
               .WithMany(t => t.AutreDepecheList)
               .HasForeignKey(t => t.DFVId);

            builder.HasOne(p => p.Depeche)
               .WithMany(t => t.AutreDepecheList)
               .HasForeignKey(t => t.DepecheId)
               .IsRequired();
        }
    }
}
