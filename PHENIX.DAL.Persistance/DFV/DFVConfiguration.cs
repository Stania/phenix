﻿using Microsoft.EntityFrameworkCore;
using Clic.DAL.Entities.FicheDFV;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class DFVConfiguration : IEntityTypeConfiguration<Entities.FicheDFV.DFV>
    {
        void IEntityTypeConfiguration<Entities.FicheDFV.DFV>.Configure(EntityTypeBuilder<Entities.FicheDFV.DFV> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Numero)
                
                .HasMaxLength(50);

            builder.Ignore(p => p.Nuit);

            builder.HasOne(t => t.Zep)
                .WithMany(t => t.DFVList)
                .HasForeignKey(t => t.ZepId)
                .IsRequired();

            builder.HasOne(p => p.VerificationZepLibre_IPCS)
                .WithOne(t => t.DFV)
                .IsRequired();

            builder.HasOne(p => p.VerificationZepLibre_Ligne)
               .WithOne(t => t.DFV)
               .IsRequired();

            builder.HasOne(p => p.ResponsableDFV)
              .WithMany(t => t.DFVList)
              .HasForeignKey(t => t.ResponsableDFVId)
              .IsRequired();

            builder.HasOne(t => t.StatusZep)
                .WithMany(t => t.DFVList)
                .HasForeignKey(t => t.StatusZepId);
        }
    }
}
