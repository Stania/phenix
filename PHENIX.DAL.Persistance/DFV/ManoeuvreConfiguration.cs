﻿using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class ManoeuvreConfiguration : IEntityTypeConfiguration<Manoeuvre>
    {
        void IEntityTypeConfiguration<Manoeuvre>.Configure(EntityTypeBuilder<Manoeuvre> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.ManoeuvreType)
                .WithMany(t => t.ManoeuvreList)
                .HasForeignKey(t => t.ManoeuvreTypeId)
                .IsRequired();

            builder.HasOne(p => p.DFV)
               .WithMany(t => t.ManoeuvreList)
               .HasForeignKey(t => t.DFVId)
               .IsRequired();

            builder.HasOne(p => p.PointRemarquable_Zep)
              .WithMany(t => t.ManoeuvreList)
              .HasForeignKey(t => t.PointRemarquable_ZepId)
              .IsRequired();
        }
    }
}
