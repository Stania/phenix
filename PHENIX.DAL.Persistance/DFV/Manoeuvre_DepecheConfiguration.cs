﻿using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class Manoeuvre_DepecheConfiguration : IEntityTypeConfiguration<Manoeuvre_Depeche>
    {
        void IEntityTypeConfiguration<Manoeuvre_Depeche>.Configure(EntityTypeBuilder<Manoeuvre_Depeche> builder)
        {
            builder.HasKey(p => p.Id);
            builder.HasOne(t => t.Depeche)
                .WithMany(t => t.Manoeuvre_DepecheList)
                .HasForeignKey(t => t.DepecheId);

            builder.HasOne(t => t.Manoeuvre)
                .WithMany(t => t.Manoeuvre_DepecheList)
                .HasForeignKey(t => t.ManoeuvreId);
        }
    }
}
