﻿using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class MaterielRoulantConfiguration : IEntityTypeConfiguration<MaterielRoulant>
    {
        void IEntityTypeConfiguration<MaterielRoulant>.Configure(EntityTypeBuilder<MaterielRoulant> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.DFV)
               .WithMany(t => t.MaterielRoulantList)
               .HasForeignKey(t => t.DFVId);
         
        }
    }
}
