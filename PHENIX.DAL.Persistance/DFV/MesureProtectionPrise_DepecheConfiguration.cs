﻿using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class MesureProtectionPrise_DepecheConfiguration : IEntityTypeConfiguration<MesureProtectionPrise_Depeche>
    {
        void IEntityTypeConfiguration<MesureProtectionPrise_Depeche>.Configure(EntityTypeBuilder<MesureProtectionPrise_Depeche> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.Depeche)
                .WithMany(p => p.MesureProtectionPrise_DepecheList)
                .HasForeignKey(p => p.DepecheId);

            builder.HasOne(p => p.MesureProtectionPrise)
                .WithMany(p => p.MesureProtectionPrise_DepecheList)
                .HasForeignKey(p => p.DepecheId);
        }
    }
}
