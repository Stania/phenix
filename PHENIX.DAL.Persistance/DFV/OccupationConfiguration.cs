﻿using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class OccupationConfiguration : IEntityTypeConfiguration<Occupation>
    {
        void IEntityTypeConfiguration<Occupation>.Configure(EntityTypeBuilder<Occupation> builder)
        {
            builder.HasKey(p => p.Id);
           
            builder.HasOne(p => p.OccupationType)
                .WithMany(t => t.OccupationList)
                .HasForeignKey(t => t.OccupationTypeId)
                .IsRequired();

            builder.HasOne(p => p.DFV)
                .WithMany(t => t.OccupationList)
                .HasForeignKey(t => t.DFVId)
                .IsRequired();
        }
    }
}
