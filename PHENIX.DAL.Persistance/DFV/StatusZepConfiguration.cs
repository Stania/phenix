﻿using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.DFV
{
    class StatutZepConfiguration : IEntityTypeConfiguration<StatutZep>
    {
        public void Configure(EntityTypeBuilder<StatutZep> builder)
        {
            builder.HasKey(t => t.Id);
            builder.Property(t => t.Statut)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}
