﻿using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class VerificationZepLibre_IPCSCConfiguration : IEntityTypeConfiguration<VerificationZepLibre_IPCS>
    {
        void IEntityTypeConfiguration<VerificationZepLibre_IPCS>.Configure(EntityTypeBuilder<VerificationZepLibre_IPCS> builder)
        {
            builder.HasKey(p => p.Id);
          
            builder.HasOne(p => p.DFV)
                .WithOne(t => t.VerificationZepLibre_IPCS)
                .HasForeignKey<VerificationZepLibre_IPCS>(p => p.DFVId)
                .IsRequired();

            builder.HasOne(p => p.Depeche)
                .WithOne(t => t.VerificationZepLibre_IPCS)
                .HasForeignKey<VerificationZepLibre_IPCS>(t => t.DepecheId)
                .IsRequired();

            builder.HasOne(t => t.PosteDemande)
                .WithOne(t => t.VerificationZepLibre_IPCS)
                .HasForeignKey<VerificationZepLibre_IPCS>(t => t.PosteDemandeId);
        }
    }
}
