﻿using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheDFV
{
    public class VerificationZepLibre_LigneConfiguration : IEntityTypeConfiguration<VerificationZepLibre_Ligne>
    {
        void IEntityTypeConfiguration<VerificationZepLibre_Ligne>.Configure(EntityTypeBuilder<VerificationZepLibre_Ligne> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Nom)
                .IsRequired()
                .HasMaxLength(50);

            builder.HasOne(p => p.DFV)
                .WithOne(t => t.VerificationZepLibre_Ligne)
                .HasForeignKey<VerificationZepLibre_Ligne>(t => t.DFVId)
                .IsRequired();

            builder.HasOne(t => t.PosteDemande)
                .WithOne(t => t.VerificationZepLibre_Ligne)
                .HasForeignKey<VerificationZepLibre_Ligne>(t => t.PosteDemandeId);

            builder.HasOne(t => t.Depeche)
                .WithMany(t => t.VerificationZepLibre_LigneList)
                .HasForeignKey(t => t.Depeche);

            builder.HasOne(t => t.Depeche2)
                .WithMany(t => t.VerificationZepLibre2_LigneList)
                .HasForeignKey(t => t.Depeche2);
        }
    }
}
