﻿using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Persistance.DbContext;
using Microsoft.EntityFrameworkCore;

namespace Clic.Data.DbContext
{
    public partial class AppDbContext
    {
        public DbSet<Depeche> Depeche { get; set; }
        public DbSet<DepecheType> DepecheType { get; set; }

        //validation uses cases
        public void ValidateDepeche(Depeche depeche)
        {
            string result = string.Empty;
            if (string.IsNullOrWhiteSpace(depeche.NumeroDonne))
                throw new DataValidationException(nameof(depeche.NumeroDonne) + " ne doit pas être vide");
            if (string.IsNullOrWhiteSpace(depeche.NumeroRecu))
                throw new DataValidationException(nameof(depeche.NumeroRecu) + " ne doit pas être vide");
            if(depeche.DateDepeche != null)
                throw new DataValidationException(nameof(depeche.NumeroRecu) + " ne doit pas être null");
            if (depeche.DepecheType == null)
                throw new DataValidationException(nameof(depeche.DepecheType) + " ne doit pas être null");
        }
    }
}
