﻿using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profil;
using Clic.DAL.Entities.FicheDFV;
using Microsoft.EntityFrameworkCore;
using Clic.DAL.Entities.Profils;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace Clic.Data.DbContext
{
    public partial class AppDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        ////////////////////////////
        /// PAS TOUCHE. MERCI :) ///
        ////////////////////////////
        /// MAHDI
        //private string _StringConnection = "server=localhost; Port=5433;user id=postgres;password=postgres;database=PHENIX_LOCAL";
        /// STANIA
        private string _StringConnection = "server=localhost; Port=5432;user id=postgres;password=root;database=PHENIX_LOCAL";

        /// BDD DE TEST SUR LE RESEAU
        //private string _StringConnection = "server=vm-srv-bdd-test; Port=5432;user id=postgres;password=clic42?;database=PHENIX_LOCAL";
        //private string _StringConnection = "server=vm-srv-bdd-test; Port=5432;user id=postgres;password=clic42?;database=PHENIX_Local_2";
        //private string _StringConnection = "server=vm-srv-bdd-test; Port=5432;user id=postgres;password=clic42?;database=PHENIX_Local_3";

        //string _StringConnection = "server=localhost; Port=5432;user id=postgres;password=postgres;database=PHENIX_LOCAL";
        //private string _StringConnection = "server=localhost; Port=5432;user id=postgres;password=Azerty@1234.;database=PHENIX_LOCAL";
        //public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public AppDbContext() { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(_StringConnection);
            optionsBuilder.UseNpgsql(_StringConnection);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            optionsBuilder.EnableSensitiveDataLogging(true);

            //optionsBuilder.UseInMemoryDatabase(databaseName: "fake_database");
        }

        public DbSet<Utilisateur> Utilisateur { get; set; }
        public DbSet<Profil> Profil { get; set; }
        public DbSet<Poste> Poste { get; set; }
        public DbSet<ProfilPoste> ProfilPoste { get; set; }
        public DbSet<UserProfil> UserProfil { get; set; }

        public DbSet<Secteur> Secteur { get; set; }
        public DbSet<Zep> Zep { get; set; }
        public DbSet<ObjetGraphique> ObjetGraphique { get; set; }
        public DbSet<ZepType> ZepType { get; set; }
        public DbSet<Aiguille> Aiguille { get; set; }
        public DbSet<CompositionZep> CompositionZep { get; set; }
        public DbSet<IncompatibiliteZep> IncompatibiliteZep { get; set; }
        public DbSet<MesureProtection_Zep> MesureProtection_Zep { get; set; }
        public DbSet<MesureProtectionType> MesureProtectionType { get; set; }
        public DbSet<Particularite> Particularite { get; set; }
        public DbSet<ParticulariteType> ParticulariteType { get; set; }
        public DbSet<PointRemarquable_Zep> PointRemarquable_Zep { get; set; }
        public DbSet<PointRemarquableType> PointRemarquableType { get; set; }
        public DbSet<Verification> Verification { get; set; }

        public DbSet<Aiguille_DFV> Aiguille_DFV { get; set; }
      
        public DbSet<DFV> DFV { get; set; }
        public DbSet<Manoeuvre> Manoeuvre { get; set; }
        public DbSet<ManoeuvreType> ManoeuvreType { get; set; }
        public DbSet<MesureProtectionPrise> MesureProtectionPrise { get; set; }
        public DbSet<MesureProtectionPrise_Depeche> MesureProtectionPrise_Depeche { get; set; }
        public DbSet<Occupation> Occupation { get; set; }
        public DbSet<OccupationType> OccupationType { get; set; }
        public DbSet<VerificationZepLibre_IPCS> VerificationZepLibre_IPCS { get; set; }
        public DbSet<VerificationZepLibre_Ligne> VerificationZepLibre_Ligne { get; set; }
        public DbSet<StatutZep> StatutZep { get; set; }
        public DbSet<AutreDepeche> AutreDepeche { get; set; }
        public DbSet<AutreDepecheType> AutreDepecheType { get; set; }
        public DbSet<Manoeuvre_Depeche> Manoeuvre_Depeche { get; set; }
        public DbSet<MaterielRoulant> MaterielRoulant { get; set; }
        

    }
}
