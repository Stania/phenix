﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.DbContext
{
    [Serializable]
    public class DataValidationException : Exception
    {
        public DataValidationException(string message, Exception innerException):base(message,innerException)
        {

        }

        public DataValidationException(string message) : base(message)
        {

        }
    }
}
