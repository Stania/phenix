﻿using Clic.DAL.Entities.Base;
using Clic.Data.DbContext;
using System;
using System.Linq;

namespace Clic.DAL.Persistance.ExtentionMethods
{
    public static class DbExtention
    {
        public static void ValidateEntity<T>(this AppDbContext context,T entity, bool isNew = false, bool doCustomValidation = false)
        where T : EntityBase
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            if (!isNew)
            {
                if (entity.Id < 1 || !context.Set<T>().Any(t => t.Id == entity.Id))
                    throw new ArgumentException("Argument invalide : " + nameof(entity) + " - " +  nameof(entity.Id));
            }
            if (doCustomValidation)
            {
                entity.ValidateData();
            }
        }

        public static void ValidateId<T>(this AppDbContext context, int Id)
        where T : EntityBase
        {
            if (Id < 1 || !context.Set<T>().Any(t => t.Id == Id))
                throw new ArgumentException("Argument invalide : " + nameof(Id) + " : " + Id);
        }
    }
}
