﻿using Clic.DAL.Entities.FicheZep;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheZep
{
    public class AiguilleConfiguration : IEntityTypeConfiguration<Aiguille>
    {
        void IEntityTypeConfiguration<Aiguille>.Configure(EntityTypeBuilder<Aiguille> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Nom)
                .IsRequired()
                .HasMaxLength(50);

            builder.HasOne(p => p.Zep)
                .WithMany(t => t.AiguilleList)
                .HasForeignKey(t => t.ZepId)
                .IsRequired();
        }
    }
}
