﻿using Clic.DAL.Entities.FicheZep;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheZep
{
    public class MesureProtectionTypeConfiguration : IEntityTypeConfiguration<MesureProtectionType>
    {
        void IEntityTypeConfiguration<MesureProtectionType>.Configure(EntityTypeBuilder<MesureProtectionType> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Nom)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}
