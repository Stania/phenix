﻿using Clic.DAL.Entities.FicheZep;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheZep
{
    public class MesureProtection_ZepConfiguration : IEntityTypeConfiguration<MesureProtection_Zep>
    {
        void IEntityTypeConfiguration<MesureProtection_Zep>.Configure(EntityTypeBuilder<MesureProtection_Zep> builder)
        {
            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.Zep)
                .WithMany(t => t.MesureProtection_ZepList)
                .HasForeignKey(p => p.ZepId);

            builder.HasOne(p => p.Poste).
                WithMany(t => t.MesureProtection_ZepList)
                .HasForeignKey(p => p.PosteId);

            builder.HasOne(p => p.MesureProtectionType)
                .WithMany(p => p.MesureProtection_ZepList)
                .HasForeignKey(p => p.MesureProtectionTypeId);

        }
    }
}
