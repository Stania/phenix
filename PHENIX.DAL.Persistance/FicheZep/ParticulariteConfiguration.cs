﻿using Clic.DAL.Entities.FicheZep;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheZep
{
    public class ParticulariteConfiguration : IEntityTypeConfiguration<Particularite>
    {
        void IEntityTypeConfiguration<Particularite>.Configure(EntityTypeBuilder<Particularite> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Valeur)
                .IsRequired()
                .HasMaxLength(50);

            builder.HasOne(p => p.ParticulariteType)
                .WithMany(t => t.ParticulariteList)
                .HasForeignKey(t => t.ParticulariteTypeId)
                .IsRequired();

            builder.HasOne(p => p.Zep)
                .WithMany(t => t.ParticulariteList)
                .HasForeignKey(t => t.ZepId)
                .IsRequired();
        }
    }
}
