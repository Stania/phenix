﻿using Clic.DAL.Entities.FicheZep;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheZep
{
    public class PointRemarquableTypeConfiguration : IEntityTypeConfiguration<PointRemarquableType>
    {
        void IEntityTypeConfiguration<PointRemarquableType>.Configure(EntityTypeBuilder<PointRemarquableType> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Nom)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}
