﻿using Clic.DAL.Entities.FicheZep;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheZep
{
    public class PosteConfiguration : IEntityTypeConfiguration<Poste>
    {
        void IEntityTypeConfiguration<Poste>.Configure(EntityTypeBuilder<Poste> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Nom)
                .IsRequired()
                .HasMaxLength(50);

            builder.HasOne(t => t.Secteur)
                .WithMany(t => t.PosteList)
                .HasForeignKey(t => t.SecteurId);
        }
    }
}
