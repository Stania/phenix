﻿using Clic.DAL.Entities.FicheZep;
using Clic.DAL.Entities.Profil;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheZep
{
    public class UtilisateurConfiguration : IEntityTypeConfiguration<Utilisateur>
    {
        void IEntityTypeConfiguration<Utilisateur>.Configure(EntityTypeBuilder<Utilisateur> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Login)
                .IsRequired()
                .HasMaxLength(50);
           

        }
    }
}
