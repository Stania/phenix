﻿using Clic.DAL.Entities.FicheZep;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheZep
{
    public class VerificationConfiguration : IEntityTypeConfiguration<Verification>
    {
        void IEntityTypeConfiguration<Verification>.Configure(EntityTypeBuilder<Verification> builder)
        {
            builder.HasKey(t => t.Id);
            builder.HasOne(t => t.Poste)
                .WithMany(t => t.VerificationList)
                .HasForeignKey(t => t.PosteId);

            builder.HasOne(t => t.Zep)
                .WithMany(t => t.VerificationList)
                .HasForeignKey(t => t.ZepId);

            builder.HasOne(p => p.VerificationType)
               .WithMany(t => t.VerificationList)
               .HasForeignKey(t => t.VerificationTypeId)
               .IsRequired();
        }
    }
}
