﻿using Clic.DAL.Entities.FicheZep;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.FicheZep
{
    public class ZepConfiguration : IEntityTypeConfiguration<Zep>
    {
        void IEntityTypeConfiguration<Zep>.Configure(EntityTypeBuilder<Zep> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Numero)
                .IsRequired()
                .HasMaxLength(50);

            builder.HasOne(p => p.ZepType)
                .WithMany(t => t.ZepList)
                .HasForeignKey(t => t.ZepTypeId)
                .IsRequired();

            builder.HasOne(t => t.PosteDemandeDFV)
                .WithMany(t => t.ZepList)
                .HasForeignKey(t => t.PosteDemandeDFVId);
        }
    }
}
