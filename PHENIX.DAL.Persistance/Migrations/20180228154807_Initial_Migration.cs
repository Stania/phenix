﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class Initial_Migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DepecheType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepecheType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ManoeuvreType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManoeuvreType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MesureProtectionType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MesureProtectionType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OccupationType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OccupationType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ParticulariteType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParticulariteType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PointRemarquableType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PointRemarquableType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Secteur",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Secteur", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Utilisateur",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    HasshedPassword = table.Column<string>(nullable: true),
                    Login = table.Column<string>(nullable: true),
                    MotDePasse = table.Column<string>(nullable: true),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Utilisateur", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VerificationType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VerificationType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ZepType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZepType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Depeche",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateDepeche = table.Column<DateTime>(nullable: true),
                    DepecheTypeId = table.Column<int>(nullable: false),
                    NumeroDonne = table.Column<string>(nullable: true),
                    NumeroRecu = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Depeche", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Depeche_DepecheType_DepecheTypeId",
                        column: x => x.DepecheTypeId,
                        principalTable: "DepecheType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PointRemarquable",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true),
                    PointRemarquableTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PointRemarquable", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PointRemarquable_PointRemarquableType_PointRemarquableTypeId",
                        column: x => x.PointRemarquableTypeId,
                        principalTable: "PointRemarquableType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Poste",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true),
                    SecteurId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Poste", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Poste_Secteur_SecteurId",
                        column: x => x.SecteurId,
                        principalTable: "Secteur",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Zep",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AutorisationLAM = table.Column<bool>(nullable: true),
                    AutorisationTTX = table.Column<bool>(nullable: true),
                    AutresDispositionsParticulieres = table.Column<string>(nullable: true),
                    DFVAvecVerificationLiberation = table.Column<bool>(nullable: true),
                    DFVRestitueeOccupee = table.Column<bool>(nullable: true),
                    DFVSansVerificationLiberationDerriereTrainOuvrant = table.Column<bool>(nullable: true),
                    DFVSansVerificationLiberationTTxDeclencheur = table.Column<bool>(nullable: true),
                    DFVSansVerificationLiberationTTxStationne = table.Column<bool>(nullable: true),
                    DFVSansVerificationLiberationTTxStationnePartieG = table.Column<bool>(nullable: true),
                    DateDebutApplicabilite = table.Column<DateTime>(nullable: true),
                    DateFinApplicabilite = table.Column<DateTime>(nullable: true),
                    FichierImage = table.Column<string>(nullable: true),
                    GeqAvecTTx = table.Column<bool>(nullable: true),
                    GeqSansTTx = table.Column<bool>(nullable: true),
                    Indice = table.Column<string>(nullable: true),
                    Numero = table.Column<string>(nullable: true),
                    PosteDemandeDFVId = table.Column<int>(nullable: true),
                    TTxStationne = table.Column<string>(nullable: true),
                    ZepTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Zep", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Zep_Poste_PosteDemandeDFVId",
                        column: x => x.PosteDemandeDFVId,
                        principalTable: "Poste",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Zep_ZepType_ZepTypeId",
                        column: x => x.ZepTypeId,
                        principalTable: "ZepType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Aiguille",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true),
                    ZepId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Aiguille", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Aiguille_Zep_ZepId",
                        column: x => x.ZepId,
                        principalTable: "Zep",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompositionZep",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    GroupeZepId = table.Column<int>(nullable: false),
                    ZepEnfantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompositionZep", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompositionZep_Zep_GroupeZepId",
                        column: x => x.GroupeZepId,
                        principalTable: "Zep",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompositionZep_Zep_ZepEnfantId",
                        column: x => x.ZepEnfantId,
                        principalTable: "Zep",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DFV",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Numero = table.Column<string>(nullable: true),
                    ResponsableDFVId = table.Column<int>(nullable: false),
                    ZepId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DFV", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DFV_Poste_ResponsableDFVId",
                        column: x => x.ResponsableDFVId,
                        principalTable: "Poste",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DFV_Zep_ZepId",
                        column: x => x.ZepId,
                        principalTable: "Zep",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IncompatibiliteZep",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ZepId = table.Column<int>(nullable: false),
                    ZepIncompatibleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IncompatibiliteZep", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IncompatibiliteZep_Zep_ZepId",
                        column: x => x.ZepId,
                        principalTable: "Zep",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IncompatibiliteZep_Zep_ZepIncompatibleId",
                        column: x => x.ZepIncompatibleId,
                        principalTable: "Zep",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MesureProtection_Zep",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    MesureProtectionTypeId = table.Column<int>(nullable: false),
                    PosteId = table.Column<int>(nullable: false),
                    Valeur = table.Column<string>(nullable: true),
                    ZepId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MesureProtection_Zep", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MesureProtection_Zep_MesureProtectionType_MesureProtectionTypeId",
                        column: x => x.MesureProtectionTypeId,
                        principalTable: "MesureProtectionType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MesureProtection_Zep_Poste_PosteId",
                        column: x => x.PosteId,
                        principalTable: "Poste",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MesureProtection_Zep_Zep_ZepId",
                        column: x => x.ZepId,
                        principalTable: "Zep",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Particularite",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ParticulariteTypeId = table.Column<int>(nullable: false),
                    Valeur = table.Column<string>(nullable: true),
                    ZepId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Particularite", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Particularite_ParticulariteType_ParticulariteTypeId",
                        column: x => x.ParticulariteTypeId,
                        principalTable: "ParticulariteType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Particularite_Zep_ZepId",
                        column: x => x.ZepId,
                        principalTable: "Zep",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PointRemarquable_Zep",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    PointRemarquableId = table.Column<int>(nullable: false),
                    PosteConcerneId = table.Column<int>(nullable: false),
                    ZepId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PointRemarquable_Zep", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PointRemarquable_Zep_PointRemarquable_PointRemarquableId",
                        column: x => x.PointRemarquableId,
                        principalTable: "PointRemarquable",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PointRemarquable_Zep_Poste_PosteConcerneId",
                        column: x => x.PosteConcerneId,
                        principalTable: "Poste",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PointRemarquable_Zep_Zep_ZepId",
                        column: x => x.ZepId,
                        principalTable: "Zep",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Verification",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true),
                    PosteId = table.Column<int>(nullable: false),
                    VerificationTypeId = table.Column<int>(nullable: false),
                    ZepId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Verification", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Verification_Poste_PosteId",
                        column: x => x.PosteId,
                        principalTable: "Poste",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Verification_VerificationType_VerificationTypeId",
                        column: x => x.VerificationTypeId,
                        principalTable: "VerificationType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Verification_Zep_ZepId",
                        column: x => x.ZepId,
                        principalTable: "Zep",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Aiguille_DFV",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AiguilleId = table.Column<int>(nullable: false),
                    DFVId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Aiguille_DFV", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Aiguille_DFV_Aiguille_AiguilleId",
                        column: x => x.AiguilleId,
                        principalTable: "Aiguille",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Aiguille_DFV_DFV_DFVId",
                        column: x => x.DFVId,
                        principalTable: "DFV",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Franchissement",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DFVId = table.Column<int>(nullable: false),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchissement", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Franchissement_DFV_DFVId",
                        column: x => x.DFVId,
                        principalTable: "DFV",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Manoeuvre",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DFVId = table.Column<int>(nullable: false),
                    ManoeuvreTypeId = table.Column<int>(nullable: false),
                    Nature = table.Column<string>(nullable: true),
                    Numero = table.Column<int>(nullable: false),
                    ParDepeche = table.Column<bool>(nullable: true),
                    ParEcrit = table.Column<bool>(nullable: true),
                    PointManoeuvre = table.Column<string>(nullable: true),
                    PointRemarquableId = table.Column<int>(nullable: false),
                    Verbale = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Manoeuvre", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Manoeuvre_DFV_DFVId",
                        column: x => x.DFVId,
                        principalTable: "DFV",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Manoeuvre_ManoeuvreType_ManoeuvreTypeId",
                        column: x => x.ManoeuvreTypeId,
                        principalTable: "ManoeuvreType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Manoeuvre_PointRemarquable_PointRemarquableId",
                        column: x => x.PointRemarquableId,
                        principalTable: "PointRemarquable",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MesureProtectionPrise",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DFVId = table.Column<int>(nullable: false),
                    DepecheId = table.Column<int>(nullable: false),
                    Nom = table.Column<string>(nullable: true),
                    PosteDemandeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MesureProtectionPrise", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MesureProtectionPrise_DFV_DFVId",
                        column: x => x.DFVId,
                        principalTable: "DFV",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MesureProtectionPrise_Depeche_DepecheId",
                        column: x => x.DepecheId,
                        principalTable: "Depeche",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MesureProtectionPrise_Poste_PosteDemandeId",
                        column: x => x.PosteDemandeId,
                        principalTable: "Poste",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Occupation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DFVId = table.Column<int>(nullable: false),
                    Nature = table.Column<string>(nullable: true),
                    Numero = table.Column<int>(nullable: false),
                    OccupationTypeId = table.Column<int>(nullable: false),
                    Position = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Occupation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Occupation_DFV_DFVId",
                        column: x => x.DFVId,
                        principalTable: "DFV",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Occupation_OccupationType_OccupationTypeId",
                        column: x => x.OccupationTypeId,
                        principalTable: "OccupationType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VerificationZepLibre_IPCS",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AutreMoyen = table.Column<bool>(nullable: true),
                    AutreMoyenDescription = table.Column<string>(nullable: true),
                    DFVId = table.Column<int>(nullable: false),
                    DepecheId = table.Column<int>(nullable: false),
                    EchangeDepeches = table.Column<bool>(nullable: true),
                    HeureArrivee = table.Column<DateTime>(nullable: true),
                    Nom = table.Column<string>(nullable: true),
                    ObservationControles = table.Column<bool>(nullable: true),
                    ObservationDirecteVoie = table.Column<bool>(nullable: true),
                    PosteDemandeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VerificationZepLibre_IPCS", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VerificationZepLibre_IPCS_DFV_DFVId",
                        column: x => x.DFVId,
                        principalTable: "DFV",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VerificationZepLibre_IPCS_Depeche_DepecheId",
                        column: x => x.DepecheId,
                        principalTable: "Depeche",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VerificationZepLibre_IPCS_Poste_PosteDemandeId",
                        column: x => x.PosteDemandeId,
                        principalTable: "Poste",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VerificationZepLibre_Ligne",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DFVId = table.Column<int>(nullable: false),
                    DepecheId = table.Column<int>(nullable: false),
                    Nom = table.Column<string>(nullable: true),
                    PosteDemandeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VerificationZepLibre_Ligne", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VerificationZepLibre_Ligne_DFV_DFVId",
                        column: x => x.DFVId,
                        principalTable: "DFV",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VerificationZepLibre_Ligne_Depeche_DepecheId",
                        column: x => x.DepecheId,
                        principalTable: "Depeche",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VerificationZepLibre_Ligne_Poste_PosteDemandeId",
                        column: x => x.PosteDemandeId,
                        principalTable: "Poste",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Aiguille_ZepId",
                table: "Aiguille",
                column: "ZepId");

            migrationBuilder.CreateIndex(
                name: "IX_Aiguille_DFV_AiguilleId",
                table: "Aiguille_DFV",
                column: "AiguilleId");

            migrationBuilder.CreateIndex(
                name: "IX_Aiguille_DFV_DFVId",
                table: "Aiguille_DFV",
                column: "DFVId");

            migrationBuilder.CreateIndex(
                name: "IX_CompositionZep_GroupeZepId",
                table: "CompositionZep",
                column: "GroupeZepId");

            migrationBuilder.CreateIndex(
                name: "IX_CompositionZep_ZepEnfantId",
                table: "CompositionZep",
                column: "ZepEnfantId");

            migrationBuilder.CreateIndex(
                name: "IX_Depeche_DepecheTypeId",
                table: "Depeche",
                column: "DepecheTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DFV_ResponsableDFVId",
                table: "DFV",
                column: "ResponsableDFVId");

            migrationBuilder.CreateIndex(
                name: "IX_DFV_ZepId",
                table: "DFV",
                column: "ZepId");

            migrationBuilder.CreateIndex(
                name: "IX_Franchissement_DFVId",
                table: "Franchissement",
                column: "DFVId");

            migrationBuilder.CreateIndex(
                name: "IX_IncompatibiliteZep_ZepId",
                table: "IncompatibiliteZep",
                column: "ZepId");

            migrationBuilder.CreateIndex(
                name: "IX_IncompatibiliteZep_ZepIncompatibleId",
                table: "IncompatibiliteZep",
                column: "ZepIncompatibleId");

            migrationBuilder.CreateIndex(
                name: "IX_Manoeuvre_DFVId",
                table: "Manoeuvre",
                column: "DFVId");

            migrationBuilder.CreateIndex(
                name: "IX_Manoeuvre_ManoeuvreTypeId",
                table: "Manoeuvre",
                column: "ManoeuvreTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Manoeuvre_PointRemarquableId",
                table: "Manoeuvre",
                column: "PointRemarquableId");

            migrationBuilder.CreateIndex(
                name: "IX_MesureProtection_Zep_MesureProtectionTypeId",
                table: "MesureProtection_Zep",
                column: "MesureProtectionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MesureProtection_Zep_PosteId",
                table: "MesureProtection_Zep",
                column: "PosteId");

            migrationBuilder.CreateIndex(
                name: "IX_MesureProtection_Zep_ZepId",
                table: "MesureProtection_Zep",
                column: "ZepId");

            migrationBuilder.CreateIndex(
                name: "IX_MesureProtectionPrise_DFVId",
                table: "MesureProtectionPrise",
                column: "DFVId");

            migrationBuilder.CreateIndex(
                name: "IX_MesureProtectionPrise_DepecheId",
                table: "MesureProtectionPrise",
                column: "DepecheId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MesureProtectionPrise_PosteDemandeId",
                table: "MesureProtectionPrise",
                column: "PosteDemandeId");

            migrationBuilder.CreateIndex(
                name: "IX_Occupation_DFVId",
                table: "Occupation",
                column: "DFVId");

            migrationBuilder.CreateIndex(
                name: "IX_Occupation_OccupationTypeId",
                table: "Occupation",
                column: "OccupationTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Particularite_ParticulariteTypeId",
                table: "Particularite",
                column: "ParticulariteTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Particularite_ZepId",
                table: "Particularite",
                column: "ZepId");

            migrationBuilder.CreateIndex(
                name: "IX_PointRemarquable_PointRemarquableTypeId",
                table: "PointRemarquable",
                column: "PointRemarquableTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PointRemarquable_Zep_PointRemarquableId",
                table: "PointRemarquable_Zep",
                column: "PointRemarquableId");

            migrationBuilder.CreateIndex(
                name: "IX_PointRemarquable_Zep_PosteConcerneId",
                table: "PointRemarquable_Zep",
                column: "PosteConcerneId");

            migrationBuilder.CreateIndex(
                name: "IX_PointRemarquable_Zep_ZepId",
                table: "PointRemarquable_Zep",
                column: "ZepId");

            migrationBuilder.CreateIndex(
                name: "IX_Poste_SecteurId",
                table: "Poste",
                column: "SecteurId");

            migrationBuilder.CreateIndex(
                name: "IX_Verification_PosteId",
                table: "Verification",
                column: "PosteId");

            migrationBuilder.CreateIndex(
                name: "IX_Verification_VerificationTypeId",
                table: "Verification",
                column: "VerificationTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Verification_ZepId",
                table: "Verification",
                column: "ZepId");

            migrationBuilder.CreateIndex(
                name: "IX_VerificationZepLibre_IPCS_DFVId",
                table: "VerificationZepLibre_IPCS",
                column: "DFVId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VerificationZepLibre_IPCS_DepecheId",
                table: "VerificationZepLibre_IPCS",
                column: "DepecheId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VerificationZepLibre_IPCS_PosteDemandeId",
                table: "VerificationZepLibre_IPCS",
                column: "PosteDemandeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VerificationZepLibre_Ligne_DFVId",
                table: "VerificationZepLibre_Ligne",
                column: "DFVId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VerificationZepLibre_Ligne_DepecheId",
                table: "VerificationZepLibre_Ligne",
                column: "DepecheId");

            migrationBuilder.CreateIndex(
                name: "IX_VerificationZepLibre_Ligne_PosteDemandeId",
                table: "VerificationZepLibre_Ligne",
                column: "PosteDemandeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Zep_PosteDemandeDFVId",
                table: "Zep",
                column: "PosteDemandeDFVId");

            migrationBuilder.CreateIndex(
                name: "IX_Zep_ZepTypeId",
                table: "Zep",
                column: "ZepTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Aiguille_DFV");

            migrationBuilder.DropTable(
                name: "CompositionZep");

            migrationBuilder.DropTable(
                name: "Franchissement");

            migrationBuilder.DropTable(
                name: "IncompatibiliteZep");

            migrationBuilder.DropTable(
                name: "Manoeuvre");

            migrationBuilder.DropTable(
                name: "MesureProtection_Zep");

            migrationBuilder.DropTable(
                name: "MesureProtectionPrise");

            migrationBuilder.DropTable(
                name: "Occupation");

            migrationBuilder.DropTable(
                name: "Particularite");

            migrationBuilder.DropTable(
                name: "PointRemarquable_Zep");

            migrationBuilder.DropTable(
                name: "Utilisateur");

            migrationBuilder.DropTable(
                name: "Verification");

            migrationBuilder.DropTable(
                name: "VerificationZepLibre_IPCS");

            migrationBuilder.DropTable(
                name: "VerificationZepLibre_Ligne");

            migrationBuilder.DropTable(
                name: "Aiguille");

            migrationBuilder.DropTable(
                name: "ManoeuvreType");

            migrationBuilder.DropTable(
                name: "MesureProtectionType");

            migrationBuilder.DropTable(
                name: "OccupationType");

            migrationBuilder.DropTable(
                name: "ParticulariteType");

            migrationBuilder.DropTable(
                name: "PointRemarquable");

            migrationBuilder.DropTable(
                name: "VerificationType");

            migrationBuilder.DropTable(
                name: "DFV");

            migrationBuilder.DropTable(
                name: "Depeche");

            migrationBuilder.DropTable(
                name: "PointRemarquableType");

            migrationBuilder.DropTable(
                name: "Zep");

            migrationBuilder.DropTable(
                name: "DepecheType");

            migrationBuilder.DropTable(
                name: "Poste");

            migrationBuilder.DropTable(
                name: "ZepType");

            migrationBuilder.DropTable(
                name: "Secteur");
        }
    }
}
