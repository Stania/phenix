﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class CreationDFV : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MesureProtectionPrise_Depeche_DepecheId",
                table: "MesureProtectionPrise");

            migrationBuilder.DropIndex(
                name: "IX_MesureProtectionPrise_DepecheId",
                table: "MesureProtectionPrise");

            migrationBuilder.DropColumn(
                name: "AutreMoyen",
                table: "VerificationZepLibre_IPCS");

            migrationBuilder.DropColumn(
                name: "AutreMoyenDescription",
                table: "VerificationZepLibre_IPCS");

            migrationBuilder.DropColumn(
                name: "EchangeDepeches",
                table: "VerificationZepLibre_IPCS");

            migrationBuilder.DropColumn(
                name: "Nom",
                table: "VerificationZepLibre_IPCS");

            migrationBuilder.DropColumn(
                name: "ObservationControles",
                table: "VerificationZepLibre_IPCS");

            migrationBuilder.DropColumn(
                name: "ObservationDirecteVoie",
                table: "VerificationZepLibre_IPCS");

            migrationBuilder.DropColumn(
                name: "Numero",
                table: "Occupation");

            migrationBuilder.DropColumn(
                name: "DepecheId",
                table: "MesureProtectionPrise");

            migrationBuilder.DropColumn(
                name: "Nom",
                table: "MesureProtectionPrise");

            migrationBuilder.DropColumn(
                name: "Numero",
                table: "Manoeuvre");

            migrationBuilder.RenameColumn(
                name: "HeureArrivee",
                table: "VerificationZepLibre_IPCS",
                newName: "HeureDemande");

            migrationBuilder.RenameColumn(
                name: "Position",
                table: "Occupation",
                newName: "TTXPosition");

            migrationBuilder.RenameColumn(
                name: "Nature",
                table: "Occupation",
                newName: "TTXNumero");

            migrationBuilder.RenameColumn(
                name: "PointManoeuvre",
                table: "Manoeuvre",
                newName: "TTXVoie");

            migrationBuilder.RenameColumn(
                name: "Nature",
                table: "Manoeuvre",
                newName: "TTXPointInitial");

            migrationBuilder.AddColumn<DateTime>(
                name: "HeureArrivee",
                table: "VerificationZepLibre_Ligne",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NumeroTrainRecu",
                table: "VerificationZepLibre_Ligne",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Commentaire",
                table: "Occupation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TTXNature",
                table: "Occupation",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "PriseDeMesure",
                table: "MesureProtectionPrise",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "VerificationLiberationVoieprevue",
                table: "MesureProtectionPrise",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Commentaire",
                table: "Manoeuvre",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Direction",
                table: "Manoeuvre",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TTXNature",
                table: "Manoeuvre",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TTXNumero",
                table: "Manoeuvre",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AHTNumero",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AccordeParEcrit",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AvecVerificationDeLiberation",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Avis",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AvisNumero",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CCTTNumero",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContratTravauxNumero",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateApplicabilite",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreation",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateDebutAccordee",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateDebutApplicabilite",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateFinAccordee",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateFinApplicabilite",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DepecheReceptionNumero",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DerriereTTXDeclencheurNumero",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DerriereTrainOuvrantNumero",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "LeveeMesuresParAC",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "MesureProtectionPiseParAC",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NumeroTelephone",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ObservationControles",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ObservationDirecteVoie",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ObservationParAutreMoyen",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ObservationParAutreMoyenDescription",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ObservationParEchangeDepeches",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OperationNumero",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RPTX",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SansTTX",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SignatureRPTX",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TransmisParDepecheNumero",
                table: "DFV",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MesureProtectionPrise_Depeche",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DepecheId = table.Column<int>(nullable: false),
                    MesureProtectionPriseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MesureProtectionPrise_Depeche", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MesureProtectionPrise_Depeche_Depeche_DepecheId",
                        column: x => x.DepecheId,
                        principalTable: "Depeche",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MesureProtectionPrise_Depeche_MesureProtectionPrise_MesureProtectionPriseId",
                        column: x => x.MesureProtectionPriseId,
                        principalTable: "MesureProtectionPrise",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MesureProtectionPrise_Depeche_DepecheId",
                table: "MesureProtectionPrise_Depeche",
                column: "DepecheId");

            migrationBuilder.CreateIndex(
                name: "IX_MesureProtectionPrise_Depeche_MesureProtectionPriseId",
                table: "MesureProtectionPrise_Depeche",
                column: "MesureProtectionPriseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MesureProtectionPrise_Depeche");

            migrationBuilder.DropColumn(
                name: "HeureArrivee",
                table: "VerificationZepLibre_Ligne");

            migrationBuilder.DropColumn(
                name: "NumeroTrainRecu",
                table: "VerificationZepLibre_Ligne");

            migrationBuilder.DropColumn(
                name: "Commentaire",
                table: "Occupation");

            migrationBuilder.DropColumn(
                name: "TTXNature",
                table: "Occupation");

            migrationBuilder.DropColumn(
                name: "PriseDeMesure",
                table: "MesureProtectionPrise");

            migrationBuilder.DropColumn(
                name: "VerificationLiberationVoieprevue",
                table: "MesureProtectionPrise");

            migrationBuilder.DropColumn(
                name: "Commentaire",
                table: "Manoeuvre");

            migrationBuilder.DropColumn(
                name: "Direction",
                table: "Manoeuvre");

            migrationBuilder.DropColumn(
                name: "TTXNature",
                table: "Manoeuvre");

            migrationBuilder.DropColumn(
                name: "TTXNumero",
                table: "Manoeuvre");

            migrationBuilder.DropColumn(
                name: "AHTNumero",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "AccordeParEcrit",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "AvecVerificationDeLiberation",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "Avis",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "AvisNumero",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "CCTTNumero",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "ContratTravauxNumero",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "DateApplicabilite",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "DateCreation",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "DateDebutAccordee",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "DateDebutApplicabilite",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "DateFinAccordee",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "DateFinApplicabilite",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "DepecheReceptionNumero",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "DerriereTTXDeclencheurNumero",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "DerriereTrainOuvrantNumero",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "LeveeMesuresParAC",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "MesureProtectionPiseParAC",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "NumeroTelephone",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "ObservationControles",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "ObservationDirecteVoie",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "ObservationParAutreMoyen",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "ObservationParAutreMoyenDescription",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "ObservationParEchangeDepeches",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "OperationNumero",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "RPTX",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "SansTTX",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "SignatureRPTX",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "TransmisParDepecheNumero",
                table: "DFV");

            migrationBuilder.RenameColumn(
                name: "HeureDemande",
                table: "VerificationZepLibre_IPCS",
                newName: "HeureArrivee");

            migrationBuilder.RenameColumn(
                name: "TTXPosition",
                table: "Occupation",
                newName: "Position");

            migrationBuilder.RenameColumn(
                name: "TTXNumero",
                table: "Occupation",
                newName: "Nature");

            migrationBuilder.RenameColumn(
                name: "TTXVoie",
                table: "Manoeuvre",
                newName: "PointManoeuvre");

            migrationBuilder.RenameColumn(
                name: "TTXPointInitial",
                table: "Manoeuvre",
                newName: "Nature");

            migrationBuilder.AddColumn<bool>(
                name: "AutreMoyen",
                table: "VerificationZepLibre_IPCS",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AutreMoyenDescription",
                table: "VerificationZepLibre_IPCS",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "EchangeDepeches",
                table: "VerificationZepLibre_IPCS",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nom",
                table: "VerificationZepLibre_IPCS",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ObservationControles",
                table: "VerificationZepLibre_IPCS",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ObservationDirecteVoie",
                table: "VerificationZepLibre_IPCS",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Numero",
                table: "Occupation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DepecheId",
                table: "MesureProtectionPrise",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Nom",
                table: "MesureProtectionPrise",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Numero",
                table: "Manoeuvre",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MesureProtectionPrise_DepecheId",
                table: "MesureProtectionPrise",
                column: "DepecheId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MesureProtectionPrise_Depeche_DepecheId",
                table: "MesureProtectionPrise",
                column: "DepecheId",
                principalTable: "Depeche",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
