﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class Nullable_PointRemarquable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manoeuvre_PointRemarquable_Zep_PointRemarquable_ZepId",
                table: "Manoeuvre");

            migrationBuilder.AlterColumn<int>(
                name: "PointRemarquable_ZepId",
                table: "Manoeuvre",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Manoeuvre_PointRemarquable_Zep_PointRemarquable_ZepId",
                table: "Manoeuvre",
                column: "PointRemarquable_ZepId",
                principalTable: "PointRemarquable_Zep",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manoeuvre_PointRemarquable_Zep_PointRemarquable_ZepId",
                table: "Manoeuvre");

            migrationBuilder.AlterColumn<int>(
                name: "PointRemarquable_ZepId",
                table: "Manoeuvre",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Manoeuvre_PointRemarquable_Zep_PointRemarquable_ZepId",
                table: "Manoeuvre",
                column: "PointRemarquable_ZepId",
                principalTable: "PointRemarquable_Zep",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
