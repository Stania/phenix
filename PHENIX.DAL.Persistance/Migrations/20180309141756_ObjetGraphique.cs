﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class ObjetGraphique : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StatusZepId",
                table: "DFV",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ObjetGraphique",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Coordonnees = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    ZepId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObjetGraphique", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ObjetGraphique_Zep_ZepId",
                        column: x => x.ZepId,
                        principalTable: "Zep",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StatutZep",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Statut = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatutZep", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DFV_StatusZepId",
                table: "DFV",
                column: "StatusZepId");

            migrationBuilder.CreateIndex(
                name: "IX_ObjetGraphique_ZepId",
                table: "ObjetGraphique",
                column: "ZepId");

            migrationBuilder.AddForeignKey(
                name: "FK_DFV_StatutZep_StatusZepId",
                table: "DFV",
                column: "StatusZepId",
                principalTable: "StatutZep",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DFV_StatutZep_StatusZepId",
                table: "DFV");

            migrationBuilder.DropTable(
                name: "ObjetGraphique");

            migrationBuilder.DropTable(
                name: "StatutZep");

            migrationBuilder.DropIndex(
                name: "IX_DFV_StatusZepId",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "StatusZepId",
                table: "DFV");
        }
    }
}
