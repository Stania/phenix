﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class Manoeuvre_Depeche : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DepecheId",
                table: "Manoeuvre",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Manoeuvre_DepecheId",
                table: "Manoeuvre",
                column: "DepecheId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Manoeuvre_Depeche_DepecheId",
                table: "Manoeuvre",
                column: "DepecheId",
                principalTable: "Depeche",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manoeuvre_Depeche_DepecheId",
                table: "Manoeuvre");

            migrationBuilder.DropIndex(
                name: "IX_Manoeuvre_DepecheId",
                table: "Manoeuvre");

            migrationBuilder.DropColumn(
                name: "DepecheId",
                table: "Manoeuvre");
        }
    }
}
