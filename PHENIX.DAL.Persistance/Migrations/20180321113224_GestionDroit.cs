﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class GestionDroit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MotDePasse",
                table: "Utilisateur");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateDepeche",
                table: "Depeche",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Profil",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nom = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Profil", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProfilPoste",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    PosteId = table.Column<int>(nullable: false),
                    ProfilId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProfilPoste", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProfilPoste_Poste_PosteId",
                        column: x => x.PosteId,
                        principalTable: "Poste",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProfilPoste_Profil_ProfilId",
                        column: x => x.ProfilId,
                        principalTable: "Profil",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserProfil",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    PosteId = table.Column<int>(nullable: true),
                    ProfilId = table.Column<int>(nullable: false),
                    SecteurId = table.Column<int>(nullable: false),
                    UtilisateurId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserProfil", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserProfil_Poste_PosteId",
                        column: x => x.PosteId,
                        principalTable: "Poste",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserProfil_Profil_ProfilId",
                        column: x => x.ProfilId,
                        principalTable: "Profil",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserProfil_Secteur_SecteurId",
                        column: x => x.SecteurId,
                        principalTable: "Secteur",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserProfil_Utilisateur_UtilisateurId",
                        column: x => x.UtilisateurId,
                        principalTable: "Utilisateur",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProfilPoste_PosteId",
                table: "ProfilPoste",
                column: "PosteId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfilPoste_ProfilId",
                table: "ProfilPoste",
                column: "ProfilId");

            migrationBuilder.CreateIndex(
                name: "IX_UserProfil_PosteId",
                table: "UserProfil",
                column: "PosteId");

            migrationBuilder.CreateIndex(
                name: "IX_UserProfil_ProfilId",
                table: "UserProfil",
                column: "ProfilId");

            migrationBuilder.CreateIndex(
                name: "IX_UserProfil_SecteurId",
                table: "UserProfil",
                column: "SecteurId");

            migrationBuilder.CreateIndex(
                name: "IX_UserProfil_UtilisateurId",
                table: "UserProfil",
                column: "UtilisateurId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProfilPoste");

            migrationBuilder.DropTable(
                name: "UserProfil");

            migrationBuilder.DropTable(
                name: "Profil");

            migrationBuilder.AddColumn<string>(
                name: "MotDePasse",
                table: "Utilisateur",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DateDepeche",
                table: "Depeche",
                nullable: true,
                oldClrType: typeof(DateTime));
        }
    }
}
