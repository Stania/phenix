﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class Options_TTX : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "DepecheReception",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "DerriereTTXDeclencheur",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "DerriereTrainOuvrant",
                table: "DFV",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DepecheReception",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "DerriereTTXDeclencheur",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "DerriereTrainOuvrant",
                table: "DFV");
        }
    }
}
