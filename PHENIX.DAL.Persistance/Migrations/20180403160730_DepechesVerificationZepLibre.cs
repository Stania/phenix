﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class DepechesVerificationZepLibre : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VerificationZepLibre_Ligne_Depeche_DepecheId",
                table: "VerificationZepLibre_Ligne");

            migrationBuilder.AlterColumn<int>(
                name: "DepecheId",
                table: "VerificationZepLibre_Ligne",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "Depeche2Id",
                table: "VerificationZepLibre_Ligne",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VerificationZepLibre_Ligne_Depeche2Id",
                table: "VerificationZepLibre_Ligne",
                column: "Depeche2Id");

            migrationBuilder.AddForeignKey(
                name: "FK_VerificationZepLibre_Ligne_Depeche_Depeche2Id",
                table: "VerificationZepLibre_Ligne",
                column: "Depeche2Id",
                principalTable: "Depeche",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VerificationZepLibre_Ligne_Depeche_DepecheId",
                table: "VerificationZepLibre_Ligne",
                column: "DepecheId",
                principalTable: "Depeche",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VerificationZepLibre_Ligne_Depeche_Depeche2Id",
                table: "VerificationZepLibre_Ligne");

            migrationBuilder.DropForeignKey(
                name: "FK_VerificationZepLibre_Ligne_Depeche_DepecheId",
                table: "VerificationZepLibre_Ligne");

            migrationBuilder.DropIndex(
                name: "IX_VerificationZepLibre_Ligne_Depeche2Id",
                table: "VerificationZepLibre_Ligne");

            migrationBuilder.DropColumn(
                name: "Depeche2Id",
                table: "VerificationZepLibre_Ligne");

            migrationBuilder.AlterColumn<int>(
                name: "DepecheId",
                table: "VerificationZepLibre_Ligne",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_VerificationZepLibre_Ligne_Depeche_DepecheId",
                table: "VerificationZepLibre_Ligne",
                column: "DepecheId",
                principalTable: "Depeche",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
