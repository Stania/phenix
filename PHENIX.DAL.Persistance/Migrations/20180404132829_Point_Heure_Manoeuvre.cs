﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class Point_Heure_Manoeuvre : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateDemandeDegagement",
                table: "Manoeuvre",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PointTTX",
                table: "Manoeuvre",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateDemandeDegagement",
                table: "Manoeuvre");

            migrationBuilder.DropColumn(
                name: "PointTTX",
                table: "Manoeuvre");
        }
    }
}
