﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class ChampsRestitutionDFV : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateRestitution",
                table: "DFV",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsResitutionParDepeche",
                table: "DFV",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "NumeroDepecheRecuRestitution",
                table: "DFV",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateRestitution",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "IsResitutionParDepeche",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "NumeroDepecheRecuRestitution",
                table: "DFV");
        }
    }
}
