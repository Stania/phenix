﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class DatesMesureProtectionLevee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateLeveeMesuresParAC",
                table: "DFV",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateMesureProtectionPiseParAC",
                table: "DFV",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateLeveeMesuresParAC",
                table: "DFV");

            migrationBuilder.DropColumn(
                name: "DateMesureProtectionPiseParAC",
                table: "DFV");
        }
    }
}
