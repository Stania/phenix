﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Clic.DAL.Persistance.Migrations
{
    public partial class materiel_roulant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TTXLieuStationnement",
                table: "Occupation");

            migrationBuilder.CreateTable(
                name: "MaterielRoulant",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DFVId = table.Column<int>(nullable: false),
                    TTXLieuStationnement = table.Column<string>(nullable: true),
                    TTXNumero = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaterielRoulant", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MaterielRoulant_DFV_DFVId",
                        column: x => x.DFVId,
                        principalTable: "DFV",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MaterielRoulant_DFVId",
                table: "MaterielRoulant",
                column: "DFVId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MaterielRoulant");

            migrationBuilder.AddColumn<string>(
                name: "TTXLieuStationnement",
                table: "Occupation",
                nullable: true);
        }
    }
}
