﻿using Clic.DAL.Entities.Profil;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.Profils
{
    public class ProfilConfiguration : IEntityTypeConfiguration<Profil>
    {
        void IEntityTypeConfiguration<Profil>.Configure(EntityTypeBuilder<Profil> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Nom)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}
