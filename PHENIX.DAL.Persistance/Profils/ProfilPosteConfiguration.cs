﻿using Clic.DAL.Entities.Profil;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.Profils
{
    public class ProfilPosteConfiguration : IEntityTypeConfiguration<ProfilPoste>
    {
        void IEntityTypeConfiguration<ProfilPoste>.Configure(EntityTypeBuilder<ProfilPoste> builder)
        {
            builder.HasKey(p => p.Id);
            builder.HasOne(p => p.Profil)
                .WithMany(t => t.ProfilPosteList)
                .HasForeignKey(t => t.ProfilId)
                .IsRequired();

            builder.HasOne(p => p.Poste)
                .WithMany(t => t.ProfilPosteList)
                .HasForeignKey(t => t.PosteId)
                .IsRequired();

            
        }
    }
}
