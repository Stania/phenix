﻿using Clic.DAL.Entities.Profil;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Clic.DAL.Persistance.Profils
{
    public class UserProfilConfiguration : IEntityTypeConfiguration<UserProfil>
    {
        void IEntityTypeConfiguration<UserProfil>.Configure(EntityTypeBuilder<UserProfil> builder)
        {
            builder.HasKey(p => p.Id);
            builder.HasOne(p => p.Profil)
                .WithMany(t => t.UserProfilList)
                .HasForeignKey(t => t.ProfilId)
                .IsRequired();

            builder.HasOne(p => p.Poste)
                .WithMany(t => t.UserProfilList)
                .HasForeignKey(t => t.PosteId)
                .IsRequired(false);

            builder.HasOne(p => p.Secteur)
                .WithMany(t => t.UserProfilList)
                .HasForeignKey(t => t.SecteurId)
                .IsRequired();

            builder.HasOne(p => p.Utilisateur)
                .WithMany(t => t.UserProfilList)
                .HasForeignKey(t => t.UtilisateurId)
                .IsRequired();
        }
    }
}
