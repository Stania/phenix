﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clic.Web.API.Common
{
    public static class CacheKeys
    {
        public static readonly string ZepList = "ZepList";
        public static readonly string ZepAutoriseList = "ZepAutoriseList";
        public static readonly string PosteList = "PosteList";
        public static readonly string OccupationTypeList = "OccupationTypeList";
        public static readonly string ManoeuvreTypeList = "ManoeuvreTypeList";
        public static readonly string DepecheTypeList = "DepecheTypeList";
    }
}
