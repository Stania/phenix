﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clic.DAL.Entities.FicheDFV;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using PHENIX.BLL.IServices;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Clic.Web.API.Controllers
{
    [Route("api/AutreDepeche")]
    public class AutreDepecheController : Controller
    {
        private IAutreDepecheService _AutreDepecheService;
        private IMemoryCache _cache;

        public AutreDepecheController(IAutreDepecheService AutreDepecheService,
             IMemoryCache memoryCache)
        {
            _AutreDepecheService = AutreDepecheService;
            _cache = memoryCache;
        }

        // POST api/<controller>
        [HttpPost]
        [Route("createAutreDepecheManoeuvre")]
        public AutreDepeche CreateAutreDepecheManoeuvre([FromBody]AutreDepeche manoeuvre)
        {
            return _AutreDepecheService.AddAutreDepecheManoeuvre(manoeuvre);
        }


        [HttpPost]
        [Route("createAutreDepecheDFV")]
        public AutreDepeche CreateAutreDepecheDFV([FromBody]AutreDepeche dfv)
        {
            return _AutreDepecheService.CreateAutreDepecheDFV(dfv);
        }

        // POST api/<controller>
        [HttpPost]
        [Route("update")]
        public AutreDepeche Update([FromBody]AutreDepeche manoeuvre)
        {
            return _AutreDepecheService.UpdateAutreDepeche(manoeuvre);
        }

        // DELETE api/<controller>/5
        [HttpPost]
        [Route("DeleteAutreDepeche/{id}")]
        public Object Delete(int id)
        {
            var manoeuvre = _AutreDepecheService.GetAutreDepecheById(id);

            try
            {
                _AutreDepecheService.DeleteAutreDepeche(manoeuvre);
                return new Object();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAllAutreDepecheType")]
        public List<AutreDepecheType> GetAllAutreDepecheType()
        {
            string key = "AutreDepecheTypeList";
            List<AutreDepecheType> AutreDepecheTypeList = new List<AutreDepecheType>();
            if (!_cache.TryGetValue<List<AutreDepecheType>>(key, out AutreDepecheTypeList))
            {
                AutreDepecheTypeList = _AutreDepecheService.GetAllAutreDepecheType();
                _cache.Set<List<AutreDepecheType>>(key, AutreDepecheTypeList);
            }

            return AutreDepecheTypeList;
        }

        [HttpPost]
        [Route("GetAllAutreDepecheByIdDFV/{id}")]
        public List<AutreDepeche> GetAllAutreDepecheByIdDFV(int id)
        {
            List<AutreDepeche> listAutreDepeche = _AutreDepecheService.GetAllAutreDepecheByIdDFV(id);
            return listAutreDepeche;
        }
    }
}
