﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Clic.BLL.IServices;
using Clic.BLL.IServices.Profil;
using Clic.BLL.Services.Commun;
using Clic.DAL.Entities.FicheDFV;
using Microsoft.AspNetCore.Mvc;
using PHENIX.BLL.IServices;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Clic.Web.API.Controllers
{
    [Route("api/dfv")]
    public class DFVController : Controller
    {
        private IDFVService _DFVService;
        private IZepService _ZepService;
        private IUtilisateurService _IUtilisateurService;
        public DFVController(IDFVService dfvService, 
            IUtilisateurService IUtilisateurService,
            IZepService ZepService)
        {
            _DFVService = dfvService;
            _IUtilisateurService = IUtilisateurService;
            _ZepService = ZepService;
        }

        // GET: api/<controller>
        [HttpGet]
        [Route("list")]
        public List<DFV> Get()
        {
            var identity = User.Identity as ClaimsIdentity;

            var profil = identity.Claims.Where(a => a.Type == Constantes.UserClaims.Profil).Select(a => a.Value).FirstOrDefault();
            var poste = identity.Claims.Where(a => a.Type == Constantes.UserClaims.Poste).Select(a => a.Value).FirstOrDefault();
            var posteBdd = this._ZepService.GetPosteByName(poste);
            var profilBdd = this._IUtilisateurService.GetProfilByName(profil);
            if (posteBdd == null)
                throw new Exception("Impossible de retrouver l'utilisateur connecté.");
            return _DFVService.GetDfvList(posteBdd, profilBdd);
        }

        [HttpGet]
        [Route("listPrevisionnelle")]
        public List<DFV> GetListPrevisionnelle()
        {
            var identity = User.Identity as ClaimsIdentity;

            var profil = identity.Claims.Where(a => a.Type == Constantes.UserClaims.Profil).Select(a => a.Value).FirstOrDefault();
            var poste = identity.Claims.Where(a => a.Type == Constantes.UserClaims.Poste).Select(a => a.Value).FirstOrDefault();
            var posteBdd = this._ZepService.GetPosteByName(poste);
            var profilBdd = this._IUtilisateurService.GetProfilByName(profil);
            if (posteBdd == null)
                throw new Exception("Impossible de retrouver l'utilisateur connecté.");
            return _DFVService.GetDfvListPrevisionnelle( profilBdd);
        }

        // GET api/<controller>/5
        [HttpGet]
        [Route("numero/{numero}")]
        public IActionResult GetByNumero(string numero)
        {
            try
            {
                DFV dfv = _DFVService.GetDFVByNumero(numero);
                return Ok(dfv);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        

        [HttpPost]
        [Route("id/{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                DFV dfv = _DFVService.GetDFVById(id);
                return Ok(dfv);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<controller>
        [HttpPost]
        [Route("create")]
        public DFV Create([FromBody]DFV dfv)
        {
            var identity = User.Identity as ClaimsIdentity;
            
            var profil = identity.Claims.Where(a => a.Type == Constantes.UserClaims.Profil).Select(a => a.Value).FirstOrDefault();
            var poste = identity.Claims.Where(a => a.Type == Constantes.UserClaims.Poste).Select(a => a.Value).FirstOrDefault();
            if (profil != Constantes.Profils.AgentCirculation )
                throw new Exception("Seul un AC peut créer des DFV.");
            var user  = _IUtilisateurService.GetUser(User.Identity.Name);
            if (user == null)
                throw new Exception("Impossible de retrouver l'utilisateur connecté.");
            dfv.ResponsableDFVId = this._ZepService.GetPosteByName(poste).Id;
            return _DFVService.AddOrUpdateDFV(dfv);
        }
        [HttpPost]
        [Route("createPrev")]
        public DFV CreatePrev([FromBody]DFV dfv)
        {
            var identity = User.Identity as ClaimsIdentity;

            var profil = identity.Claims.Where(a => a.Type == Constantes.UserClaims.Profil).Select(a => a.Value).FirstOrDefault();
            var poste = identity.Claims.Where(a => a.Type == Constantes.UserClaims.Poste).Select(a => a.Value).FirstOrDefault();
            if (profil != Constantes.Profils.PoleTravaux)
                throw new Exception("Seul le pôle travaux peut créer des DFV previsionelles");
            var user = _IUtilisateurService.GetUser(User.Identity.Name);
            if (user == null)
                throw new Exception("Impossible de retrouver l'utilisateur connecté.");
            dfv.ResponsableDFVId = dfv.Zep.PosteDemandeDFVId.Value;
            return _DFVService.AddOrUpdateDFV(dfv);
        }
        // POST api/<controller>
        [HttpPost]
        [Route("accordeDFV")]
        public IActionResult AccordeDFV([FromBody]DFV dfv)
        {
            return Ok(_DFVService.AccorderDFV(dfv));
        }
        // POST api/<controller>
        [HttpPost]
        [Route("update")]
        public IActionResult Update([FromBody]DFV dfv)
        {
            return Ok(_DFVService.AddOrUpdateDFV(dfv));
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {

        }

        [HttpPost]
        [Route("UpdateVerificationLiberationZep")]
        public IActionResult UpdateVerificationLiberationZep([FromBody]VerificationZepLibre_Ligne verif)
        {
            return Ok(_DFVService.UpdateVerificationZepLibre_Ligne(verif));
        }

        [HttpPost]
        [Route("validerDemande")]
        public IActionResult ValiderDemandeDFV([FromBody] DFV dfv)
        {
            return Ok(_DFVService.ValiderDemandeDFV(dfv));
        }


        [HttpPost]
        [Route("updateAiguilleDFV")]
        public IActionResult UpdateAiguilleDFV([FromBody]List<Aiguille_DFV> pListe)
        {
            return Ok(_DFVService.UpdateAiguilleDFV(pListe));
        }

        [HttpPost]
        [Route("checkOrUpdateDfvStatutLeveeMesure/{id}")]
        public IActionResult CheckOrUpdateDfvStatutLeveeMesure(int id)
        {
            return Ok(_DFVService.CheckOrUpdateDfvStatutLeveeMesure(id));
        }

        [HttpPost]
        [Route("restituer")]
        public IActionResult Restituer([FromBody]DFV pDFV)
        {
            return Ok(_DFVService.RestituerDFV(pDFV));
        }

        [HttpPost]
        [Route("ValiderDemandeVerification")]
        public IActionResult ValiderDemandeVerification([FromBody] VerificationZepLibre_Ligne verif)
        {
            return Ok(_DFVService.ValiderDemandeVerification(verif));
        }

        [HttpPost]
        [Route("ValiderConfirmationVerification")]
        public IActionResult ValiderConfirmationVerification([FromBody] VerificationZepLibre_Ligne verif)
        {
            return Ok(_DFVService.ValiderConfirmationVerification(verif));
        }
    }
        
}
