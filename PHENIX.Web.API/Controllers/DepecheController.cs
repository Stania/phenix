﻿using System.Collections.Generic;
using Clic.DAL.Entities.FicheDFV;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using PHENIX.BLL.IServices;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Clic.Web.API.Controllers
{
    [Route("api/Depeche")]
    public class DepcheController : Controller
    {
        private IDepecheService _DepecheService;
        private IMemoryCache _cache;

        public DepcheController(IDepecheService DepecheService,
            IMemoryCache memoryCache)
        {
            this._DepecheService = DepecheService;
            _cache = memoryCache;
        }
        
        [HttpPost]
        [Route("Create")]
        public Depeche Create([FromBody]Depeche depeche)
        {
           return _DepecheService.AddDepeche(depeche);
        }
        [HttpPost]
        [Route("GetListManoeuvreDepecheByIdDfv/{id}")]
        public List<Manoeuvre_Depeche> GetAllAutreDepecheByIdDFV(int id)
        {
            List<Manoeuvre_Depeche> listManoeuvreDepeche = _DepecheService.GetListManoeuvreDepecheByIdDfv(id);
            return listManoeuvreDepeche;
        }
        
        [HttpPost]
        [Route("AddDepecheMesure")]
        public Depeche AddDepecheMesure([FromBody] DepecheMesureRequest depecheMesure)
        {
            return _DepecheService.AddDepecheToMesureProtection(depecheMesure.Depeche, depecheMesure.MesureProtectionPriseId);
        }

        [HttpPost]
        [Route("AddDepecheNotification")]
        public Depeche AddDepecheNotification([FromBody] DepecheNotificationRequest depecheManoeuvre)
        {

            return _DepecheService.AddDepecheNotification(depecheManoeuvre.Depeche, depecheManoeuvre.ManoeuvreId); 
        }

        [HttpGet]
        [Route("GetAllDepecheType")]
        public List<DepecheType> GetAllDepecheType()
        {
            string key = "DepecheTypeList";
            List<DepecheType> depecheTypeList = new List<DepecheType>();
            if (!_cache.TryGetValue<List<DepecheType>>(key, out depecheTypeList))
            {
                depecheTypeList = _DepecheService.GetAllDepecheType();
                _cache.Set<List<DepecheType>>(key, depecheTypeList);
            }
            return depecheTypeList;
        }
    }

    public struct DepecheMesureRequest
    {
        public Depeche Depeche { get; set; }
        public int MesureProtectionPriseId { get; set; }
    }

    public struct DepecheNotificationRequest
    {
        public Depeche Depeche { get; set; }
        public int ManoeuvreId { get; set; }
    }


}
