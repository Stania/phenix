﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using PHNIX.BLL.IServices;
using Clic.DAL.Entities.FicheZep;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.Logging;
using Clic.BLL.Services.Commun;
using PHENIX.BLL.IServices.Commun;
using Microsoft.Extensions.Caching.Memory;
using Clic.Web.API.Common;

namespace PHENIX.Web.API.Controllers
{
    [Route("api/ImportZep")]
    public class ImportZepController : Controller
    {
        private IImportZepService _ImportZepService;
        private readonly ILogger _logger;
        private IMemoryCache _cache;

        public ImportZepController(IImportZepService ImportZepService, 
            ILogger<ImportZepController> logger,
             IMemoryCache memoryCache)
        {
            _ImportZepService = ImportZepService;
            _logger = logger;
            _cache = memoryCache;
        }

        [HttpPost, DisableRequestSizeLimit, Route("ImportZepFile")]
        public IActionResult ImportZepFile()
        {
            var files = Request.Form.Files; // now you have them
            Stream ficheStream = null;

            if (files != null && files.Count > 0)
            {
                ficheStream = files[0].OpenReadStream();
                IResult<int> nb = _ImportZepService.ImportFichesZepsExcel(ficheStream);
                if (_cache.TryGetValue(CacheKeys.ZepList, out object v)) _cache.Remove(CacheKeys.ZepList);
                return Ok(nb);
            }
            return BadRequest("Aucun fichier à importer");
        }
    }
}