﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clic.DAL.Entities.FicheDFV;
using Clic.Web.API.Common;
using Clic.Web.API.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using PHENIX.BLL.IServices;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Clic.Web.API.Controllers
{
    [Route("api/Manoeuvre")]
    public class ManoeuvreController : Controller
    {
        private IManoeuvreService _ManoeuvreService;
        private IMemoryCache _cache;

        public ManoeuvreController(IManoeuvreService ManoeuvreService,
            IMemoryCache memoryCache)
        {
            _ManoeuvreService = ManoeuvreService;
            _cache = memoryCache;
        }
       
        // POST api/<controller>
        [HttpPost]
        [Route("create")]
        public Manoeuvre Create([FromBody]Manoeuvre manoeuvre)
        {
           return  _ManoeuvreService.AddManoeuvre(manoeuvre);
        }

        // POST api/<controller>
        [HttpPost]
        [Route("update")]
        public Manoeuvre Update([FromBody]Manoeuvre manoeuvre)
        {
           return _ManoeuvreService.UpdateManoeuvre(manoeuvre);
        }

        // DELETE api/<controller>/5
        [HttpPost]
        [Route("DeleteManoeuvre/{id}")]
        public Object Delete(int id)
        {
            var manoeuvre = _ManoeuvreService.GetManoeuvreById(id);
            
            try
            {
                _ManoeuvreService.DeleteManoeuvre(manoeuvre);
                return new Object(); 
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetAllManoeuvreType")]
        public List<ManoeuvreType> GetAllManoeuvreType()
        {
            string key = CacheKeys.ManoeuvreTypeList;
            List<ManoeuvreType> ManoeuvreTypeList = new List<ManoeuvreType>();
            if (!_cache.TryGetValue<List<ManoeuvreType>>(key, out ManoeuvreTypeList))
            {
                ManoeuvreTypeList = _ManoeuvreService.GetAllManoeuvreType();
                _cache.Set<List<ManoeuvreType>>(key, ManoeuvreTypeList);
            }
            return ManoeuvreTypeList;
        }

        [HttpPost]
        [Route("EffectueManoeuvre")]
        public Manoeuvre EffectueManoeuvre([FromBody] EffectueManoeuvreViewModel manoeuvre)
        {
            if(manoeuvre.Manoeuvre.ParDepeche == true)
            {
                manoeuvre.Manoeuvre.Manoeuvre_DepecheList.Add(new Manoeuvre_Depeche() { Depeche = manoeuvre.DepecheToAdd });
            }   
            return _ManoeuvreService.EffectueManoeuvre(manoeuvre.Manoeuvre);
        }
    }
}
