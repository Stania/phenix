﻿using Clic.BLL.IServices;
using Clic.DAL.Entities.FicheDFV;
using Microsoft.AspNetCore.Mvc;
using PHENIX.BLL.IServices;
using System;

namespace PHENIX.Web.API.Controllers
{
    [Route("api/mesures")]
    public class MesureProtectionController : Controller
    {
        private IMesureService _MesureService;
        private IZepService _ZepService;
        
        public MesureProtectionController(IZepService zepS, IMesureService mesureS)
        {
            this._MesureService = mesureS;
            this._ZepService = zepS;
        }

        [Route("dfv/{IdDfv}")]
        [HttpGet]
        public IActionResult GetMesuresByDFVId(int IdDfv)
        {
            var liste = _MesureService.GetMesuresProtectionByDfvId(IdDfv);
            return Ok(liste);
        }

        [Route("create")]
        [HttpPost]
        public IActionResult AddMesurePrise([FromBody] MesureProtectionPrise mesure)
        {
            MesureProtectionPrise result = _MesureService.AddMesureProtectionPrise(mesure);
            return Ok(result);
        }

        [Route("update")]
        [HttpPost]
        public IActionResult UpdateMesurePrise([FromBody] MesureProtectionPrise mesure)
        {
            MesureProtectionPrise result = _MesureService.UpdateMesureProtectionPrise(mesure);
            return Ok(result);
        }

        [Route("delete")]
        [HttpPost]
        public IActionResult DeleteMesurePrise([FromBody] MesureProtectionPrise mesure)
        {
            //A remplacer par l'id
            int result = _MesureService.DeleteMesureProtectionPrise(mesure);
            return Ok(result);
        }

        [Route("postes")]
        [HttpGet]
        public IActionResult GetAllPostes()
        {
            return Ok(_ZepService.GetAllPostes());
        }
    }
}