﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Entities.FicheZep;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using PHENIX.BLL.IServices;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Clic.Web.API.Controllers
{
    [Route("api/PointRemarquable")]
    public class PointRemarquableController : Controller
    {
        private IPointRemarquableService _PointRemarquableService;
        private IMemoryCache _cache;

        public PointRemarquableController(IPointRemarquableService PointRemarquableService,
            IMemoryCache memoryCache)
        {
            _PointRemarquableService = PointRemarquableService;
            _cache = memoryCache;
        }
        [HttpGet]
        [Route("GetListPointRemarquableByIdZep/{idZep}")]
        public List<PointRemarquable_Zep> GetListPointRemarquableByIdZep([FromHeader] int IdZep)
        {
            List<PointRemarquable_Zep> list = _PointRemarquableService.GetListPointRemarquableByIdZep(IdZep);
            return list;
        }

    }
}
