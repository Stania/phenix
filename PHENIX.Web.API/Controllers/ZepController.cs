﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Clic.BLL.IServices;
using Clic.BLL.IServices.Profil;
using Clic.BLL.Services.Commun;
using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Entities.FicheZep;
using Clic.Web.API.Common;
using Clic.Web.API.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace PHENIX.Web.API.Controllers
{
    [Route("api/Zep")]
    public class ZepController : Controller
    {
        private IZepService _ZepService;
        private IUtilisateurService _IUtilisateurService;
        private IMemoryCache _cache;

        public ZepController(IZepService ZepService, IUtilisateurService IUtilisateurService, IMemoryCache memoryCache)
        {
            _ZepService = ZepService;
            _IUtilisateurService = IUtilisateurService;
            _cache = memoryCache;
        }

        // GET api/values
        [HttpGet]
        [Route("GetAllZep")]
        public List<Zep> GetAllZep()
        {
            string key = CacheKeys.ZepList;
            List<Zep> listZep = new List<Zep>();
            if (!_cache.TryGetValue<List<Zep>>(key, out listZep))
            {
                listZep = _ZepService.GetAllZeps();
                _cache.Set<List<Zep>>(key, listZep);
            }

            return listZep;
        }

        [HttpGet]
        [Route("GetZepsAutorises")]
        public List<Zep> GetZepsAutorises()
        {
            List<Zep> zepList = new List<Zep>();
            var identity = User.Identity as ClaimsIdentity;
            var poste = identity.Claims.Where(a => a.Type == Constantes.UserClaims.Poste).Select(a => a.Value).FirstOrDefault();
            var profil = identity.Claims.Where(a => a.Type == Constantes.UserClaims.Profil).Select(a => a.Value).FirstOrDefault();

            var user = _IUtilisateurService.GetUser(User.Identity.Name);
            if (user == null)
                throw new Exception("Impossible de retrouver l'utilisateur connecté.");


            string key = CacheKeys.ZepAutoriseList;
            if (!_cache.TryGetValue<List<Zep>>(key, out zepList))
            {
                zepList = profil == Constantes.Profils.PoleTravaux ? _ZepService.GetZepsAutorises(poste, profil) : _ZepService.GetZepsAutorises(poste);
                _cache.Set<List<Zep>>(key, zepList);
            }

            //listZep = _ZepService.GetZepsAutorises(poste);
            return zepList;
        }

        // GET api/values
        [HttpGet]
        [Route("GetAllPostes")]
        public List<Poste> GetAllPostes()
        {
            string key = CacheKeys.PosteList;
            List<Poste> posteList = new List<Poste>();
            if (!_cache.TryGetValue<List<Poste>>(key, out posteList))
            {
                posteList = _ZepService.GetAllPostes();
                _cache.Set<List<Poste>>(key, posteList);
            }
            return posteList;
        }

        [HttpGet]
        [Route("GetAllZepTypes")]
        public List<ZepType> GetAllZepTypes()
        {
            string key = "ZepTypeList";
            List<ZepType> typeZepList = new List<ZepType>();
            if (!_cache.TryGetValue<List<ZepType>>(key, out typeZepList))
            {
                typeZepList = _ZepService.GetAllZepTypes();
                _cache.Set<List<ZepType>>(key, typeZepList);
            }
            return typeZepList;
        }

        [HttpGet]
        [Route("GetZepById/{id}")]
        public Zep GetZepById([FromHeader] int Id)
        {
            Zep zep = _ZepService.GetZepById(Id);
            return zep;
        }
        

        [HttpGet]
        [Route("GetNotDrawedZepList")]
        public List<Zep> GetNotDrawedZepList()
        {
            List<Zep> listZep = _ZepService.GetNotDrawedZepList();
            return listZep;
        }

        [HttpGet]
        [Route("GetDrawedZepList")]
        public List<Zep> GetDrawedZepList()
        {
            
            List<Zep> listZep = _ZepService.GetDrawedZepList();
            return listZep;
        }

        [HttpGet]
        [Route("GetEtatOpeationnel")]
        public List<ZepSynoptiqueViewModel> GetEtatOpeationnel()
        {

            List<ZepSynoptiqueViewModel> listZep = _ZepService.GetEtatOperationnel()
                .Select(a => new ZepSynoptiqueViewModel(a)).ToList();
            return listZep;
        }



        [HttpPost]
        [AllowAnonymous]
        [Route("LinkDrawZep")]
        public Zep LinkDrawZep([FromBody] Zep pZep)
        {
            Zep zep = _ZepService.UpdateObjetGraphique(pZep);
            return zep;
        }
    }
}