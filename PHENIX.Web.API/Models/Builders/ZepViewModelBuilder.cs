﻿using Clic.DAL.Entities.FicheZep;
using Clic.Web.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clic.Web.API.Builders
{
    public static class ZepViewModelBuilder
    {
        public static ZepViewModel GetZepViewModel(Zep zep)
        {
            ZepViewModel zvm = new ZepViewModel
            {
                Zep = zep
            };

            return zvm;
        }

        public static List<ZepViewModel> GetListZepViewModel(List<Zep> zeps)
        {
            List<ZepViewModel> ListeZep = new List<ZepViewModel>();
            zeps.ForEach(t => {
                ListeZep.Add(ZepViewModelBuilder.GetZepViewModel(t));
            });
            return ListeZep;
        }
    }
}
