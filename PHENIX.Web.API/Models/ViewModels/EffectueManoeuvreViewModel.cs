﻿using Clic.DAL.Entities.FicheDFV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clic.Web.API.Models.ViewModels
{
    public class EffectueManoeuvreViewModel
    {
        public Manoeuvre Manoeuvre { get; set; }
        public Depeche DepecheToAdd { get; set; }
    }
}
