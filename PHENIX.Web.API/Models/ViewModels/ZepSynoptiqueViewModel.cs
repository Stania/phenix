﻿using Clic.BLL.Services.Commun;
using Clic.DAL.Entities.FicheDFV;
using Clic.DAL.Entities.FicheZep;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clic.Web.API.ViewModels
{
    public class ZepSynoptiqueViewModel
    {
        public Zep Zep { get; set; }

        public Boolean IsTTXOnIt { get; set; }
        public Boolean IsGroup { get; set; }

        public DFV DFV { get; set; }

        public ZepSynoptiqueViewModel(Zep pZep)
        {
            this.Zep = pZep;
            if (pZep != null)
            {
                if (this.Zep.CompositionGroupeZepList.Any()) this.IsGroup = true;
                if (pZep.DFVList != null)
                {
                    this.DFV = pZep.DFVList.OrderBy(a => a.DateDebutApplicabilite).FirstOrDefault();
                    if (this.DFV != null)
                    {
                        IsTTXOnIt = 
                            (this.DFV.OccupationList != null && this.DFV.OccupationList.Any()) || 
                            (this.DFV.ManoeuvreList != null && this.DFV.ManoeuvreList.Any(a => a.ManoeuvreType.Nom == Constantes.ManoeuvreTypes.Engagement));
                    }
                }
                this.Zep.DFVList = null;

            }
        }

        public ZepSynoptiqueViewModel()
        {

        }
        
    }
}
