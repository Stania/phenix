﻿using Clic.DAL.Entities.FicheZep;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clic.Web.API.ViewModels
{
    public class ZepViewModel
    {
        public Zep Zep { get; set; }

        public bool IsNumeroValide
        {
            get
            {
                return Zep.Numero.Length > 2;
            }
        }
    }
}
