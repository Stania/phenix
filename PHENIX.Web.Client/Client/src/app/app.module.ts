import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateService, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { LayoutModule } from './layout/layout.module';
import { SharedModule } from './shared/shared.module';
import { RoutesModule } from './routes/routes.module';
import {WindowRef} from './shared/global/WindowRef';
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import { FicheDfvModule } from './routes/fiche-dfv/fiche-dfv.module';
import { FicheZepModule } from './routes/fiche-zep/fiche-zep.module';

// https://github.com/ocombe/ng2-translate/issues/218
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        HttpClientModule,
        BrowserAnimationsModule, // required for ng2-tag-input
        BrowserModule,
        FormsModule,
        HttpModule,
        CoreModule,
        LayoutModule,
        SharedModule.forRoot(),
        RoutesModule,

        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        BrowserModule,
        FicheDfvModule,
        FicheZepModule,

    ],
    providers: [WindowRef ],
    bootstrap: [AppComponent]
})
export class AppModule { }
