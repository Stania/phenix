import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthenticationService } from '../../shared/services/administration/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authenticationService: AuthenticationService, private router: Router) { }

    canActivate() {
        if (this.authenticationService.isAuthenticated()) {
            return true;
        } else {
            this.router.navigate([this.authenticationService.getLoginUrl()]);
            return false;
        }
    }
}
