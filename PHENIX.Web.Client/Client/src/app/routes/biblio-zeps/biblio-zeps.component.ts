import { Component, OnInit } from '@angular/core';
import { ZepService } from '../../shared/services/ficheZep/zep.service';
import { OuiNonPipe } from '../../../app/shared/directives/pipes/boolean.pipe';
import { ColorsService } from '../../shared/colors/colors.service';
import { BibliothequeZepVM } from '../../shared/models/ViewModels/BibliothequeZepVM';
import { Zep } from '../../shared/models/FicheZep/zep.models';
import { BsModalService, BsModalRef, tr } from 'ngx-bootstrap';
import { FicheZepComponent } from '../fiche-zep/fiche-zep.component';
import { ImportZepService } from '../../shared/services/ficheZep/importZep.service';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Ng4FilesService, Ng4FilesSelected, Ng4FilesConfig, Ng4FilesStatus } from '../../shared/directives/ng4-files';
import { Router } from '@angular/router';
import { PhenixToasterService } from '../../shared/settings/phenix-toaster.service';
import { Result, ResultStatus } from '../../shared/models/Commun/Result';
import swal from 'sweetalert2';
import { Constantes_Phenix } from '../../shared/models/Commun/constantes.model';
import { UtilisateurVM } from '../../shared/models/ViewModels/UtilisateurVM';
import { AuthenticationService } from '../../shared/services/administration/authentication.service';

@Component({
    selector: 'app-biblio-zeps',
    templateUrl: './biblio-zeps.component.html',
    styleUrls: ['./biblio-zeps.component.scss']
})

export class BiblioZepsComponent implements OnInit {
    modelRef: BsModalRef;
    /* When we select file */
    form = new FormGroup({});
    Name: string;
    myFile: File; /* property of File type */
    fileChange(files: any) {
        console.log(files);
        this.myFile = files[0].nativeElement;
    }
    uploadForm: any;
    importedFile: any;
    public selectedFiles;
    public loading = false;
    public currentUser : UtilisateurVM;
    public length: number = 0;

    public zepsList: Array<BibliothequeZepVM>;
    public zepsListApplicable: Array<BibliothequeZepVM>;
    public zepsListValide: Array<BibliothequeZepVM>;
    public zepsListEnTravail: Array<BibliothequeZepVM>;

    constructor(private zepService: ZepService,
        private modalService: BsModalService,
        private importZepService: ImportZepService,
        private ng4FilesService: Ng4FilesService,
        private router: Router,
        private toastSrv: PhenixToasterService,
        private authSrv: AuthenticationService
      ) {
        //this.length = this.zepsList.length;

        this.uploadForm = new FormGroup({
            file1: new FormControl()
        });
        this.zepsList = new Array<BibliothequeZepVM>();
        this.zepsListApplicable = new Array<BibliothequeZepVM>();
        this.zepsListValide = new Array<BibliothequeZepVM>();
        this.zepsListEnTravail = new Array<BibliothequeZepVM>();
    }
    isAutorised(){
        if(this.currentUser.Profil == Constantes_Phenix.Profils.PoleTravaux)
        return true;
        else return false;
    }

    private testConfig: Ng4FilesConfig = {
        acceptExtensions: ['xlsm'],
        // acceptExtensions: ['jpg'],
        maxFilesCount: 1,
        maxFileSize: 5120000,
        totalFilesSize: 10120000
    };
    public ngOnInit(): void {
        this.ng4FilesService.addConfig(this.testConfig);
        this.Displayzeps();
        this.actualiseListZep();
        this.currentUser = this.authSrv.getLoggedUser();
    }

    public Displayzeps(){
        this.loading = true;
        const result = this.zepService.getAllZep();
        result.subscribe(
            data => {
                data.forEach(element => {
                    element.DateDebutApplicabilite = new Date(element.DateDebutApplicabilite);
                    element.DateFinApplicabilite = new Date(element.DateFinApplicabilite);
                });

                this.zepsList = data.map(a => new BibliothequeZepVM(a));

                this.zepsList.forEach(x => {
                    x.ButtonZep = "<button class='btn btn-primary btn-xs'><i class='fa fa-edit'></i></button>";
                })

                this.length = this.zepsList.length;
                this.actualiseListZep();
            },
            error => {
                this.toastSrv.showError("Erreur", error.error.error);
            },
            () => this.loading = false);
    }

    actualiseListZep(){
        this.zepsListApplicable = this.zepsList.filter(x =>
            x.Zep.DFVRestitueeOccupee === true)
        this.zepsListValide = this.zepsList.filter(x =>
            x.Zep.DFVRestitueeOccupee === false)
        this.zepsListEnTravail = this.zepsList.filter(x =>
            x.Zep.DFVRestitueeOccupee == null)
      }

    goSynoCreation() {
        this.router.navigateByUrl('/synoptique/creation');
    };

    onFileChangess(event) {
        console.log(event.target.files);
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.form.get('zepImport').setValue({
                    filename: file.name,
                    filetype: file.type,

                    value: reader.result.split(',')[1]
                })
            };
        }
    }

    onFileChange(e) {
        this.importedFile = e.target.files[0];
        console.log(this.importedFile + " importedFile est ici");

        const uploadData: FormData = new FormData();
        var _data = { filename: 'File' }

        uploadData.append("data", JSON.stringify(this.importedFile), this.importedFile.name);
        var options = { content: uploadData };

        const result = this.importZepService.importZepFile(uploadData);
        result.subscribe(
            data => {
                console.log(this);
            },
            error => {
                this.toastSrv.showError("Erreur", error.error.error);
            });
    }

    handleResponse(response: any) {
        console.log(response + "API succeeded !");
    }

    onSubmit() {
        let formdata = this.form.value;
        console.log(this.uploadForm)
        formdata.append("avatar", this.importedFile);

        const result = this.importZepService.importZepFile(formdata);
        result.subscribe(
            data => {
                this.handleResponse(data);
            },
            error => {
                this.toastSrv.showError("Erreur", error.message);
            });
    }

    public filesSelect(selectedFiles: Ng4FilesSelected): void {
        this.loading = true;
        if (selectedFiles.status !== Ng4FilesStatus.STATUS_SUCCESS) {
            this.selectedFiles = selectedFiles.status;
            return;
        }

        if (selectedFiles.files.length > 1) {
            alert('Merci de sélectionner un seul fichier');
        }

        var file = selectedFiles.files;

        const result = this.importZepService.importZepFile(file)
        result.subscribe(
            data => {
                if(data.Status === ResultStatus.Success){
                    this.toastSrv.showSuccess("Succès","Import terminé " + data.Value + " Fiche(s) Zep importée(s). ");
                    if(data.ErrorMessage != "")
                        swal("Zeps non importées", data.ErrorMessage,"error");
                }
                this.loading = false;
                this.Displayzeps();
            },
            error => {
                if(error.status != 200){
                    this.toastSrv.showError("Erreur", error.error.error);
                    console.log(error.error.error);
                }
                this.loading = false;
            });
    }
}








