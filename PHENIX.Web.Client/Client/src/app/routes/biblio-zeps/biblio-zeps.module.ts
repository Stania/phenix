import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, Routes, RouterModule } from '@angular/router';
import { BiblioZepsComponent } from './biblio-zeps.component';
import {OuiNonPipe} from '../.././shared/directives/pipes/boolean.pipe';
import { FormsModule } from '@angular/forms';
import { Ng4FilesModule } from '../../shared/directives/ng4-files';
import { SharedModule } from '../../shared/shared.module';
import { TabZepComponent } from './tab-zep/tab-zep.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { FilterZepPipe } from '../../shared/directives/pipes/filterZep.pipe';

const routes : Routes = [
  {path:'', component: BiblioZepsComponent}
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    Ng4FilesModule,
    SharedModule,
    NgxPaginationModule
  ],
  declarations: [
    BiblioZepsComponent, 
    OuiNonPipe,
    TabZepComponent,
    FilterZepPipe
  ],
  exports:[RouterModule, TabZepComponent]
})
export class BiblioZepsModule { }
