import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabZepComponent } from './tab-zep.component';

describe('TabZepComponent', () => {
  let component: TabZepComponent;
  let fixture: ComponentFixture<TabZepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabZepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabZepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
