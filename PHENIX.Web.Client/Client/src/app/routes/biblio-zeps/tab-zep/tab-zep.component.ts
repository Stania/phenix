import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ZepService } from '../../../shared/services/ficheZep/zep.service';
import { OuiNonPipe } from '../../../../app/shared/directives/pipes/boolean.pipe';
import { ColorsService } from '../../../shared/colors/colors.service';
import { BibliothequeZepVM } from '../../../shared/models/ViewModels/BibliothequeZepVM';
import { Zep } from '../../../shared/models/FicheZep/zep.models';
import { BsModalService, BsModalRef, tr } from 'ngx-bootstrap';
import { FicheZepComponent } from '../../fiche-zep/fiche-zep.component';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';
import { PhenixToasterService } from '../../../shared/settings/phenix-toaster.service';

@Component({
  selector: 'tab-zep',
  templateUrl: './tab-zep.component.html',
  styleUrls: ['./tab-zep.component.scss']
})
export class TabZepComponent implements OnInit {
  @Input() zepsList: Array<BibliothequeZepVM>;
  @Input() isApplicable : boolean;
  @Input() isValide : boolean;
  @Input() isEnTravail : boolean;
  modelRef: BsModalRef;
  public loading = false;
  filter : string;
  p : any;

  constructor(private zepService: ZepService,
    private modalService: BsModalService,
    private router: Router,
    private toastSrv: PhenixToasterService)   {
        this.filter = "";
    }

public ngOnInit(): void {
}

public showZep(event: any): any {
  this.zepService.getZepById(event.Zep.Id).subscribe(
      data => {
          this.modelRef = this.modalService.show(FicheZepComponent, { class: "modal-lg" });
          this.modelRef.content.zep = data;
          this.modelRef.content.PopulateListes();
      },
      err => console.log(err)
  )
}

}

