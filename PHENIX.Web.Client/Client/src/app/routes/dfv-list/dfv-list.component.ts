import { Component, OnInit ,Input} from '@angular/core';
import { DfvListService } from '../../shared/services/ficheDfv/dfv-list.service';
import { DFV } from '../../shared/models/DFV/dfv.models';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { FicheDfvComponent } from '../fiche-dfv/fiche-dfv.component';
import { FicheDfvService } from '../../shared/services/ficheDfv/fiche-dfv.service';
import { PhenixToasterService } from '../../shared/settings/phenix-toaster.service';
import { AddDfvComponent } from './modal/add-dfv/add-dfv.component';
import { AuthenticationService } from '../../shared/services/administration/authentication.service';
import { UtilisateurVM } from '../../shared/models/ViewModels/UtilisateurVM';
import { Constantes_Phenix } from '../../shared/models/Commun/constantes.model';
import { FormGroup } from '@angular/forms';


@Component({
	selector: 'dfv-list',
	templateUrl: 'dfv-list.component.html',
})
export class DfvListComponent implements OnInit {
	modelRef: BsModalRef;
	showingDFV: boolean = false;
	loading :boolean = false;

  public dfvList: Array<DFV>;
  public dfvListEnCours: Array<DFV>;
  public dfvListRestitue: Array<DFV>;
	public currentUser: UtilisateurVM;

	constructor(
    private modalService: BsModalService,
    private authSrv: AuthenticationService,
		private ficheDFVService: FicheDfvService,
		private toastSrv: PhenixToasterService) {
      this.dfvList = new Array<DFV>();
      this.dfvListEnCours = new Array<DFV>();
			this.dfvListRestitue = new Array<DFV>();
	}

	public ngOnInit(): void {
		this.loading = true;
		this.currentUser = this.authSrv.getLoggedUser();
		const result = this.ficheDFVService.getAllDFV();
		result.subscribe(
			data => {
				this.dfvList = data;
        this.actualiseListDfv();
			},
			error => {
				this.toastSrv.showError("Erreur", error.error.error);
			},
		() => this.loading = false);
  }
  addDfv(dfv : DFV){
    this.dfvList.push(dfv);
    this.actualiseListDfv();
  }
	public showDFV(dfv: DFV): any {
    if(this.modelRef != null)
    return;
      this.showingDFV = true;
      let indexDFv = this.dfvList.findIndex(x => x.Id == dfv.Id);
      if(indexDFv == -1)
      {
        this.toastSrv.showError("Erreur","Impossible de retrouver la Dfv.");
        return;
      }
      this.loading = true;
			this.ficheDFVService.getDFVById(dfv.Id).subscribe(
				data => {
					this.modelRef = this.modalService.show(FicheDfvComponent, { class: "modal-lg",ignoreBackdropClick: true });
					this.modelRef.content.dfv = data;
					this.modelRef.content.populateListe();
          this.showingDFV = false;
          (<FicheDfvComponent>this.modelRef.content).onClose.subscribe(result => {
            this.modelRef = null;
            if (result != null) {
              this.dfvList[indexDFv] = result;
              this.actualiseListDfv();
            } else {

            }
          });
				},
        err => console.log(err),
        ()=>this.loading = false
      )
      }

      actualiseListDfv(){
        this.dfvListEnCours = this.dfvList.filter(x =>
          x.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Accordee ||
          x.StatusZep.Statut == Constantes_Phenix.ZepStatuts.EnCreation ||
          x.StatusZep.Statut == Constantes_Phenix.ZepStatuts.EnAccord)
        this.dfvListRestitue = this.dfvList.filter(x =>
            x.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Restituee ||
            x.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Refusee ||
            x.StatusZep.Statut == Constantes_Phenix.ZepStatuts.LeveeMesure ||
            x.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Annulee)
      }
    }
