import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, Routes, RouterModule } from '@angular/router';
import { DfvListComponent } from './dfv-list.component';

import { SharedModule } from '../../shared/shared.module';
import { FicheDfvModule } from '../fiche-dfv/fiche-dfv.module';
import { AddDfvComponent } from './modal/add-dfv/add-dfv.component';
import { FormsModule } from '@angular/forms';
import { TabDfvComponent } from './tab-dfv/tab-dfv.component';
import { FilterDfvPipe } from '../../shared/directives/pipes/filterDfv.pipe';
import { OrderByStatutPipe } from '../../shared/directives/pipes/sortDfvState.pipe';

const routes : Routes = [
  {path:'', component: DfvListComponent},
]

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
  ],
  declarations: [
    DfvListComponent,
    AddDfvComponent,
    TabDfvComponent,

],
  exports:[
    RouterModule,
    TabDfvComponent,
  ],
  entryComponents:[
    AddDfvComponent
  ]
})
export class DfvListModule { }
