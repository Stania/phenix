import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DFV } from '../../../shared/models/DFV/dfv.models';
import { AuthenticationService } from '../../../shared/services/administration/authentication.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { FicheDfvService } from '../../../shared/services/ficheDfv/fiche-dfv.service';
import { PhenixToasterService } from '../../../shared/settings/phenix-toaster.service';
import { AddDfvComponent } from '../modal/add-dfv/add-dfv.component';
import { FicheDfvComponent } from '../../fiche-dfv/fiche-dfv.component';
import { UtilisateurVM } from '../../../shared/models/ViewModels/UtilisateurVM';
import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';
@Component({
  selector: 'app-tab-dfv',
  templateUrl: './tab-dfv.component.html',
  styleUrls: ['./tab-dfv.component.scss']
})
export class TabDfvComponent implements OnInit {
  @Input() dfvList: Array<DFV>;
  @Input() isRestitue : boolean;
  @Output() showDfv = new EventEmitter<DFV>();
  @Output() addDfv = new EventEmitter<DFV>();

  filter : string;
	modelRef: BsModalRef;
	public currentUser: UtilisateurVM;

  constructor(
     private modalService: BsModalService,
     private authSrv: AuthenticationService,
		private ficheDFVService: FicheDfvService,
		private toastSrv: PhenixToasterService) {
      this.filter = "";
    }

  ngOnInit() {
    this.currentUser = this.authSrv.getLoggedUser();

  }

  showDFV(dfv:DFV){
    this.showDfv.emit(dfv);
  }
  canAddDfv(): boolean {
		if (this.currentUser != null)
			return this.currentUser.Profil == Constantes_Phenix.Profils.AgentCirculation;
		else
			return false;
  }
  public showAddDFVModal(): any {
		this.modelRef = this.modalService.show(AddDfvComponent, { class: "modal-md",ignoreBackdropClick: true });
		(<AddDfvComponent>this.modelRef.content).onClose.subscribe(result => {
			if (result != null) {
        this.addDfv.emit(result);

				this.showDFVAfterAdd(result);
			} else {

			}
		});
  }
  public showDFVAfterAdd(dfv: DFV) {
		this.ficheDFVService.getDFVById(dfv.Id).subscribe(
			data => {
				this.modelRef = this.modalService.show(FicheDfvComponent, { class: "modal-lg",ignoreBackdropClick: true });
				this.modelRef.content.dfv = data;
				this.modelRef.content.populateListe();
			},
			err => console.log(err)
		)
  }
  public getIconByStatut(statut : string) : string{
      if( statut == Constantes_Phenix.ZepStatuts.Restituee)
        return "badge badge-success";
      else if(statut == Constantes_Phenix.ZepStatuts.EnCreation ||statut == Constantes_Phenix.ZepStatuts.Accordee || statut == Constantes_Phenix.ZepStatuts.LeveeMesure)
        return "badge badge-info";
        else if(statut == Constantes_Phenix.ZepStatuts.EnAccord)
          return "badge badge-warning";
      else if(statut == Constantes_Phenix.ZepStatuts.Refusee ||
         statut == Constantes_Phenix.ZepStatuts.Annulee)
         return "badge badge-danger";
  }
}
