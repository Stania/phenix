import { Component, OnInit, Input, OnChanges, SimpleChanges, Output } from '@angular/core';
import { DepecheType, Depeche, Manoeuvre, DFV, Occupation } from '../../../shared/models/DFV/dfv.models';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { FicheDfvService } from '../../../shared/services/ficheDfv/fiche-dfv.service';
import { DepechesService } from '../../../shared/services/ficheDfv/depeches.service';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';
import * as _ from "lodash";
import { PointRemarquable_Zep } from '../../../shared/models/FicheZep/zep.models';
import { PointRemarquableService } from '../../../shared/services/pointRemarquable/point_remarquable.service';
import { ManoeuvreService } from '../../../shared/services/manoeuvre/manoeuvre.service';
import { OccupationService } from '../../../shared/services/occupation/occupation.service';

@Component({
  selector: 'app-depeche',
  templateUrl: './depeche.component.html',
  styleUrls: ['./depeche.component.scss']
})
export class DepecheComponent implements OnInit, OnChanges {

  @Input() depecheType : DepecheType;
  @Input() mesureId : number;
  @Input() manoeuvreId : number;
  @Input() dfv : DFV;
  @Input() listPointRemarquable : Array<PointRemarquable_Zep>;


  zepLibre : boolean = true;
  posteNotif : string;
  occupationResitutionList : Array<Occupation>;
  degagementList : Array<Manoeuvre>;
  listManoeuvrePosteNotif : Array<Manoeuvre>;



  depeche : Depeche;


  public onClose: Subject<Depeche>;

  form : FormGroup;
  fields:FormlyFieldConfig[];
  options : FormlyFormOptions = {
    formState: {
      disabled: false,
      awesomeIsForced: false,
      model: this.depeche,
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initForm();
  }

  constructor(private depechesService : DepechesService,  public modalRef: BsModalRef,private occupationService : OccupationService,
    private manoeuvreService : ManoeuvreService,
    private pointRemarquableService : PointRemarquableService) {
    this.form = new FormGroup({});
    this.depeche = new Depeche();
    this.onClose = new Subject();
    this.dfv = new DFV();
    this.posteNotif = "";
    this.occupationResitutionList = new Array<Occupation>();
    this.degagementList = new Array<Manoeuvre>();
    this.listPointRemarquable = new Array<PointRemarquable_Zep>();
    // this.listManoeuvrePosteNotif = new Array<Manoeuvre>();
    this.initForm();


  }

  initForm(){

    this.fields = [
      {
        fieldGroupClassName:"row",
        fieldGroup:[
          {
              key:"NumeroDonne",
              className:"col-md-6",
              type:"input",
              templateOptions:{
                type:"text",
                label: 'Numéro donné',
                required: true,
              }
          },
          {
            key:"NumeroRecu",
            className:"col-md-6",
            type:"input",
            templateOptions:{
              type:"text",
              label: 'Numéro reçu',
              required: true,
            }
          }
        ]
      },
      {
        key:"DateDepeche",
        className:"col-md-12",
        type:"datetimepicker",
        defaultValue : new Date(this.depeche.DateDepeche),
        templateOptions:{
          label: 'Date et heure de la dépêche',
          required: true, showTime : true
        }
      }
    ]
  }

  ngOnInit() {

  }

  initCollation(){
    if(!_.isEmpty(this.dfv)){
      if(this.depecheType.Nom == Constantes_Phenix.DepecheType.RestitutionEngagement|| this.depecheType.Nom == Constantes_Phenix.DepecheType.RestitutionDegagement){
         let manoeuvre = this.dfv.ManoeuvreList.find(x => x.Id == this.manoeuvreId);
         if(manoeuvre != null){
           this.posteNotif = manoeuvre.PointRemarquable_Zep.PosteConcerne.Nom;
            //  let listManoeuvrePosteNotif = this.dfv.ManoeuvreList.filter(x => x.PointRemarquable_Zep.PosteConcerneId == manoeuvre.PointRemarquable_Zep.PosteConcerneId)
            // if(listManoeuvrePosteNotif.length> 0)
            // {
            //   this.listManoeuvrePosteNotif = listManoeuvrePosteNotif;
            // }
            this.occupationResitutionList = this.getOccupationRestitutionZep();
            this.degagementList = this.getManoeuvreDegagement();
            if (this.occupationResitutionList.length > 0 || this.degagementList.length > 0) {
              this.zepLibre = false;
            }
         }
      }
    }
  }

  getOccupationRestitutionZep():Array<Occupation>{
    return this.occupationService.filtrerOccupations(Constantes_Phenix.OccupationTypes.OccupeeALaRestitution, this.dfv.OccupationList);
  }
  getManoeuvreDegagement():Array<Manoeuvre>{
    return this.manoeuvreService.filtrerManoeuvre(Constantes_Phenix.ManoeuvreTypes.Degagement, this.dfv.ManoeuvreList)
      .filter(a=>a.Verbale !== true && a.ParEcrit !== true && a.ParDepeche !== true);
  }
  getPointRemarquableById(idPr : number):PointRemarquable_Zep{
    return this.pointRemarquableService.getPointRemarquableById(idPr,this.listPointRemarquable)
  }

  public closePopup(){
    this.modalRef.hide();
    this.onClose.next(this.depeche);
  }

  submit() {
    this.depeche.DepecheTypeId = this.depecheType.Id;
    var response = null;
    if(this.depecheType.Nom == Constantes_Phenix.DepecheType.MesureProtection){
      response = this.depechesService.AddDepeche(this.depeche, this.mesureId);
    }else if(this.depecheType.Nom == Constantes_Phenix.DepecheType.Notification || this.depecheType.Nom == Constantes_Phenix.DepecheType.Restitution){
      response = this.depechesService.AddDepecheManoeuvre(this.depeche, this.manoeuvreId);
    }
    else{
      response = this.depechesService.AddDepeche(this.depeche, this.mesureId);
    }

    response.subscribe(
      data => {
        this.depeche = data;
        this.closePopup();
      },
      err => {
        alert("erreur");
        console.log(err);
      }
    );

  }

}
