import * as moment from 'moment';
import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { DFV } from '../../../shared/models/DFV/dfv.models';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormlyFormOptions } from '@ngx-formly/core';
import { FicheDfvService } from '../../../shared/services/ficheDfv/fiche-dfv.service';

@Component({
  selector: 'app-dfv-bloc-application',
  templateUrl: './dfv-bloc-application.component.html',
  styleUrls: ['./dfv-bloc-application.component.scss']
})
export class DfvBlocApplicationComponent implements OnInit, OnChanges {

  @Input() public dfv: DFV;
  @Output() dfvChange = new EventEmitter<DFV>();
  @Input() isPosteCreateur : boolean;


  isBtnDemandeDisabled = false;
  form = new FormGroup({});
  fields: FormlyFieldConfig[];
  options: FormlyFormOptions = {
    formState: {
      disabled: false,
      awesomeIsForced: false,
    }
  };

  ngOnChanges(changes: SimpleChanges): void {
    let current = changes.dfv.currentValue;
    if (!this.dfv.DepecheReceptionNumero) {
      this.dfv.DepecheReceptionNumero = this.dfv.Numero;
    }
    this.isBtnDemandeDisabled = this.isBlocDemandeDisabled();
    this.dfv.RecuParDepeche = true;
    this.initForm();
  }

  isBlocDemandeDisabled(): boolean {
    return !(this.dfv && this.dfv.StatusZep && this.dfv.StatusZep.Statut === "En création");
  }

  initForm() {

    let dateAHT ="S"+ moment(this.dfv.DateDebutApplicabilite).week() + " / "+ moment(this.dfv.DateDebutApplicabilite).year();
    if(this.dfv.AHTNumero == null)
    {
      this.dfv.AHTNumero = dateAHT;
    }
    // this.dfv.AHTNumero = date;
    this.fields = [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            key: "OperationNumero",
            className: 'row col-md-4',
            type: 'input',
            templateOptions:
              {
                label: 'OPÉRATION N°',
                disabled: this.isBlocDemandeDisabled()
              }
          },
          {
            key: "AHTNumero",
            className: 'col-md-4',
            type: 'input',
            defaultValue: dateAHT,
              templateOptions:
              {
                label: 'AHT N°',
                type:"text",
                required: false,
                disabled: this.isBlocDemandeDisabled()
              }
          },
          {
            key: "AvisNumero",
            className: 'row col-md-4',
            type: 'input',
            templateOptions:
              {
                label: 'NUMÉRO AVIS',
                required: false,
                disabled: this.isBlocDemandeDisabled()
              }
          },
        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            key: "ContratTravauxNumero",
            className: 'row col-md-6',
            type: 'input',
            templateOptions:
              {
                label: 'CONTRAT DE TRAVAUX N°',
                required: false,
                disabled: this.isBlocDemandeDisabled()
              }
          },
          {
            key: "CCTTNumero",
            className: 'col-md-6',
            type: 'input',
            templateOptions:
              {
                label: 'CCTT N°',
                required: false,
                disabled: this.isBlocDemandeDisabled()
              }
          },
        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            key: "DateApplicabilite",
            className: 'row col-md-12',
            type: 'datetimepicker',
            templateOptions:
              {
                label: 'APPLICABLE LE',
                required: true,
                disabled: this.isBlocDemandeDisabled(), showTime : true
              },
          },

        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            key: "RecuParDepeche",
            className: 'col-md-6',
            type: 'checkbox',
            defaultValue : true,
            templateOptions:
              {
                label: 'Reçu par dépêche ',
                required: true,
                disabled: true
              },
            //hideExpression:"!model.DepecheReceptionNumero"
          },
          {
            key: "DepecheReceptionNumero",
            className: 'col-md-6',
            defaultValue: this.dfv ? this.dfv.Numero : "",
            type: 'input',
            templateOptions:
              {
                label: 'NUMÉRO DE DÉPÊCHE',
                required: true,
                disabled: this.isBlocDemandeDisabled(),
              },
            hideExpression: "!model.RecuParDepeche",

          },
        ]
      }
    ];
  }

  constructor(private ficheDFVService: FicheDfvService) {
    this.dfv = new DFV();
  }

  ngOnInit() {
  }

  submit() {
    let model = this.dfv;
    this.ficheDFVService.validerDemande(this.dfv).subscribe(
      data => {
        this.dfv = data;
        this.initForm();
        this.dfvChange.emit(this.dfv);
      },
      err => {
        console.log(err);
        alert("erreur");
      }
    )
  }
}
