/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DfvBlocAutreDepecheComponent } from './dfv-bloc-autre-depeche.component';

describe('DfvBlocAutreDepecheComponent', () => {
  let component: DfvBlocAutreDepecheComponent;
  let fixture: ComponentFixture<DfvBlocAutreDepecheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DfvBlocAutreDepecheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DfvBlocAutreDepecheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
