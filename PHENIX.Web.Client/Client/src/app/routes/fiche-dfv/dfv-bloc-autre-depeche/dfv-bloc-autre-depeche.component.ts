import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AutreDepeche, DFV } from '../../../shared/models/DFV/dfv.models';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { AutreDepecheService } from '../../../shared/services/autreDepeche/autre_depeche.service';
import { PhenixToasterService } from '../../../shared/settings/phenix-toaster.service';
import { AddAutreDepecheDfvComponent } from './modals/add-autre-depeche-dfv/add-autre-depeche-dfv.component';
import * as _  from 'lodash';

@Component({
  selector: 'app-dfv-bloc-autre-depeche',
  templateUrl: './dfv-bloc-autre-depeche.component.html',
  styleUrls: ['./dfv-bloc-autre-depeche.component.scss']
})
export class DfvBlocAutreDepecheComponent implements OnInit {
  @Input() listAutresDepeche : Array<AutreDepeche>;
  @Input() dfv : DFV;

  @Output() autreDepecheAdded: EventEmitter<void> = new EventEmitter<void>();

	modelRef : BsModalRef;

  constructor(private modalService: BsModalService,
    private autreDepecheService : AutreDepecheService,
    private toastSrv : PhenixToasterService,) {
  }

  ngOnInit() {
  }

  showAddAutreDepecheDFVModal(){
    this.modelRef = this.modalService.show(AddAutreDepecheDfvComponent, {class:"modal-md"});
    this.modelRef.content.dfv = _.cloneDeep(this.dfv);
    this.modelRef.content.initForm() ;
    (<AddAutreDepecheDfvComponent>this.modelRef.content).onClose.subscribe(result => {
      if (result != null) {
        this.dfv = result;
        this.autreDepecheAdded.emit();
      } else {

      }
    });
  }

  getAllAutreDepecheManoeuvre(){
    return this.listAutresDepeche.filter(x => x.Manoeuvre != null);
  }
  getAllAutreDepecheDFV(){
    return this.listAutresDepeche.filter(x => x.DFV != null);
  }
}
