/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AddAutreDepecheDfvComponent } from './add-autre-depeche-dfv.component';

describe('AddAutreDepecheDfvComponent', () => {
  let component: AddAutreDepecheDfvComponent;
  let fixture: ComponentFixture<AddAutreDepecheDfvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAutreDepecheDfvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAutreDepecheDfvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
