import { Component, OnInit } from '@angular/core';
import { AutreDepeche, DFV, Depeche } from '../../../../../shared/models/DFV/dfv.models';
import {  BsModalRef } from 'ngx-bootstrap';
import { AutreDepecheService } from '../../../../../shared/services/autreDepeche/autre_depeche.service';
import { PhenixToasterService } from '../../../../../shared/settings/phenix-toaster.service';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { Constantes_Phenix } from '../../../../../shared/models/Commun/constantes.model';
import { ChoixAutreDepecheVM } from '../../../../../shared/models/ViewModels/ChoixAutreDepecheVM.model';

@Component({
  selector: 'app-add-autre-depeche-dfv',
  templateUrl: './add-autre-depeche-dfv.component.html',
  styleUrls: ['./add-autre-depeche-dfv.component.scss']
})
export class AddAutreDepecheDfvComponent implements OnInit {
  public onClose: Subject<DFV>;
  public dfv : DFV;
  public newAutreDepeche : AutreDepeche;
  public choixAutreDepecheVm : ChoixAutreDepecheVM;

  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [];
  options: FormlyFormOptions = {
    formState: {
      awesomeIsForced: false,
      model: this.newAutreDepeche
    }
  };

  constructor(public modal: BsModalRef,private autreDepecheService : AutreDepecheService,private toastSrv : PhenixToasterService) {
      this.onClose = new Subject();
      this.dfv = new DFV();
      this.newAutreDepeche = new AutreDepeche();
      this.newAutreDepeche.Depeche = new Depeche();
      this.newAutreDepeche.DFV = new DFV();
      this.choixAutreDepecheVm = new ChoixAutreDepecheVM();
      this.choixAutreDepecheVm.AutreDepeche = this.newAutreDepeche;
     }

  ngOnInit() {
  }
  initForm() {
    this.choixAutreDepecheVm.AutreDepeche.DFV = this.dfv;
    this.fields = [
      {
        key: "ChoixAutreDepeche",
        className: "col-md-12",
        type: "select",
        templateOptions: {
          type: "text",
          label: "Type d'autre dépêche",
          required: true,
          options: [{
            label:"Modification de RPTX",
            value: 1
          }]
        }
      },
      {
        className: "section-label",
        template:
          "<hr /><div class='col-md-12'><strong>&nbsp;</strong></div>"
      },
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "AutreDepeche.DFV.RPTX",
            className: "col-md-6",
            type: "input",
            templateOptions: {
              type: "text",
              label: "Nouvelle valeur de RPTX",
              required: true,
            },
            hideExpression: "model.ChoixAutreDepeche != 1"
      },
      {
        key: "AutreDepeche.DFV.NumeroTelephone",
        className: "col-md-6",
        type: "input",
        templateOptions: {
          type: "text",
          label: "Nouveaux N° de téléphone",
          required: true,
        },
        hideExpression: "model.ChoixAutreDepeche != 1"
      }
        ]
      },

      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "AutreDepeche.Depeche.NumeroDonne",
            className: "col-md-5",
            type: "input",
            templateOptions: {
              type: "text",
              disabled : false,
              label: "Numéro donné",
              required: true
            },
            hideExpression: "!model.ChoixAutreDepeche"
          },
          {
            key: "AutreDepeche.Depeche.NumeroRecu",
            className: "col-md-5",
            type: "input",
            templateOptions: {
              type: "text",
              disabled : false,
              label: "Numéro reçu",
              required: true,
            },
            hideExpression: "!model.ChoixAutreDepeche"
          },
        ]
      },
      {
        key:"AutreDepeche.Depeche.DateDepeche",
        className:"col-md-12",
        type:"datetimepicker",
        templateOptions:{
          label: 'Date et heure de la depeche',
          required: true,
          showTime : true
        },
        hideExpression: "!model.ChoixAutreDepeche"

      }
    ];
  }
  submit(model: ChoixAutreDepecheVM) {
    // if(model.AutreDepeche.Depeche){
    //   model.AutreDepeche.Depeche.DateDepeche.setHours(model.AutreDepeche.Depeche.DateDepeche.getHours()+2);
    // }
    var resp = this.autreDepecheService.createAutreDepecheDFV(model.AutreDepeche);
    resp.subscribe(
      a => {
        this.toastSrv.showSuccess(
          "Succès",
          "La dépêche à été créée."
        );
        this.closePopup(model.AutreDepeche.DFV);
      },
      error => {
        this.toastSrv.showError("Erreur",error.error.error);
      }
    );
  }

  closePopup(item: DFV) {
    this.onClose.next(item);
    this.modal.hide();
  }
}
