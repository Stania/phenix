import { Component, OnInit, Input, Output ,EventEmitter} from '@angular/core';
import { PointRemarquable_Zep } from '../../../shared/models/FicheZep/zep.models';
import { ManoeuvreType, DFV, Manoeuvre, Manoeuvre_Depeche } from '../../../shared/models/DFV/dfv.models';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { ManoeuvreService } from '../../../shared/services/manoeuvre/manoeuvre.service';
import { PhenixToasterService } from '../../../shared/settings/phenix-toaster.service';

import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';
import * as _  from 'lodash';
import { EffectuerEngagementComponent } from './modals/effectuer-engagement/effectuer-engagement.component';
import { DemandeDegagementComponent } from './modals/demande-degagement/demande-degagement.component';
import { EffectuerDegagementComponent } from './modals/effectuer-degagement/effectuer-degagement.component';
import { AuthenticationService } from '../../../shared/services/administration/authentication.service';

@Component({
  selector: 'app-dfv-bloc-engagement-degagement',
  templateUrl: './dfv-bloc-engagement-degagement.component.html',
  styleUrls: ['./dfv-bloc-engagement-degagement.component.scss']
})
export class DfvBlocEngagementDegagementComponent implements OnInit {

  @Input() dfv : DFV;
  @Input() listTypeManoeuvre : Array<ManoeuvreType>;
  @Input() listPointRemarquable : Array<PointRemarquable_Zep>;
  @Input() isPosteCreateur : boolean;
  @Input() listManoeuvreDepeche : Array<Manoeuvre_Depeche>;
  @Output() depecheAdded: EventEmitter<void> = new EventEmitter<void>();

	modelRef : BsModalRef;


  constructor(private modalService: BsModalService,
    private manoeuvreService : ManoeuvreService,
    private toastSrv : PhenixToasterService,
    private authService : AuthenticationService) {
     }
  ngOnInit() {
  }

////Manoeuvre
filtrerManoeuvre(typeManoeuvre):Array<Manoeuvre>{
  let typeManoeuvreObject = this.listTypeManoeuvre.find(x => x.Nom == typeManoeuvre);
  if(this.dfv.ManoeuvreList == null)
    return new Array<Manoeuvre>();
  else{
    if (this.isPosteCreateur){
      return this.dfv.ManoeuvreList.filter(t => t.ManoeuvreTypeId == typeManoeuvreObject.Id);
    }
    else{
      return this.dfv.ManoeuvreList.filter(t => t.ManoeuvreTypeId == typeManoeuvreObject.Id
         && t.PointRemarquable_Zep.PosteConcerne.Nom === this.authService.getLoggedUser().Poste);
    }

  }

}

canPerformAction(manoeuvre : Manoeuvre) : boolean {

  return manoeuvre.PointRemarquable_Zep.PosteConcerne.Nom === this.authService.getLoggedUser().Poste;
}

////Engagement
getManoeuvreEngagement():Array<Manoeuvre>{
  return this.filtrerManoeuvre(Constantes_Phenix.ManoeuvreTypes.Engagement);
}

showEffectuerEngagementModal(manoeuvre : Manoeuvre){
  let indexEngagement = this.dfv.ManoeuvreList.indexOf(manoeuvre);
  if(indexEngagement == -1){
    this.toastSrv.showError("Erreur","Impossible de retrouver la manoeuvre sélectionné.");
    return;
  }
  this.modelRef = this.modalService.show(EffectuerEngagementComponent, {class:"modal-md"});
  this.modelRef.content.engagement = _.cloneDeep(manoeuvre);
  this.modelRef.content.dfv = this.dfv;
  this.modelRef.content.listPointRemarquable = this.listPointRemarquable;
  this.modelRef.content.initForm() ;
  (<EffectuerEngagementComponent>this.modelRef.content).onClose.subscribe(result => {
    if (result != null) {
      this.dfv.ManoeuvreList[indexEngagement]=result;
      this.depecheAdded.emit();
    } else {

    }
  });
}
isManoeuvreDone(manoeuvre : Manoeuvre){
  if(manoeuvre.Verbale==true || manoeuvre.ParEcrit==true || manoeuvre.ParDepeche==true )
  return true;
  else
  return false;
}
isManoeuvreDegagementDone(manoeuvre : Manoeuvre){
  if((manoeuvre.Verbale==null || manoeuvre.ParEcrit==null || manoeuvre.ParDepeche==null)  && manoeuvre.DateDemandeDegagement == null)
  return true;
  else
  return false;
}
////Degagement
getManoeuvreDegagement():Array<Manoeuvre>{
  return this.filtrerManoeuvre(Constantes_Phenix.ManoeuvreTypes.Degagement);
}
isDemandeDegagementDone(manoeuvre : Manoeuvre){
  return manoeuvre.DateDemandeDegagement != null  ? true: false;
}
showDemandeDegagementModal(manoeuvre :Manoeuvre){
  let indexDegagement = this.dfv.ManoeuvreList.indexOf(manoeuvre);
  if(indexDegagement == -1){
    this.toastSrv.showError("Erreur","Impossible de retrouver la manoeuvre sélectionné.");
    return;
  }
  this.modelRef = this.modalService.show(DemandeDegagementComponent, {class:"modal-md"});
  this.modelRef.content.degagement = _.cloneDeep(manoeuvre);
  this.modelRef.content.dfv = this.dfv;
  this.modelRef.content.listPointRemarquable = this.listPointRemarquable;
  this.modelRef.content.initForm() ;
  (<DemandeDegagementComponent>this.modelRef.content).onClose.subscribe(result => {
    if (result != null) {
      this.dfv.ManoeuvreList[indexDegagement]=result;
    } else {

    }
  });
}
showEffectuerDegagementModal(manoeuvre : Manoeuvre){
  let indexDegagement = this.dfv.ManoeuvreList.indexOf(manoeuvre);
  if(indexDegagement == -1){
    this.toastSrv.showError("Erreur","Impossible de retrouver la manoeuvre sélectionné.");
    return;
  }
  this.modelRef = this.modalService.show(EffectuerDegagementComponent, {class:"modal-md"});
  this.modelRef.content.degagement = _.cloneDeep(manoeuvre);
  this.modelRef.content.listPointRemarquable = this.listPointRemarquable;
  this.modelRef.content.initForm() ;
  (<EffectuerDegagementComponent>this.modelRef.content).onClose.subscribe(result => {
    if (result != null) {
      this.dfv.ManoeuvreList[indexDegagement]=result;
      this.depecheAdded.emit();
    } else {

    }
  });
}
getDepecheByManoeuvreEngagement(manoeuvre:Manoeuvre):Manoeuvre_Depeche{
  let depeche = new Manoeuvre_Depeche();
  if(this.listManoeuvreDepeche != null)
  depeche = this.listManoeuvreDepeche.find(x => x.Depeche.DepecheType.Nom == Constantes_Phenix.DepecheType.Engagement && x.ManoeuvreId == manoeuvre.Id);
  return depeche;
}
getDepecheByManoeuvreDegagement(manoeuvre:Manoeuvre):Manoeuvre_Depeche{
  let depeche = new Manoeuvre_Depeche();
  if(this.listManoeuvreDepeche != null)
  depeche = this.listManoeuvreDepeche.find(x => x.Depeche.DepecheType.Nom == Constantes_Phenix.DepecheType.Degagement && x.ManoeuvreId == manoeuvre.Id);
  return depeche;
}
/////Point remarquable
getPointRemarquableById(idPr : number):PointRemarquable_Zep{
  if(idPr != null){
      return this.listPointRemarquable.find(x=> x.Id == idPr);
  }
}
getIconEtatManoeuvre(manoeuvre : Manoeuvre){
  if(manoeuvre.ParDepeche)
  return "fa fa-newspaper-o text-success";
  else if(manoeuvre.ParEcrit)
  return "fa fa-pencil text-success"
  else if(manoeuvre.Verbale)
  return "fa fa-volume-up text-success"
  else if(this.isDemandeDegagementDone(manoeuvre))
  return "fa fa-question-circle text-warning"
  else
    return "fa fa-close text-danger"
}
getTextEtatManoeuvre(manoeuvre : Manoeuvre){
  if(manoeuvre.ParEcrit)
  return "Autorisation donnée par écrit"
  else if(manoeuvre.Verbale)
  return "Autorisation donnée verbalement"
  else if(this.isDemandeDegagementDone(manoeuvre)){
    return "Demande effectuée"
  }
  else
    return "Manoeuvre non effectuée."
}
}
