import { Component, OnInit } from "@angular/core";
import { Subject } from "rxjs";
import {
  Depeche,
  Manoeuvre,
  DFV
} from "../../../../../shared/models/DFV/dfv.models";
import { PhenixToasterService } from "../../../../../shared/settings/phenix-toaster.service";
import { ManoeuvreService } from "../../../../../shared/services/manoeuvre/manoeuvre.service";
import { BsModalRef } from "ngx-bootstrap";
import { FormlyFormOptions, FormlyFieldConfig } from "@ngx-formly/core";
import { FormGroup } from "@angular/forms";
import { PointRemarquable_Zep } from "../../../../../shared/models/FicheZep/zep.models";
import { Constantes_Phenix } from "../../../../../shared/models/Commun/constantes.model";

@Component({
  selector: "app-demande-degagement",
  templateUrl: "./demande-degagement.component.html",
  styleUrls: ["./demande-degagement.component.scss"]
})
export class DemandeDegagementComponent implements OnInit {
  public degagement: Manoeuvre;
  public dfv: DFV;
  public listPointRemarquable: Array<PointRemarquable_Zep>;
  public onClose: Subject<Manoeuvre>;

  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [];
  options: FormlyFormOptions = {
    formState: {
      awesomeIsForced: false,
      model: this.degagement
    }
  };
  constructor(
    public modal: BsModalRef,
    private manoeuvreService: ManoeuvreService,
    private toastSrv: PhenixToasterService
  ) {
    this.listPointRemarquable = new Array<PointRemarquable_Zep>();
    this.degagement = new Manoeuvre();
    this.onClose = new Subject();
    this.dfv = new DFV();
  }
  ngOnInit() {}
  initForm() {
    this.degagement.PointTTX = this.degagement.PointRemarquable_Zep.PosteConcerne.Nom + " / " +  this.degagement.PointRemarquable_Zep.Valeur,

    this.fields = [
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "TTXNumero",
            className: "col-md-4",
            type: "input",
            templateOptions: {
              type: "text",
              label: "Numéro des TTX (xx)",
              required: true,
              disabled: true
            }
          },
          {
            key: "TTXNature",
            className: "col-md-8",
            type: "select",
            templateOptions: {
              type: "text",
              label: "Nature des TTX",
              required: true,
              disabled: true,
              options: Constantes_Phenix.TypeTTX.ListAllType.map(x => ({
                label: x,
                value: x
              }))
            }
          }
        ]
      },
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "PointRemarquable_ZepId",
            className: "col-md-6",
            type: "select",
            templateOptions: {
              type: "text",
              label: "Point de dégagement",
              required: true,
              disabled: true,
              options: this.listPointRemarquable.map(x => ({
                label: x.PosteConcerne.Nom + " / " + x.Valeur,
                value: x.Id
              }))
            }
          },
          {
            key: "TTXVoie",
            className: "col-md-6",
            type: "input",
            templateOptions: {
              type: "text",
              label: "Voie TTX (vv)",
              required: true,
              disabled: true
            }
          }
        ]
      },

      {
        key: "Commentaire",
        className: "col-md-12",
        type: "textarea",
        templateOptions: {
          label: "Commentaire",
          required: false,
          disabled: true,
          rows: 2
        }
      },
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "PointTTX",
            className: "col-md-6",
            type: "input",
            templateOptions: {
              type: "text",
              label: "Point où se situe le ttx (ww)",
              required: true
            }
          },
          {
            key: "DateDemandeDegagement",
            className: "col-md-6",
            defaultValue: new Date(),
            type: "datetimepicker",
            templateOptions: {
              label: "Date et heure de la demande",
              required: true,
              datePosition: "top", showTime : true
            }
          }
        ]
      }
    ];
  }
  submit(model: Manoeuvre) {


    var resp = this.manoeuvreService.updateManoeuvre(model);
    resp.subscribe(
      a => {
        this.toastSrv.showSuccess(
          "Succès",
          "La demande de dégagement à été effectuée."
        );
        this.closePopup(model);
      },
      error => {
        this.toastSrv.showError("Erreur",error.error.error);
      }
    );
  }

  closePopup(item: Manoeuvre) {
    this.onClose.next(item);
    this.modal.hide();
  }
}
