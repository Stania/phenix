import { Component, OnInit } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap";
import { ManoeuvreService } from "../../../../../shared/services/manoeuvre/manoeuvre.service";
import { PhenixToasterService } from "../../../../../shared/settings/phenix-toaster.service";
import { PointRemarquable_Zep } from "../../../../../shared/models/FicheZep/zep.models";
import { Manoeuvre, DFV, Depeche, Manoeuvre_Depeche } from "../../../../../shared/models/DFV/dfv.models";
import { Subject } from "rxjs";
import { Constantes_Phenix } from "../../../../../shared/models/Commun/constantes.model";
import { FormGroup } from "@angular/forms";
import { FormlyFieldConfig, FormlyFormOptions } from "@ngx-formly/core";
import { EffectueManoeuvreViewModel } from "../../../../../shared/models/ViewModels/EffectueManoeuvreViewModel.model";

@Component({
  selector: "app-effectuer-degagement",
  templateUrl: "./effectuer-degagement.component.html",
  styleUrls: ["./effectuer-degagement.component.scss"]
})
export class EffectuerDegagementComponent implements OnInit {
  public degagement: Manoeuvre;
  public dfv: DFV;
  public listPointRemarquable: Array<PointRemarquable_Zep>;
  public onClose: Subject<Manoeuvre>;
  public depeche : Depeche;
  public effectueManoeuvreModel : EffectueManoeuvreViewModel;
  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [];
  options: FormlyFormOptions = {
    formState: {
      awesomeIsForced: false,
      model: this.effectueManoeuvreModel
    }
  };

  constructor(
    public modal: BsModalRef,
    private manoeuvreService: ManoeuvreService,
    private toastSrv: PhenixToasterService
  ) {
    this.listPointRemarquable = new Array<PointRemarquable_Zep>();
    this.degagement = new Manoeuvre();
    this.onClose = new Subject();
    this.dfv = new DFV();
    this.depeche = new Depeche();
    this.effectueManoeuvreModel = new EffectueManoeuvreViewModel();

  }

  ngOnInit() {}

  initForm() {

this.effectueManoeuvreModel.DepecheToAdd = this.depeche;
this.effectueManoeuvreModel.Manoeuvre = this.degagement;


    this.degagement.ParEcrit = false;
    this.fields = [
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "Manoeuvre.TTXNumero",
            className: "col-md-4",
            type: "input",
            templateOptions: {
              type: "text",
              label: "Numéro des TTX (xx)",
              required: true,
              disabled: true
            }
          },
          {
            key: "Manoeuvre.TTXNature",
            className: "col-md-8",
            type: "select",
            templateOptions: {
              type: "text",
              label: "Nature des TTX",
              required: true,
              disabled: true,
              options: Constantes_Phenix.TypeTTX.ListAllType.map(x => ({
                label: x,
                value: x
              }))
            }
          }
        ]
      },
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "Manoeuvre.PointRemarquable_ZepId",
            className: "col-md-6",
            type: "select",
            templateOptions: {
              type: "text",
              label: "Point de dégagement",
              required: true,
              disabled: true,
              options: this.listPointRemarquable.map(x => ({
                label: x.PosteConcerne.Nom + " / " + x.Valeur,
                value: x.Id
              }))
            }
          },
          {
            key: "Manoeuvre.TTXVoie",
            className: "col-md-6",
            type: "input",
            templateOptions: {
              type: "text",
              label: "Voie TTX (vv)",
              required: true,
              disabled: true
            }
          }
        ]
      },
      {
        key: "Manoeuvre.Commentaire",
        className: "col-md-12",
        type: "textarea",
        templateOptions: {
          label: "Commentaire",
          required: false,
          disabled: true,
          rows: 2
        }
      },
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "Manoeuvre.PointTTX",
            className: "col-md-6",
            type: "input",
            templateOptions: {
              type: "text",
              label: "Point où se situe le ttx (ww)",
              required: true,
              disabled :true
            }
          },
          {
            key: "Manoeuvre.DateDemandeDegagement",
            className: "col-md-6",
            defaultValue: new Date(),
            type: "datetimepicker",
            templateOptions: {
              label: "Date et heure de la demande",
              required: true,
              disabled :true, showTime : true
            }
          }
        ]
      },
      {
        className: "section-label",
        template:
          "<hr /><div class='col-md-12'><strong>Autorisation :</strong></div>"
      },
      {
        key: "Manoeuvre.autorisationCheck",
        type: "multicheckbox",
        className: "col-md-12",
        templateOptions: {
          checkClick: (selectedOption: { key; value }, event: any) => {
            let optionsKey = ["Verbale", "ParDepeche"];
            this.effectueManoeuvreModel.Manoeuvre[selectedOption.key] = true;
            let tempOptionKeyArray = optionsKey.filter(
              x => x != selectedOption.key
            );
            this.effectueManoeuvreModel.Manoeuvre.SignatureRptx = false;
            let object = {};
            object[selectedOption.key] = true;
            tempOptionKeyArray.forEach(x => {
              this.effectueManoeuvreModel.Manoeuvre[x] = false;
              object[x] = false;
            });
            let autorisationField = this.form.get("Manoeuvre.autorisationCheck");
            if (autorisationField != null) autorisationField.setValue(object);
          },
          options: [
            { key: "Verbale", value: "Verbable" },
            { key: "ParDepeche", value: "Par dépêche", }
          ]
        }
      },

      {
        key: "DepecheToAdd.NumeroDonne",
        className: "col-md-4 col-md-offset-2",
        type: "input",

        templateOptions: {
          type: "text",
          disabled: false,
          label: "Numéro donné",
          required: true
        },
        hideExpression: "!model.Manoeuvre.ParDepeche"
      },

      {
        key: "DepecheToAdd.NumeroRecu",
        className: "col-md-4",
        type: "input",
        templateOptions: {
          type: "text",
          disabled: false,
          label: "Numéro reçu",
          required: true
        },
        hideExpression: "!model.Manoeuvre.ParDepeche"
      },
      {
        key: "DepecheToAdd.DateDepeche",
        className: "col-md-12",
        defaultValue: new Date(),
        type: "datetimepicker",
        templateOptions: {
          label: "Date et heure de la depeche",
          required: true,
          datePosition:"top", showTime : true
        },
        hideExpression: "!model.Manoeuvre.ParDepeche"
      },

    ];
  }

  submit(model: EffectueManoeuvreViewModel) {
    // if(model.Manoeuvre.ParDepeche){
    //   model.DepecheToAdd.DateDepeche.setHours(model.DepecheToAdd.DateDepeche.getHours()+2);
    // }

    var resp = this.manoeuvreService.effectueManoeuvre(model);
    resp.subscribe(
      a => {
        this.toastSrv.showSuccess("Succès","La demande de dégagement à été effectuée.");
        this.closePopup(a);
      },
      error => {
        this.toastSrv.showError("Erreur",error.error.error);
      }
    );
  }

  closePopup(item: Manoeuvre) {
    this.onClose.next(item);
    this.modal.hide();
  }
}
