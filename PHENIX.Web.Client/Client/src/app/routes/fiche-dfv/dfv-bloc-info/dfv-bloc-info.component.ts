import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DFV } from '../../../shared/models/DFV/dfv.models';
import { ValidationService } from '../../../shared/settings/validators.service';
import { FormlyFormOptions } from '@ngx-formly/core';
import { ZepService } from '../../../shared/services/ficheZep/zep.service';
import { ZepType, Zep } from '../../../shared/models/FicheZep/zep.models';
import "rxjs/Rx";
import { FicheDfvService } from '../../../shared/services/ficheDfv/fiche-dfv.service';
import { tr } from 'ngx-bootstrap';

@Component({
  selector: 'app-dfv-bloc-info',
  templateUrl: './dfv-bloc-info.component.html',
  styleUrls: ['./dfv-bloc-info.component.scss']
})
export class DfvBlocInfoComponent implements OnInit, OnChanges {

  @Input() dfv : DFV;
  @Input() isPosteCreateur : boolean;
  @Input() zepTypeList : Array<ZepType>;
  @Input() zepList : Array<Zep>;

  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [];
  options: FormlyFormOptions = {
    formState: {
      disabled : false,
      awesomeIsForced: false,
      model : this.dfv,
    }
  };

  constructor(private zepService : ZepService, private dfvService : FicheDfvService) {
    this.dfv = new DFV();
    this.zepTypeList = new Array<ZepType>();
    this.zepList = new Array<Zep>();
  }

  ngOnChanges(changes: SimpleChanges): void {
      this.initForm();
  }

  initForm(){
    this.form = new FormGroup({});
    this.fields =  [
      {
        fieldGroupClassName: 'row',
        fieldGroup:
        [
          {
            key: "Numero", className: 'row col-md-4',type: 'input',
            templateOptions: {type: 'text',label: 'NUMÉRO DFV', required: true}
          },
          {
            key: "RPTX", className: 'col-md-4',type: 'input',
            templateOptions: {type: 'text',label: 'RPTx', required: true}
          },
          {
            key: "NumeroTelephone", className: 'col-md-4',type: 'input',
            templateOptions: {type: 'text',label: 'TÉLÉPHONE RPTx', required: true}
          },
        ]
      },
      {
        type: "horizontalRadio", key: "AvecVerificationDeLiberation", id: "AvecVerificationDeLiberation",
        templateOptions: {
          required: true,
          label: '',
          inputClass: 'radio-inline',
          options: [{ key: true, value: "Avec vérification de libération" }, { key: false, value: "Sans vérification de libération" },],
        }
      },
      {
        key:"ZepId",
        type : "select",
        templateOptions:
        {
          label:"NUMÉRO ZEP",
          options: this.zepList,
          valueProp: 'Id',
          labelProp: 'Numero',
          required: true
        }
      },
      {
        key: "Zep.ZepTypeId",
        type: 'select',
        templateOptions:
        {
          label: 'TYPE ZEP',
          options: this.zepTypeList,
          valueProp: 'Id',
          labelProp: 'Nom',
          required:true,
        },
        lifecycle: {
          onInit: (form, field) => {
            form.get('ZepId').valueChanges.subscribe( (selected : any) =>
              {
                var zep = this.zepList.find(t => t.Id == parseInt(selected));
                field.templateOptions.options = this.zepTypeList.filter(t => t.Id === zep.ZepTypeId);
              }
            )
          },
        },
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup:
        [
          {
            key: "DateDebutApplicabilite",
            className: 'row col-md-6',
            type: 'datetimepicker',
            defaultValue: new Date(this.dfv.DateDebutApplicabilite),
            templateOptions:{
              label: 'À PARTIR DU', 
              required: true, 
            },
            validators:
            {
              validation: (x) => ValidationService.periodValidator( x ,this.form, 'DateFinApplicabilite',true)
            },
            expressionProperties: {
              'templateOptions.type': function(model: any, formState: any){
                var isDateonly = (model.DerriereTTXDeclencheur || model.DerriereTrainOuvrant );
                return isDateonly  ? 'dateonly' : 'datetime';
              }
            }
          },
          {
            key: "DateFinApplicabilite",
            className: 'col-md-6',
            type: 'datetimepicker',
            defaultValue: new Date(this.dfv.DateFinApplicabilite),
            templateOptions: {
              label: 'JUSQU\'AU', 
              required: true, 
            },
            validators:
            {
              validation: (x) => ValidationService.periodValidator( x ,this.form, 'DateDebutApplicabilite',false)
            },
          },
        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup:
        [
          {
            key: "DerriereTTXDeclencheur",
            className: 'row col-md-6',
            type: 'checkbox',
            templateOptions: {
              label: 'Derrière TTx déclencheur',
            },
            hideExpression: 'model.DerriereTrainOuvrant',
          },
          {
            key: "DerriereTTXDeclencheurNumero",
            className: 'col-md-6',
            type: 'input',
            templateOptions: {
              label: 'N°',
            },
            hideExpression: '!model.DerriereTTXDeclencheur',
            expressionProperties: {
              'templateOptions.required': function(model: any, formState: any){
                  return model.DerriereTTXDeclencheur;
              }
            }
          }
        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup:
        [
          {
            key: "DerriereTrainOuvrant",
            className: 'row col-md-6',
            type: 'checkbox',
            templateOptions: {
              label: 'Derrière train ouvrant',
              required: false},
            hideExpression: 'model.DerriereTTXDeclencheur',
          },
          {
            key: "DerriereTrainOuvrantNumero",
            className: 'col-md-6',
            type: 'input',
            templateOptions:{
              label: 'N°',
            },
            hideExpression : "!model.DerriereTrainOuvrant",
            expressionProperties: {
              'templateOptions.required': function(model: any, formState: any){
                  return model.DerriereTrainOuvrant;
              }
            }
          },
        ]
      },
      // {
      //   key: "SansTTX",
      //   className: 'row col-md-12',
      //   type: 'checkbox',
      //   defaultValue: false,
      //   templateOptions: {label: 'Sans TTX ?', required: false}
      // }
    ];
    this.applyDisable();
    this.options.formState.model = this.dfv;
  }

  ngOnInit() {

  }

  isBlocDemandeDisabled():boolean{
    return !(this.dfv && this.dfv.StatusZep && this.dfv.StatusZep.Statut === "En création");
  }

  applyDisable(){
     this.options.formState.disabled =  this.isBlocDemandeDisabled();
     // apply expressionProperty for disabled based on formState to all fields
     this.fields.forEach(field => {
      field.expressionProperties = field.expressionProperties || {};
      field.expressionProperties['templateOptions.disabled'] = 'formState.disabled';
      if(field.fieldGroup){
        field.fieldGroup.forEach(t => {
          t.expressionProperties = t.expressionProperties || {};
          t.expressionProperties['templateOptions.disabled'] = 'formState.disabled';
        });
      }
    });
  }

  toggleDisabled() {
    this.options.formState.disabled = !this.options.formState.disabled;
  }

}
