import { Component, OnInit, Input, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { DFV, DepecheType, MesureProtectionPrise, OccupationType, ManoeuvreType } from '../../../shared/models/DFV/dfv.models';
import { Poste, PointRemarquable_Zep } from '../../../shared/models/FicheZep/zep.models';
import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { FicheDfvService } from '../../../shared/services/ficheDfv/fiche-dfv.service';
import { DepecheComponent } from '../depeche/depeche.component';
import swal from 'sweetalert2';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { AuthenticationService } from '../../../shared/services/administration/authentication.service';
import { OutputType } from '@angular/core/src/view';
import * as _ from "lodash";

@Component({
  selector: 'app-dfv-bloc-levee-mesure',
  templateUrl: './dfv-bloc-levee-mesure.component.html',
  styleUrls: ['./dfv-bloc-levee-mesure.component.scss']
})
export class DfvBlocLeveeMesureComponent implements OnInit {

  constructor(private modalService : BsModalService,
    private modalRef : BsModalRef,
    private ficheDfvService : FicheDfvService,
    private authService: AuthenticationService
  ) {
    this.dfv = new DFV();
    this.listDepecheType = new Array<DepecheType>();
    this.postes = new Array<Poste>();
    this.isLeveeMesuresParAC = false;
   }

  @Input() dfv: DFV;
  @Output() dfvChange = new EventEmitter<DFV>();
  @Input() listTypeOccupation : Array<OccupationType>;
  @Input() listTypeManoeuvre : Array<ManoeuvreType>;
  @Input() listPointRemarquable : Array<PointRemarquable_Zep>;
  @Input() listDepecheType : Array<DepecheType>;
  @Input() isPosteCreateur : boolean;


  onDfvChange(dfv : DFV){
    this.dfv = dfv;
    this.dfvChange.emit(this.dfv);
  }

  postes : Array<Poste>;
  isLeveeMesuresParAC : boolean;

  formLeveeMesure = new FormGroup({});
  fieldsLeveeMesure: FormlyFieldConfig[] = new Array<FormlyFieldConfig>();

  optionsFormLeveeMesure: FormlyFormOptions = {
    formState: {
      disabled : false,
      awesomeIsForced: false,
      model : this.dfv,
    }
  };

  initFormLeveeMesure(){
    this.formLeveeMesure = new FormGroup({});
    this.fieldsLeveeMesure =  [
      {

        fieldGroupClassName:"row",
        fieldGroup:[
          {
            key:"LeveeMesuresParAC",
            className:"col-md-12",
            type:'checkbox',
            templateOptions:{
              label: 'Levée des mesures par L\'AC',
              required: true,
              disabled: this.isLeveeMesuresParAC
            }
          },
          {
            key:"DateLeveeMesuresParAC",
            className:"col-md-12",
            type:"datetimepicker",
            defaultValue : this.dfv.DateLeveeMesuresParAC == null ? new Date() : new Date(this.dfv.DateLeveeMesuresParAC),
            hideExpression:'model.LeveeMesuresParAC != true',
            templateOptions:{
              label: '',
              required: this.dfv.LeveeMesuresParAC,
              disabled: this.isLeveeMesuresParAC, showTime : true
            }
          },
        ]
      }

    ]
    this.optionsFormLeveeMesure.formState.model = this.dfv;
  }

  ngOnChanges(changes: SimpleChanges): void {
    let model = this.dfv;
  }

  ngOnInit() {
    if (this.dfv.LeveeMesuresParAC === true) this.isLeveeMesuresParAC = true;
    this.initFormLeveeMesure();
  }

  PasDeDepeche(mesure : MesureProtectionPrise){
    let yapas = this.getPropDepeche(mesure,"Date") === "";
    return yapas;
  }

  getPropDepeche(mesure : MesureProtectionPrise, typeNumero : string){
    var depeches = mesure.MesureProtectionPrise_DepecheList;
    if(depeches && depeches.length > 0){
      let md =  depeches.find(t => t.Depeche.DepecheType.Nom == Constantes_Phenix.DepecheType.LeveeDesMesures);
      if(md){
        switch(typeNumero){
          case "Donne":
            return md.Depeche.NumeroDonne;
          case "Recu":
            return md.Depeche.NumeroRecu;
          case "Date":
            return md.Depeche.DateDepeche;
        }
      }else{
        return  "";
      }
    }else{
      return "";
    }
  }

  filterMesureDeProtection():Array<MesureProtectionPrise>{
    if(this.dfv.MesureProtectionPriseList == null ) return new Array<MesureProtectionPrise>();
    if (this.isPosteCreateur) return this.dfv.MesureProtectionPriseList ;
    else return this.dfv.MesureProtectionPriseList.filter(a=>a.PosteDemande != null && a.PosteDemande.Nom == this.authService.getLoggedUser().Poste )
  }
  addDepeche(mesure : MesureProtectionPrise){
    this.modalRef = this.modalService.show(DepecheComponent);
    this.modalRef.content.depecheType = this.listDepecheType.find(t => t.Nom == Constantes_Phenix.DepecheType.LeveeDesMesures);
    this.modalRef.content.mesureId = mesure.Id;

    this.modalRef.content.onClose.subscribe(result => {
      if (result != null) {
        this.ficheDfvService.checkOrUpdateDfvStatutLeveeMesure(this.dfv.Id).subscribe(
          data => {
            this.dfv = data;
            this.dfvChange.emit(this.dfv);

          },
          err => {
            console.log(err);
            alert("erreur");
          }
        )
      } else {
        alert("erreur");
      }
    });
  }

  submitLeveeMesure(){
    let DEF = _.cloneDeep(this.dfv);
    DEF.ManoeuvreList = null;
    this.ficheDfvService.UpdateDfv(DEF).subscribe(
      data => {
        this.ficheDfvService.checkOrUpdateDfvStatutLeveeMesure(this.dfv.Id).subscribe(
          dataDfv => {
            this.dfv = dataDfv;
            this.dfvChange.emit(this.dfv);
            if (this.dfv.LeveeMesuresParAC === true) this.isLeveeMesuresParAC = true;
            this.initFormLeveeMesure();
            swal('Mise à jour !', "La modification à été enregistrée", 'success');
          },
          err => {
            console.log(err);
            alert("erreur");
          }
        )

      },
      err => {
        alert("erreur");
        console.log(err);
      }
    )
  }

}
