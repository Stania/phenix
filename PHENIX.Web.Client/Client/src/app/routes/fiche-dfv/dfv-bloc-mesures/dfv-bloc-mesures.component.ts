import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { DFV, MesureProtectionPrise, DepecheType } from '../../../shared/models/DFV/dfv.models';
import { MesureProtection_Zep, Poste, Zep } from '../../../shared/models/FicheZep/zep.models';
import { MomentModule } from 'angular2-moment';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { DepecheComponent } from '../depeche/depeche.component';
import { FicheDfvService } from '../../../shared/services/ficheDfv/fiche-dfv.service';
import { CreateMesureComponent } from './modals/create-mesure/create-mesure.component';
import Swal from 'sweetalert2';
import { MesuresService } from '../../../shared/services/ficheDfv/mesures.service';
import { isDate } from 'moment';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { AuthenticationService } from '../../../shared/services/administration/authentication.service';
import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';
import * as moment from 'moment'
import { ValeurParTypeVM } from '../../../shared/models/ViewModels/MesureProtectionVM/ValeurParTypeVM';
import { PosteParTypeVM } from '../../../shared/models/ViewModels/MesureProtectionVM/PosteParType';

@Component({
  selector: 'app-dfv-bloc-mesures',
  templateUrl: './dfv-bloc-mesures.component.html',
  styleUrls: ['./dfv-bloc-mesures.component.scss']
})
export class DfvBlocMesuresComponent implements OnInit, OnChanges {

  @Input() dfv : DFV;
  @Input() listDepecheType : Array<DepecheType>;
  @Input() isPosteCreateur : boolean;
  @Input() listpostes : Poste[];
  @Output() dfvChange = new EventEmitter<DFV>();
  listParticularite :Map<string,string[]> = new Map<string,string[]>();
  listMesureProtection : Array<PosteParTypeVM> = new Array<PosteParTypeVM>();
  listConditionsParticuliereDeVerif : Array<PosteParTypeVM> = new Array<PosteParTypeVM>();

  modalRef : BsModalRef;
  modalMesure : BsModalRef;
  postes : Poste[];
  isAccordee : boolean = false;

  constructor(
    private modalService : BsModalService,
    private ficheDfvService : FicheDfvService,
    private mesuresService : MesuresService,
    private authService :AuthenticationService
  )
  {
    this.dfv = new DFV();
    this.listDepecheType = new Array<DepecheType>();
    this.postes = new Array<Poste>();
    this.isMesuresParAC = false;
    this.listpostes = new Array<Poste>();
  }

  isMesuresParAC : boolean;
  formMesure = new FormGroup({});
  fieldsMesure: FormlyFieldConfig[] = new Array<FormlyFieldConfig>();
  optionsFormMesure: FormlyFormOptions = {
    formState: {
      disabled : false,
      awesomeIsForced: false,
      model : this.dfv,
    }
  };
  initFormMesure(){
    this.formMesure = new FormGroup({});
    this.dfv.MesureProtectionPiseParAC = true;
    this.fieldsMesure =  [
      {

        fieldGroupClassName:"row",
        fieldGroup:[

          {
            key:"DateMesureProtectionPiseParAC",
            className:"col-md-12",
            type:"datetimepicker",
            defaultValue : this.dfv.DateMesureProtectionPiseParAC == null ?  new Date()  : new Date(this.dfv.DateMesureProtectionPiseParAC),
            hideExpression:'model.MesureProtectionPiseParAC != true',
            templateOptions:{
              label: '',
              required: this.dfv.MesureProtectionPiseParAC,
              disabled: this.isMesuresParAC || this.isAccordee, showTime : true
            }
          },
        ]
      }

    ]
    this.optionsFormMesure.formState.model = this.dfv;
  }
  mesurePriseParAc():boolean{
    if(!this.needMesureByAc()){
      return true;
    }
    if(this.dfv.DateMesureProtectionPiseParAC != null  && this.isMesuresParAC){
      return true;
    }else{
      return false;
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
      let model = this.dfv;
  }

  mesureParAutreAgentPrise(){
    let isOk = true;
    let allMesurePrise = this.filterMesures();
    allMesurePrise.forEach(x => {
      if(this.PasDeDepeche(x))
      isOk = false;
    });
    return isOk;
  }

  ngOnInit() {
    if (this.dfv.MesureProtectionPiseParAC === true) this.isMesuresParAC = true;
    if (this.dfv != null && this.dfv.StatusZep != null){
      if(this.dfv.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Accordee
        || this.dfv.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Restituee){
        this.isAccordee = true;
      }
    }

      if (this.dfv.ZepId != null)
      {
        //Particularités
        let particuMap  = this.dfv.Zep.ParticulariteList.map(x => { return { particularite : x.ParticulariteType.Nom, valeur: x.Valeur}} );
        particuMap.forEach(x => {
          let goodDico = this.listParticularite.get(x.particularite);
          if(goodDico == null)
            this.listParticularite.set(x.particularite,new Array<string>(x.valeur));
          else
            goodDico.push(x.valeur);
        });
        
        //Mesures Protection
        let mesureProtectionMap  = this.dfv.Zep.MesureProtection_ZepList.map(x => { return { typeMesure : x.MesureProtectionType.Nom,
          poste : x.Poste.Nom,
          valeur :x.Valeur}}
        );
        //Retourne [Poste , {Type , [ListValeur]}]
        let mesuresProtectionLocal = new Array<PosteParTypeVM>();
        mesureProtectionMap.forEach(x => {
              let typeMesureVm = new PosteParTypeVM();
              let indexType = mesuresProtectionLocal.findIndex(z => z.NomPoste == x.poste) ;
              if(indexType == -1){
                typeMesureVm.NomPoste = x.poste;
                let newType = new ValeurParTypeVM();
                newType.Type = x.typeMesure;
                newType.ListValeur =new Array<string>(x.valeur);
                typeMesureVm.ListType.push(newType);
                mesuresProtectionLocal.push(typeMesureVm)
              }else{
                let indexPoste = mesuresProtectionLocal[indexType].ListType.findIndex(z => z.Type == x.typeMesure);
                if(indexPoste == -1){
                  let newType = new ValeurParTypeVM();
                  newType.Type = x.typeMesure;
                  newType.ListValeur = new Array<string>(x.valeur);
                  mesuresProtectionLocal[indexType].ListType.push(newType);
                }else{
                  mesuresProtectionLocal[indexType].ListType[indexPoste].ListValeur.push(x.valeur);
                }
              }
              });
              mesuresProtectionLocal.forEach(poste => {
                if(poste.ListType.find(x => x.Type == Constantes_Phenix.MesureProtectionTypes.ConditionsParticuliereVerificationLiberationZep) != null){
                  this.listConditionsParticuliereDeVerif.push(poste);
                }
                if(poste.ListType.find(x =>
                  [Constantes_Phenix.MesureProtectionTypes.ItinérairesAdetruirePRCI,
                    Constantes_Phenix.MesureProtectionTypes.ItinérairesAdetruireEtAMunirDunDA ,
                    Constantes_Phenix.MesureProtectionTypes.OrganeDeCommandeAplacerEtAmunirDeDA ,
                    Constantes_Phenix.MesureProtectionTypes.DialogueDeProtection ,
                    Constantes_Phenix.MesureProtectionTypes.AutorisationADetruire ,
                    Constantes_Phenix.MesureProtectionTypes.AiguilleImmobiliseeDansPositionIndiquee ,
                    Constantes_Phenix.MesureProtectionTypes.JalonDarretAplacerSurLeTerrain,
                  ].includes(x.Type))){
                    if (this.dfv.ResponsableDFV != null && this.dfv.ResponsableDFV.Nom == poste.NomPoste)
                    {
                      this.listMesureProtection.push(poste);
                    }
                }
              })
      }
    
    this.initFormMesure();
  }

  getRowSpan(mesure : MesureProtectionPrise){
      if(this.getPropDepeche(mesure,"Date") != ""){
        return  3;
      }else{
        return  1;
      }
  }

  getDateFormat(date){
    if(isDate(date)){
      return date;
    }else{
      return null;
    }
  }

  PasDeDepeche(mesure : MesureProtectionPrise){
    let yapas = this.getPropDepeche(mesure,"Date") === "";
    return yapas;
  }

  getPropDepeche(mesure : MesureProtectionPrise, typeNumero : string){
    var depeches = mesure.MesureProtectionPrise_DepecheList;

    if(depeches && depeches.length > 0){
      let md =  depeches.find(t => t.Depeche.DepecheType.Nom == "Mesure de protection");
      if(md){
        console.log(md);
        switch(typeNumero){
          case "Donne":
            return md.Depeche.NumeroDonne;
          case "Recu":
            return md.Depeche.NumeroRecu;
          case "Date":
            return md.Depeche.DateDepeche;
        }
      }else{
        return  "";
      }
    }else{
      return "";
    }
  }

  addDepeche(mesure : MesureProtectionPrise){
    this.modalRef = this.modalService.show(DepecheComponent);
    this.modalRef.content.depecheType = this.listDepecheType.find(t => t.Nom == "Mesure de protection");
    this.modalRef.content.mesureId = mesure.Id;

    this.modalRef.content.onClose.subscribe(result => {
      if (result != null) {
        this.ficheDfvService.getDFVById(this.dfv.Id).subscribe(
          data => {
            this.dfv = data;
            this.dfvChange.emit(this.dfv);
          },
          err => {
            console.log(err);
            alert("erreur");
          }
        )
      } else {
        alert("erreur");
      }
    });
  }

  addMesure(){
    this.postes = this.dfv.Zep.MesureProtection_ZepList.map(t => t.Poste);
    let posteDemandes = this.dfv.MesureProtectionPriseList.map(t => t.PosteDemande);
    this.postes = this.postes.filter(t => posteDemandes.filter((x,i) => x.Nom === t.Nom).length < 1);
    if(this.postes.length<1){
      // alert("Tous les postes prévus dans la Zep ont été ajoutés");
      Swal("Tous les postes prévus dans la Zep ont été ajoutés!");
      return;
    }

    let mesure = new MesureProtectionPrise();
    mesure.DFVId = this.dfv.Id;
    this.modalMesure = this.modalService.show(CreateMesureComponent);
    this.modalMesure.content.add = true;
    this.modalMesure.content.mesure = mesure;
    this.modalMesure.content.postes = this.postes;
    this.modalMesure.content.initForm();
    this.modalMesure.content.onClose.subscribe(data => {
      if(data != null){
        this.dfv.MesureProtectionPriseList.push(data);
      }
    });
  }
  needMesureByAc(){
    if(this.dfv.Zep == null)
    return false;
    let mesure =  this.dfv.Zep.MesureProtection_ZepList.find(x => x.PosteId == this.dfv.ResponsableDFVId);
    let isok = mesure == undefined ? false : true;
    return isok;
  }
  editMesure(mesure : MesureProtectionPrise ){
    this.postes = this.dfv.Zep.MesureProtection_ZepList.map(t => t.Poste);
    this.modalMesure = this.modalService.show(CreateMesureComponent);
    this.modalMesure.content.add = false;
    this.modalMesure.content.mesure = mesure;
    this.modalMesure.content.postes = this.postes;
    this.modalMesure.content.initForm();
    this.modalMesure.content.onClose.subscribe(data => {
      if(data != null){
        var edited = this.dfv.MesureProtectionPriseList.find(t => t.Id == data.Id);
        edited = data;
      }
    });
  }



  deleteMesure(mesure : MesureProtectionPrise){
    this.mesuresService.DeleteMesure(mesure).subscribe(
      data => {
        this.dfv.MesureProtectionPriseList =
        this.dfv.MesureProtectionPriseList.filter(t => t.Id != mesure.Id);
        Swal('Suppression',"Mesure supprimée", "success");
      },
      err => {
        console.log(err);
        alert("erreur");
      }
    )
  }

  submitMesure(){
    let stop = "stop";
    // this.dfv.DateMesureProtectionPiseParAC.setHours(this.dfv.DateMesureProtectionPiseParAC.getHours()+2);
    this.ficheDfvService.UpdateDfv(this.dfv).subscribe(
      data => {
        this.dfv = data;
        Swal('Mise à jour !', "La modification à été enregistrée", 'success');
        if (this.dfv.MesureProtectionPiseParAC === true) this.isMesuresParAC = true;
        this.initFormMesure();
      },
      err => {
        alert("erreur");
        console.log(err);
      }
    )
  }
  isStatutPrioritaire(mesure : MesureProtectionPrise):boolean{
    let isPrioritaire : boolean = true;
      if(this.dfv.Zep.MesureProtection_ZepList != null){
        let mesureProtec =  this.dfv.Zep.MesureProtection_ZepList.filter(x => x.MesureProtectionType.Nom == Constantes_Phenix.MesureProtectionTypes.ProtectionEtVerification);

        let allOtherMesurePrise =  this.filterMesures().filter(x => x.Id != mesure.Id );

        let mesureDuPoste = mesureProtec.find(x => x.PosteId == mesure.PosteDemandeId);
        if(mesureDuPoste == null)
        return true;
        switch(mesureDuPoste.Valeur){
          case Constantes_Phenix.ProtectionVerificationPoste.Verification :
             for(let mesure of allOtherMesurePrise){
                let currentMesure = mesureProtec.find(x => x.PosteId == mesure.PosteDemandeId);
                if(!this.PasDeDepeche(mesure))
                  continue;
                if(currentMesure != null && ( currentMesure.Valeur == Constantes_Phenix.ProtectionVerificationPoste.Verification_Protection ||Constantes_Phenix.ProtectionVerificationPoste.Protection) )
                  isPrioritaire = false;
              }
              break;
          case Constantes_Phenix.ProtectionVerificationPoste.Protection :
              return true;
          case Constantes_Phenix.ProtectionVerificationPoste.Verification_Protection :
                for(let mesure of allOtherMesurePrise){
                  let currentMesure = mesureProtec.find(x => x.PosteId == mesure.PosteDemandeId);
                  if(!this.PasDeDepeche(mesure))
                    continue;
                  if(currentMesure != null && Constantes_Phenix.ProtectionVerificationPoste.Protection )
                    isPrioritaire = false;
                }
                break;
          default:return true;
        }
      }
      return isPrioritaire;
  }

  filterMesures() : Array<MesureProtectionPrise>{
    if (this.dfv.MesureProtectionPriseList == null){
      return new Array<MesureProtectionPrise>();
    }
    if (this.isPosteCreateur){
      return this.dfv.MesureProtectionPriseList;
    }
    else{
      return this.dfv.MesureProtectionPriseList.filter(a=>a.PosteDemande.Nom === this.authService.getLoggedUser().Poste);
    }
  }

}
