import { Component, OnInit, Input, OnChanges, SimpleChanges, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
import { DFV, MesureProtectionPrise } from '../../../../../shared/models/DFV/dfv.models';
import { MesuresService } from '../../../../../shared/services/ficheDfv/mesures.service';
import { Poste } from '../../../../../shared/models/FicheZep/zep.models';

@Component({
  selector: 'app-create-mesure',
  templateUrl: './create-mesure.component.html',
  styleUrls: ['./create-mesure.component.scss']
})
export class CreateMesureComponent implements OnInit, OnChanges {


  //IdDFV obligatoire
  @Input() mesure : MesureProtectionPrise;
  @Input() postes : Poste[];
  @Input() add : boolean;//add or update

  public onClose: Subject<MesureProtectionPrise>;

  form : FormGroup;
  fields:FormlyFieldConfig[];
  options : FormlyFormOptions = {
    formState: {
      disabled: false,
      awesomeIsForced: false,
      model: this.mesure,
    }
  }

  ngOnChanges(changes: SimpleChanges): void {

    this.initForm();
  }

  constructor(
    private mesuresService : MesuresService,
    public modalRef: BsModalRef,
  )
  {
    this.form = new FormGroup({});
    this.mesure = new MesureProtectionPrise();
    this.onClose = new Subject();
    // this.mesure.PriseDeMesure
    // this.mesure.VerificationLiberationVoieprevue
    this.initForm();
  }

  initForm(){
    this.fields = [
      {
        key: "PosteDemandeId",
        className:"",
        type:"select",
        templateOptions:{
          label:"POSTE DEMANDÉ",
          valueProp : "Id",
          labelProp : "Nom",
          options: this.postes,
          required:true,
          disabled: this.add == false
        }
      },
      {
        key:"PriseDeMesure",
        type : "checkbox",
        templateOptions :{
          label:"J'ai pris les mesures de protection",
        }
      },
      {
        key:"VerificationLiberationVoieprevue",
        type : "checkbox",
        templateOptions :{
          label:"J'ai vérifié la libération de voie prévue",
        }
      },
      {
        key:"DateDePrise",
        className:"col-md-12",
        type:"datetimepicker",
        defaultValue : new Date(this.mesure.DateDePrise),
        templateOptions:{
          label: 'DATE ET HEURE DE LA DEMANDE',
          required: true, showTime : true
        }
      }
    ]
  }

  ngOnInit() {

  }

  public closePopup(){
    this.modalRef.hide();

  }

  submit() {
    if(this.add){
      this.mesuresService.AddMesure(this.mesure).subscribe(
        data => {
          this.mesure = data;
          this.onClose.next(this.mesure);
          this.closePopup();
        },
        err => {
          console.log(err);
          alert("erreur");
        }
      )
    }else{
      this.mesuresService.UpdateMesure(this.mesure).subscribe(
        data => {
          this.mesure = data;
          this.onClose.next(this.mesure);
          this.closePopup();
        },
        err => {
          console.log(err);
          alert("erreur");
        }
      )
    }

  }

}
