import { Component, OnInit, Input,  Output, EventEmitter } from '@angular/core';
import { DFV, Aiguille_DFV, Occupation, Manoeuvre, ManoeuvreType, OccupationType } from '../../../shared/models/DFV/dfv.models';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';
import { OccupationService } from '../../../shared/services/occupation/occupation.service';
import { ManoeuvreService } from '../../../shared/services/manoeuvre/manoeuvre.service';
import { PointRemarquable_Zep } from '../../../shared/models/FicheZep/zep.models';
import { PointRemarquableService } from '../../../shared/services/pointRemarquable/point_remarquable.service';
import { RestitutionService } from '../../../shared/services/ficheDfv/restitution.service';
import swal from 'sweetalert2';
import { PhenixToasterService } from '../../../shared/settings/phenix-toaster.service';
import * as _ from "lodash";

@Component({
  selector: 'app-dfv-bloc-restitution',
  templateUrl: './dfv-bloc-restitution.component.html',
  styleUrls: ['./dfv-bloc-restitution.component.scss']
})
export class DfvBlocRestitutionComponent implements OnInit {
  @Input() dfv : DFV;
  @Input() listTypeOccupation : Array<OccupationType>;
  @Input() listTypeManoeuvre : Array<ManoeuvreType>;
  @Input() listPointRemarquable : Array<PointRemarquable_Zep>;
  @Input() isPosteCreateur : boolean;
  @Output() dfvChange = new EventEmitter<DFV>();

  dfvCopy : DFV;


  isRestituee : boolean;
  zepLibre : boolean | null;
  disableRadioZepLibre : boolean | null;
  reserveAiquille : any;
  occupationResitutionList : Array<Occupation>;
  degagementList : Array<Manoeuvre>;

  formRestitution = new FormGroup({});
  fieldsRestition: FormlyFieldConfig[] = new Array<FormlyFieldConfig>();

  formResrveAiguille = new FormGroup({});
  fieldsResrveAiguille: FormlyFieldConfig[] = new Array<FormlyFieldConfig>();

  optionsFormRestitution: FormlyFormOptions = {
    formState: {
      disabled : false,
      awesomeIsForced: false,
      model : this.dfvCopy,
    }
  };
  optionsFormResrveAiguille: FormlyFormOptions = {
    formState: {
      disabled : true,
      awesomeIsForced: false,
      model : this.reserveAiquille
    }
  };



  constructor(private occupationService : OccupationService,
              private manoeuvreService : ManoeuvreService,
              private pointRemarquableService : PointRemarquableService,
              private restitutionService: RestitutionService,
              private toaster : PhenixToasterService
            )
    {
    this.dfv = new DFV();
    this.dfvCopy = new DFV();
    if (this.dfv.IsResitutionParDepeche != null)
      this.isRestituee = true;
    else this.isRestituee = false;
    this.occupationResitutionList = new Array<Occupation>();
    this.degagementList = new Array<Manoeuvre>();
    this.disableRadioZepLibre = false;
    this.reserveAiquille = {
      avecReserve : false,
      aiquilleDFVList : new Array<Aiguille_DFV>(),
      hideButtonsInsideForm:false
    }
  }



  initFormRestitution(){

    this.formRestitution = new FormGroup({});
    if(_.isEmpty(this.dfvCopy))
    this.dfvCopy = _.cloneDeep(this.dfv);

    this.fieldsRestition =  [
      {
        type: "horizontalRadio", key: "IsResitutionParDepeche", id: "isResitutionParDepeche",
        templateOptions: {
          required: true,
          label: 'Réception de la restitution',
          inputClass: 'radio-inline',
          options: [{ key: true, value: "Par dépêche du RPTx" }],
          disabled: this.isRestituee
        }
      },
      {
        fieldGroupClassName:"row",
        fieldGroup:[
          {
            key:"DateRestitution",
            className:"col-md-6",
            type:"datetimepicker",
            defaultValue : this.dfvCopy.DateRestitution == null ? new Date() : new Date(this.dfvCopy.DateRestitution),
            hideExpression:'model.IsResitutionParDepeche == null',
            templateOptions:{
              label: 'Date et heure de la réstitution',
              required: true,
              disabled: this.isRestituee, showTime : true
            }
          },
          {
              key:"NumeroDepecheRecuRestitution",
              className:"col-md-6",
              hideExpression:'model.IsResitutionParDepeche != true',
              type:"input",
              templateOptions:{
                type:"text",
                label: 'Numéro donné',
                required: true,
                disabled: this.isRestituee
              }
          },
        ]
      }

    ]
    this.optionsFormRestitution.formState.model = this.dfvCopy;
  }

  initFormAuiquille(){
    this.formResrveAiguille = new FormGroup({});
    if(_.isEmpty(this.dfvCopy))
    this.dfvCopy = _.cloneDeep(this.dfv);
    this.fieldsResrveAiguille =  [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            className: 'col-md-5', key: 'avecReserve', type: 'checkbox',
            templateOptions: {
              label: 'Avec réserve sur aiguille',
              disabled: this.isRestituee
            }
          },
          {
            hideExpression: `model.avecReserve === false || model.hideButtonsInsideForm == true` ,
            className: 'col-md-3',type: 'button', templateOptions: {
              inputClass: 'btn btn-success',
              iconClass: 'fa fa-plus',
              withLabel:false,
              titre:'',
              btnClick: (event: any) => {
                let aiguille_DFV = new Aiguille_DFV();
                aiguille_DFV.DFVId = this.dfv.Id;
                this.reserveAiquille.aiquilleDFVList.push(aiguille_DFV);
                this.initFormAuiquille();
              },
            },
          },
          {
            hideExpression: `!(model.aiquilleDFVList.length > 0) || model.hideButtonsInsideForm == true` ,
            className: 'col-md-3',type: 'button', templateOptions: {
              inputClass: 'btn btn-success',
              iconClass: 'fa fa-save',
              withLabel:false,
              titre:' Enregistrer',
              type:'submit',
              btnClick: (event: any) => {
              },
            },
          }
        ],
      },
      {
        hideExpression: `model.avecReserve === false` ,
        fieldGroupClassName: 'row',

        fieldGroup: [
          {
            key: 'aiquilleDFVList',
            type: 'repeat',
            templateOptions:{
              showRemoveButton: !this.isRestituee,
              buttonText:'',
              buttonIconClass:'fa fa-close',
              buttonClass:'btn btn-danger',
              buttonFatherClass:'col-md-1',
              disabled: this.isRestituee
            },
            fieldArray: {
              className: 'row',
              fieldGroup: [
                {
                  key: "AiguilleId",
                  className:"col-md-5",
                  type:"select",
                  templateOptions:{
                    label:"Aiguille",
                    options: this.dfvCopy.Zep.AiguilleList !== null ? this.dfvCopy.Zep.AiguilleList.map(x => ({ label: x.Nom, value: x.Id })) : [],
                    required:true,
                    disabled: this.isRestituee
                  }
                },
                {
                  key: "Commentaire", className: 'col-md-6',type: 'textarea',
                  templateOptions: {type: 'text',label: 'Commentaire',    disabled: this.isRestituee}
                },

              ],
            },
          },
        ],
      },
    ]
    this.optionsFormResrveAiguille.formState.model = this.reserveAiquille;
  }
  submitReserveAiguille(){
    if (this.formResrveAiguille.valid) {
      let response = this.restitutionService.updateAiguilleDFV(this.reserveAiquille.aiquilleDFVList);
      response.subscribe(
        result => {
          this.reserveAiquille.aiquilleDFVList = result;
          swal('Mise à jour', "Les réserves ont été enregistrées", 'success');
        },
        err => {
          console.log(err);
        }

      )
    }
  }
  submitRestituer(){
    if (this.formRestitution.valid) {
      let response = this.restitutionService.restituer(this.dfvCopy);
      response.subscribe(
        result => {
          this.dfv = result;
          this.dfvCopy = result;
          this.dfvChange.emit(this.dfv);
          this.isRestituee = true;
          this.reserveAiquille.hideButtonsInsideForm = true;
          this.initFormAuiquille();
          this.initFormRestitution();
        },
        err => {
          this.toaster.showError('Erreur', err)
          console.log(err);
        }

      )
    }
  }
  ngOnInit() {
    if (this.dfv.IsResitutionParDepeche != null){
      this.isRestituee = true;
      this.reserveAiquille.hideButtonsInsideForm = true;
    }
    else this.isRestituee = false;
    this.occupationResitutionList = this.getOccupationRestitutionZep();
    this.degagementList = this.getManoeuvreDegagement();
    if(this.dfv.Aiguille_DFVList != null){
      this.reserveAiquille.aiquilleDFVList = this.dfv.Aiguille_DFVList;
      this.reserveAiquille.avecReserve = true;
    }
    if (this.occupationResitutionList.length > 0 || this.degagementList.length > 0) {
      this.disableRadioZepLibre = true;
      this.zepLibre = false;
    }
    this.initFormAuiquille();
    this.initFormRestitution();
  }
  getOccupationRestitutionZep():Array<Occupation>{
    return this.occupationService.filtrerOccupations(Constantes_Phenix.OccupationTypes.OccupeeALaRestitution, this.dfv.OccupationList);
  }
  getManoeuvreDegagement():Array<Manoeuvre>{
    return this.manoeuvreService.filtrerManoeuvre(Constantes_Phenix.ManoeuvreTypes.Degagement, this.dfv.ManoeuvreList)
      .filter(a=>a.Verbale !== true && a.ParEcrit !== true && a.ParDepeche !== true);
  }
  setradioRestitTTx(value : boolean){
    this.zepLibre = value;
  }
  /////Point remarquable
  getPointRemarquableById(idPr : number):PointRemarquable_Zep{
    return this.pointRemarquableService.getPointRemarquableById(idPr,this.listPointRemarquable)
  }

}
