import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DFV, Occupation, OccupationType, Manoeuvre, ManoeuvreType, DepecheType, Manoeuvre_Depeche, AutreDepeche } from '../../../shared/models/DFV/dfv.models';
import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';
import { FicheDfvService } from '../../../shared/services/ficheDfv/fiche-dfv.service';
import { OccupationService } from '../../../shared/services/occupation/occupation.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { CreateOccupationComponent } from './modals/create-occupation/create-occupation.component';
import { UpdateOccupationComponent } from './modals/update-occupation/update-occupation.component';
import * as _  from 'lodash';
import Swal from 'sweetalert2';
import {PointRemarquable_Zep} from '../../../shared/models/FicheZep/zep.models';
import { CreateManoeuvreComponent } from './modals/create-manoeuvre/create-manoeuvre.component';
import { UpdateManoeuvreComponent } from './modals/update-manoeuvre/update-manoeuvre.component';
import { ManoeuvreService } from '../../../shared/services/manoeuvre/manoeuvre.service';
import { CreateAutreDepecheComponent } from './modals/create-autre-depeche/create-autre-depeche.component';
import { PointRemarquableService } from '../../../shared/services/pointRemarquable/point_remarquable.service';
import { DepecheComponent } from '../depeche/depeche.component';
import { AuthenticationService } from '../../../shared/services/administration/authentication.service';

@Component({
  selector: 'app-dfv-bloc-ttx',
  templateUrl: './dfv-bloc-ttx.component.html',
  styleUrls: ['./dfv-bloc-ttx.component.scss']
})
export class DfvBlocTtxComponent implements OnInit {
  modalRef: BsModalRef;
  @Input() dfv: DFV;
  @Output() dfvChange = new EventEmitter<DFV>();
  @Input() listTypeOccupation : Array<OccupationType>;
  @Input() listTypeManoeuvre : Array<ManoeuvreType>;
  @Input() listPointRemarquable : Array<PointRemarquable_Zep>;
  @Input() isPosteCreateur : boolean;
  @Input() isNotification : boolean = false;
  @Input() isRestitution : boolean = false;
  @Input() listDepecheType : Array<DepecheType>;
  isAccordee : boolean = false

  @Output() autreDepecheAdded: EventEmitter<void> = new EventEmitter<void>();


	modelRef : BsModalRef;

  constructor( public occupationService : OccupationService
    ,private modalService: BsModalService
    ,private manoeuvreService : ManoeuvreService
    ,private pointRemarquableService : PointRemarquableService
    ,private ficheDfvService : FicheDfvService
    ,private authenticationService : AuthenticationService
  )
  {
    this.listDepecheType = new Array<DepecheType>();
  }

  ngOnInit() {
    if (this.dfv != null && this.dfv.StatusZep != null){
      if(this.dfv.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Accordee
        || this.dfv.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Restituee){
        this.isAccordee = true;
      }
    }
  }

  getDepecheNotification (manoeuvre : Manoeuvre, typeManoeuvre: string ){
    var man_dep : Manoeuvre_Depeche;

    if(typeManoeuvre == "Notification") {
      man_dep =  manoeuvre.Manoeuvre_DepecheList.find(t => t.Depeche.DepecheType.Nom == Constantes_Phenix.DepecheType.Notification);
    }else if(typeManoeuvre == "Restitution") {
      man_dep =  manoeuvre.Manoeuvre_DepecheList.find(t => t.Depeche.DepecheType.Nom == Constantes_Phenix.DepecheType.Restitution);
    }
    if(man_dep){
      return "N° donné : " + man_dep.Depeche.NumeroDonne + ", N° reçu : " + man_dep.Depeche.NumeroDonne;
    }
    return "";
  }

  isNotified(manoeuvre : Manoeuvre, typeManoeuvre ){
    var man_dep : Manoeuvre_Depeche;

    if(typeManoeuvre == "Notification") {
      man_dep =  manoeuvre.Manoeuvre_DepecheList
      .find(t => t.Depeche.DepecheType.Nom == Constantes_Phenix.DepecheType.Notification);
    }
    else if(typeManoeuvre == "Restitution") {
      man_dep =  manoeuvre.Manoeuvre_DepecheList
      .find(t => t.Depeche.DepecheType.Nom == Constantes_Phenix.DepecheType.Restitution);
    }
    return man_dep && man_dep.Depeche;
  }

  /**
   * Notifier le poste concerné qu'un engagement ou un dégagement
   * sera effectué sur un carré dont il a la responsabilité
   * La notification se fait par échange de dépêche
   */
  notifierParDepeche(manoeuvre : Manoeuvre, typeManoeuvre : string){
    this.modalRef = this.modalService.show(DepecheComponent);
    var depType = null;
    if(typeManoeuvre == "Notification"){
      depType = this.listDepecheType.find(t => t.Nom == Constantes_Phenix.DepecheType.Notification);
    }else if (typeManoeuvre == "Restitution"){
      depType = this.listDepecheType.find(t => t.Nom == Constantes_Phenix.DepecheType.Restitution);
    }
    this.modalRef.content.depecheType = depType;
    this.modalRef.content.manoeuvreId = manoeuvre.Id;
    this.modalRef.content.dfv = this.dfv;
    this.modalRef.content.listPointRemarquable = this.listPointRemarquable;
    this.modalRef.content.initCollation();
    this.modalRef.content.onClose.subscribe(result => {
      if (result != null) {
        this.ficheDfvService.getDFVById(this.dfv.Id).subscribe(
          data => {
            this.dfv = data;
            this.dfvChange.emit(data);
          },
          err => {
            console.log(err);
            alert("erreur");
          }
        )
      } else {
        alert("erreur");
      }
    });
  }

  isDemandeEnCreation(){
    return this.dfv.StatusZep.Statut == Constantes_Phenix.ZepStatuts.EnCreation;
  }
  isDemandeEnAttenteAccord(){
    return this.dfv.StatusZep.Statut == Constantes_Phenix.ZepStatuts.EnAccord;
  }
////Occupation, Accord DFV
  showCreateOccupationAccordModal(){
    let typeOccupation = this.listTypeOccupation.find(x => x.Nom == Constantes_Phenix.OccupationTypes.OccupeeALAccord);
    if(typeOccupation != null){
      this.showCreateOccupationModal(typeOccupation);
    }else{
      alert("impossible de retrouver le type d'occupation Occupée a l'accord");
    }
  }

  removeOccupation(occupation : Occupation){
    let indexOccup = this.dfv.OccupationList.findIndex(x => x == occupation);
    if(indexOccup == -1){
      console.log("Erreur impossible de retrouver l'occupation");
      return;
    }
    Swal({
      title: 'Êtes-vous sur ?',
      text: 'Voulez vous supprimer cette occupation ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Supprimer',
      cancelButtonText: 'Annuler',
    }).then((result) => {
      if (result.value) {
        let resp = this.occupationService.deleteOccupation(occupation.Id) ;
        resp.subscribe(
          data => {
            this.dfv.OccupationList.splice(indexOccup,1);
            //Swal('Supprimé !', 'L\'occupation à été supprimée ', 'success');
          },
          err => console.log(err)
        )
      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })
  }

  getOccupationAccordDFV():Array<Occupation>{
    return this.occupationService.filtrerOccupations(Constantes_Phenix.OccupationTypes.OccupeeALAccord, this.dfv.OccupationList);
  }
////Occupation, Restitution Zep
  showCreateOccupationRestitutionModal(){
    let typeOccupation = this.listTypeOccupation.find(x => x.Nom == Constantes_Phenix.OccupationTypes.OccupeeALaRestitution);
    if(typeOccupation != null){
      this.showCreateOccupationModal(typeOccupation);
    }else{
      alert("impossible de retrouver le type d'occupation Occupée a l'accord");
    }
  }
  getOccupationRestitutionZep():Array<Occupation>{
    return this.occupationService.filtrerOccupations(Constantes_Phenix.OccupationTypes.OccupeeALaRestitution, this.dfv.OccupationList);
  }

////Occupation
 showCreateOccupationModal(typeOccup : OccupationType){
    this.modelRef = this.modalService.show(CreateOccupationComponent, {class:"modal-md"});
    this.modelRef.content.typeOccupation = typeOccup;
    this.modelRef.content.dfv = this.dfv;
    this.modelRef.content.listPointRemarquable = this.listPointRemarquable;

    this.modelRef.content.initForm() ;
    (<CreateOccupationComponent>this.modelRef.content).onClose.subscribe(result => {
      if (result != null) {
        this.dfv.OccupationList.push(result);
      } else {

      }
    });
  }
  showUpdateOccupationModal( occupation : Occupation){
    let occupIndex = this.dfv.OccupationList.findIndex(x => x.Id == occupation.Id);
    this.modelRef = this.modalService.show(UpdateOccupationComponent, {class:"modal-md"});
    this.modelRef.content.occupation = _.cloneDeep(occupation);
    this.modelRef.content.initForm() ;
    (<UpdateOccupationComponent>this.modelRef.content).onClose.subscribe(result => {
      if (result != null) {
        this.dfv.OccupationList[occupIndex] = result;
      } else {

      }
    });
  }




////Engagement
getManoeuvreEngagement():Array<Manoeuvre>{
  let user = this.authenticationService.getLoggedUser();
  if(this.isNotification){
    return this.manoeuvreService.filtrerManoeuvre(Constantes_Phenix.ManoeuvreTypes.Engagement, this.dfv.ManoeuvreList, true, user.Poste);
  }else{
    return this.manoeuvreService.filtrerManoeuvre(Constantes_Phenix.ManoeuvreTypes.Engagement, this.dfv.ManoeuvreList);
  }
}
showCreateEngagementModal(){
  let typeManoeuvre = this.listTypeManoeuvre.find(x => x.Nom == Constantes_Phenix.ManoeuvreTypes.Engagement);
    if(typeManoeuvre != null){
      this.showCreateManoeuvreModal(typeManoeuvre);
    }else{
      alert("impossible de retrouver le type de manoeuvre engagement");
    }
}
////Degagement
getManoeuvreDegagement():Array<Manoeuvre>{
  let user = this.authenticationService.getLoggedUser();
  if(this.isNotification){
    return this.manoeuvreService.filtrerManoeuvre(Constantes_Phenix.ManoeuvreTypes.Degagement, this.dfv.ManoeuvreList,  true, user.Poste);
  }else{
    return this.manoeuvreService.filtrerManoeuvre(Constantes_Phenix.ManoeuvreTypes.Degagement, this.dfv.ManoeuvreList);
  }
}

showCreateDegagementModal(){
  let typeManoeuvre = this.listTypeManoeuvre.find(x => x.Nom == Constantes_Phenix.ManoeuvreTypes.Degagement);
    if(typeManoeuvre != null){
      this.showCreateManoeuvreModal(typeManoeuvre);
    }else{
      alert("impossible de retrouver le type de manoeuvre degagement");
    }
}
////Autorisation Franchissement
showCreateAutorisationModal(){
  let typeManoeuvre = this.listTypeManoeuvre.find(x => x.Nom == Constantes_Phenix.ManoeuvreTypes.Franchissement);
    if(typeManoeuvre != null){
      this.showCreateManoeuvreModal(typeManoeuvre);
    }else{
      alert("impossible de retrouver le type de manoeuvre degagement");
    }
}
getManoeuvreAutorisationFranchissement():Array<Manoeuvre>{
  return this.manoeuvreService.filtrerManoeuvre(Constantes_Phenix.ManoeuvreTypes.Franchissement, this.dfv.ManoeuvreList);
}


showCreateManoeuvreModal(typeManoeuvre : ManoeuvreType){
  this.modelRef = this.modalService.show(CreateManoeuvreComponent, {class:"modal-md"});
  this.modelRef.content.typeManoeuvre = typeManoeuvre;
  this.modelRef.content.listPointRemarquable = this.listPointRemarquable;
  this.modelRef.content.dfv = this.dfv;
  this.modelRef.content.initForm() ;
  (<CreateManoeuvreComponent>this.modelRef.content).onClose.subscribe(result => {
    if (result != null) {
      this.dfv.ManoeuvreList.push(result);
    } else {

    }
  });
}
showCreateAutreDepecheModal(manoeuvre : Manoeuvre){
  let manoeuvreIndex = this.dfv.ManoeuvreList.findIndex(x => x.Id == manoeuvre.Id);
  let typeManoeuvre = this.listTypeManoeuvre.find(x => x.Id == manoeuvre.ManoeuvreTypeId);
  if(typeManoeuvre != null){
    this.modelRef = this.modalService.show(CreateAutreDepecheComponent, {class:"modal-md"});
    this.modelRef.content.typeManoeuvre = typeManoeuvre;
    this.modelRef.content.listPointRemarquable = this.listPointRemarquable;
    this.modelRef.content.manoeuvre = _.cloneDeep(manoeuvre);
    this.modelRef.content.initForm() ;
    (<CreateAutreDepecheComponent>this.modelRef.content).onClose.subscribe(result => {
      if (result != null) {
        this.dfv.ManoeuvreList[manoeuvreIndex] = result;
        this.autreDepecheAdded.emit();
      } else {

      }
    });
  }else{
    alert("impossible de retrouver le type de manoeuvre");
  }

}

showUpdateManoeuvreModal(manoeuvre : Manoeuvre){
  let manoeuvreIndex = this.dfv.ManoeuvreList.findIndex(x => x.Id == manoeuvre.Id);
  let typeManoeuvre = this.listTypeManoeuvre.find(x => x.Id == manoeuvre.ManoeuvreTypeId);
  if(typeManoeuvre != null){
    this.modelRef = this.modalService.show(UpdateManoeuvreComponent, {class:"modal-md"});
    this.modelRef.content.typeManoeuvre = typeManoeuvre;
    this.modelRef.content.listPointRemarquable = this.listPointRemarquable;
    this.modelRef.content.manoeuvre = _.cloneDeep(manoeuvre);
    this.modelRef.content.initForm() ;
    (<UpdateManoeuvreComponent>this.modelRef.content).onClose.subscribe(result => {
      if (result != null) {
        this.dfv.ManoeuvreList[manoeuvreIndex] = result;
      } else {

      }
    });
  }else{
    alert("impossible de retrouver le type de manoeuvre");
  }
}
removeManoeuvre(manoeuvre : Manoeuvre){
  let indexManoeuvre = this.dfv.ManoeuvreList.findIndex(x => x == manoeuvre);
  if(indexManoeuvre == -1){
    console.log("Erreur impossible de retrouver la manoeuvre");
    return;
  }
  Swal({
    title: 'Êtes-vous sur ?',
    text: 'Voulez vous supprimer cette manoeuvre ?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Supprimer',
    cancelButtonText: 'Annuler',
  }).then((result) => {
    if (result.value) {
      let resp = this.manoeuvreService.deleteManoeuvre(manoeuvre.Id) ;
      resp.subscribe(
        data => {
          this.dfv.ManoeuvreList.splice(indexManoeuvre,1);
          //Swal('Supprimé !', 'La manoeuvre à été supprimée ', 'success');
        },
        err => console.log(err)
      )
    } else if (result.dismiss === Swal.DismissReason.cancel) {

    }
  })
}

/////Point remarquable
getPointRemarquableById(idPr : number):PointRemarquable_Zep{
  return this.pointRemarquableService.getPointRemarquableById(idPr,this.listPointRemarquable)
}
}
