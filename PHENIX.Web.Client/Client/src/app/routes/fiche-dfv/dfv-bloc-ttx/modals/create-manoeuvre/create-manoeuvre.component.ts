import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import {
  Manoeuvre,
  ManoeuvreType,
  DFV
} from "../../../../../shared/models/DFV/dfv.models";
import { Subject } from "rxjs/Subject";
import { FormlyFieldConfig, FormlyFormOptions } from "@ngx-formly/core";
import { BsModalRef } from "ngx-bootstrap";
import { ManoeuvreService } from "../../../../../shared/services/manoeuvre/manoeuvre.service";
import { Constantes_Phenix } from "../../../../../shared/models/Commun/constantes.model";
import { PointRemarquable_Zep } from "../../../../../shared/models/FicheZep/zep.models";
import { PhenixToasterService } from "../../../../../shared/settings/phenix-toaster.service";

@Component({
  selector: "app-create-manoeuvre",
  templateUrl: "./create-manoeuvre.component.html",
  styleUrls: ["./create-manoeuvre.component.scss"]
})
export class CreateManoeuvreComponent implements OnInit {
  public typeManoeuvre: ManoeuvreType;
  public manoeuvre: Manoeuvre;
  public listPointRemarquable: Array<PointRemarquable_Zep>;

  public listPointRemarquableEngage_Degage : Array<PointRemarquable_Zep>;
  public listPointRemarquableFranchissement : Array<PointRemarquable_Zep>;

  public dfv: DFV;
  public onClose: Subject<Manoeuvre>;

  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [];
  options: FormlyFormOptions = {
    formState: {
      awesomeIsForced: false,
      model: this.manoeuvre
    }
  };

  constructor(
    public modal: BsModalRef,
    private manoeuvreService: ManoeuvreService,
    private toastSrv : PhenixToasterService,
  ) {
    this.manoeuvre = new Manoeuvre();
    this.onClose = new Subject();
  }

  ngOnInit() {

  }

  initForm() {
    this.manoeuvre.ManoeuvreTypeId = this.typeManoeuvre.Id;
    this.manoeuvre.DFVId = this.dfv.Id;
    this.form = new FormGroup({});
    this.listPointRemarquableEngage_Degage = this.listPointRemarquable.filter(x =>
                                    x.PointRemarquableType.Nom == Constantes_Phenix.PointRemarquableTypes.PointDegagement ||
                                    x.PointRemarquableType.Nom == Constantes_Phenix.PointRemarquableTypes.PointEngagement
                                   )
    this.listPointRemarquableFranchissement = this.listPointRemarquable.filter(x =>
    x.PointRemarquableType.Nom == Constantes_Phenix.PointRemarquableTypes.SignauxIntermediaires
    )
    if (this.typeManoeuvre.Nom == Constantes_Phenix.ManoeuvreTypes.Engagement)
      this.initFormEngagement();
    else if (
      this.typeManoeuvre.Nom == Constantes_Phenix.ManoeuvreTypes.Degagement
    )
      this.initFormDegagement();
    else if (
      this.typeManoeuvre.Nom == Constantes_Phenix.ManoeuvreTypes.Franchissement
    )
      this.initFormAutorisationFranchissement();
    else this.closePopup(null);
  }

  initFormEngagement() {
    this.fields = [
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "TTXNumero",
            className: "col-md-4",
            type: "input",
            templateOptions: {
              type: "text",
              label: "Numéro du TTx",
              required: false
            }
          },
          {
            key: "TTXNature",
            className: "col-md-8",
            type: "select",
            templateOptions: {
              type: "text",
              label: "Nature du TTx",
              required: true,
              options: Constantes_Phenix.TypeTTX.ListAllType.map(x => ({
                label: x,
                value: x
              })),
            }
          },
        ]
      },
      {
        key: "PointRemarquable_ZepId",
        className: "col-md-8",
        type: "select",
        templateOptions: {
          type: "text",
          label: "Point d\'engagement",
          required: true,
          options: this.listPointRemarquableEngage_Degage.map(x => ({
            label: x.PosteConcerne.Nom + ' / '+ x.Valeur,
            value: x.Id
          })),
        }
      },
    ];
  }
  initFormDegagement() {
    this.fields = [
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "TTXNumero",
            className: "col-md-4",
            type: "input",
            templateOptions: {
              type: "text",
              label: "Numéro du TTx",
              required: false
            }
          },
          {
            key: "TTXNature",
            className: "col-md-8",
            type: "select",
            templateOptions: {
              type: "text",
              label: "Nature du TTx",
              required: true,
              options: Constantes_Phenix.TypeTTX.ListAllType.map(x => ({
                label: x,
                value: x
              })),
            }
          },
        ]
      },

      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "PointRemarquable_ZepId",
            className: "col-md-6",
            type: "select",
            templateOptions: {
              type: "text",
              label: "Point de dégagement",
              required: true,
              options: this.listPointRemarquableEngage_Degage.map(x => ({
                label: x.PosteConcerne.Nom + ' / '+ x.Valeur,
                value: x.Id
              })),
            }
          },
          {
            key: "TTXVoie",
            className: "col-md-6",
            type: "input",
            templateOptions: {
              type: "text",
              label: "Voie TTX",
              required: true     ,
            }
          },
        ]
      },

      // {
      //   key: "Commentaire",
      //   className: "col-md-12",
      //   type: "textarea",
      //   templateOptions: {
      //     label: "Commentaire",
      //     required: false,
      //     rows: 4,
      //   }
      // },
    ];
  }
  initFormAutorisationFranchissement() {
    this.fields = [
      {
        key: "PointRemarquable_ZepId",
        className: "col-md-12",
        type: "select",
        templateOptions: {
          type: "text",
          label: "Signal intermédiaire",
          required: true,
          options: this.listPointRemarquableFranchissement.map(x => ({
            label: x.PosteConcerne.Nom + ' / '+ x.Valeur,
            value: x.Id
          })),
        }
      },
      {
        key: "Commentaire",
        className: "col-md-12",
        type: "textarea",
        templateOptions: {
          label: "Commentaire",
          required: true,
          rows: 4,
        }
      },
    ];
  }

  submit(model) {
    var resp = this.manoeuvreService.createManoeuvre(model);
    resp.subscribe(
      a => {
        this.toastSrv.showSuccess("Succès","Ajoutée.");
        this.closePopup(a);
      },
      error => {
        this.toastSrv.showError("Erreur",error.error.error);
      }
    );
  }

  closePopup(item: Manoeuvre) {
    this.onClose.next(item);
    this.modal.hide();
  }
}
