import { Component, OnInit } from '@angular/core';
import { Manoeuvre, ManoeuvreType } from '../../../../../shared/models/DFV/dfv.models';
import { Subject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap';
import { ManoeuvreService } from '../../../../../shared/services/manoeuvre/manoeuvre.service';
import { FormGroup } from '@angular/forms';
import { Constantes_Phenix } from '../../../../../shared/models/Commun/constantes.model';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { PointRemarquable_Zep } from '../../../../../shared/models/FicheZep/zep.models';
import { PhenixToasterService } from '../../../../../shared/settings/phenix-toaster.service';

@Component({
  selector: 'app-update-manoeuvre',
  templateUrl: './update-manoeuvre.component.html',
  styleUrls: ['./update-manoeuvre.component.scss']
})
export class UpdateManoeuvreComponent implements OnInit {

  public manoeuvre : Manoeuvre;
  public onClose: Subject<Manoeuvre>;
  public typeManoeuvre: ManoeuvreType;
  public listPointRemarquable: Array<PointRemarquable_Zep>;

  public listPointRemarquableEngage_Degage : Array<PointRemarquable_Zep>;
  public listPointRemarquableFranchissement : Array<PointRemarquable_Zep>;

  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [];
  options: FormlyFormOptions = {
    formState: {
      awesomeIsForced: false,
      model: this.manoeuvre
    }
  };

  constructor(public modal: BsModalRef,private manoeuvreService : ManoeuvreService,private toastSrv :PhenixToasterService) {
    this.onClose = new Subject();
    this.manoeuvre = new Manoeuvre();
   }

  ngOnInit() {
  }
  initForm() {
    this.listPointRemarquableEngage_Degage = this.listPointRemarquable.filter(x =>
      x.PointRemarquableType.Nom == Constantes_Phenix.PointRemarquableTypes.PointDegagement ||
      x.PointRemarquableType.Nom == Constantes_Phenix.PointRemarquableTypes.PointEngagement
     )
      this.listPointRemarquableFranchissement = this.listPointRemarquable.filter(x =>
      x.PointRemarquableType.Nom == Constantes_Phenix.PointRemarquableTypes.SignauxIntermediaires
      )
    this.form = new FormGroup({});
    if (this.typeManoeuvre.Nom == Constantes_Phenix.ManoeuvreTypes.Engagement)
      this.initFormEngagement();
    else if (
      this.typeManoeuvre.Nom == Constantes_Phenix.ManoeuvreTypes.Degagement
    )
      this.initFormDegagement();
    else if (
      this.typeManoeuvre.Nom == Constantes_Phenix.ManoeuvreTypes.Franchissement
    )
      this.initFormAutorisationFranchissement();
    else this.closePopup(null);
  }
  initFormEngagement() {
    this.fields = [
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "TTXNumero",
            className: "col-md-4",
            type: "input",
            defaultValue:this.manoeuvre.TTXNumero,
            templateOptions: {
              type: "text",
              label: "Numéro du TTx",
              required: false
            }
          },
          {
            key: "TTXNature",
            className: "col-md-8",
            type: "select",
            defaultValue:this.manoeuvre.TTXNature,
            templateOptions: {
              type: "text",
              label: "Nature du TTx",
              required: true,
              options: Constantes_Phenix.TypeTTX.ListAllType.map(x => ({
                label: x,
                value: x
              })),
            }
          },
        ]
      },
      {
        key: "PointRemarquable_ZepId",
        className: "col-md-8",
        type: "select",
        defaultValue:this.manoeuvre.PointRemarquable_ZepId,
        templateOptions: {
          type: "text",
          label: "Point d'engagement",
          required: true,
          options: this.listPointRemarquable.map(x => ({
            label: x.PosteConcerne.Nom + ' / '+ x.Valeur,
            value: x.Id
          })),
        }
      },
    ];
  }
  initFormDegagement() {
    this.fields = [
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "TTXNumero",
            className: "col-md-4",
            type: "input",
            defaultValue:this.manoeuvre.TTXNumero,
            templateOptions: {
              type: "text",
              label: "Numéro du TTx",
              required: false
            }
          },
          {
            key: "TTXNature",
            className: "col-md-8",
            type: "select",
            defaultValue:this.manoeuvre.TTXNature,
            templateOptions: {
              type: "text",
              label: "Nature du TTx",
              required: true,
              options: Constantes_Phenix.TypeTTX.ListAllType.map(x => ({
                label: x,
                value: x
              })),
            }
          },
        ]
      },
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "PointRemarquable_ZepId",
            className: "col-md-6",
            type: "select",
            defaultValue:this.manoeuvre.PointRemarquable_ZepId,
            templateOptions: {
              type: "text",
              label: "Point de dégagement",
              required: true,
              options: this.listPointRemarquable.map(x => ({
                label: x.PosteConcerne.Nom + ' / '+ x.Valeur,
                value: x.Id
              })),
            }
          },
          {
            key: "TTXVoie",
            className: "col-md-6",
            defaultValue:this.manoeuvre.TTXVoie,
            type: "input",
            templateOptions: {
              type: "text",
              label: "Voie TTx",
              required: true
            }
          },
        ]
      },



      // {
      //   key: "Commentaire",
      //   className: "col-md-12",
      //   type: "textarea",
      //   defaultValue:this.manoeuvre.Commentaire,
      //   templateOptions: {
      //     label: "Commentaire",
      //     required: false,
      //     rows: 4,
      //   }
      // },
    ];
  }
  initFormAutorisationFranchissement() {
    this.fields = [
      {
        key: "PointRemarquable_ZepId",
        className: "col-md-12",
        type: "select",
        templateOptions: {
          type: "text",
          label: "Signal intermédiaire",
          required: true,
          options: this.listPointRemarquableFranchissement.map(x => ({
            label: x.PosteConcerne.Nom + ' / '+ x.Valeur,
            value: x.Id
          })),
        }
      },
      {
        key: "Commentaire",
        className: "col-md-12",
        type: "textarea",
        defaultValue:this.manoeuvre.Commentaire,
        templateOptions: {
          label: "Commentaire",
          required: true,
          rows: 4,
        }
      },
    ];
  }

  submit(model) {
    var resp = this.manoeuvreService.updateManoeuvre(model);
    resp.subscribe(
      a => {
        this.toastSrv.showSuccess("Succès","Modifiée.");
        this.closePopup(a);
      },
      error => {
        this.toastSrv.showError("Erreur",error.error.error);
      }
    );
  }

  closePopup(item: Manoeuvre) {
    this.onClose.next(item);
    this.modal.hide();
  }
}
