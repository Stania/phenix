import { Component, OnInit } from '@angular/core';
import { Occupation, OccupationType } from '../../../../../shared/models/DFV/dfv.models';
import { BsModalRef } from 'ngx-bootstrap';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { Constantes_Phenix } from '../../../../../shared/models/Commun/constantes.model';
import { Subject } from 'rxjs/Subject';
import { OccupationService } from '../../../../../shared/services/occupation/occupation.service';
import { PhenixToasterService } from '../../../../../shared/settings/phenix-toaster.service';

@Component({
  selector: 'app-update-occupation',
  templateUrl: './update-occupation.component.html',
  styleUrls: ['./update-occupation.component.scss']
})
export class UpdateOccupationComponent implements OnInit {

  public occupation : Occupation;
  public onClose: Subject<Occupation>;

  constructor(public modal: BsModalRef,private occupationService : OccupationService, private toastSrv :PhenixToasterService) {
    this.onClose = new Subject();

   }

  ngOnInit() {
  }
  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [];
  options: FormlyFormOptions  = {
    formState: {
      awesomeIsForced: false,
      model: this.occupation
    }
  };

  initForm() {
    this.form = new FormGroup({});
    this.fields = [
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "TTXNumero",
            className: "row col-md-6",
            type: "input",
            defaultValue: this.occupation.TTXNumero,
            templateOptions: {
              type: "text",
              label: "Numéro du TTx",
              required: false
            }
          },
          {
            key: "TTXPosition",
            defaultValue: this.occupation.TTXPosition,
            className: "col-md-6",
            type: "input",
            templateOptions: { type: "text", label: "Position du TTx", required: true },
            hideExpression: this.occupation.OccupationType.Nom == Constantes_Phenix.OccupationTypes.OccupeeALAccord
          }
        ]
      },
      {
        fieldGroupClassName: "row",
        fieldGroup: [
          {
            key: "TTXNature",
            className: "row col-md-12",
            type: "select",
            defaultValue: this.occupation.TTXNature,
            templateOptions: {
              type: "text",
              label: "Nature du TTx",
              required: true,
              options: Constantes_Phenix.TypeTTX.ListAllType.map(x => ({
                label: x,
                value: x
              })),
            }
          },

        ]
      }
    ];
  }
  submit(model) {
    var resp = this.occupationService.updateOccupation(model);
    resp.subscribe(a=>{
        this.toastSrv.showSuccess("Succès","Modifiée.");
        this.closePopup(model);
      },error=>{
        console.log(error);
      });
  }
  closePopup(item: Occupation) {
    this.onClose.next(item);
    this.modal.hide();
  }
}
