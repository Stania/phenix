import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormlyFormOptions } from '@ngx-formly/core';
import { DFV, VerificationZepLibre_Ligne, Depeche } from '../../../shared/models/DFV/dfv.models';
import { Poste } from '../../../shared/models/FicheZep/zep.models';
import { FicheDfvService } from '../../../shared/services/ficheDfv/fiche-dfv.service';
import swal from 'sweetalert2';
import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';

@Component({
  selector: 'app-dfv-bloc-verification',
  templateUrl: './dfv-bloc-verification.component.html',
  styleUrls: ['./dfv-bloc-verification.component.scss']
})
export class DfvBlocVerificationComponent implements OnInit, OnChanges {

  @Input() dfv : DFV;
  @Input() postes : Poste[];

  form : FormGroup;
  fields:FormlyFieldConfig[];
  isAccordee : boolean = false;
  options : FormlyFormOptions = {
    formState: {
      disabled: false,
      awesomeIsForced: false,
    }
  }

  form2 : FormGroup;
  fields2 : FormlyFieldConfig[];
  options2 : FormlyFormOptions = {
    formState: {
      disabled: false,
      awesomeIsForced: false,
    }
  }

  selectedEntry;
  choixList = [
    {
      id: 1,
      description: 'Par observation des contrôles'
    },
    {
      id: 2,
      description: 'Par observation directe de la voie'
    },
    {
      id: 3,
      description: 'Par un autre moyen'
    },
    {
      id: 4,
      description: 'Par dépêche'
    },

  ]

  constructor(private ficheDfvService : FicheDfvService) {
    this.selectedEntry= this.choixList[0];
    this.dfv = new DFV();
    this.form = new FormGroup({});
    this.form2 = new FormGroup({});
  }

  initSelectedRadio(){
    var index = 0;
    if(this.dfv.ObservationControles) index = 0;
    if(this.dfv.ObservationDirecteVoie) index = 1;
    if(this.dfv.ObservationParAutreMoyen) index = 2;
    if(this.dfv.ObservationParEchangeDepeches) index = 3;
    this.selectedEntry = this.choixList[index];
  }

  ngOnChanges(changes: SimpleChanges): void {
      this.initSelectedRadio();
      this.initForm();
      if(this.depecheEffectuee()){
        this.choixList = this.choixList.filter(t => t.id == 4);
        this.selectedEntry.id = 4;
      }
  }

  initVerificationZepLibre(){
    if(!this.isParDepehe()) return;
    if(!this.dfv) return;
    if(!this.dfv.VerificationZepLibre_Ligne){
      this.dfv.VerificationZepLibre_Ligne = new VerificationZepLibre_Ligne();
      this.dfv.VerificationZepLibre_Ligne.Id = 0;
      this.dfv.VerificationZepLibre_Ligne.DFVId = this.dfv.Id;
      var dep = new Depeche();
      dep.DateDepeche = new Date();
      this.dfv.VerificationZepLibre_Ligne.Depeche = new Depeche();
      var dep2 = new Depeche();
      dep2.DateDepeche = new Date();
      this.dfv.VerificationZepLibre_Ligne.Depeche2 = new Depeche();
    }else if(!this.dfv.VerificationZepLibre_Ligne.Depeche){
      this.dfv.VerificationZepLibre_Ligne.Depeche = new Depeche();
    }else if(!this.dfv.VerificationZepLibre_Ligne.Depeche2){
      this.dfv.VerificationZepLibre_Ligne.Depeche2 = new Depeche();
    }
  }

  isDepeche1Empty():boolean{
    let verif = this.dfv.VerificationZepLibre_Ligne;
    return  !verif || !verif.DepecheId; // == 0 or undefined
  }

  isDepeche2Empty():boolean{
    let verif = this.dfv.VerificationZepLibre_Ligne;
    return ! verif.Depeche2Id ;
  }

  initForm(){
    if(!this.isParDepehe()) return;
    this.initVerificationZepLibre();
    this.dfv.VerificationZepLibre_Ligne.Depeche.DateDepeche = new Date(this.dfv.VerificationZepLibre_Ligne.Depeche.DateDepeche);

    this.fields = [
      {
        fieldGroupClassName:"row",
        fieldGroup:[
          {
              key:"ResponsableDFV.Nom",
              className:"col-md-6",
              type:"input",
              templateOptions:{
                type:"text",
                label: 'AC : ',
                required: true,
                disabled:true
              }
          },
          {
            key:"VerificationZepLibre_Ligne.PosteDemandeId",
            className:"col-md-6",
            type:"select",
            templateOptions:{
              label: 'demande à poste : ',
              options: this.postes,
              labelProp :"Nom",
              valueProp :"Id",
              // placeholder: 'Poste demandé',
              required: true,
              disabled: !this.isDepeche1Empty() || this.isAccordee
            },
          },
        ],
      },
      {
        fieldGroupClassName:"row",
        fieldGroup:[
          {
            key:"VerificationZepLibre_Ligne.DestinationTrain",
            className:"col-md-6",
            type:"input",
            templateOptions:{
              type:"text",
              label: 'd\'indiquer le numéro et l\'heure d\'arrivée à : ',
              required: true,
              placeholder: 'Destination du train',
              disabled: !this.isDepeche1Empty() || this.isAccordee
            }
          },
          {
            key:"VerificationZepLibre_Ligne.OrigineTrain",
            className:"col-md-6",
            type:"input",
            templateOptions:{
              type:"text",
              label: 'du dernier train reçu de : ',
              required: true,
              placeholder: 'Origine du train',
              disabled: !this.isDepeche1Empty() || this.isAccordee
            }
          },
        ]
      },
      {
        fieldGroupClassName:"row",
        fieldGroup:[
          {
            key:"VerificationZepLibre_Ligne.Depeche.NumeroDonne",
            className:"col-md-4",
            type:"input",
            templateOptions:{
              type:"text",
              label: 'Numéro donné : ',
              placeholder: 'N° dépêche donné',
              required: true,
              disabled: !this.isDepeche1Empty() || this.isAccordee
            }
          },
          {
            key:"VerificationZepLibre_Ligne.Depeche.NumeroRecu",
            className:"col-md-4",
            type:"input",
            templateOptions:{
              type:"text",
              label: 'Numéro reçu : ',
              placeholder: 'N° dépêche reçu',
              required: true,
              disabled: !this.isDepeche1Empty() || this.isAccordee
            }
          },

        ]
      },
      {
        key:"VerificationZepLibre_Ligne.Depeche.DateDepeche",
        className:"col-md-12",
        type:"datetimepicker",
        defaultValue: this.dfv.VerificationZepLibre_Ligne.Depeche.DateDepeche,
        templateOptions:{
          label: 'Date et heure de la dépêche',
          required: true,
          disabled: !this.isDepeche1Empty() || this.isAccordee,
           showTime : true
        }
      }
    ]

    this.fields2 = [
      {
        fieldGroupClassName:"row",
        fieldGroup:[
          {
            key:"VerificationZepLibre_Ligne.NumeroTrainRecu",
            className:"col-md-6",
            type:"input",
            templateOptions:{
              type:"text",
              label: 'Train numéro : ',
              required: true,
              disabled: !this.isDepeche2Empty() || this.isAccordee
            }
          },
          {
            key:"VerificationZepLibre_Ligne.HeureArrivee",
            className:"col-md-6",
            type:"datetimepicker",
            templateOptions:{
              label: 'Date et heure d\'arrivée à destination',
              required: true,
              disabled: !this.isDepeche2Empty() || this.isAccordee,
              showTime : true
            }
          }
        ],
      },
      {
        fieldGroup:[
          {
            key:"VerificationZepLibre_Ligne.Depeche2.NumeroDonne",
            className:"col-md-4",
            type:"input",
            templateOptions:{
              type:"text",
              label: 'Numéro donné : ',
              placeholder: 'N° dépêche donné',
              required: true,
              disabled: !this.isDepeche2Empty() || this.isAccordee
            }
          },
          {
            key:"VerificationZepLibre_Ligne.Depeche2.NumeroRecu",
            className:"col-md-4",
            type:"input",
            templateOptions:{
              type:"text",
              label: 'Numéro reçu : ',
              placeholder: 'N° dépêche reçu',
              required: true,
              disabled: !this.isDepeche2Empty() || this.isAccordee
            }
          },

        ]
      },
      {
        key:"VerificationZepLibre_Ligne.Depeche2.DateDepeche",
        className:"col-md-4",
        type:"datetimepicker",
        templateOptions:{
          label: 'Heure de la dépêche',
          required: true,
          disabled: !this.isDepeche2Empty() || this.isAccordee,
           showTime : true
        }
      }
    ]
  }

  validerDemandeVerification(){
    var verif = this.dfv.VerificationZepLibre_Ligne;
    verif.Depeche2 = null;
    verif.Depeche2Id = null;
    this.ficheDfvService.ValiderDemandeVerification(verif).subscribe(
      data => {
        this.dfv.VerificationZepLibre_Ligne = data;
        this.initForm();
      },
      err => {
        console.log("erreur")
        console.log(err);
      }
    )
  }

  validerConfirmationVerification(){
    var v = this.dfv.VerificationZepLibre_Ligne;
    this.ficheDfvService.ValiderConfirmationVerification(v).subscribe(
      data => {
        this.dfv.VerificationZepLibre_Ligne = data;
        this.initForm();
      },
      err => {
        console.log("erreur")
        console.log(err);
      }
    )
  }

  ngOnInit() {
    if (this.dfv != null && this.dfv.StatusZep != null){
      if(this.dfv.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Accordee
        || this.dfv.StatusZep.Statut == Constantes_Phenix.ZepStatuts.Restituee){
        this.isAccordee = true;
        this.initForm();
      }
    }
  }

  getPosteName(){
    var name = (this.dfv && this.dfv.VerificationZepLibre_Ligne && this.dfv.VerificationZepLibre_Ligne.PosteDemande)?
                  this.dfv.VerificationZepLibre_Ligne.PosteDemande.Nom :
                  "";
    return name;
  }

  getOrigineTrain(){
    var name = (this.dfv && this.dfv.VerificationZepLibre_Ligne)?
                  this.dfv.VerificationZepLibre_Ligne.OrigineTrain :
                  "";
    return name;
  }

  isParDepehe(){
    return this.selectedEntry && this.selectedEntry.id == 4;
  }

  idChecked(choix){
    return choix.id === this.selectedEntry.id;
  }

  depecheEffectuee(){
    return this.dfv
        && this.dfv.VerificationZepLibre_Ligne
        && this.dfv.VerificationZepLibre_Ligne.DepecheId > 0
  }

  onSelectionChange(entry) {
    this.selectedEntry = entry;
     switch(this.selectedEntry.id){
       case 1:
          this.dfv.ObservationControles = true;
          this.dfv.ObservationDirecteVoie = false;
          this.dfv.ObservationParAutreMoyen = false;
          this.dfv.ObservationParEchangeDepeches = false;
          break;
       case 2:
          this.dfv.ObservationControles = false;
          this.dfv.ObservationDirecteVoie = true;
          this.dfv.ObservationParAutreMoyen = false;
          this.dfv.ObservationParEchangeDepeches = false;
          break;
       case 3:
          this.dfv.ObservationControles = false;
          this.dfv.ObservationDirecteVoie = false;
          this.dfv.ObservationParAutreMoyen = true;
          this.dfv.ObservationParEchangeDepeches = false;
          break;
       case 4:
          this.dfv.ObservationControles = false;
          this.dfv.ObservationDirecteVoie = false;
          this.dfv.ObservationParAutreMoyen = false;
          this.dfv.ObservationParEchangeDepeches = true;
          break;
        default:

     }

     this.updateDFV();
  }

  updateDFV(){
    this.ficheDfvService.UpdateDfv(this.dfv)
    .subscribe(
      data => {
        this.dfv = data;
        swal("Option enregistrée","","success");
        this.initForm();
      },
      err => {
        alert("erreur");
        console.log(err);
      }
    )
  }

}
