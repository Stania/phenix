import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Route, Routes, RouterModule } from "@angular/router";
import { FicheDfvComponent } from "./fiche-dfv.component";
import { DfvBlocInfoComponent } from "./dfv-bloc-info/dfv-bloc-info.component";
import { DfvBlocMesuresComponent } from "./dfv-bloc-mesures/dfv-bloc-mesures.component";
import { DfvBlocAccordComponent } from "./dfv-bloc-accord/dfv-bloc-accord.component";
import { DfvBlocRestitutionComponent } from "./dfv-bloc-restitution/dfv-bloc-restitution.component";
import { DfvBlocTtxComponent } from "./dfv-bloc-ttx/dfv-bloc-ttx.component";
import { DfvBlocApplicationComponent } from "./dfv-bloc-application/dfv-bloc-application.component";
import { SharedModule } from "../../shared/shared.module";
import { CreateOccupationComponent } from "./dfv-bloc-ttx/modals/create-occupation/create-occupation.component";
import { UpdateOccupationComponent } from "./dfv-bloc-ttx/modals/update-occupation/update-occupation.component";
import { UpdateManoeuvreComponent } from "./dfv-bloc-ttx/modals/update-manoeuvre/update-manoeuvre.component";
import { CreateManoeuvreComponent } from "./dfv-bloc-ttx/modals/create-manoeuvre/create-manoeuvre.component";
import { DepecheComponent } from './depeche/depeche.component';
import { CreateMesureComponent } from './dfv-bloc-mesures/modals/create-mesure/create-mesure.component';
import { CreateAutreDepecheComponent } from "./dfv-bloc-ttx/modals/create-autre-depeche/create-autre-depeche.component";
import { DfvBlocVerificationComponent } from './dfv-bloc-verification/dfv-bloc-verification.component';
import { DfvBlocNotificationComponent } from './dfv-bloc-notification/dfv-bloc-notification.component';
import { DfvBlocEngagementDegagementComponent } from './dfv-bloc-engagement-degagement/dfv-bloc-engagement-degagement.component';
import { EffectuerEngagementComponent } from "./dfv-bloc-engagement-degagement/modals/effectuer-engagement/effectuer-engagement.component";
import { EffectuerDegagementComponent } from "./dfv-bloc-engagement-degagement/modals/effectuer-degagement/effectuer-degagement.component";
import { DemandeDegagementComponent } from "./dfv-bloc-engagement-degagement/modals/demande-degagement/demande-degagement.component";
import { DfvBlocLeveeMesureComponent } from "./dfv-bloc-levee-mesure/dfv-bloc-levee-mesure.component";
import { DfvBlocAutreDepecheComponent } from './dfv-bloc-autre-depeche/dfv-bloc-autre-depeche.component';
import { AddAutreDepecheDfvComponent } from "./dfv-bloc-autre-depeche/modals/add-autre-depeche-dfv/add-autre-depeche-dfv.component";
import { FicheZepModule } from "../fiche-zep/fiche-zep.module";

const routes: Routes = [{ path: "", component: FicheDfvComponent }];

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(routes), FicheZepModule],
  declarations: [
    FicheDfvComponent,
    DfvBlocInfoComponent,
    DfvBlocMesuresComponent,
    DfvBlocAccordComponent,
    DfvBlocRestitutionComponent,
    DfvBlocTtxComponent,
    DfvBlocApplicationComponent,
    CreateOccupationComponent,
    UpdateOccupationComponent,
    CreateManoeuvreComponent,
    UpdateManoeuvreComponent,
    DepecheComponent,
    CreateMesureComponent,
    CreateAutreDepecheComponent,
    DfvBlocVerificationComponent,
    DfvBlocNotificationComponent,
    EffectuerEngagementComponent,
    EffectuerDegagementComponent,
    DemandeDegagementComponent,
    DfvBlocEngagementDegagementComponent,
    CreateAutreDepecheComponent,
    DfvBlocLeveeMesureComponent,
    DfvBlocAutreDepecheComponent,
    DfvBlocAutreDepecheComponent,
    AddAutreDepecheDfvComponent,
],
  exports: [RouterModule],
  entryComponents: [
    FicheDfvComponent,
    CreateOccupationComponent,
    UpdateOccupationComponent,
    CreateManoeuvreComponent,
    UpdateManoeuvreComponent,
    DepecheComponent,
    CreateMesureComponent,
    CreateAutreDepecheComponent,
    EffectuerEngagementComponent,
    EffectuerDegagementComponent,
    DemandeDegagementComponent,
    AddAutreDepecheDfvComponent
  ]
})
export class FicheDfvModule {}
