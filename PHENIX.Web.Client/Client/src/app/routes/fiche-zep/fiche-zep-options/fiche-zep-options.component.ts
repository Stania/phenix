import { Component, OnInit, Input } from '@angular/core';
import { Zep } from '../../../shared/models/FicheZep/zep.models';
import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';

@Component({
  selector: 'fiche-zep-options',
  templateUrl: './fiche-zep-options.component.html',
  styleUrls: ['./fiche-zep-options.component.scss']
})
export class FicheZepOptionsComponent implements OnInit {
  @Input()   zep: Zep;
   typeG =  Constantes_Phenix.ZepTypes.TypeG;
   typeL =  Constantes_Phenix.ZepTypes.TypeL;
   typeLG =  Constantes_Phenix.ZepTypes.TypeLG;

  constructor() {
    this.typeG =  Constantes_Phenix.ZepTypes.TypeG;
    this.typeL =  Constantes_Phenix.ZepTypes.TypeL;
    this.typeLG =  Constantes_Phenix.ZepTypes.TypeLG;
   }
  
  ngOnInit() {
  }

}
