/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { FicheZepProcedeDfvComponent } from './fiche-zep-procede-dfv.component';

describe('FicheZepProcedeDfvComponent', () => {
  let component: FicheZepProcedeDfvComponent;
  let fixture: ComponentFixture<FicheZepProcedeDfvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheZepProcedeDfvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheZepProcedeDfvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
