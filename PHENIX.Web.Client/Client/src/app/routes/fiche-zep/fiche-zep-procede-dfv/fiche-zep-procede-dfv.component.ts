import { Component, OnInit, Input } from '@angular/core';
import { PosteParTypeVM } from '../../../shared/models/ViewModels/MesureProtectionVM/PosteParType';
import { Zep } from '../../../shared/models/FicheZep/zep.models';
import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';
import { ValeurParTypeVM } from '../../../shared/models/ViewModels/MesureProtectionVM/ValeurParTypeVM';

@Component({
  selector: 'fiche-zep-procede-dfv',
  templateUrl: './fiche-zep-procede-dfv.component.html',
  styleUrls: ['./fiche-zep-procede-dfv.component.scss']
})
export class FicheZepProcedeDfvComponent implements OnInit {
  @Input() zep : Zep;
  @Input() listParticularite :Map<string,string[]>;
  @Input() listMesureProtection : Array<PosteParTypeVM>;
  @Input() listConditionsParticuliereDeVerif : Array<PosteParTypeVM>;
  constructor() { }

  ngOnInit() {
  }
  //Particularités
  getAiguillesPositionOblige(): Array<string>{
    return this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.AiguillesAPositionObligee)  == null ?
                new Array<string>() :
                this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.AiguillesAPositionObligee);
  }

  getAiguillesADisposer(): Array<string>{
    return this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.AiguillesADisposerDansPositionPrevue)  == null ?
                new Array<string>() :
                this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.AiguillesADisposerDansPositionPrevue);
  }

  //Mesures de protections
  getItineraireADetruire(poste : string): ValeurParTypeVM{
    let listMesureParPoste = this.listMesureProtection.find(x => x.NomPoste == poste);
    if(listMesureParPoste != null)
    return listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.ItinérairesAdetruirePRCI) == null ?
           new ValeurParTypeVM() :
           listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.ItinérairesAdetruirePRCI)
  }

  getItineraireADetruireEtMunir(poste : string): ValeurParTypeVM{
    let listMesureParPoste = this.listMesureProtection.find(x => x.NomPoste == poste);
    if(listMesureParPoste != null)
    return listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.ItinérairesAdetruireEtAMunirDunDA) == null ?
           new ValeurParTypeVM() :
           listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.ItinérairesAdetruireEtAMunirDunDA)
  }

  getOrganeDeCommande(poste : string): ValeurParTypeVM{
    let listMesureParPoste = this.listMesureProtection.find(x => x.NomPoste == poste);
    if(listMesureParPoste != null)
    return listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.OrganeDeCommandeAplacerEtAmunirDeDA) == null ?
           new ValeurParTypeVM() :
           listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.OrganeDeCommandeAplacerEtAmunirDeDA)
  }

  getDialogueDeProtection(poste : string): ValeurParTypeVM{
    let listMesureParPoste = this.listMesureProtection.find(x => x.NomPoste == poste);
    if(listMesureParPoste != null)
    return listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.DialogueDeProtection) == null ?
           new ValeurParTypeVM() :
           listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.DialogueDeProtection)
  }

  getAutorisationADetruire(poste : string): ValeurParTypeVM{
    let listMesureParPoste = this.listMesureProtection.find(x => x.NomPoste == poste);
    if(listMesureParPoste != null)
    return listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.AutorisationADetruire) == null ?
           new ValeurParTypeVM() :
           listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.AutorisationADetruire)
  }

  getAiguilleImmobilise(poste : string): ValeurParTypeVM{
    let listMesureParPoste = this.listMesureProtection.find(x => x.NomPoste == poste);
    if(listMesureParPoste != null)
    return listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.AiguilleImmobiliseeDansPositionIndiquee) == null ?
           new ValeurParTypeVM() :
           listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.AiguilleImmobiliseeDansPositionIndiquee)
  }

  getJalonDarret(poste : string): ValeurParTypeVM{
    let listMesureParPoste = this.listMesureProtection.find(x => x.NomPoste == poste);
    if(listMesureParPoste != null)
    return listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.JalonDarretAplacerSurLeTerrain) == null ?
           new ValeurParTypeVM() :
           listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.JalonDarretAplacerSurLeTerrain)
  }

  getConditionParticuliereDeVerification(poste : string){
    let listMesureParPoste = this.listConditionsParticuliereDeVerif.find(x => x.NomPoste == poste);
    if(listMesureParPoste != null)
    return listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.ConditionsParticuliereVerificationLiberationZep) == null ?
           new ValeurParTypeVM() :
           listMesureParPoste.ListType.find(x=> x.Type == Constantes_Phenix.MesureProtectionTypes.ConditionsParticuliereVerificationLiberationZep)
  }

  getDispositionsParticuliereExploitation(): Array<string>{
    return this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.DispositionsParticulieresParExploitation)== null ?
    new Array<string>() :
    this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.DispositionsParticulieresParExploitation);
  }
  getOutilDeBouclage(): Array<string>{
    return this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.OutilDeBouclageParLEquipement)== null ?
    new Array<string>() :
    this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.OutilDeBouclageParLEquipement);
  }
  getDispositionsParticuliereEquipement(): Array<string>{
    return this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.DispositionsParticulieresParLEquipement)== null ?
    new Array<string>() :
    this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.DispositionsParticulieresParLEquipement);
  }

  getAutresMesureEventuelles(): Array<string>{
    return this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.AutresMesuresEventiellesParLEquipement)== null ?
    new Array<string>() :
    this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.AutresMesuresEventiellesParLEquipement);
  }
}
