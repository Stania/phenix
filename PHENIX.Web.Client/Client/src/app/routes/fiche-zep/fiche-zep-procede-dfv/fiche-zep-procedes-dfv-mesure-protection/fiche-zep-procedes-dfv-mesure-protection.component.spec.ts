import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheZepProcedesDfvMesureProtectionComponent } from './fiche-zep-procedes-dfv-mesure-protection.component';

describe('FicheZepProcedesDfvMesureProtectionComponent', () => {
  let component: FicheZepProcedesDfvMesureProtectionComponent;
  let fixture: ComponentFixture<FicheZepProcedesDfvMesureProtectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheZepProcedesDfvMesureProtectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheZepProcedesDfvMesureProtectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
