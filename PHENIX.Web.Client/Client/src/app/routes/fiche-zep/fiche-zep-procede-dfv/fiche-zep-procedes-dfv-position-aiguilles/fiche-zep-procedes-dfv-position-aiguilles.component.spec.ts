import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheZepProcedesDfvPositionAiguillesComponent } from './fiche-zep-procedes-dfv-position-aiguilles.component';

describe('FicheZepProcedesDfvPositionAiguillesComponent', () => {
  let component: FicheZepProcedesDfvPositionAiguillesComponent;
  let fixture: ComponentFixture<FicheZepProcedesDfvPositionAiguillesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheZepProcedesDfvPositionAiguillesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheZepProcedesDfvPositionAiguillesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
