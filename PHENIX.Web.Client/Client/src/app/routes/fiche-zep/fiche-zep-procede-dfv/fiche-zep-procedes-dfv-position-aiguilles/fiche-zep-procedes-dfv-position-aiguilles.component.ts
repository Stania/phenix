import { Component, OnInit, Input } from '@angular/core';
import { PosteParTypeVM } from '../../../../shared/models/ViewModels/MesureProtectionVM/PosteParType';
import { Zep } from '../../../../shared/models/FicheZep/zep.models';
import { Constantes_Phenix } from '../../../../shared/models/Commun/constantes.model';
import { ValeurParTypeVM } from '../../../../shared/models/ViewModels/MesureProtectionVM/ValeurParTypeVM';


@Component({
  selector: 'fiche-zep-procedes-dfv-position-aiguilles',
  templateUrl: './fiche-zep-procedes-dfv-position-aiguilles.component.html',
  styleUrls: ['./fiche-zep-procedes-dfv-position-aiguilles.component.scss']
})
export class FicheZepProcedesDfvPositionAiguillesComponent implements OnInit {
  @Input() listParticularite :Map<string,string[]>;
  @Input() zep : Zep;
  constructor() { }

  ngOnInit() {
  }
  //Particularités
  getAiguillesPositionOblige(): Array<string>{
    return this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.AiguillesAPositionObligee)  == null ?
                new Array<string>() :
                this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.AiguillesAPositionObligee);
  }

  getAiguillesADisposer(): Array<string>{
    return this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.AiguillesADisposerDansPositionPrevue)  == null ?
                new Array<string>() :
                this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.AiguillesADisposerDansPositionPrevue);
  }
}
