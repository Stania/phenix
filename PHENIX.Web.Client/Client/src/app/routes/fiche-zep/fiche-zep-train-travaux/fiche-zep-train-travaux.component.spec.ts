/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { FicheZepTrainTravauxComponent } from './fiche-zep-train-travaux.component';

describe('FicheZepTrainTravauxComponent', () => {
  let component: FicheZepTrainTravauxComponent;
  let fixture: ComponentFixture<FicheZepTrainTravauxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheZepTrainTravauxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheZepTrainTravauxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
