import { Component, OnInit, Input } from '@angular/core';
import { TypeParPosteVM } from '../../../shared/models/ViewModels/PointRemarquablesVM/TypeParPosteVM';
import { Zep } from '../../../shared/models/FicheZep/zep.models';
import { Constantes_Phenix } from '../../../shared/models/Commun/constantes.model';


@Component({
  selector: 'fiche-zep-train-travaux',
  templateUrl: './fiche-zep-train-travaux.component.html',
  styleUrls: ['./fiche-zep-train-travaux.component.css']
})
export class FicheZepTrainTravauxComponent implements OnInit {
  @Input()   zep: Zep;
  @Input() listParticularite :Map<string,string[]>;
  @Input() listPointRemarquable : Array<TypeParPosteVM>;

  constructor() { }

  ngOnInit() {

  }
    //// Particularité
  getDFVRestitue(): Array<string>{
    return this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.DFVResititueeOccupee)  == null ? new Array<string>() : this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.DFVResititueeOccupee);
  }
  getTTXStationne(): Array<string>{
    return this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.TTXStationne)  == null ? new Array<string>() : this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.TTXStationne);
  }
  getAutresDispositionsParticulieresTTX(): Array<string>{
    return this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.AutresDispositionsParticulieresTTX)  == null ? new Array<string>() : this.listParticularite.get(Constantes_Phenix.ParticulariteTypes.AutresDispositionsParticulieresTTX);
  }

  //// Points Remarquable
  getPointRemarquableEngagement(): TypeParPosteVM{
    return this.listPointRemarquable.find(x => x.Type == Constantes_Phenix.PointRemarquableTypes.PointEngagement) == null ? new TypeParPosteVM() : this.listPointRemarquable.find(x => x.Type == Constantes_Phenix.PointRemarquableTypes.PointEngagement) ;
  }
  getPointRemarquableDegagement(): TypeParPosteVM{
    return this.listPointRemarquable.find(x => x.Type == Constantes_Phenix.PointRemarquableTypes.PointDegagement) == null ? new TypeParPosteVM() : this.listPointRemarquable.find(x => x.Type == Constantes_Phenix.PointRemarquableTypes.PointDegagement);
  }
  getPointRemarquableSignauxInter(): TypeParPosteVM{
    return this.listPointRemarquable.find(x => x.Type == Constantes_Phenix.PointRemarquableTypes.SignauxIntermediaires) == null ? new TypeParPosteVM() : this.listPointRemarquable.find(x => x.Type == Constantes_Phenix.PointRemarquableTypes.SignauxIntermediaires) ;
  }


}
