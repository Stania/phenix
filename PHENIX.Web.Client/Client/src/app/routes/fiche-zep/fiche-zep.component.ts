import { Component, OnInit, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs/Subject';

import { TypeParPosteVM } from '../../shared/models/ViewModels/PointRemarquablesVM/TypeParPosteVM';
import { PosteParTypeVM } from '../../shared/models/ViewModels/MesureProtectionVM/PosteParType';
import { ZepService } from '../../shared/services/ficheZep/zep.service';
import { Zep } from '../../shared/models/FicheZep/zep.models';
import { ValeurParPosteVM } from '../../shared/models/ViewModels/PointRemarquablesVM/ValeurParPosteVM';
import { ValeurParTypeVM } from '../../shared/models/ViewModels/MesureProtectionVM/ValeurParTypeVM';
import { Constantes_Phenix } from '../../shared/models/Commun/constantes.model';
import { PhenixToasterService } from '../../shared/settings/phenix-toaster.service';


@Component({
  selector: 'fiche-zep',
  templateUrl: './fiche-zep.component.html',
  styleUrls: ['./fiche-zep.component.scss']
})

export class FicheZepComponent implements OnInit {

  settingActive : number;
  zep: Zep;
  listParticularite :Map<string,string[]> = new Map<string,string[]>();
  listPointRemarquable : Array<TypeParPosteVM> = new Array<TypeParPosteVM>();

  listMesureProtection : Array<PosteParTypeVM> = new Array<PosteParTypeVM>();
  listConditionsParticuliereDeVerif : Array<PosteParTypeVM> = new Array<PosteParTypeVM>();

  PointsRemarquablesParPosteList : any;
  PointsRemarquablesValues : any;
  MesuresProtectionParPosteList : any;
  MesuresProtectionValues : any;
  ParticularitesList : any;
  ParticularitesValues : any;

  public onClose: Subject<Zep>;
  constructor(public modal: BsModalRef, private zepService : ZepService,
    private toastSrv : PhenixToasterService ) {
    this.zep = new Zep();

    this.PointsRemarquablesParPosteList = new Array();
    this.PointsRemarquablesValues = new Array();
    this.MesuresProtectionParPosteList = new Array();
    this.MesuresProtectionValues = new Array();
    this.settingActive = 1;
  }

  ngOnInit() {
  }

  refreshZep(){
    let id = this.zep.Id;
    const result =  this.zepService.getZepById(id);
    result.subscribe(
      data => {
        this.zep = data;
        this.PopulateListes();
     },
     error => {
      this.toastSrv.showError("Erreur",error.error.error);
     });
  }

  PopulateListes(){
    //Particularités
    let particuMap  = this.zep.ParticulariteList.map(x => { return { particularite : x.ParticulariteType.Nom, valeur: x.Valeur}} );
    particuMap.forEach(x => {
      let goodDico = this.listParticularite.get(x.particularite);
      if(goodDico == null)
        this.listParticularite.set(x.particularite,new Array<string>(x.valeur));
      else
        goodDico.push(x.valeur);
    });
    
    //Points remarquables
    //Retourne [Type , {Poste , [ListValeur]}]
    let pointRemarquableMap  = this.zep.PointRemarquable_ZepList.map(x => { return { typePointRemarquable : x.PointRemarquableType.Nom,
                                                                                                    poste : x.PosteConcerne.Nom,
                                                                                                    valeur :x.Valeur}}
                                                                    );
     pointRemarquableMap.forEach(x => {
      let typePointVm = new TypeParPosteVM();
      let indexType = this.listPointRemarquable.findIndex(z => z.Type == x.typePointRemarquable) ;
      if(indexType == -1){
        typePointVm.Type = x.typePointRemarquable;
        let newPostePoint = new ValeurParPosteVM();
        newPostePoint.NomPoste = x.poste;
        newPostePoint.ListValue =new Array<string>(x.valeur);
        typePointVm.ListPosteVm.push(newPostePoint);
        this.listPointRemarquable.push(typePointVm)
      }else{
        let indexPoste = this.listPointRemarquable[indexType].ListPosteVm.findIndex(z => z.NomPoste == x.poste);
        if(indexPoste == -1){
          let newPoste = new ValeurParPosteVM();
          newPoste.NomPoste = x.poste;
          newPoste.ListValue = new Array<string>(x.valeur);
          this.listPointRemarquable[indexType].ListPosteVm.push(newPoste);
        }else{
          this.listPointRemarquable[indexType].ListPosteVm[indexPoste].ListValue.push(x.valeur);
        }
      }
    });
    //Mesures Protection
    let mesureProtectionMap  = this.zep.MesureProtection_ZepList.map(x => { return { typeMesure : x.MesureProtectionType.Nom,
      poste : x.Poste.Nom,
      valeur :x.Valeur}}
    );
//Retourne [Poste , {Type , [ListValeur]}]
let mesuresProtectionLocal = new Array<PosteParTypeVM>();
mesureProtectionMap.forEach(x => {
      let typeMesureVm = new PosteParTypeVM();
      let indexType = mesuresProtectionLocal.findIndex(z => z.NomPoste == x.poste) ;
      if(indexType == -1){
        typeMesureVm.NomPoste = x.poste;
        let newType = new ValeurParTypeVM();
        newType.Type = x.typeMesure;
        newType.ListValeur =new Array<string>(x.valeur);
        typeMesureVm.ListType.push(newType);
        mesuresProtectionLocal.push(typeMesureVm)
      }else{
        let indexPoste = mesuresProtectionLocal[indexType].ListType.findIndex(z => z.Type == x.typeMesure);
        if(indexPoste == -1){
          let newType = new ValeurParTypeVM();
          newType.Type = x.typeMesure;
          newType.ListValeur = new Array<string>(x.valeur);
          mesuresProtectionLocal[indexType].ListType.push(newType);
        }else{
          mesuresProtectionLocal[indexType].ListType[indexPoste].ListValeur.push(x.valeur);
        }
      }
      });
      mesuresProtectionLocal.forEach(poste => {
        if(poste.ListType.find(x => x.Type == Constantes_Phenix.MesureProtectionTypes.ConditionsParticuliereVerificationLiberationZep) != null){
          this.listConditionsParticuliereDeVerif.push(poste);
        }
        if(poste.ListType.find(x =>
           [Constantes_Phenix.MesureProtectionTypes.ItinérairesAdetruirePRCI,
            Constantes_Phenix.MesureProtectionTypes.ItinérairesAdetruireEtAMunirDunDA ,
            Constantes_Phenix.MesureProtectionTypes.OrganeDeCommandeAplacerEtAmunirDeDA ,
            Constantes_Phenix.MesureProtectionTypes.DialogueDeProtection ,
            Constantes_Phenix.MesureProtectionTypes.AutorisationADetruire ,
            Constantes_Phenix.MesureProtectionTypes.AiguilleImmobiliseeDansPositionIndiquee ,
            Constantes_Phenix.MesureProtectionTypes.JalonDarretAplacerSurLeTerrain,
          ].includes(x.Type))){
          this.listMesureProtection.push(poste);
        }
      })
  }

//Retourne [Type , {Poste , [ListValeur]}]


  //PRs
  GetPostesPRsZep(typePR:string){
    this.PointsRemarquablesParPosteList.forEach(e => {
      if(e.type == typePR){
        this.PointsRemarquablesValues.push(e);
      }
    });
  }
  GetPointsRemarquableArray(typePR:string, poste:string){
    //retourne les valeurs des PRs : ['C22','c10']
  }

  //Mesures protection
  GetPostesMesuresZep(typeMesure:string){

  }
  GetMesuresArray(typeMesure:string, poste :string){
    //retourne les valeurs : ['','']
  }

  close(item: Zep) {
    //this.onClose.next(item);
    this.modal.hide();
  }
}

