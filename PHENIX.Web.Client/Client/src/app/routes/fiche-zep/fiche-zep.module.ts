import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, Routes, RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { FicheZepProcedeDfvComponent } from './fiche-zep-procede-dfv/fiche-zep-procede-dfv.component';
import { FicheZepOptionsComponent } from './fiche-zep-options/fiche-zep-options.component';
import { FicheZepTrainTravauxComponent } from './fiche-zep-train-travaux/fiche-zep-train-travaux.component';
import { FicheZepInfosComponent } from './fiche-zep-infos/fiche-zep-infos.component';
import { FicheZepComponent } from './fiche-zep.component';

const routes : Routes = [
  {path:'', component: FicheZepComponent}
]

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    FicheZepComponent,
    FicheZepInfosComponent,
    FicheZepTrainTravauxComponent,
    FicheZepOptionsComponent,
    FicheZepProcedeDfvComponent
  ],
  exports:[
    RouterModule,
    FicheZepInfosComponent,
    FicheZepTrainTravauxComponent,
    FicheZepOptionsComponent,
    FicheZepProcedeDfvComponent,
  ],
  entryComponents:[FicheZepComponent]
})
export class FicheZepModule { }
