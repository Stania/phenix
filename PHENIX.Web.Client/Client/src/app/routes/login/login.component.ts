import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { LoginVM } from '../../shared/models/ViewModels/LoginVM';
import { AuthenticationService } from '../../shared/services/administration/authentication.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    valForm: FormGroup;
    identifiants : LoginVM;
    invalidLogin: boolean;
    constructor( public settings : SettingsService,
      fb: FormBuilder,
       private authService : AuthenticationService,
       private router : Router) {

        this.valForm = fb.group({
            'login': [null, Validators.compose([Validators.required])],
            'password': [null, Validators.required]
        });
        this.invalidLogin = false;

    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        for (let c in this.valForm.controls) {
            this.valForm.controls[c].markAsTouched();
        }
        if (this.valForm.valid) {
          this.identifiants = new LoginVM();
          this.identifiants.Login = this.valForm.value['login'];
          this.identifiants.Password = this.valForm.value['password'];
          var resp = this.authService.login(this.identifiants).subscribe(a=>{
            if (a === true ){
              this.invalidLogin = false;
              this.router.navigate([this.authService.getRedirectUrl()])
            }
            else{
              this.invalidLogin = true;

            }
          },error=>{
            console.log(error);
            this.invalidLogin = true;
          });
        }
    }

    ngOnInit() {

    }

}
