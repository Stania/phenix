import { Constantes_Phenix } from "../shared/models/Commun/constantes.model";

const Home = {
    text: 'Synoptique',
    link: '/synoptique/operationnel',
    icon: 'icon-vector',
    role : [Constantes_Phenix.Profils.Administrateur,
      Constantes_Phenix.Profils.AgentCirculation,
      Constantes_Phenix.Profils.CCL,
      Constantes_Phenix.Profils.CelluleTravaux,
      Constantes_Phenix.Profils.PoleTravaux]
};

const DfvList = {
    text: 'Liste DFV',
    link: '/dfvlist',
    icon: 'icon-vector',
    role : [Constantes_Phenix.Profils.Administrateur,
      Constantes_Phenix.Profils.Aiguilleur,
      Constantes_Phenix.Profils.AgentCirculation]
}

const BiblioZeps = {
    text: 'Bibliotheque des zeps',
    link: '/zeps',
    icon: 'icon-vector',
    role : [
      Constantes_Phenix.Profils.Administrateur,
      Constantes_Phenix.Profils.Aiguilleur,
      Constantes_Phenix.Profils.AgentCirculation,
      Constantes_Phenix.Profils.PoleTravaux]
}

const CreationDfvPreOp = {
  text: 'Prévision DFV',
  link: '/previsiondfv',
  icon: 'icon-vector',
  role : [
    Constantes_Phenix.Profils.Administrateur,
    Constantes_Phenix.Profils.PoleTravaux]
}

const headingMain = {
    text: 'Navigation',
    heading: true,

};

export const menu = [
    headingMain,
    Home,
    DfvList,
    BiblioZeps,
    CreationDfvPreOp
];





