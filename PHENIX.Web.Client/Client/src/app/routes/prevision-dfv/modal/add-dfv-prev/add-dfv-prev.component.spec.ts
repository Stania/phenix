/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AddDfvPrevComponent } from './add-dfv-prev.component';

describe('AddDfvPrevComponent', () => {
  let component: AddDfvPrevComponent;
  let fixture: ComponentFixture<AddDfvPrevComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDfvPrevComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDfvPrevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
