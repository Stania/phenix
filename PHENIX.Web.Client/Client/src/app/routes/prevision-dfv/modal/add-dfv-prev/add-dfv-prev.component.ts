import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { DFV } from '../../../../shared/models/DFV/dfv.models';
import { Zep, ZepType } from '../../../../shared/models/FicheZep/zep.models';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { PhenixToasterService } from '../../../../shared/settings/phenix-toaster.service';
import { ZepService } from '../../../../shared/services/ficheZep/zep.service';
import { FicheDfvService } from '../../../../shared/services/ficheDfv/fiche-dfv.service';
import { Observable } from "rxjs/Rx";
import { FicheZepComponent } from '../../../fiche-zep/fiche-zep.component';
import { ValidationService } from '../../../../shared/settings/validators.service';

@Component({
  selector: 'app-add-dfv-prev',
  templateUrl: './add-dfv-prev.component.html',
  styleUrls: ['./add-dfv-prev.component.scss']
})
export class AddDfvPrevComponent implements OnInit {

  public newDfv: DFV;
  public zepList: Array<Zep>;
  public zepTypeList: Array<ZepType>;
  modelRef: BsModalRef ;
  public onClose: Subject<DFV>;
  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [];
  options: FormlyFormOptions = {
    formState: {
      awesomeIsForced: false,
      model: this.newDfv
    }
  };

  constructor(
    public modal: BsModalRef,
    private toastSrv: PhenixToasterService,
    private zepService: ZepService,
    private dfvService : FicheDfvService,
    private modalService: BsModalService ) {
    this.onClose = new Subject();
    this.newDfv = new DFV();
  }

  ngOnInit() {
    Observable.forkJoin([
      this.zepService.getZepsAutorises(),
      this.zepService.getAllZepTypes()
    ])
    .subscribe((response: any[]) => {
      this.zepList = response[0];
      this.zepTypeList = response[1];
      this.initForm();
    },
    error => this.toastSrv.showError("Erreur", error.error.error));
  }
  initForm() {
    let now = new Date();
    let hourAfter = new Date();
    hourAfter.setHours(now.getHours()+1)
    this.newDfv.IsPrevisonelle = true;
    this.fields =  [
      {
        fieldGroupClassName: 'row',
        fieldGroup:
        [
          {
            key: "Numero", className: 'row col-md-4',type: 'input',
            templateOptions: {type: 'text',label: 'Numéro DFV', required: false}
          },
          {
            key: "RPTX", className: 'col-md-4',type: 'input',
            templateOptions: {type: 'text',label: 'RPTx', required: true}
          },
          {
            key: "NumeroTelephone", className: 'col-md-4',type: 'input',
            templateOptions: {type: 'text',label: 'Téléphone RPTx', required: true}
          },
        ]
      },
      {
        type: "horizontalRadio",
        key: "AvecVerificationDeLiberation",
        id: "AvecVerificationDeLiberation",
        defaultValue:false,
        templateOptions: {
          required: true,
          label: '',
          inputClass: 'radio-inline',
          options: [
            { key: true, value: "Avec vérification de libération" },
            { key: false, value: "Sans vérification de libération" },
          ],
        },
        lifecycle: {
          onInit: (form, field) => {
            form.get('ZepId').valueChanges.subscribe( (selected : any) =>
              {
                var zep = this.zepList.find(t => t.Id == parseInt(selected));

                if(!zep.DFVAvecVerificationLiberation){
                  form.get('AvecVerificationDeLiberation').setValue (false);
                  field.templateOptions.disabled = true;
                  return;
                }

                if( zep.DFVSansVerificationLiberationDerriereTrainOuvrant == false
                  || zep.DFVSansVerificationLiberationTTxDeclencheur == false
                  || zep.DFVSansVerificationLiberationTTxStationne == false
                  || zep.DFVSansVerificationLiberationTTxStationnePartieG == false){
                  //avec vérification de libération
                  form.get('AvecVerificationDeLiberation').setValue(true);
                 // field.templateOptions.disabled = true;
                }else{
                  field.templateOptions.disabled = false;
                }
              }
            )
          },
        },
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            key:"ZepId",
            type : "select",
            className:'row col-md-10',
            templateOptions:
            {
              label:"Numéro Zep",
              options: this.zepList,
              valueProp: 'Id',
              labelProp: 'Numero',
              required: true
            }

          },

          {
            hideExpression: `!model.ZepId > 0` ,
            className: ' col-md-2',type: 'button', templateOptions: {
              inputClass: 'btn btn-info',
              iconClass: '',
              withLabel:true,
              titre:'Afficher la zep',
              btnClick: (event: any) => {
                event.stopPropagation();
                this.zepService.getZepById(this.newDfv.ZepId).subscribe(
                  data => {
                    this.modelRef = this.modalService.show(FicheZepComponent, {class:"modal-lg"});
                    this.modelRef.content.zep = data;
                    this.modelRef.content.PopulateListes();
                  },
                  err => console.log(err)
                )
              },
            },
          },

        ]
      },
      {
        key: "Zep.ZepTypeId",
        type: 'select',

        templateOptions:
        {
          label: 'Type Zep',
          options: this.zepTypeList,
          valueProp: 'Id',
          labelProp: 'Nom',
          required:false
        },
        lifecycle: {
          onInit: (form, field) => {
            form.get('ZepId').valueChanges.subscribe( (selected : any) =>
              {
                var zep = this.zepList.find(t => t.Id == parseInt(selected));
                field.templateOptions.options = this.zepTypeList.filter(t => t.Id === zep.ZepTypeId);
              }
            )
          },
        },
      },

      {
        fieldGroupClassName: 'row',
        fieldGroup:
        [
          {
            key: "DateDebutApplicabilite",
            className: 'row col-md-6',
            type: 'datetimepicker',
            defaultValue: now,
            templateOptions:{
                label: 'à partir du',
                required: true,
                type:'dateonly'
              },
            validators:
            {
              validation: (x) => ValidationService.periodValidator( x ,this.form, 'DateFinApplicabilite',true)
            },
            expressionProperties: {
              'templateOptions.type': function(model: any, formState: any){
                return (model.DerriereTTXDeclencheur || model.DerriereTrainOuvrant ) ? 'dateonly' : 'datetime';
              }
            }
          },
          {
            key: "DateFinApplicabilite",
            className: 'col-md-6',
            type: 'datetimepicker',
            defaultValue: hourAfter,
            templateOptions: {
              label: 'jusqu\'au',
              required: true,
            },
            validators:
            {
              validation: (x) => ValidationService.periodValidator( x ,this.form, 'DateDebutApplicabilite',false)
            },
          },
        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup:
        [
          {
            key: "DerriereTTXDeclencheur",
            className: 'row col-md-6',
            type: 'checkbox',
            templateOptions: {label: 'Derrière TTx déclencheur',required: false},
            hideExpression: 'model.DerriereTrainOuvrant'
          },
        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup:
        [
          {
            key: "DerriereTrainOuvrant",
            className: 'row col-md-6',
            type: 'checkbox',
            templateOptions: {
              label: 'Derrière train ouvrant',
            },
            hideExpression: 'model.DerriereTTXDeclencheur'
          },
        ]
      },
    ];
    this.applyDisable();
  }
  applyDisable(){
    // apply expressionProperty for disabled based on formState to all fields
    this.fields.forEach(field => {
     field.expressionProperties = field.expressionProperties || {};
     field.expressionProperties['templateOptions.disabled'] = 'formState.disabled';
     if(field.fieldGroup){
       field.fieldGroup.forEach(t => {
         t.expressionProperties = t.expressionProperties || {};
         t.expressionProperties['templateOptions.disabled'] = 'formState.disabled';
       });
     }
   });
 }
 submit(model: DFV) {
  if (this.form.valid){
    let zep = this.zepList.find(x => x.Id == model.ZepId);
    if(zep == null){
      this.toastSrv.showError("Erreur","Veuillez sélectionner une ZEP.");
      return;
    }
    model.Zep = zep;
    var resp = this.dfvService.createDfvPrev(model);
    resp.subscribe(
      a => {
        this.toastSrv.showSuccess("Succès","La DFV à été créée.");
        this.closePopup(a);
      },
      error => {
        this.toastSrv.showError("Erreur",error.error.error);
      }
  );
  }

}

closePopup(item: DFV) {
  this.onClose.next(item);
  this.modal.hide();
}
}
