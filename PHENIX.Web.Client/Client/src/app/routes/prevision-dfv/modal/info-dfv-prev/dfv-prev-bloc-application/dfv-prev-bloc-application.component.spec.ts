/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DfvPrevBlocApplicationComponent } from './dfv-prev-bloc-application.component';

describe('DfvPrevBlocApplicationComponent', () => {
  let component: DfvPrevBlocApplicationComponent;
  let fixture: ComponentFixture<DfvPrevBlocApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DfvPrevBlocApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DfvPrevBlocApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
