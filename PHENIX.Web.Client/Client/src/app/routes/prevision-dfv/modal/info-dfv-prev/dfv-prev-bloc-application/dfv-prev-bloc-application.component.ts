import { Component, OnInit, Input, Output,EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { DFV } from '../../../../../shared/models/DFV/dfv.models';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { FicheDfvService } from '../../../../../shared/services/ficheDfv/fiche-dfv.service';
import * as moment from 'moment';
import { PhenixToasterService } from '../../../../../shared/settings/phenix-toaster.service';

@Component({
  selector: 'app-dfv-prev-bloc-application',
  templateUrl: './dfv-prev-bloc-application.component.html',
  styleUrls: ['./dfv-prev-bloc-application.component.scss']
})
export class DfvPrevBlocApplicationComponent implements OnInit,OnChanges {
  @Input() dfv: DFV;
  @Output() dfvChange = new EventEmitter<DFV>();

  form = new FormGroup({});
  fields: FormlyFieldConfig[];
  options: FormlyFormOptions = {
    formState: {
      disabled: false,
      awesomeIsForced: false,
    }
  };

  constructor(private dfvService: FicheDfvService,private toastrService: PhenixToasterService) {
    this.dfv = new DFV();
  }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    this.initForm();
  }

  initForm() {

    let dateAHT ="S"+ moment(this.dfv.DateDebutApplicabilite).week() + " / "+ moment(this.dfv.DateDebutApplicabilite).year();
    if(this.dfv.AHTNumero == null)
    {
      this.dfv.AHTNumero = dateAHT;
    }
    this.fields = [
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            key: "OperationNumero",
            className: 'row col-md-4',
            type: 'input',
            templateOptions:
              {
                label: 'Opération N°',
              }
          },
          {
            key: "AHTNumero",
            className: 'col-md-4',
            type: 'input',
            defaultValue: dateAHT,
              templateOptions:
              {
                label: 'AHT N°',
                type:"text",
                required: false,
              }
          },
          {
            key: "AvisNumero",
            className: 'row col-md-4',
            type: 'input',
            templateOptions:
              {
                label: 'Numéro Avis',
                required: false,
              }
          },
        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            key: "ContratTravauxNumero",
            className: 'row col-md-6',
            type: 'input',
            templateOptions:
              {
                label: 'Contrat de travaux N°',
                required: false,
              }
          },
          {
            key: "CCTTNumero",
            className: 'col-md-6',
            type: 'input',
            templateOptions:
              {
                label: 'CCTT N°',
                required: false,
              }
          },
        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup: [
          {
            key: "DateApplicabilite",
            className: 'row col-md-12',
            type: 'datetimepicker',
            templateOptions:
              {
                label: 'Applicable le',
                required: true,
                 showTime : true
              },
          },
        ]
      },
    ];
  }

  submit(dfv){
    if (this.form.valid){
       var resp = this.dfvService.UpdateDfv(dfv);
       resp.subscribe(
         a => {
           this.toastrService.showSuccess("Succès","La DFV à été modifiée.");
           this.dfv = a;
           this.dfvChange.emit(a);
         },
         error => {
           this.toastrService.showError("Erreur",error.error.error);
         }
     );
     }
    }

}
