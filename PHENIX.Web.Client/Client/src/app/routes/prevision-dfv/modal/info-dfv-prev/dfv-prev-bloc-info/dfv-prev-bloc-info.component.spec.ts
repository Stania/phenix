/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DfvPrevBlocInfoComponent } from './dfv-prev-bloc-info.component';

describe('DfvPrevBlocInfoComponent', () => {
  let component: DfvPrevBlocInfoComponent;
  let fixture: ComponentFixture<DfvPrevBlocInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DfvPrevBlocInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DfvPrevBlocInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
