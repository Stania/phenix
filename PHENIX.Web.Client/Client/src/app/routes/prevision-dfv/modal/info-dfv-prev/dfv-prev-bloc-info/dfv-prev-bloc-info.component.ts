import { Component, OnInit, Input, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { DFV } from '../../../../../shared/models/DFV/dfv.models';
import { ZepType, Zep } from '../../../../../shared/models/FicheZep/zep.models';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { ZepService } from '../../../../../shared/services/ficheZep/zep.service';
import { FicheDfvService } from '../../../../../shared/services/ficheDfv/fiche-dfv.service';
import { ValidationService } from '../../../../../shared/settings/validators.service';
import { PhenixToasterService } from '../../../../../shared/settings/phenix-toaster.service';

@Component({
  selector: 'app-dfv-prev-bloc-info',
  templateUrl: './dfv-prev-bloc-info.component.html',
  styleUrls: ['./dfv-prev-bloc-info.component.scss']
})
export class DfvPrevBlocInfoComponent implements OnInit {
  @Input() dfv: DFV;
  @Input() zepTypeList : Array<ZepType>;
  @Input() zepList : Array<Zep>;
  @Output() dfvChange = new EventEmitter<DFV>();

  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [];
  options: FormlyFormOptions = {
    formState: {
      disabled : false,
      awesomeIsForced: false,
      model : this.dfv,
    }
  };

  constructor(private zepService : ZepService, private dfvService : FicheDfvService, private toastrService: PhenixToasterService)
    {
    this.dfv = new DFV();
    this.zepTypeList = new Array<ZepType>();
    this.zepList = new Array<Zep>();
  }

  ngOnChanges(changes: SimpleChanges): void {
      this.initForm();
  }

  ngOnInit() {
  }

  initForm(){
    this.form = new FormGroup({});
    this.fields =  [
      {
        fieldGroupClassName: 'row',
        fieldGroup:
        [
          {
            key: "Numero", className: 'row col-md-4',type: 'input',
            templateOptions: {type: 'text',label: 'Numéro DFV'}
          },
          {
            key: "RPTX", className: 'col-md-4',type: 'input',
            templateOptions: {type: 'text',label: 'RPTx', required: true}
          },
          {
            key: "NumeroTelephone", className: 'col-md-4',type: 'input',
            templateOptions: {type: 'text',label: 'Téléphone RPTx', required: true}
          },
        ]
      },
      {
        type: "horizontalRadio", key: "AvecVerificationDeLiberation", id: "AvecVerificationDeLiberation",
        templateOptions: {
          required: true,
          label: '',
          inputClass: 'radio-inline',
          options: [{ key: true, value: "Avec vérification de libération" }, { key: false, value: "Sans vérification de libération" },],
        }
      },
      {
        key:"ZepId",
        type : "select",
        templateOptions:
        {
          label:"Numéro Zep",
          options: this.zepList,
          valueProp: 'Id',
          labelProp: 'Numero',
          required: true
        }
      },
      {
        key: "Zep.ZepTypeId",
        type: 'select',
        templateOptions:
        {
          label: 'Type Zep',
          options: this.zepTypeList,
          valueProp: 'Id',
          labelProp: 'Nom',
          required:true,
        },
        lifecycle: {
          onInit: (form, field) => {
            form.get('ZepId').valueChanges.subscribe( (selected : any) =>
              {
                var zep = this.zepList.find(t => t.Id == parseInt(selected));
                field.templateOptions.options = this.zepTypeList.filter(t => t.Id === zep.ZepTypeId);
              }
            )
          },
        },
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup:
        [
          {
            key: "DateDebutApplicabilite",
            className: 'row col-md-6',
            type: 'datetimepicker',
            defaultValue: new Date(this.dfv.DateDebutApplicabilite),
            templateOptions:{
              label: 'à partir du',
              required: true,
            },
            validators:
            {
              validation: (x) => ValidationService.periodValidator( x ,this.form, 'DateFinApplicabilite',true)
            },
            expressionProperties: {
              'templateOptions.type': function(model: any, formState: any){
                var isDateonly = (model.DerriereTTXDeclencheur || model.DerriereTrainOuvrant );
                return isDateonly  ? 'dateonly' : 'datetime';
              }
            }
          },
          {
            key: "DateFinApplicabilite",
            className: 'col-md-6',
            type: 'datetimepicker',
            defaultValue: new Date(this.dfv.DateFinApplicabilite),
            templateOptions: {
              label: 'jusqu\'au',
              required: true,
            },
            validators:
            {
              validation: (x) => ValidationService.periodValidator( x ,this.form, 'DateDebutApplicabilite',false)
            },
          },
        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup:
        [
          {
            key: "DerriereTTXDeclencheur",
            className: 'row col-md-6',
            type: 'checkbox',
            templateOptions: {
              label: 'Derrière TTx déclencheur',
            },
            hideExpression: 'model.DerriereTrainOuvrant',
          },

        ]
      },
      {
        fieldGroupClassName: 'row',
        fieldGroup:
        [
          {
            key: "DerriereTrainOuvrant",
            className: 'row col-md-6',
            type: 'checkbox',
            templateOptions: {
              label: 'Derrière train ouvrant',
              required: false},
            hideExpression: 'model.DerriereTTXDeclencheur',
          },

        ]
      },

    ];
    this.applyDisable();
    this.options.formState.model = this.dfv;
  }
  applyDisable(){
    // apply expressionProperty for disabled based on formState to all fields
    this.fields.forEach(field => {
     field.expressionProperties = field.expressionProperties || {};
     field.expressionProperties['templateOptions.disabled'] = 'formState.disabled';
     if(field.fieldGroup){
       field.fieldGroup.forEach(t => {
         t.expressionProperties = t.expressionProperties || {};
         t.expressionProperties['templateOptions.disabled'] = 'formState.disabled';
       });
     }
   });
 }

 submit(dfv){
 if (this.form.valid){
    var resp = this.dfvService.UpdateDfv(dfv);
    resp.subscribe(
      a => {
        this.toastrService.showSuccess("Succès","La DFV à été modifiée.");
        this.dfv = a;
        this.dfvChange.emit(a);
      },
      error => {
        this.toastrService.showError("Erreur",error.error.error);
      }
  );
  }
 }
}
