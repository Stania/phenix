/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DfvPrevBlocTtxComponent } from './dfv-prev-bloc-ttx.component';

describe('DfvPrevBlocTtxComponent', () => {
  let component: DfvPrevBlocTtxComponent;
  let fixture: ComponentFixture<DfvPrevBlocTtxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DfvPrevBlocTtxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DfvPrevBlocTtxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
