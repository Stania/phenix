import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DFV, OccupationType, ManoeuvreType, Manoeuvre } from '../../../../../shared/models/DFV/dfv.models';
import { PointRemarquable_Zep } from '../../../../../shared/models/FicheZep/zep.models';


@Component({
  selector: 'app-dfv-prev-bloc-ttx',
  templateUrl: './dfv-prev-bloc-ttx.component.html',
  styleUrls: ['./dfv-prev-bloc-ttx.component.scss']
})
export class DfvPrevBlocTtxComponent implements OnInit {
  @Input() dfv: DFV;
  @Input() listTypeOccupation : Array<OccupationType>;
  @Input() listTypeManoeuvre : Array<ManoeuvreType>;
  @Input() listPointRemarquable : Array<PointRemarquable_Zep>;
  @Output() dfvChange = new EventEmitter<DFV>();


  constructor() { }

  ngOnInit() {
  }

}
