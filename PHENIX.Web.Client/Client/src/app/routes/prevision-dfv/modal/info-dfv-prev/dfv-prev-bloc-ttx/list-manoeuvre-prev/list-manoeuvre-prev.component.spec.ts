/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ListManoeuvrePrevComponent } from './list-manoeuvre-prev.component';

describe('ListManoeuvrePrevComponent', () => {
  let component: ListManoeuvrePrevComponent;
  let fixture: ComponentFixture<ListManoeuvrePrevComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListManoeuvrePrevComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListManoeuvrePrevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
