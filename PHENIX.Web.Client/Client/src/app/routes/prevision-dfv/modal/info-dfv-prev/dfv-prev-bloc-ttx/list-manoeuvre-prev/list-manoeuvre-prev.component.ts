import { Component, OnInit, Input } from '@angular/core';
import { DFV, ManoeuvreType, Manoeuvre } from '../../../../../../shared/models/DFV/dfv.models';
import { PointRemarquable_Zep } from '../../../../../../shared/models/FicheZep/zep.models';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ManoeuvreService } from '../../../../../../shared/services/manoeuvre/manoeuvre.service';
import { PointRemarquableService } from '../../../../../../shared/services/pointRemarquable/point_remarquable.service';
import { UpdateManoeuvreComponent } from '../../../../../fiche-dfv/dfv-bloc-ttx/modals/update-manoeuvre/update-manoeuvre.component';
import Swal from 'sweetalert2';
import * as _  from 'lodash';
import { Constantes_Phenix } from '../../../../../../shared/models/Commun/constantes.model';
import { CreateManoeuvreComponent } from '../../../../../fiche-dfv/dfv-bloc-ttx/modals/create-manoeuvre/create-manoeuvre.component';
@Component({
  selector: 'app-list-manoeuvre-prev',
  templateUrl: './list-manoeuvre-prev.component.html',
  styleUrls: ['./list-manoeuvre-prev.component.scss']
})
export class ListManoeuvrePrevComponent implements OnInit {
  @Input() listTypeManoeuvre : Array<ManoeuvreType>;
  @Input() listPointRemarquable : Array<PointRemarquable_Zep>;
  @Input() dfv: DFV;
	modelRef : BsModalRef;

  constructor(private modalService: BsModalService
    ,private manoeuvreService : ManoeuvreService
    ,private pointRemarquableService : PointRemarquableService) {
      this.listPointRemarquable = new Array<PointRemarquable_Zep>();
      this.listTypeManoeuvre = new Array<ManoeuvreType>();
      this.dfv = new DFV();
     }

  ngOnInit() {
  }

  showUpdateManoeuvreModal(manoeuvre : Manoeuvre){
    let manoeuvreIndex = this.dfv.ManoeuvreList.findIndex(x => x.Id == manoeuvre.Id);
    let typeManoeuvre = this.listTypeManoeuvre.find(x => x.Id == manoeuvre.ManoeuvreTypeId);
    if(typeManoeuvre != null){
      this.modelRef = this.modalService.show(UpdateManoeuvreComponent, {class:"modal-md"});
      this.modelRef.content.typeManoeuvre = typeManoeuvre;
      this.modelRef.content.listPointRemarquable = this.listPointRemarquable;
      this.modelRef.content.manoeuvre = _.cloneDeep(manoeuvre);
      this.modelRef.content.initForm() ;
      (<UpdateManoeuvreComponent>this.modelRef.content).onClose.subscribe(result => {
        if (result != null) {
          this.dfv.ManoeuvreList[manoeuvreIndex] = result;
        } else {

        }
      });
    }else{
      alert("impossible de retrouver le type de manoeuvre");
    }
  }
  removeManoeuvre(manoeuvre : Manoeuvre){
    let indexManoeuvre = this.dfv.ManoeuvreList.findIndex(x => x == manoeuvre);
    if(indexManoeuvre == -1){
      console.log("Erreur impossible de retrouver la manoeuvre");
      return;
    }
    Swal({
      title: 'Êtes-vous sur ?',
      text: 'Voulez vous supprimer cette manoeuvre ?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Supprimer',
      cancelButtonText: 'Annuler',
    }).then((result) => {
      if (result.value) {
        let resp = this.manoeuvreService.deleteManoeuvre(manoeuvre.Id) ;
        resp.subscribe(
          data => {
            this.dfv.ManoeuvreList.splice(indexManoeuvre,1);
            //Swal('Supprimé !', 'La manoeuvre à été supprimée ', 'success');
          },
          err => console.log(err)
        )
      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })
  }
  getManoeuvreEngagement():Array<Manoeuvre>{
    return this.manoeuvreService.filtrerManoeuvre(Constantes_Phenix.ManoeuvreTypes.Engagement, this.dfv.ManoeuvreList);
}
showCreateEngagementModal(){
  let typeManoeuvre = this.listTypeManoeuvre.find(x => x.Nom == Constantes_Phenix.ManoeuvreTypes.Engagement);
    if(typeManoeuvre != null){
      this.showCreateManoeuvreModal(typeManoeuvre);
    }else{
      alert("impossible de retrouver le type de manoeuvre engagement");
    }
}

showCreateDegagementModal(){
  let typeManoeuvre = this.listTypeManoeuvre.find(x => x.Nom == Constantes_Phenix.ManoeuvreTypes.Degagement);
    if(typeManoeuvre != null){
      this.showCreateManoeuvreModal(typeManoeuvre);
    }else{
      alert("impossible de retrouver le type de manoeuvre degagement");
    }
}
////Degagement
getManoeuvreDegagement():Array<Manoeuvre>{
    return this.manoeuvreService.filtrerManoeuvre(Constantes_Phenix.ManoeuvreTypes.Degagement, this.dfv.ManoeuvreList);
}
  showCreateManoeuvreModal(typeManoeuvre : ManoeuvreType){
    this.modelRef = this.modalService.show(CreateManoeuvreComponent, {class:"modal-md"});
    this.modelRef.content.typeManoeuvre = typeManoeuvre;
    this.modelRef.content.listPointRemarquable = this.listPointRemarquable;
    this.modelRef.content.dfv = this.dfv;
    this.modelRef.content.initForm() ;
    (<CreateManoeuvreComponent>this.modelRef.content).onClose.subscribe(result => {
      if (result != null) {
        this.dfv.ManoeuvreList.push(result);
      } else {

      }
    });
  }
  getPointRemarquableById(idPr : number):PointRemarquable_Zep{
    return this.pointRemarquableService.getPointRemarquableById(idPr,this.listPointRemarquable)
  }
}
