import { Component, OnInit, Input } from '@angular/core';
import { DFV, OccupationType, Occupation } from '../../../../../../shared/models/DFV/dfv.models';
import { PointRemarquable_Zep } from '../../../../../../shared/models/FicheZep/zep.models';
import { Constantes_Phenix } from '../../../../../../shared/models/Commun/constantes.model';
import { CreateOccupationComponent } from '../../../../../fiche-dfv/dfv-bloc-ttx/modals/create-occupation/create-occupation.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { OccupationService } from '../../../../../../shared/services/occupation/occupation.service';
import { UpdateOccupationComponent } from '../../../../../fiche-dfv/dfv-bloc-ttx/modals/update-occupation/update-occupation.component';
import Swal from 'sweetalert2';
import * as _  from 'lodash';

@Component({
  selector: 'app-list-occupation-prev',
  templateUrl: './list-occupation-prev.component.html',
  styleUrls: ['./list-occupation-prev.component.scss']
})
export class ListOccupationPrevComponent implements OnInit {
  @Input() dfv: DFV;
  @Input() listTypeOccupation : Array<OccupationType>;
  @Input() listPointRemarquable : Array<PointRemarquable_Zep>;
	modelRef : BsModalRef;

  constructor(
    public occupationService : OccupationService
    ,private modalService: BsModalService) {
      this.listPointRemarquable = new Array<PointRemarquable_Zep>();
      this.listTypeOccupation = new Array<OccupationType>();
      this.dfv = new DFV();
     }

  ngOnInit() {
  }

   showCreateOccupationAccordModal(){
    let typeOccupation = this.listTypeOccupation.find(x => x.Nom == Constantes_Phenix.OccupationTypes.OccupeeALAccord);
    if(typeOccupation != null){
      this.showCreateOccupationModal(typeOccupation);
    }else{
      alert("impossible de retrouver le type d'occupation Occupée a l'accord");
    }
  }
  ///Occupation
 showCreateOccupationModal(typeOccup : OccupationType){
  this.modelRef = this.modalService.show(CreateOccupationComponent, {class:"modal-md"});
  this.modelRef.content.typeOccupation = typeOccup;
  this.modelRef.content.dfv = this.dfv;
  this.modelRef.content.listPointRemarquable = this.listPointRemarquable;

  this.modelRef.content.initForm() ;
  (<CreateOccupationComponent>this.modelRef.content).onClose.subscribe(result => {
    if (result != null) {
      this.dfv.OccupationList.push(result);
    } else {

    }
  });
}
getOccupationAccordDFV():Array<Occupation>{
  return this.occupationService.filtrerOccupations(Constantes_Phenix.OccupationTypes.OccupeeALAccord, this.dfv.OccupationList);
}

showCreateOccupationRestitutionModal(){
  let typeOccupation = this.listTypeOccupation.find(x => x.Nom == Constantes_Phenix.OccupationTypes.OccupeeALaRestitution);
  if(typeOccupation != null){
    this.showCreateOccupationModal(typeOccupation);
  }else{
    alert("impossible de retrouver le type d'occupation Occupée a l'accord");
  }
}
getOccupationRestitutionZep():Array<Occupation>{
  return this.occupationService.filtrerOccupations(Constantes_Phenix.OccupationTypes.OccupeeALaRestitution, this.dfv.OccupationList);
}

showUpdateOccupationModal( occupation : Occupation){
  let occupIndex = this.dfv.OccupationList.findIndex(x => x.Id == occupation.Id);
  this.modelRef = this.modalService.show(UpdateOccupationComponent, {class:"modal-md"});
  this.modelRef.content.occupation = _.cloneDeep(occupation);
  this.modelRef.content.initForm() ;
  (<UpdateOccupationComponent>this.modelRef.content).onClose.subscribe(result => {
    if (result != null) {
      this.dfv.OccupationList[occupIndex] = result;
    } else {

    }
  });
}
removeOccupation(occupation : Occupation){
  let indexOccup = this.dfv.OccupationList.findIndex(x => x == occupation);
  if(indexOccup == -1){
    console.log("Erreur impossible de retrouver l'occupation");
    return;
  }
  Swal({
    title: 'Êtes-vous sur ?',
    text: 'Voulez vous supprimer cette occupation ?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Supprimer',
    cancelButtonText: 'Annuler',
  }).then((result) => {
    if (result.value) {
      let resp = this.occupationService.deleteOccupation(occupation.Id) ;
      resp.subscribe(
        data => {
          this.dfv.OccupationList.splice(indexOccup,1);
          //Swal('Supprimé !', 'L\'occupation à été supprimée ', 'success');
        },
        err => console.log(err)
      )
    } else if (result.dismiss === Swal.DismissReason.cancel) {

    }
  })
}
}
