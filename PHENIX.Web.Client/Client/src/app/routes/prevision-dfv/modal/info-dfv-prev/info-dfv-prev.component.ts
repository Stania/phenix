import { Component, OnInit } from '@angular/core';
import { DFV, OccupationType, ManoeuvreType } from '../../../../shared/models/DFV/dfv.models';
import { Zep, ZepType, PointRemarquable_Zep } from '../../../../shared/models/FicheZep/zep.models';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
import { PhenixToasterService } from '../../../../shared/settings/phenix-toaster.service';
import { ZepService } from '../../../../shared/services/ficheZep/zep.service';
import { FicheDfvService } from '../../../../shared/services/ficheDfv/fiche-dfv.service';
import { Observable} from "rxjs/Rx";
import { OccupationService } from '../../../../shared/services/occupation/occupation.service';
import { PointRemarquableService } from '../../../../shared/services/pointRemarquable/point_remarquable.service';
import { ManoeuvreService } from '../../../../shared/services/manoeuvre/manoeuvre.service';

@Component({
  selector: 'app-info-dfv-prev',
  templateUrl: './info-dfv-prev.component.html',
  styleUrls: ['./info-dfv-prev.component.scss']
})
export class InfoDfvPrevComponent implements OnInit {
  public loading : boolean;
  public dfv: DFV;
  public zepList: Array<Zep>;
  public zepTypeList: Array<ZepType>;
  modelRef: BsModalRef ;
  public onClose: Subject<DFV>;

  settingActive = 1;
  public listTypeOccupation: Array<OccupationType>;
  public listPointRemarquable: Array<PointRemarquable_Zep>;
  public listTypeManoeuvre: Array<ManoeuvreType>;

  constructor(
    public modal: BsModalRef,
    private toastrService: PhenixToasterService,
    private ficheDFVService: FicheDfvService,
    private zepService: ZepService,
    private dfvService : FicheDfvService,
    private modalService: BsModalService,
    private occupationService: OccupationService,
    private pointRemarquebleService: PointRemarquableService,
    private manoeuvreService: ManoeuvreService, ) {
    this.onClose = new Subject();
    this.dfv = new DFV();
  }
  ngOnInit() {
  }
  populateListe(){
    Observable.forkJoin([
      this.occupationService.getAllOccupationType(),
      this.manoeuvreService.getAllManoeuvreType(),
      this.pointRemarquebleService.GetListPointRemarquableByIdZep(this.dfv.ZepId),
      this.zepService.getAllZepTypes(),
      this.zepService.getZepsAutorises()
    ]).subscribe((response: any[]) => {
      this.listTypeOccupation = response[0];
      this.listTypeManoeuvre = response[1];
      this.listPointRemarquable = response[2];
      this.zepTypeList = response[3];
      this.zepList = response[4];
      this.loading = false;
    });
  }

  onDfvChange(dfv : DFV){
    this.dfv = dfv;
  }

  isBlocTtxDisabled(){
    if(this.dfv.Zep.AutorisationTTX == true)
      this.settingActive=2
      else
      this.toastrService.showError("Erreur","Cette ZEP n'autorise pas la présence de TTx.");
  }

  closePopup(dfv: DFV) {
    this.ficheDFVService.getDFVById(dfv.Id).subscribe(
      data => {
        this.onClose.next(data);
        this.modal.hide();
      });

  }
}
