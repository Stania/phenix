import { Component, OnInit } from "@angular/core";
import { DFV } from "../../shared/models/DFV/dfv.models";
import { AuthenticationService } from "../../shared/services/administration/authentication.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap";
import { PhenixToasterService } from "../../shared/settings/phenix-toaster.service";
import { FicheDfvService } from "../../shared/services/ficheDfv/fiche-dfv.service";
import { UtilisateurVM } from "../../shared/models/ViewModels/UtilisateurVM";
import { AddDfvPrevComponent } from "./modal/add-dfv-prev/add-dfv-prev.component";
import { Constantes_Phenix } from "../../shared/models/Commun/constantes.model";
import { InfoDfvPrevComponent } from "./modal/info-dfv-prev/info-dfv-prev.component";
import * as _ from 'lodash';

@Component({
  selector: "app-prevision-dfv",
  templateUrl: "./prevision-dfv.component.html",
  styleUrls: ["./prevision-dfv.component.scss"]
})
export class PrevisionDfvComponent implements OnInit {
  public listDfvPrev: Array<DFV>;
  loading :boolean = false;
	public currentUser: UtilisateurVM;
  filter : string;
	modelRef: BsModalRef;

  constructor(
    private modalService: BsModalService,
    private authSrv: AuthenticationService,
    private ficheDFVService: FicheDfvService,
    private toastSrv: PhenixToasterService
  ) {
    this.listDfvPrev = new Array<DFV>();
    this.filter = "";
  }

  ngOnInit() {
    this.loading = true;
    this.currentUser = this.authSrv.getLoggedUser();
		const result = this.ficheDFVService.getAllDFVPrevisionnelle();
		result.subscribe(
			data => {
				this.listDfvPrev = data;
			},
			error => {
				this.toastSrv.showError("Erreur", error.error.error);
			},
		() => this.loading = false);
  }

  showAddDFVPrevModal(){
    this.modelRef = this.modalService.show(AddDfvPrevComponent, { class: "modal-md",ignoreBackdropClick: true });
		(<AddDfvPrevComponent>this.modelRef.content).onClose.subscribe(result => {
			if (result != null) {
        this.listDfvPrev.push(result);
        this.showInfoDfv(result)
			} else {

			}
		});
  }
  showInfoDfv(dfv:DFV){
    let indexDFv = this.listDfvPrev.findIndex(x => x.Id == dfv.Id);
    if(indexDFv == -1)
    {
      this.toastSrv.showError("Erreur","Impossible de retrouver la Dfv.");
      return;
    }
    this.ficheDFVService.getDFVById(dfv.Id).subscribe(
      data => {
        this.modelRef = this.modalService.show(InfoDfvPrevComponent, { class: "modal-lg",ignoreBackdropClick: true });
        this.modelRef.content.dfv = _.cloneDeep(data);
        this.modelRef.content.populateListe();
        (<InfoDfvPrevComponent>this.modelRef.content).onClose.subscribe(result => {
          if (result != null) {
            this.listDfvPrev[indexDFv] = result;
          } else {

          }
        });
      },
      err => console.log(err)
    )
  }


  public getIconByStatut(statut : string) : string{
    if( statut == Constantes_Phenix.ZepStatuts.Restituee)
      return "badge badge-success";
    else if(statut == Constantes_Phenix.ZepStatuts.EnCreation ||statut == Constantes_Phenix.ZepStatuts.Accordee || statut == Constantes_Phenix.ZepStatuts.LeveeMesure)
      return "badge badge-info";
      else if(statut == Constantes_Phenix.ZepStatuts.EnAccord)
        return "badge badge-warning";
    else if(statut == Constantes_Phenix.ZepStatuts.Refusee ||
       statut == Constantes_Phenix.ZepStatuts.Annulee)
       return "badge badge-danger";
}
}
