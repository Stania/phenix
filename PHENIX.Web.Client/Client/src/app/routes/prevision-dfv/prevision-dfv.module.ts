import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PrevisionDfvComponent } from "./prevision-dfv.component";
import { SharedModule } from "../../shared/shared.module";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { FilterDfvPipe } from "../../shared/directives/pipes/filterDfv.pipe";
import { OrderByStatutPipe } from "../../shared/directives/pipes/sortDfvState.pipe";
import { AddDfvPrevComponent } from "./modal/add-dfv-prev/add-dfv-prev.component";
import { InfoDfvPrevComponent } from "./modal/info-dfv-prev/info-dfv-prev.component";
import { DfvPrevBlocApplicationComponent } from "./modal/info-dfv-prev/dfv-prev-bloc-application/dfv-prev-bloc-application.component";
import { DfvPrevBlocInfoComponent } from "./modal/info-dfv-prev/dfv-prev-bloc-info/dfv-prev-bloc-info.component";
import { DfvPrevBlocTtxComponent } from "./modal/info-dfv-prev/dfv-prev-bloc-ttx/dfv-prev-bloc-ttx.component";
import { ListManoeuvrePrevComponent } from "./modal/info-dfv-prev/dfv-prev-bloc-ttx/list-manoeuvre-prev/list-manoeuvre-prev.component";
import { ListOccupationPrevComponent } from "./modal/info-dfv-prev/dfv-prev-bloc-ttx/list-occupation-prev/list-occupation-prev.component";

const routes: Routes = [{ path: "", component: PrevisionDfvComponent }];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule
  ],
  declarations: [
    PrevisionDfvComponent,
    AddDfvPrevComponent,
    InfoDfvPrevComponent,
    DfvPrevBlocTtxComponent,
    DfvPrevBlocInfoComponent,
    DfvPrevBlocApplicationComponent,
    ListOccupationPrevComponent,
    ListManoeuvrePrevComponent
   ],
  exports: [RouterModule],
  entryComponents:[
    AddDfvPrevComponent,
    InfoDfvPrevComponent,
    DfvPrevBlocTtxComponent,
    DfvPrevBlocInfoComponent,
    DfvPrevBlocApplicationComponent
  ]
})
export class PrevisionDfvModule {}
