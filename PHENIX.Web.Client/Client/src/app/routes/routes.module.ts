import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslatorService } from '../core/translator/translator.service';
import { MenuService } from '../core/menu/menu.service';
import { SharedModule } from '../shared/shared.module';
import { menu } from './menu';
import { routes } from './routes';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { SynoptiqueService } from '../routes/synoptique/services/synoptique.service';
import { InMemoryZepsService } from  '../routes/synoptique/services/InMemoryDbService';
import { DrawService } from '../shared/services/draw.service';
import { ZepService } from '../shared/services/ficheZep/zep.service';
import { LoginComponent } from './login/login.component';
import { ImportZepService } from '../shared/services/ficheZep/importZep.service';
import { DfvListService } from '../shared/services/ficheDfv/dfv-list.service';
import { FicheDfvService } from '../shared/services/ficheDfv/fiche-dfv.service';
import { ChoixProfilComponent } from './choix-profil/choix-profil.component';
//import { UserModule } from './user/user.module';
//import { BiblioZepsComponent } from './biblio-zeps/biblio-zeps.component';


@NgModule({
    imports: [
        SharedModule,

        //HttpClientInMemoryWebApiModule.forRoot(InMemoryZepsService),
        RouterModule.forRoot(routes )
    ],
    declarations: [
        LoginComponent, 
        ChoixProfilComponent
    ],
    exports: [
        RouterModule
    ],
    providers:
    [
        SynoptiqueService,
        InMemoryZepsService,
        DrawService,
        ZepService,
        FicheDfvService
    ]
})

export class RoutesModule {
    constructor(public menuService: MenuService, tr: TranslatorService) {
        menuService.addMenu(menu);
    }
}
