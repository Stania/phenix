import { CanActivate } from '@angular/router';
import { LayoutComponent } from '../layout/layout.component';
import { AuthGuard } from '../core/guards/auth.guards';
import { LoginComponent } from './login/login.component';
import { ChoixProfilComponent } from './choix-profil/choix-profil.component';

export const routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dfvlist', pathMatch: 'full' },
            { path: 'synoptique', loadChildren: './synoptique/synoptique.module#SynoptiqueModule' },
            { path: 'dfv', loadChildren: './fiche-dfv/fiche-dfv.module#FicheDfvModule' },
            { path: 'dfvlist', loadChildren: './dfv-list/dfv-list.module#DfvListModule'},
            { path: 'zeps', loadChildren: './biblio-zeps/biblio-zeps.module#BiblioZepsModule'},
            { path: 'previsiondfv', loadChildren: './prevision-dfv/prevision-dfv.module#PrevisionDfvModule'},

        ],
        canActivate: [ AuthGuard ]
    },

    { path: 'login', component : LoginComponent},
    { path: 'choixprofil', component : ChoixProfilComponent, canActivate: [ AuthGuard ]},
    // Not found
    { path: '**', redirectTo: 'synoptique' }
];
