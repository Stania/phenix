import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkZepComponent } from './link-zep.component';

describe('AddZEPComponent', () => {
  let component: LinkZepComponent;
  let fixture: ComponentFixture<LinkZepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkZepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkZepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
