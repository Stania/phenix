import { Component, OnInit, AfterContentInit, AfterViewInit, AfterViewChecked, AfterContentChecked, Input, OnDestroy } from '@angular/core';
import * as Konva from 'konva';
import { ObjetGraphiqueVM} from '../../shared/models/ViewModels/ObjetGraphiqueVM';
import {WindowRef} from '../../shared/global/WindowRef';
import {SynoptiqueService} from  './services/synoptique.service';
import {DrawService} from  '../../shared/services/draw.service';
import { Jsonp } from '@angular/http/src/http';
import { concat } from 'rxjs/operator/concat';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { LinkZepComponent } from './modals/link-zep/link-zep.component';
import { error } from 'util';
import { forEach, last } from '@angular/router/src/utils/collection';
import { tr } from 'ngx-bootstrap/bs-moment/i18n/tr';
import { Subscriber } from 'rxjs/Subscriber';
import { ZepService } from '../../shared/services/ficheZep/zep.service';
import { Zep } from '../../shared/models/FicheZep/zep.models';

import { FormControl, FormGroup } from '@angular/forms';
import { ImportZepService } from '../../shared/services/ficheZep/importZep.service';


import {Observable} from 'rxjs/Rx';
import { ActivatedRoute } from '@angular/router';
import { FicheZepComponent } from '../fiche-zep/fiche-zep.component';
import { DFV } from '../../shared/models/DFV/dfv.models';
import { FicheDfvService } from '../../shared/services/ficheDfv/fiche-dfv.service';
import { FicheDfvComponent } from '../fiche-dfv/fiche-dfv.component';
import { element } from 'protractor';
import { Constantes_Phenix } from '../../shared/models/Commun/constantes.model';


@Component({
    selector: 'app-synoptique',
    templateUrl: './synoptique.component.html',
    styleUrls: ['./synoptique.component.scss']
})

export class SynoptiqueComponent implements  AfterViewInit, OnInit, OnDestroy {

    public isOperationnel: boolean
    public loading : boolean = false;
    sub: any;

    zepsList:Array<Zep>;
    listDFV:Array<DFV>;
    zep: Zep;

    objets:Array<ObjetGraphiqueVM>;
    Points : number[];
    _MargePointedLine : number = 20;
    _PointedLine : number[] = [];
    _CurrentLineLayer : Konva.Layer;//mouse hold
    _NotSavedLinesLayerList : Konva.Layer; //not saved
    _SavedZepsList : Konva.Layer;//saved
    _CircleLayer:Konva.Layer;
    _Stage : Konva.Stage;
    _Tooltip : Konva.Label;
    _SiteImageObj : any;
    _SiteImage : Konva.Image;
    _TTXImageObj : any;
    _TTXImageObjList : Array<Konva.Image>;
    _ImageLayer : Konva.Layer;
    _TTXLayer : Konva.Layer;

    _CtrlPressed = false;
    _DrawZEP = false;
    _ModeOperationnel = false;
    _LastCollisionLine : Konva.Line;
    modalRef: BsModalRef;
    IsMouseDown :boolean;
    IsShiftDown : boolean;
    IsCtrlDown:boolean;
    InitialCursor:string;
    PlagesAngles : any[];



    constructor(private route: ActivatedRoute,
      private zepService: ZepService,
      private winRef: WindowRef,
      private drawService: DrawService,
      private modalService: BsModalService,
      private importZepService: ImportZepService,
      private dfvService: FicheDfvService) {


        this.objets = new Array<ObjetGraphiqueVM>();;
        this.Points = [];
        this._SavedZepsList = new Konva.Layer();
        this._CircleLayer = new Konva.Layer();
        this.IsMouseDown = false;
        this.IsShiftDown = false;
        this.PlagesAngles = this.drawService.dividCircle(24);
      //this.Name = new string;


    }




    ngAfterViewInit  (){
      this.refresh(this._ModeOperationnel);
    }
    ngOnInit() {
      this.sub = this.route
        .data
        .subscribe(v => {
          this.isOperationnel = v.isOperationnel;
          this._DrawZEP = !this.isOperationnel;
          this._ModeOperationnel = this.isOperationnel;
        });
    }


    ngOnDestroy() {
      this.sub.unsubscribe();
    }


    redo(){
        this.objets = this.objets.splice(this.objets.length-2,2);
        this._CurrentLineLayer.draw();
    }

    ctrl(){
        this._CtrlPressed = ! this._CtrlPressed;
    }

    typeSelectedZep(){
        this._DrawZEP = true;
        this._ModeOperationnel = false;
        this.refresh();

    }
    typeSelectedAutre(){
      this._DrawZEP = false;
      this._ModeOperationnel = false;
      this.refresh();

    }


    typeSelectedOperationnel(){
      this._ModeOperationnel = true;
      this._DrawZEP = false;
      this.refresh(true);
    }

    associerZep(){

        this.modalRef = this.modalService.show(LinkZepComponent);
        this.modalRef.content.objetsGraphiquesIHMList =  this.objets.map(a=>a);
        (<LinkZepComponent>this.modalRef.content).onClose.subscribe(result => {
          if (result != null) {
                    this.objets.length = 0;
                    this.Points.length = 0;
                    this._NotSavedLinesLayerList.removeChildren();
                    //console.log(result);
                   this.getZeps();
          } else {
            //alert("Aucune ZEP n\'a été créée");
          }
        });
    }

    // showZep(){
    //     this.modalRef = this.modalService.show(FicheZepComponent, {
    //         class: 'modal-lg'
    //     });
    //     this.modalRef.content.zep = this.zep;
    //     this.modalRef.content.PopulateListes();

    //     //const result = this.zepService.getZepById(id);

    // }

    refresh(modeOperationnel : boolean = false){
        this.clear(modeOperationnel);

        //this.displayExtremite();
    }

    getZeps(){
        this.loading = true;
        this.zepService.getDrawedZepList().subscribe(
            (res) => {
                this.zepsList = res;
                console.log(res);
                this.objets.length = 0;
                this.DisplayZeps();
            },
            (err) => {console.log(err)},
            () => {this.loading = false; }
        )
    }

    getEtatOpeationnel(){
        this.zepService.getEtatOpeationnel().subscribe(
          (res) => {
            this._TTXImageObjList = new Array<Konva.Image>();
            this.zepsList = new Array<Zep>();
            this.listDFV = new Array<DFV>();
            let listZepEnfant = new Array<any>();
            res.forEach(element => {
              let coord;
              if (element.DFV == null && element.Zep.CompositionZepEnfantList == null){
                console.log(element.Zep)
                element.Zep.ObjetGraphiqueList.forEach(obj => {
                  obj.Color = 'black'
                });
              }
              else if (element.DFV != null){
                this.listDFV.push(element.DFV);
                if (element.IsGroup){
                  listZepEnfant.push({ ZepPere : element.Zep, ListZep :element.Zep.CompositionGroupeZepList.map(a=>a.ZepEnfant)});
                  if (element.IsTTXOnIt){
                    coord = element.Zep.CompositionGroupeZepList[0].ZepEnfant.ObjetGraphiqueList[0].Coordonnees.split('##');
                  }
                }
                if (element.Zep.ObjetGraphiqueList !== null && element.Zep.ObjetGraphiqueList.length > 0) {
                  if (element.IsTTXOnIt) coord = element.Zep.ObjetGraphiqueList[0].Coordonnees.split('##');
                  if(element.DFV.StatusZep.Statut === Constantes_Phenix.ZepStatuts.EnAccord){
                    element.Zep.ObjetGraphiqueList.forEach(obj => {
                      obj.Type = Constantes_Phenix.ObjetGraphique.TypesObjets.DashedLigne;
                    })
                  }
                }
                if (coord != undefined && coord != null)
                {
                  let angle = this.drawService.angle(Number(coord[0]),Number(coord[1]),Number(coord[2]),Number(coord[3]));
                  let center =  this.drawService.getLineCenter(Number(coord[0]),Number(coord[1]),Number(coord[2]),Number(coord[3]));
                  let projectionAngle = 90
                  if (angle >= 0  &&  angle < 90)
                  {
                    angle = -angle;
                  }
                  else if (angle >= 90  &&  angle < 180){
                    angle = angle + 180
                  }
                  else if (angle >= 180  &&  angle < 225){
                    angle = -angle - 180
                  }
                  else {
                    angle = -angle
                    projectionAngle = 0;
                  }
                  let centerPorject = this.drawService.createCoord(center[0], center[1], angle + projectionAngle,9 +this._TTXImageObj.width/2)
                  centerPorject = this.drawService.createCoord(centerPorject.x, centerPorject.y, -angle, -this._TTXImageObj.height)
                  let ttxImage = new Konva.Image({
                    x: centerPorject.x,
                    y: centerPorject.y,
                    image: this._TTXImageObj,
                    rotation: angle
                  });
                  this._TTXImageObjList.push(ttxImage);
                }
              }
              else{

              }
              this.zepsList.push(element.Zep);
            });
            listZepEnfant.forEach(element => {
              if (element.ListZep.length > 1){
                this.zepsList.forEach(z => {
                  if (element.ListZep.some(a=>a.Numero === z.Numero)){
                    z.ObjetGraphiqueList.forEach(obj=>{
                      obj.Color = element.ListZep[0].ObjetGraphiqueList[0].Color;
                    })
                  }
                });
              }
            });

            this.objets.length = 0;
            this.DisplayZeps();
            this.DisplayTTXs();
          },
          (err) => {console.log(err)},
          () => {}
      )
    }

    DisplayTTXs(){
      this._TTXLayer.removeChildren();
      this._TTXImageObjList.forEach(element => {
        element.moveToTop();
        this._TTXLayer.add(element);
      });
      this._TTXLayer.moveToTop();
      this._TTXLayer.draw();
    }

    clear(modeOperationnel : boolean = false){
      this.loading = true;
      var container = document.getElementById('container');
      this._SiteImageObj = new Image(8000,500);
      this._SiteImageObj.src = './assets/app/Annexe_Mantes.png';
      this._TTXImageObj = new Image(40,25);
      this._TTXImageObj.src = './assets/app/TTX.PNG';
      this._SiteImageObj.onload = function() {
        console.log('aaaaaaaaaaa')
        var width = this.winRef.nativeWindow.innerWidth;
        var height = this.winRef.nativeWindow.innerHeight;
        var stage = new Konva.Stage({
          container:container,
          width:this._SiteImageObj.width + 135,
          height:this._SiteImageObj.height + 100,
          draggable: true,
          dragBoundFunc: function(pos) {
            if (this.IsCtrlDown){
              return {
                x: pos.x,
                y: pos.y
              }
            }


          }.bind(this),

        });
        this._Stage = stage;
        this._Stage.removeChildren();
        this._SiteImage = new Konva.Image({
          x: 75,
          y: 50,
          image: this._SiteImageObj,
          stroke: 'black',
          strokeWidth: 1,

        });

        this._ImageLayer = new Konva.Layer();
        this._ImageLayer.name("Image");
        if (this._DrawZEP)
        this._ImageLayer.add(this._SiteImage);

        this._TTXLayer = new Konva.Layer();
        this._TTXLayer.name("TTX");

        this._Stage.add(this._ImageLayer);
        this._Stage.add(this._TTXLayer);

        this._CurrentLineLayer = new Konva.Layer();
        this._CurrentLineLayer.name("Current line");
        this._Stage.add(this._CurrentLineLayer);

        this._NotSavedLinesLayerList = new Konva.Layer();
        this._NotSavedLinesLayerList.name('Note saved lines');
        this._Stage.add(this._NotSavedLinesLayerList);

        this._SavedZepsList = new Konva.Layer();
        this._SavedZepsList.name("Saved zeps");
        this._Tooltip = new Konva.Label({
          opacity: 0.75,
          visible: false,
          listening: false
        });
        this._Tooltip.add(new Konva.Tag({
          fill: 'black',
          pointerDirection: 'down',
          pointerWidth: 10,
          pointerHeight: 10,
          lineJoin: 'round',
          shadowColor: 'black',
          shadowBlur: 10,
          shadowOffset: {x : 10, y : 10},
          shadowOpacity: 0.2
        }));
        this._Tooltip.add(new Konva.Text({
          text: '',
          fontFamily: 'Calibri',
          fontSize: 18,
          padding: 5,
          fill: 'white'
        }));
        this._SavedZepsList.add(this._Tooltip);
        this._Stage.add(this._SavedZepsList);

        this.subscribe();
        if (!modeOperationnel)
          this.getZeps();
        else
          this.getEtatOpeationnel();
      }.bind(this);

      this.loading = false;
    }

    DisplayZeps(){

        this.zepsList.forEach(element => {
          if(element.ObjetGraphiqueList && element.ObjetGraphiqueList.length>0){
            this.displayObjetcs(element.ObjetGraphiqueList.map(a=>ObjetGraphiqueVM.CreateFromObjetGraphique(a)), this._SavedZepsList,element.Numero);
          }
        });
        this.displayExtremite();
    }

    displayExtremite(){
        for(let i=0;i<this.zepsList.length;i++){
            let zep = this.zepsList[i];

            if(!(zep.ObjetGraphiqueList && zep.ObjetGraphiqueList.length>0)) continue;
            let size = zep.ObjetGraphiqueList.length;
            //la pire manière de chercher les points de croisement
            //qualité du code = qualité de l'environnement de travail


            var arrays = [];

            for(let obj of zep.ObjetGraphiqueList){
              // une ligne d'une zep
                let objIHM = ObjetGraphiqueVM.CreateFromObjetGraphique(obj);
                let x1 = objIHM.coordonnees[0];
                let y1 = objIHM.coordonnees[1];
                let x2 = objIHM.coordonnees[2];
                let y2 = objIHM.coordonnees[3];

                //parcourt des autres zeps
                for(let j=i+1; j< this.zepsList.length;j++){
                    var zep2 = this.zepsList[j];
                    if(!(zep2.ObjetGraphiqueList && zep2.ObjetGraphiqueList.length>0)) continue;
                    for(let obj2 of zep2.ObjetGraphiqueList){
                      let objIHM2 = ObjetGraphiqueVM.CreateFromObjetGraphique(obj2);
                      arrays.push(objIHM2.coordonnees);
                    }
                }
                this.drawExtremiteFromCoord(x1,y1,arrays);
                this.drawExtremiteFromCoord(x2,y2,arrays);
            }
        }
    }

    drawExtremiteFromCoord(x1, y1, arrays){
        for(let i=0;i<arrays.length;i++){
            let arr = arrays[i];
            let sx1 = arr[0];
            let sy1 = arr[1];
            let sx2 = arr[2];
            let sy2 = arr[3];

            if( (x1 != undefined && y1 != undefined) && ((x1 === sx1 && y1 === sy1) || (x1 === sx2 && y1 === sy2)) ){
                this.drawExtremite([x1,y1]);
            }
        }
    }

    subscribe(){
        var container = document.getElementById('container');
        var scaleBy = 1.01;
        container.addEventListener('wheel', function(e) {
            if(!this.IsCtrlDown) return;
            e.preventDefault();
            var oldScale = this._Stage.scaleX();

            var mousePointTo = {
                x: this._Stage.getPointerPosition().x / oldScale - this._Stage.x() / oldScale,
                y: this._Stage.getPointerPosition().y / oldScale - this._Stage.y() / oldScale,
            };

            var newScale = e.deltaY < 0 ? oldScale * scaleBy : oldScale / scaleBy;
            this._Stage.scale({ x: newScale, y: newScale });

            var newPos = {
                x: -(mousePointTo.x - this._Stage.getPointerPosition().x / newScale) * newScale,
                y: -(mousePointTo.y - this._Stage.getPointerPosition().y / newScale) * newScale
            };
            this._Stage.position(newPos);
            this._Stage.batchDraw();
        }.bind(this));
        document.addEventListener('keydown', function(event){

            if(event.keyCode === 16 || event.charCode === 16){
                this.IsShiftDown = true;
            }
            if(event.keyCode === 17){
              this.IsCtrlDown = true;
              if(this._Stage.container().style.cursor != 'move'){
                this.InitialCursor = this._Stage.container().style.cursor ;
              }
              this._Stage.container().style.cursor = 'move';
            }
        }.bind(this));
        document.addEventListener('keyup', function(event){
            if(event.keyCode === 16 || event.charCode === 16){
                this.IsShiftDown = false;
            }
            if(event.keyCode === 17){
              this.IsCtrlDown = false;
              this._Stage.container().style.cursor = this.InitialCursor;
            }
        }.bind(this));


        this._Stage.on("mousedown",function(){

           if(!this._DrawZEP) return;
           if (!this.IsCtrlDown){
            //let mousePos = this._Stage.getPointerPosition();
            var oldScale = this._Stage.scaleX();
            var mousePointTo = {
                x: this._Stage.getPointerPosition().x / oldScale - this._Stage.x() / oldScale,
                y: this._Stage.getPointerPosition().y / oldScale - this._Stage.y() / oldScale,
            };
            let X = mousePointTo.x;
            let Y = mousePointTo.y;
            if (this._PointedLine.length > 0){
                if ((this._PointedLine[0] <= X + this._MargePointedLine && this._PointedLine[0] >= X - this._MargePointedLine) &&
                (this._PointedLine[1] <= Y + this._MargePointedLine &&  this._PointedLine[1] >= Y - this._MargePointedLine))
                {
                    X = this._PointedLine[0];
                    Y = this._PointedLine[1];
                }
                else if ((this._PointedLine[2] <= X + this._MargePointedLine &&  this._PointedLine[2] >= X - this._MargePointedLine) &&
                    (this._PointedLine[3] <= Y + this._MargePointedLine &&  this._PointedLine[3] >= Y - this._MargePointedLine))
                {
                    X = this._PointedLine[2];
                    Y = this._PointedLine[3];
                }
                else{
                    let pt = this.drawService.projectionPerpendiculaire(
                        this._PointedLine[0],
                        this._PointedLine[1],
                        this._PointedLine[2],
                        this._PointedLine[3],
                        X, Y);
                    if(pt){
                        X = pt.X;
                        Y = pt.Y;
                    }
                }
            }
            if(this._DrawZEP){
                this.IsMouseDown = true;
                this.Points.length = 0;
                this.Points.push(X,Y);
            }
            this.Points.push(X,Y);
            this.drawLine(this.Points, this._CurrentLineLayer);
           }
        }.bind(this));
        this._Stage.on("mousemove",function(){

            if(!this._DrawZEP) return;

            if(!this.IsMouseDown) return;

            //let mousePos = this._Stage.getPointerPosition();
            var oldScale = this._Stage.scaleX();
            var mousePointTo = {
                x: this._Stage.getPointerPosition().x / oldScale - this._Stage.x() / oldScale,
                y: this._Stage.getPointerPosition().y / oldScale - this._Stage.y() / oldScale,
            };
            let X = mousePointTo.x;
            let Y = mousePointTo.y;
            if(this.Points.length > 0){
                this.Points = this.Points.filter((x,i)=>i<2);
            }
            if (this.IsShiftDown){
                var angle = this.drawService.angle(this.Points[0], this.Points[1], X, Y)
                    for (var an in this.PlagesAngles){
                    if (this.PlagesAngles[an].debut < this.PlagesAngles[an].fin)
                    {
                        if (angle >= this.PlagesAngles[an].debut && angle <= this.PlagesAngles[an].fin ){
                            var point = this.drawService.createCoord(this.Points[0], this.Points[1],this.PlagesAngles[an].center,this.drawService.distance(this.Points[0], this.Points[1], X, Y));
                            X = point.x;
                            Y = point.y;
                        }
                    }
                    else
                    {
                        if (angle >= 180 )
                        {
                            if (angle >= this.PlagesAngles[an].debut){
                                var point = this.drawService.createCoord(this.Points[0], this.Points[1],this.PlagesAngles[an].center,this.drawService.distance(this.Points[0], this.Points[1], X, Y));
                                X = point.x;
                                Y = point.y;
                            }
                        }
                        else
                        {
                            if (angle <= this.PlagesAngles[an].fin)
                            {
                                var point = this.drawService.createCoord(this.Points[0], this.Points[1],this.PlagesAngles[an].center,this.drawService.distance(this.Points[0], this.Points[1], X, Y));
                                X = point.x;
                                Y = point.y;
                            }
                        }
                    }
                }
            }
            this.Points.push(X,Y);
            this._CurrentLineLayer.removeChildren();
            this.drawLine(this.Points, this._CurrentLineLayer,10, 'black');

        }.bind(this));
        this._Stage.on("mouseup",function(){

            if(!this._DrawZEP) return;
            this.IsMouseDown = false;
           // let mousePos = this._Stage.getPointerPosition();
            var oldScale = this._Stage.scaleX();
            var mousePointTo = {
                x: this._Stage.getPointerPosition().x / oldScale - this._Stage.x() / oldScale,
                y: this._Stage.getPointerPosition().y / oldScale - this._Stage.y() / oldScale,
            };
            let X = mousePointTo.x;
            let Y = mousePointTo.y;
            if (this._PointedLine.length > 0){
                if(this._LastCollisionLine){
                  this._LastCollisionLine.stroke('black');
                  this._LastCollisionLine.draw();
                }

                if ((this._PointedLine[0] <= X + this._MargePointedLine &&  this._PointedLine[0] >= X - this._MargePointedLine) &&
                (this._PointedLine[1] <= Y + this._MargePointedLine &&  this._PointedLine[1] >= Y - this._MargePointedLine))
                {
                    this.Points[2] = this._PointedLine[0];
                    this.Points[3] = this._PointedLine[1];
                }
                else if ((this._PointedLine[2] <= X + this._MargePointedLine &&  this._PointedLine[2] >= X - this._MargePointedLine) &&
                    (this._PointedLine[3] <= Y + this._MargePointedLine &&  this._PointedLine[3] >= Y - this._MargePointedLine))
                {
                    this.Points[2] = this._PointedLine[2];
                    this.Points[3] = this._PointedLine[3];
                }
                else{
                     let pt = this.drawService.projectionPerpendiculaire(
                        this._PointedLine[0],
                        this._PointedLine[1],
                        this._PointedLine[2],
                        this._PointedLine[3],
                        X, Y);
                    if(pt){
                        this.Points[2] = pt.X;
                        this.Points[3] = pt.Y;
                    }
                }
            }

            var pts = this.Points.map(x=>x);
            var objG : ObjetGraphiqueVM = new ObjetGraphiqueVM() ;

            objG.type = Constantes_Phenix.ObjetGraphique.TypesObjets.Ligne;
            objG.coordonnees = pts;
            this.objets.push(objG);
            this._CurrentLineLayer.removeChildren();
            this.drawLine(this.Points.map(t=>t), this._NotSavedLinesLayerList, 10, 'black');
            this.Points.length = 0;
            this._PointedLine.length = 0;
            this._Stage.draw();
        }.bind(this));

        this._NotSavedLinesLayerList.on("mouseleave", (function(event){

          var line = event.target;
          if (line.className !== 'Line') return;
          line.stroke(this._PointedLineColor);
          line.draw();
          this._PointedLine.length = 0;

        }).bind(this));
        this._NotSavedLinesLayerList.on("mouseenter", (function(event){
          var line = event.target;
          if (line.className !== 'Line') return;
          this._PointedLine = line.attrs.points.map(t=>t);
          if (line.stroke() != 'red') this._PointedLineColor =  line.stroke();
          line.stroke('red');
          line.draw();
          if (!this._DrawZEP) return;
          this._Stage.container().style.cursor = 'url(./assets/app/redPencil.cur),auto';
        }).bind(this));

        this._SiteImage.on('mouseenter', function () {
          if (!this._DrawZEP) return;
          this._Stage.container().style.cursor = 'url(./assets/app/redPencil.cur),auto';
        }.bind(this));
        this._SiteImage.on('mouseleave', function () {
          if (!this._DrawZEP) return;
          this._Stage.container().style.cursor = 'default';
        }.bind(this));

        this._CurrentLineLayer.on('mouseenter', function (e) {
          if (!this._DrawZEP) return;
          this._Stage.container().style.cursor = 'url(./assets/app/redPencil.cur),auto';

        }.bind(this));

        this._SavedZepsList.on('mouseenter', function (event) {
          var line = event.target;
          if (line.className !== 'Line') return;
          this._PointedLine = line.attrs.points.map(t=>t);
          if (line.stroke() != 'red') this._PointedLineColor =  line.stroke();
          if (this._DrawZEP) {
            line.stroke('red');
            line.draw();
            this._Stage.container().style.cursor = 'url(./assets/app/redPencil.cur),auto';

          }
          else {

            this._Stage.container().style.cursor = 'pointer';
            this._SavedZepsList.children.forEach(element => {
              if (element.attrs.id === line.attrs.id){
                element.stroke('red');
                element.draw();
              }
            });

            var mousePos = line.getStage().getPointerPosition();
            var oldScale = this._Stage.scaleX();
            var mousePointTo = {
                x: this._Stage.getPointerPosition().x / oldScale - this._Stage.x() / oldScale,
                y: this._Stage.getPointerPosition().y / oldScale - this._Stage.y() / oldScale,
            };
            this._Tooltip.position({
                x : mousePointTo.x,
                y : mousePointTo.y
            });
            this._Tooltip.getText().setText("Zep numéro : " + line.getId());
            this._Tooltip.show();
            this._Tooltip.moveToTop();
            this._SavedZepsList.batchDraw();

          }

        }.bind(this));
        this._SavedZepsList.on('mouseleave', function (event) {
          var line = event.target;
          if (line.className !== 'Line') return;
          line.stroke(this._PointedLineColor);
          this._Tooltip.hide();
          line.draw();
          this._PointedLine.length = 0;
          if (!this._DrawZEP){
            this._Stage.container().style.cursor = 'default';
            this._SavedZepsList.children.forEach(element => {
              if (element.attrs.id === line.attrs.id){
                element.stroke(this._PointedLineColor);
                element.draw();
              }
            });
          }
          this._SavedZepsList.batchDraw();
        }.bind(this));
        this._SavedZepsList.on('click', function (event) {
          if (this._DrawZEP) return;
          event.cancelBubble = true;
          var line = event.target;
          if (!this._ModeOperationnel)
            this.showZep (line.attrs.id);
          else{
            this.showDFV (line.attrs.id);
          }
        }.bind(this));
        this._Stage.on('contentContextmenu', function(e) {

        });
    }

    displayObjetcs(objets: ObjetGraphiqueVM[], linesLayer: Konva.Layer , id:string = null){
        var coorLine = [];
        var coorExt = [];
        var coorSDM = [];

        for(let obj of objets){
            if(obj.type === Constantes_Phenix.ObjetGraphique.TypesObjets.Ligne){
                this.drawLine(obj.coordonnees, linesLayer,10, obj.color, id);
            }
            else if(obj.type === Constantes_Phenix.ObjetGraphique.TypesObjets.DashedLigne) {
              this.drawLine(obj.coordonnees, linesLayer,10, obj.color, id,true);
            }
            if(obj.type === Constantes_Phenix.ObjetGraphique.TypesObjets.Extremite){
                //coorExt = coorExt.concat(obj.coordonnees);
                //this.drawExtremite(obj.coordonnees);
            }
            if(obj.type === Constantes_Phenix.ObjetGraphique.TypesObjets.SautDeMouton){
                coorSDM = coorSDM.concat(obj.coordonnees);
            }
        }
    }

    drawLine(coorLine:number[], layer : Konva.Layer,strokeWidth : number = 10 , color:string = 'black' , id:string = null, dashed:boolean = false){
        if (coorLine === undefined
          || coorLine === null
          || coorLine.length != 4
          || coorLine.some(function(element) {
          // checks whether an element is even
          return element === undefined || element === null;
        })) return;
        var dash = dashed ? [33, 20] : [0,0]
        var line = new Konva.Line({
            points: coorLine.map(a=>a),
            fill: color,
            stroke: color,
            strokeWidth: 3,
            lineCap: 'round',
            lineJoin: 'round',
            //dash: [29, 20, 0.001, 20],
            draggable: false,
            dash: dash
        });
        if (id != null) line.id(id);
        layer.add(line);
        layer.moveToTop();
        layer.draw();



    }

    drawCircle(coor:number[]){
        if(coor.length<2) return;
        let circle = new Konva.Circle({
            x: coor[0],
            y: coor[1],
            radius: 2,
            fill: 'blue',
            stroke: 'blue',
            strokeWidth: 0
        });
        this._CircleLayer.add(circle);
        this._CircleLayer.draw();
    }

    drawExtremite(coor : number[]){
        if(coor.length<2) return;
        var extremite = new Konva.Group({
            draggable: false
        });

        var partLeft = new Konva.Arc({
            x: coor[0],
            y: coor[1],
            innerRadius: 0,
            outerRadius: 7,
            angle: 180,
            rotation: 90,
            fill: 'white',
            stroke: 'black',
            strokeWidth: 1
        });

        var partRight = new Konva.Arc({
            x: coor[0],
            y: coor[1],
            innerRadius: 0,
            outerRadius:7,
            angle: 180,
            rotation: 270,
            fill: 'black',
            stroke: 'black',
            strokeWidth: 1
        });

        extremite.add(partLeft).add(partRight);
        this._SavedZepsList.add(extremite);
        extremite.moveToTop();
        extremite.draw();
    }

    drawSautMouton(coor : number[]){
        var sautmouton = new Konva.Group({
            draggable: true
        });

        var lineUp = new Konva.Line({
            points: [coor[0], coor[1]+20 , coor[0] , coor[1]+40],
            stroke: 'black',
            strokeWidth: 2,
            lineCap: 'round',
            lineJoin: 'round'
          });

        var arcUp = new Konva.Arc({
            x: coor[0],
            y: coor[1]-20,
            innerRadius: 9,
            outerRadius: 10,
            angle: 180,
            fill: 'black'
          });

          var lineDown = new Konva.Line({
            points: [coor[0], coor[1]-40 , coor[0] , coor[1]-20],
            stroke: 'black',
            strokeWidth: 2,
            lineCap: 'round',
            lineJoin: 'round'
          });

        var arcDown = new Konva.Arc({
            x: coor[0],
            y: coor[1]+20,
            innerRadius: 9,
            outerRadius: 10,
            angle: 180,
            rotation:180,
            fill: 'black'
          });

        sautmouton.add(lineUp).add(arcUp).add(lineDown).add(arcDown);
        this._SavedZepsList.add(sautmouton);
        sautmouton.moveToTop();
        sautmouton.draw();
    }

    public showZep(numeroZep:string): any {
        let id = this.zepsList.filter(a=>a.Numero == numeroZep)[0].Id
        this.zepService.getZepById(id).subscribe(
          data => {
            this.modalRef = this.modalService.show(FicheZepComponent, {class:"modal-lg"});
            this.modalRef.content.zep = data;
            this.modalRef.content.PopulateListes();
          },
          err => console.log(err)
        )

    }
    public showDFV(numeroZep:string): any {
      if (this.listDFV
        .some(a=>a.Zep.Numero == numeroZep
              || a.Zep.CompositionGroupeZepList.some(b=>b.ZepEnfant.Numero == numeroZep)))
      {
        let Id = this.listDFV
          .filter(a=>a.Zep.Numero == numeroZep
                || a.Zep.CompositionGroupeZepList.some(b=>b.ZepEnfant.Numero == numeroZep)
              )[0].Id
        this.dfvService.getDFVById(Id).subscribe(
          data => {

            this.modalRef = this.modalService.show(FicheDfvComponent, {class:"modal-lg"});
            this.modalRef.content.dfv = data;
            this.modalRef.content.populateListe();
          },
          err => console.log(err)
        )
      }
      else{
        this.showZep(numeroZep);
      }


  }





}
