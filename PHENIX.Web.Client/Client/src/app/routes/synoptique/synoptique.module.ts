import { NgModule } from '@angular/core';
import { SynoptiqueComponent } from './synoptique.component';
import { Routes, RouterModule } from '@angular/router';
import {LinkZepComponent} from './modals/link-zep/link-zep.component';
import { FormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material/tabs';
import { CommonModule } from '@angular/common';
import { LoginComponent } from '../login/login.component';
import { SharedModule } from '../../shared/shared.module';
import { FicheZepComponent } from '../fiche-zep/fiche-zep.component';

const routes: Routes = [

    { path: 'creation', component: SynoptiqueComponent, data : { isOperationnel : false } },
    { path: 'operationnel', component: SynoptiqueComponent, data : { isOperationnel : true } }
];


@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
    CommonModule,
    SharedModule,

  ],
  declarations: [

    SynoptiqueComponent,
    LinkZepComponent,
  ],
  exports: [
    RouterModule,

      ],
  entryComponents: [LinkZepComponent]
})
export class SynoptiqueModule {}
