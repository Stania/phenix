import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'ouiNonPipe'
})

export class OuiNonPipe implements PipeTransform {
    transform(value) {
        return value ? 'Oui' : 'Non';
    }
}


/* version Stania
export class OuiNonPipe implements PipeTransform {
    transform(value: boolean): string {
        return value == true ? 'Oui' : 'Non';
    }
}
*/



