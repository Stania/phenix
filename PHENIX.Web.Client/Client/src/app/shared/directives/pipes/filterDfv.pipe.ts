import { Pipe, PipeTransform  } from '@angular/core';
import { DFV } from '../../models/DFV/dfv.models';


@Pipe({
  name: 'filterDfv',
  pure: false
})
export class FilterDfvPipe implements PipeTransform {

  /**
  * Support a function or a value or the shorthand ['key', value] like the lodash shorthand.
  */
  transform (input: Array<DFV>, fn: any): any {

    if(fn == "" || fn == null)
    return input;
    if (fn) {
      return input.filter((item: DFV) =>
      (item.Numero != null &&  item.Numero.match(fn))  ||
      (item.OperationNumero != null && item.OperationNumero.match(fn))  ||
      (item.ContratTravauxNumero != null &&item.ContratTravauxNumero.match(fn))  ||
      (item.HeureTheoriqueDebut != null &&item.HeureTheoriqueDebut.match(fn) ) ||
      (item.HeureTheoriqueFin != null && item.HeureTheoriqueFin.match(fn))  ||
      (item.Zep != null && item.Zep.Numero.match(fn)) ||
      (item.StatusZep != null && item.StatusZep.Statut.match(fn))

      );
    }
    else {
      return input;
    }
  }
}
