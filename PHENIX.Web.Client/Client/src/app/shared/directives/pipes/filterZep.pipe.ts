import { Pipe, PipeTransform  } from '@angular/core';
import { BibliothequeZepVM } from '../../models/ViewModels/BibliothequeZepVM';

@Pipe({
  name: 'filterZep',
  pure: false
})
export class FilterZepPipe implements PipeTransform {

  /**
  * Support a function or a value or the shorthand ['key', value] like the lodash shorthand.
  */
  transform (input: Array<BibliothequeZepVM>, fn: any): any {

    if(fn == "" || fn == null)
    return input;
    if (fn) {
      return input.filter((item: BibliothequeZepVM) =>
      (item.Zep != null &&  item.Zep.Numero.match(fn) ) ||
      (item.Zep.PosteDemandeDFV != null && item.Zep.PosteDemandeDFV.Nom.match(fn))  ||
      (item.DateDebutApplicabilite != null && item.DateDebutApplicabilite.toString().match(fn))  ||
      (item.DateFinApplicabilite != null && item.DateFinApplicabilite.toString().match(fn) )
      );
    }
    else {
      return input;
    }
  }
}
