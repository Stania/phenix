import { Pipe, PipeTransform  } from '@angular/core';
import { DFV } from '../../models/DFV/dfv.models';
import { Constantes_Phenix } from '../../models/Commun/constantes.model';

@Pipe({
  name: 'orderByStatut',
  pure:false
})
export class OrderByStatutPipe implements PipeTransform {
  transform(items: any[]): any[] {
    let filtered = [];
    items.forEach( (item :DFV) => {
      filtered.push(item);
    });
    filtered.sort( (a : DFV, b : DFV) => {

     if(a.StatusZep != null)
      return (this.CustomOrder(a.StatusZep.Statut,a.IsPrevisonelle) > this.CustomOrder(b.StatusZep.Statut,a.IsPrevisonelle) ? 1 : -1);
    });

    return filtered;

  }

  CustomOrder(statut : string , isPrev : boolean)
  {
    switch(statut) {
    case Constantes_Phenix.ZepStatuts.EnCreation:
        if(isPrev === true)
          return 1;
        else
          return 2;
        case Constantes_Phenix.ZepStatuts.EnAccord:
        return 3;
        case Constantes_Phenix.ZepStatuts.Accordee:
        return 4;
        case Constantes_Phenix.ZepStatuts.LeveeMesure:
        return 5;
        case Constantes_Phenix.ZepStatuts.Restituee:
        return 6;
        case Constantes_Phenix.ZepStatuts.Refusee:
        return 7;
        case Constantes_Phenix.ZepStatuts.Annulee:
        return 8;
}}
}
