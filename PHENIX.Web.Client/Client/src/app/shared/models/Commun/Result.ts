export class Result<T> 
{
  Value: T;
  Status: ResultStatus;
  SuccessMessage: string;
  ErrorMessage: string;
}

export enum ResultStatus {
  Success = 1,
  Error = 2,
  Warning = 3
}
