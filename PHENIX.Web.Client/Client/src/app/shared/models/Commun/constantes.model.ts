export namespace Constantes_Phenix {
  export class ObjetGraphique{
    static readonly TypesObjets =
    {
        Ligne : "Ligne",
        Extremite : "Extremite",
        SautDeMouton : "SautDeMouton",
        DashedLigne : "DashedLigne",
    };
    static readonly colors = ["red", "orange", "yellow", "green", "blue", "cyan", "purple"];
    static readonly MAX = 120;
    static readonly MIN = 1;
  }


  export class Profils {
    public static Administrateur: string = "Administrateur";
    public static AgentCirculation: string = "Agent Circulation";
    public static Aiguilleur: string = "Aiguilleur";
    public static CCL: string = "CCL";
    public static CelluleTravaux: string = "Cellule Travaux";
    public static DPx: string = "DPx";
    public static PoleTravaux: string = "Pôle Travaux";
  }
  export class MesureProtectionTypes {
    public static ProtectionEtVerification: string = "Conditions de protection et vérifications des postes";
    public static ItinérairesAdetruirePRCI: string = "Itinéraires à détruire(PRCI)";
    public static ItinérairesAdetruireEtAMunirDunDA: string = "Itinéraires à détruire et à munir de DA";
    public static OrganeDeCommandeAplacerEtAmunirDeDA: string = "Organe de commande à placer dans la position indiquée et munis d'un DA";
    public static DialogueDeProtection: string = "Dialogue de protection";
    public static AutorisationADetruire: string = "Autorisation à détruire";
    public static AiguilleImmobiliseeDansPositionIndiquee: string = "Aiguille immobilisée dans la position indiquée";
    public static JalonDarretAplacerSurLeTerrain: string = "Jalon d'arrêt à placer sur le terrain";
    public static ConditionsParticuliereVerificationLiberationZep = "Conditions particulières de vérification de libération de la ZEP";
  }
  export class ParticulariteTypes {
    public static ApplicableDansCondition: string = "Applicable dans les conditions(Poste ouvert / Poste fermé)";
    public static DFVResititueeOccupee: string = "DFV restituée occupée";
    public static TTXStationne: string = "TTX Stationné";
    public static AutresDispositionsParticulieresTTX: string = "Autre dispositions particulières TTX";
    public static AiguillesAPositionObligee: string = "Aiguilles à position obligée";
    public static AiguillesADisposerDansPositionPrevue: string = "Aiguilles à disposer dans la position prévue avant accord";
    public static DispositionsParticulieresParExploitation: string = "Dispositions particulières par l'Exploitation";
    public static OutilDeBouclageParLEquipement: string = "Outil de bouclage par l'Equipement";
    public static DispositionsParticulieresParLEquipement: string = "Dispositions particulières par l'Equipement";
    public static AutresMesuresEventiellesParLEquipement: string = "Autres mesures éventielles par l'Equipement";
  }

  export class Postes {
    public static PosteList: Array<string> = new Array<string>(
      "Mantes Gare",
      "Mantes PRCI",
      "Poste 1",
      "Poste 2",
      "Poste 11",
      "EP Port de Paris",
      "EP Port de Limay",
      "Vernon",
      "Les Mureaux",
      "Plaisir",
      "Conflans",
      "Meulan",
      "Gargenville",
      "Evreux",
      "RPTx"
    );
  }
  export class TypeTTX {
    public static ListAllType: Array<string> = new Array<string>(
      "Autre Train Travaux",
      "Bigrue",
      "Bourreuse 1er Niveau (BML 1N)",
      "Bourreuse 2ème Niveau (BML 2N)",
      "Bourreuse 3ème Niveau (BML 3N)",
      "Carotteuse",
      "Combi",
      "Dérouleur CAT",
      "Draisine",
      "Draisine + Remorques",
      "Draisine + Wagons",
      "Draisine CAT : ESC",
      "Engin Diesel + Wagons",
      "Engin Electrique + Wagons",
      "Engin Maintenance Cat. (EMC)",
      "Engin Multif. Maint. Voie -EMV",
      "Locotracteur + Wagons",
      "Lorry Automoteur",
      "Régaleuse",
      "Stabilisateur",
      "Suite MRT-Méca. Remplact Trav.",
      "Train Auto-Lavant",
      "Train Désherbeur",
      "Train Meuleur",
      "Train Travaux : Ballast",
      "Train Travaux Entreprise",
      "Train Travaux Terre",
      "TTx : Appro./Ramassage",
      "TTx : Autres engins spéciaux",
      "TTx : Curage/Assainissement",
      "TTx : Déchargt/Rechargt LRS",
      "TTx : Finitions",
      "TTx : Pose/Dépose",
      "TTx : Surveillance",
      "TTx CAT : Plateformes (APMC,.)",
      "TTx Coupe",
      "TTx OA : Plateforme (PF)",
      "Véhicule Rail/Route CAT :VRRSC",
      "Wagons d'Inspection Tunnel-WIT"
    );

    public static AutreTrainTravaux: string = "Autre Train Travaux";
    public static Bigrue: string = "Bigrue";
    public static Bourreuse1erNiveau: string = "Bourreuse 1er Niveau (BML 1N)";
    public static Bourreuse2erNiveau: string = "Bourreuse 2ème Niveau (BML 2N)";
    public static Bourreuse3erNiveau: string = "Bourreuse 3ème Niveau (BML 3N)";
    public static Carotteuse: string = "Carotteuse";
    public static Combi: string = "Combi";
    public static DerouleurCAT: string = "Dérouleur CAT";
    public static Draisine: string = "Draisine";
    public static DraisineRemorques: string = "Draisine + Remorques";
    public static DraisineWagons: string = "Draisine + Wagons";
    public static DraisineCATESC: string = "Draisine CAT : ESC";
    public static EnginDieselWagons: string = "Engin Diesel + Wagons";
    public static EnginElectriqueWagons: string = "Engin Electrique + Wagons";
    public static EnginMaintenanceCat: string = "Engin Maintenance Cat. (EMC)";
    public static EnginMultiMaintVoie: string = "Engin Multif. Maint. Voie -EMV";
    public static LocotracteurWagons: string = "Locotracteur + Wagons";
    public static LorryAutomoteur: string = "Lorry Automoteur";
    public static Regaleuse: string = "Régaleuse";
    public static Stabilisateur: string = "Stabilisateur";
    public static SuiteMRTMecaRemplactTrav: string = "Suite MRT-Méca. Remplact Trav.";
    public static TrainAutoLavant: string = "Train Auto-Lavant";
    public static TrainDesherbeur: string = "Train Désherbeur";
    public static TrainMeuleur: string = "Train Meuleur";
    public static TrainTravauxBallast: string = "Train Travaux : Ballast";
    public static TrainTravauxEntreprise: string = "Train Travaux Entreprise";
    public static TrainTravauxTerre: string = "Train Travaux Terre";
    public static TTxApproRamassage: string = "TTx : Appro./Ramassage";
    public static TTxAutresEnginsspéciaux: string = "TTx : Autres engins spéciaux";
    public static TTxCurageAssainissement: string = "TTx : Curage/Assainissement";
    public static TTxDéchargtRechargtLRS: string = "TTx : Déchargt/Rechargt LRS";
    public static TTxFinitions: string = "TTx : Finitions";
    public static TTxPoseDepose: string = "TTx : Pose/Dépose";
    public static TTxSurveillance: string = "TTx : Surveillance";
    public static TTxCATPlateformesAPMC: string = "TTx CAT : Plateformes (APMC,.)";
    public static TTxCoupe: string = "TTx Coupe";
    public static TTxOAPlateformePF: string = "TTx OA : Plateforme (PF)";
    public static VehiculeRailRouteCATVRRSC: string = "Véhicule Rail/Route CAT :VRRSC";
    public static WagonsInspectionTunnelWIT: string = "Wagons d'Inspection Tunnel-WIT";
  }
  export class ZepTypes {
    public static TypeL: string = "Type L";
    public static TypeG: string = "Type G";
    public static TypeLG: string = "Type L + G";
  }
  export class PointRemarquableTypes {
    public static PointEngagement: string = "Point d'engagement";
    public static PointDegagement: string = "Point de dégagement";
    public static SignauxIntermediaires: string = "Signaux intermédiaires";
  }
  export class ManoeuvreTypes {
    public static Engagement: string = "Engagement";
    public static Degagement: string = "Dégagement";
    public static Franchissement: string = "Franchissement";
  }
  export class OccupationTypes {
    public static OccupeeALAccord: string = "ZEP occupée par TTx à l'accord de la DFV";
    public static OccupeeALaRestitution: string = "ZEP occupée par TTx à la restitution";
  }
  export class ZepStatuts {
    public static EnCreation: string = "En création";
    public static EnAccord: string = "En attente d'accord";
    public static Accordee: string = "Accordée";
    public static Restituee: string = "Restituée";
    public static Annulee: string = "Annulée";
    public static Refusee: string = "Refusée";
    public static LeveeMesure : string = "Levee de mesure à faire";

  }
  export class Secteurs {
    public static Mantes: string = "Mantes";
  }
  export class DepecheType {
    public static Engagement: string = "Engagement";
    public static Degagement: string = "Degagement";
    public static AutreDepeche : string = "Autre Depeche";
    public static Notification: string = "Notification";
    public static Restitution: string = "Restitution";


    public static RestitutionEngagement: string = "Réstitution engagement";
    public static RestitutionDegagement: string = "Réstitution dégagement";

    public static MesureProtection: string = "Mesure de protection";
    public static VerificationZepLibre_Ligne: string = "Verification Zep Libre Ligne";
    public static VerificationZepLibre_IPCS: string = "Verification Zep Libre IPCS";
    public static AvisRestitutionDFV: string = "Avis de restitution de la DFV";
    public static LeveeDesMesures: string = "Levee des mesures";
  }
  export class ProtectionVerificationPoste {
    public static Verification: string = "Vérification";
    public static Protection: string = "Protection";
    public static Verification_Protection: string = "Vérification et Protection";
  }
}
