import { EntityBase } from "../Commun/entityBase.model";
import {
  Aiguille,
  Poste,
  PointRemarquable_Zep,
  Zep
} from "../FicheZep/zep.models";

export class Aiguille_DFV extends EntityBase {
  public AiguilleId: number;
  public Aiguille: Aiguille;
  public DFVId: number;
  public DFV: DFV;
  public Commentaire: string;
}

export class Depeche extends EntityBase {
  public Depeche() {}
  public NumeroDonne: string;
  public NumeroRecu: string;
  public DateDepeche: Date;
  public DepecheTypeId: number;
  public DepecheType: DepecheType;
  public VerificationZepLibre_IPCS: VerificationZepLibre_IPCS;
  public Manoeuvre: Manoeuvre;
  public VerificationZepLibre_LigneList: Array<VerificationZepLibre_Ligne>;
  public MesureProtectionPrise_DepecheList: Array<
    MesureProtectionPrise_Depeche
  >;
  public AutreDepecheList: Array<AutreDepeche>;
}

export class DepecheType extends EntityBase {
  public Nom: string;
  public DepecheList: Array<Depeche>;
}

export class AutreDepeche extends EntityBase {
  public AutreDepecheTypeId: number;
  public AutreDepecheType: AutreDepecheType;
  public DepecheId: number;
  public Depeche: Depeche;
  public ManoeuvreId: number | null;
  public Manoeuvre: Manoeuvre;
  public DFVId: number | null;
  public DFV: DFV;
  public AncienneValeur: string;
  public NouvelleValeur: string;
}

export class AutreDepecheType extends EntityBase {
  public Nom: string;
  public Propriete: string;
  public AutreDepecheList: Array<AutreDepeche>;
}

export class DFV extends EntityBase {
  public Numero: string;
  public StatusZep: StatutZep;
  public StatusZepId: number;
  public DateCreation: Date | null;
  public NumeroTelephone: string;
  public RPTX: string;
  public AvecVerificationDeLiberation: boolean;
  public DateDebutApplicabilite: Date;
  public DateFinApplicabilite: Date;
  public IsPrevisonelle: boolean | null;
  public DerriereTrainOuvrant: boolean | null;
  public DerriereTrainOuvrantNumero: string;
  public DerriereTTXDeclencheur: boolean | null;
  public DerriereTTXDeclencheurNumero: string;
  public SansTTX: boolean | null;
  public OperationNumero: string;
  public AHTNumero: string;
  public Avis: string;
  public AvisNumero: string;
  public ContratTravauxNumero: string;
  public CCTTNumero: string;
  public DateApplicabilite: Date | null;
  public RecuParDepeche: boolean | null;
  public DepecheReceptionNumero: string;
  public SignatureRPTX: boolean | null;
  public ZepId: number;
  public Zep: Zep;
  public ResponsableDFVId: number;
  public ResponsableDFV: Poste;
  public ObservationControles: boolean | null;
  public ObservationDirecteVoie: boolean | null;
  public ObservationParAutreMoyen: boolean | null;
  public ObservationParAutreMoyenDescription: string;
  public ObservationParEchangeDepeches: boolean | null;
  public VerificationZepLibre_IPCS: VerificationZepLibre_IPCS;
  public VerificationZepLibre_Ligne: VerificationZepLibre_Ligne;
  public MesureProtectionPiseParAC: boolean | null;
  public DateMesureProtectionPiseParAC: Date | null;
  public LeveeMesuresParAC: boolean | null;
  public DateLeveeMesuresParAC: Date | null;
  public DateDebutAccord: Date | null;
  public DateFinAccord: Date | null;
  public AccordeParEcrit: boolean | null;
  public TransmisParDepecheNumero: string;
  public ManoeuvreList: Array<Manoeuvre>;
  public OccupationList: Array<Occupation>;
  public MesureProtectionPriseList: Array<MesureProtectionPrise>;
  public Aiguille_DFVList: Array<Aiguille_DFV>;
  public MaterielRoulantList: Array<MaterielRoulant>;
  public DateRestitution: Date | null;
  public IsResitutionParDepeche: boolean | null;
  public NumeroDepecheRecuRestitution: number | null;
  public HeureTheoriqueDebut : string | null;
  public HeureTheoriqueFin : string | null;

}

export class Manoeuvre extends EntityBase {
  public TTXNumero: string;
  public TTXNature: string;
  public TTXVoie: string;
  public TTXPointInitial: string;
  public Direction: string;
  public Verbale: boolean | null;
  public ParEcrit: boolean | null;
  public ParDepeche: boolean | null;
  public FranchissementEffectue: boolean | null;
  public SignatureRptx: boolean | null;
  public DateDemandeDegagement: Date | null;
  public PointTTX: string;
  public ManoeuvreTypeId: number;
  public Commentaire: string;
  public ManoeuvreType: ManoeuvreType;
  public PointRemarquable_ZepId: number | null;
  public PointRemarquable_Zep: PointRemarquable_Zep;
  public DFVId: number;
  public DFV: DFV;
  public AutreDepecheList: Array<AutreDepeche>;
  public Manoeuvre_DepecheList: Array<Manoeuvre_Depeche>;
}

export class ManoeuvreType extends EntityBase {
  public Nom: string;
  public ManoeuvreList: Array<Manoeuvre>;
}

export class Manoeuvre_Depeche extends EntityBase {
  public Depeche: Depeche;
  public DepecheId: number;
  public ManoeuvreId: number;
  public Manoeuvre: Manoeuvre;
}
export class MaterielRoulant extends EntityBase {
  public TTXNumero: string;
  public TTXLieuStationnement: string;
  public DFVId: number;
  public DFV: DFV;
}

export class MesureProtectionPrise extends EntityBase {
  public PriseDeMesure: boolean | null;
  public DateDePrise: Date | null;
  public VerificationLiberationVoieprevue: boolean | null;
  public PosteDemandeId: number;
  public PosteDemande: Poste;
  public DFVId: number;
  public DFV: DFV;
  public MesureProtectionPrise_DepecheList: Array<
    MesureProtectionPrise_Depeche
  >;
}

export class MesureProtectionPrise_Depeche extends EntityBase {
  public MesureProtectionPriseId: number;
  public DepecheId: number;
  public MesureProtectionPrise: MesureProtectionPrise;
  public Depeche: Depeche;
}

export class Occupation extends EntityBase {
  public TTXNumero: string;
  public TTXNature: string;
  public TTXPosition: string;
  public Commentaire: string;
  public OccupationTypeId: number;
  public OccupationType: OccupationType;
  public DFVId: number;
  public DFV: DFV;
}

export class OccupationType extends EntityBase {
  public Nom: string;
  public OccupationList: Array<Occupation>;
}

export class StatutZep extends EntityBase {
  public Statut: string;
  public DFVList: Array<DFV>;
}

export class VerificationZepLibre_IPCS extends EntityBase {
  public HeureDemande: Date | null;
  public PosteDemandeId: number;
  public PosteDemande: Poste;
  public DFVId: number;
  public DFV: DFV;
  public DepecheId: number;
  public Depeche: Depeche;
}

export class VerificationZepLibre_Ligne extends EntityBase {
  public Nom: string;
  public HeureArrivee: Date;
  public NumeroTrainRecu: string;
  public OrigineTrain: string;
  public DestinationTrain: string;
  public PosteDemandeId: number;
  public PosteDemande: Poste;
  public DFVId: number;
  public DFV: DFV;
  public DepecheId: number | null;
  public Depeche: Depeche;
  public Depeche2Id: number | null;
  public Depeche2: Depeche;
}
