import { EntityBase } from "../Commun/entityBase.model";
import { Poste } from "../FicheZep/zep.models";

export class Profil extends EntityBase {
  public Nom: string;
  public UserProfilList: Array<UserProfil>;
  public ProfilPosteList: Array<ProfilPoste>;
}

export class Secteur extends EntityBase {
  public Nom: string;
  public PosteList: Array<Poste>;
  public UserProfilList: Array<UserProfil>;
}

export class ProfilPoste extends EntityBase {
  public ProfilId: number;
  public Profil: Profil;
  public PosteId: number;
  public Poste: Poste;
}

export class UserProfil extends EntityBase {
  public ProfilId: number;
  public Profil: Profil;
  public PosteId: number;
  public Poste: Poste;
  public SecteurId: number;
  public Secteur: Secteur;
  public UtilisateurId: number;
  public Utilisateur: Utilisateur;
}

export class Utilisateur extends EntityBase {
  public Nom: string;
  public Login: string;
  public UserProfilList: Array<UserProfil>;
}
