import { Utilisateur, Profil, Secteur } from "../Profils/profil.models";
import { Poste } from "../FicheZep/zep.models";



export class AddUtilisateurVM {
  Utilisateur: Utilisateur;
  Password: string;
  Profil: Profil;
  Poste: Poste;
  Secteur: Secteur;
  constructor() {
    this.Utilisateur = new Utilisateur();
    this.Profil = new Profil();
    this.Poste = new Poste();
    this.Secteur = new Secteur();
  }
}
