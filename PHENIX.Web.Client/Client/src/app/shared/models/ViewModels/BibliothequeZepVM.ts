import { Zep } from "../FicheZep/zep.models";


export class BibliothequeZepVM {
  Zep : Zep;
  AutorisationTTX : String;
  DateDebutApplicabilite : String;
  DateFinApplicabilite : String;
  ButtonZep : string;

  /**
   *
   */
  constructor(pZep : Zep) {
    this.Zep = pZep;
    this.AutorisationTTX = this.Zep.AutorisationTTX === true ? 'Autorisé' : 'Non autorisé'
    this.DateDebutApplicabilite = this.Zep.DateDebutApplicabilite.toLocaleDateString();
    this.DateFinApplicabilite = this.Zep.DateFinApplicabilite.toLocaleDateString();
  }
}
