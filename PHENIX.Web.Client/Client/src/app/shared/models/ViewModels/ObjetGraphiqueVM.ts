import { ObjetGraphique } from "../FicheZep/zep.models";


export class ObjetGraphiqueVM{
    public coordonnees : number[];
    public type : string;
    public color :string;

    public toObjetGraphique(): ObjetGraphique {
      var obj : ObjetGraphique ;
      obj = new  ObjetGraphique();

      obj.Coordonnees = this.coordonnees.join('##');
      console.log(this.coordonnees);
      console.log(obj.Coordonnees);
      obj.Type = this.type;
      obj.Color = this.color;
      return obj;
    }
    public static CreateFromObjetGraphique(obj : ObjetGraphique):ObjetGraphiqueVM {
      var ret :ObjetGraphiqueVM;
      ret = new ObjetGraphiqueVM();
      ret.coordonnees = obj.Coordonnees.split('##').map(a=>parseFloat(a));
      ret.type = obj.Type;
      ret.color = obj.Color;
      return ret;
    }



}







