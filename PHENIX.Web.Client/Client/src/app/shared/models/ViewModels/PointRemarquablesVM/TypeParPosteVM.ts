import { ValeurParPosteVM } from "./ValeurParPosteVM";

export class TypeParPosteVM {
   Type : string;
   ListPosteVm : Array<ValeurParPosteVM>;

   constructor() {
    this.ListPosteVm = new Array<ValeurParPosteVM>();

  }
}
