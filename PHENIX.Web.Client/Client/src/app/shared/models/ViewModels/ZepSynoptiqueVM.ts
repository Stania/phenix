import { Zep } from "../FicheZep/zep.models";
import { DFV } from "../DFV/dfv.models";


export class ZepSynoptiqueVM {
  Zep : Zep;
  IsTTXOnIt : Boolean;
  DFV : DFV;
  IsGroup : Boolean;
}
