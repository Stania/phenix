import { Injectable } from '@angular/core';
import { Http, Headers, Response,RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { UrlService } from '../../settings/url.service';
import { UtilisateurVM } from '../../models/ViewModels/UtilisateurVM';
import { LoginVM } from '../../models/ViewModels/LoginVM';
import { UserProfil } from '../../models/Profils/profil.models';
import { HttpClient } from '@angular/common/http';
import { Constantes_Phenix } from '../../models/Commun/constantes.model';

@Injectable()
export class AuthenticationService {

    private redirectUrl = '/choixprofil';
    private redirectUrlAfterChoix = '/dfvlist';
    private loginUrl = '/login';
    private clientId = '51f783b666a52ec79a70b268e9a256Gg';
    private currentUser: UtilisateurVM;
    private urlService: UrlService;
    constructor(private http: Http, private httpClient: HttpClient
    ) {
        // set token if saved in local storage
        this.currentUser = JSON.parse(localStorage.getItem('51f783b666a52ebibic79a70b268e9a256Gg')) as UtilisateurVM;
        this.urlService = new UrlService();
    }

    login(identifiants : LoginVM): Observable<boolean> {

         return this.http.post(this.urlService.getUrlApi() + this.urlService.getAuthenticationUrl(), identifiants)
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                const user = response.json() && response.json();
                if (user) {
                    // set token property
                    this.currentUser = user as UtilisateurVM;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.removeItem('51f783b666a52ebibic79a70b268e9a256Gg');
                    localStorage.setItem('51f783b666a52ebibic79a70b268e9a256Gg', JSON.stringify( this.currentUser ));

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return true;
                }
            });
    }

    choixProfil(choix: UserProfil): Observable<boolean> {
      return this.httpClient.post<UtilisateurVM>(this.urlService.getUrlApi() + this.urlService.getUrlChoixProfil(),choix)
          .map((response) => {
              // login successful if there's a jwt token in the response
              const user = response;
              if (user) {
                  // set token property
                  this.currentUser = user as UtilisateurVM;
                  console.log(this.currentUser);
                  // store username and jwt token in local storage to keep user logged in between page refreshes
                  localStorage.removeItem('51f783b666a52ebibic79a70b268e9a256Gg');
                  localStorage.setItem('51f783b666a52ebibic79a70b268e9a256Gg', JSON.stringify( this.currentUser ));

                  // return true to indicate successful login
                  return true;
              } else {
                  // return false to indicate failed login
                  return true;
              }
          });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.currentUser = null;
        localStorage.removeItem('51f783b666a52ebibic79a70b268e9a256Gg');
    }

    getRedirectUrl(): string {
        return this.redirectUrl;
    }

    getRedirectUrlAfterChoix(): string {
      if(this.currentUser.Profil == Constantes_Phenix.Profils.PoleTravaux)
      return "/previsiondfv";
      return this.redirectUrlAfterChoix;
    }

    setRedirectUrl(url: string): void {
        this.redirectUrl = url;
    }
    getLoginUrl(): string {
        return this.loginUrl;
    }

    getToken(): string {
        return this.currentUser != null ? this.currentUser.access_token : null;
    }

    isAuthenticated(): boolean {
        return this.currentUser === null ? false : true;
    }

    getLoggedUser(): UtilisateurVM{
        return this.currentUser;
    }
}
