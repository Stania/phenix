import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import {HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { UrlService } from '../../settings/url.service';

import { AddUtilisateurVM } from '../../models/ViewModels/AddUtilisateurVM';
import { UserProfil } from '../../models/Profils/profil.models';


@Injectable()
export class UtilisateurService {
    baseUrl: string = this.urlService.getUrlApi() + 'api/Identity';
    constructor(private urlService: UrlService, private http: HttpClient ) {

    }

    createUser(user :AddUtilisateurVM): Observable<AddUtilisateurVM> {
      const url =  this.baseUrl + '/CreateUserSite';
      return this.http.post<AddUtilisateurVM>(url,user);
    }

    private extractData(res: Response) {
      let body = res.json();
      return body || {};
    }

    getChoixProfilList(): Observable<Array<UserProfil>> {
      const url = this.baseUrl + "/GetChoixProfilList";
      return this.http.get<Array<UserProfil>>(url);
    }


}
