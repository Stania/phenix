import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }from '@angular/common/http';

@Injectable()
export class DrawService {

  constructor() {

  }

  dividCircle (nbPlages :number): any[] {
    const circleAngle = 360;
    let plages = [];
    if (nbPlages < 0 || nbPlages > 360) return [{debut : 0, fin : 360}] ;
    const pas = circleAngle / Math.trunc(nbPlages);
    for(var i= 0 ; i < 360; i = i + pas){
      var deb = i - (pas / 2);
      if (deb < 0) deb = 360 - (pas / 2);
      var f =  i + (pas / 2);
      if (f > 360) f = 0 + (pas / 2);
      plages.push({debut : deb , fin: f, center: i})
    }
    return plages;
  }
  angle(cx, cy, ex, ey) {
    var dy =  cy - ey;
    var dx = ex - cx;
    var theta = Math.atan2(dy, dx); // range (-PI, PI]
    theta *= 180 / Math.PI; // rads to degs, range (-180, 180]
    if (theta < 0) theta = 360 + theta; // range [0, 360)
    return theta;
  }
  distance(x1, y1, x2, y2){
    var a = x2 - x1;
    var b = y2 - y1;

    return Math.sqrt( a*a + b*b )
  }

  isOnLine(x1, y1, x2, y2, X, Y, marge){
    var distanceSeg = this.distance(x1, y1, x2, y2);
    var distance1 = this.distance(x1, y1, X, Y);
    var distance2 = this.distance(x2, y2, X, Y);
    return (distanceSeg + marge <= distance1 + distance2) && ((distanceSeg - marge >= distance1 + distance2))
  }


  createCoord(x, y, angle, distance) {
    var result = {x : null, y : null};
    if (angle > 180 ) angle = angle - 360;
    result.x = Math.round(Math.cos(angle * Math.PI / 180) * distance + x);
    result.y = Math.round(Math.sin(angle * Math.PI / -180) * distance + y);

    return result;
  }
  projectionPerpendiculaire(x1, y1, x2, y2, x0, y0) {
    var isValid = false;
    var r = {X : 0, Y : 0};
    if (x1 == x2 && y1 == y2) x1 = 0.00001;

    var U = ((x0 - x1) * (x2 - x1)) + ((y0 - y1) * (y2 - y1));

    var Udenom = Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2);

    U /= Udenom;

    r.X = x1 + (U * (x2 - x1));
    r.Y = y1 + (U * (y2 - y1));

    var minx, maxx, miny, maxy;

    minx = Math.min(x1, x2);
    maxx = Math.max(x1, x2);

    miny = Math.min(y1, y2);
    maxy = Math.max(y1, y2);

    isValid = (r.X >= minx && r.X <= maxx) && (r.Y >= miny && r.Y <= maxy);

    return isValid ? r : null;

  }

  getRandomColor() {
    var r = Math.round(Math.random() * 255);
    var g = Math.round(Math.random() * 255);
    var b = Math.round(Math.random() * 255);
    var color = [r, g, b];
    return color;
}

getLineCenter(x1,y1,x2,y2) {
  return [((x1+x2)/2), ((y1+y2)/2)];
}





}
