import { Injectable } from '@angular/core';
import { UrlService } from '../../settings/url.service';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { DepecheType, Depeche, Manoeuvre_Depeche } from '../../models/DFV/dfv.models';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DepechesService {
  urlApi :string;

  constructor(urlService : UrlService, private http : HttpClient) {
      this.urlApi = urlService.getUrlApi() + "api/depeche/";
  }

  getAllDepecheType(): Observable<DepecheType[]> {
    const url = this.urlApi + "GetAllDepecheType";
    return this.http.get<DepecheType[]>(url);
  }

  AddDepeche(depeche: Depeche, mesureId: number): Observable<Depeche> {
    let url = this.urlApi + "AddDepecheMesure";
    return this.http.post<Depeche>(url,{"Depeche" : depeche, "MesureProtectionPriseId" : mesureId });
  }

  AddDepecheManoeuvre(depeche: Depeche, manoeuvreId: number): Observable<Depeche> {
    let url = this.urlApi + "AddDepecheNotification";
    return this.http.post<Depeche>(url,{"Depeche" : depeche, "ManoeuvreId" : manoeuvreId });
  }

  getListManoeuvreDepecheByIdDfv(id: Number) :Observable<Array<Manoeuvre_Depeche>> {
    let url = this.urlApi +"GetListManoeuvreDepecheByIdDfv/" + id;
    return this.http.post<any>(url,null,{
      headers: new HttpHeaders({'Content-Type':'application/json'})
   })
  }
}
