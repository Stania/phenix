import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { DFV } from '../../models/DFV/dfv.models';
import { UrlService } from '../../settings/url.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DfvListService {
	urlApi :string;
	
	constructor(urlService : UrlService, private http : HttpClient) {
		this.urlApi = urlService.getUrlApi() + "api/dfv";
	}

	getDfvList(): Observable<DFV[]> {
		let url = this.urlApi + "/list";
		return this.http.get<DFV[]>(url);
	}
}