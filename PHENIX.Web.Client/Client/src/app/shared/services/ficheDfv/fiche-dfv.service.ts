import { Injectable } from '@angular/core';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UrlService } from '../../settings/url.service';
import { DFV, Occupation, MesureProtectionPrise, Depeche, VerificationZepLibre_Ligne } from '../../models/DFV/dfv.models';
import { promise } from 'protractor';

@Injectable()
export class FicheDfvService {

  urlApi :string;

  constructor(urlService : UrlService, private http : HttpClient) {
      this.urlApi = urlService.getUrlApi() + "api/dfv/";
  }

  public getDFVById(id: Number) : Observable<DFV>{
    let url = this.urlApi +"id/" + id;
      return this.http.post<DFV>(url,null,{
        headers: new HttpHeaders({'Content-Type':'application/json'})
     })
  }
  public checkOrUpdateDfvStatutLeveeMesure(id: Number):Observable<DFV>{
    let url = this.urlApi + "checkOrUpdateDfvStatutLeveeMesure/"+id;
    return this.http.post<DFV>(url,null,{
      headers: new HttpHeaders({'Content-Type':'application/json'})
   })
  }

  public createDfv(dfv : DFV):Observable<DFV>  {
    const url = this.urlApi + "create";
    return this.http.post<DFV>(url,dfv,{
      headers: new HttpHeaders({'Content-Type':'application/json'})
   });
  }
  public createDfvPrev(dfv : DFV):Observable<DFV>  {
    const url = this.urlApi + "createPrev";
    return this.http.post<DFV>(url,dfv,{
      headers: new HttpHeaders({'Content-Type':'application/json'})
   });
  }

  public getDFVByNumero(num: string) : Observable<DFV>{
    let url = this.urlApi + "numero/" +  num;
    return this.http.get<DFV>(url);
  }

  public getAllDFV():Observable<DFV[]>{
    let url = this.urlApi + "list";
    return this.http.get<DFV[]>(url);
  }
  public getAllDFVPrevisionnelle():Observable<DFV[]>{
    let url = this.urlApi + "listPrevisionnelle";
    return this.http.get<DFV[]>(url);
  }

  public validerDemande(dfv:DFV): Observable<DFV>{
    let url = this.urlApi + "validerDemande";
    return this.http.post<DFV>(url, dfv);
  }
  public accordeDFV(dfv:DFV):Observable<DFV>{
    let url = this.urlApi + "accordeDFV";
    return this.http.post<DFV>(url, dfv);
  }
  public UpdateDfv(dfv:DFV): Observable<DFV>{
    let url = this.urlApi + "update";
    return this.http.post<DFV>(url, dfv);
  }

  public ValiderDemandeVerification(v : VerificationZepLibre_Ligne):Observable<VerificationZepLibre_Ligne>{
    let url = this.urlApi + "ValiderDemandeVerification";
    console.log(JSON.stringify(v));
    return this.http.post<VerificationZepLibre_Ligne>(url, v);
  }

  public ValiderConfirmationVerification(v : VerificationZepLibre_Ligne):Observable<VerificationZepLibre_Ligne>{
    let url = this.urlApi + "ValiderConfirmationVerification";
    return this.http.post<VerificationZepLibre_Ligne>(url, v);
  }

  public updateVerificationZepLibre(v:VerificationZepLibre_Ligne):Observable<VerificationZepLibre_Ligne>{
    let url = this.urlApi + "UpdateVerificationLiberationZep";
    return this.http.post<VerificationZepLibre_Ligne>(url, v);
  }

}
