import { Injectable } from '@angular/core';
import { UrlService } from '../../settings/url.service';
import { HttpClient } from '@angular/common/http';
import { MesureProtectionPrise } from '../../models/DFV/dfv.models';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MesuresService {
 
 
  urlApi :string;

  constructor(urlService : UrlService, private http : HttpClient) {
      this.urlApi = urlService.getUrlApi() + "api/mesures/";
  }

  public GetMesuresProtectionList():Observable<MesureProtectionPrise[]>{
    let url = this.urlApi + "";
    return null;
  }

  AddMesure(mesure: MesureProtectionPrise): Observable<MesureProtectionPrise> {
    let url = this.urlApi + "create";
    return  this.http.post<MesureProtectionPrise>(url,mesure);
  }

  UpdateMesure(mesure: MesureProtectionPrise): Observable<MesureProtectionPrise> {
    let url = this.urlApi + "update";
    return  this.http.post<MesureProtectionPrise>(url,mesure);
  }

  DeleteMesure(mesure: MesureProtectionPrise):Observable<number> {
    let url = this.urlApi + "delete";
    return this.http.post<number>(url,mesure);
  }

}
