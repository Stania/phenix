import { Injectable } from "@angular/core";
import { Headers, RequestOptions, Response, Request } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { UrlService } from "../../settings/url.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { PointRemarquable_Zep } from "../../models/FicheZep/zep.models";
import { Aiguille_DFV, DFV } from "../../models/DFV/dfv.models";

@Injectable()
export class RestitutionService {

    urlApi :string;

    constructor(urlService : UrlService, private http : HttpClient) {
      this.urlApi = urlService.getUrlApi() + "api/dfv/";
    }

    updateAiguilleDFV(pList : Array<Aiguille_DFV>): Observable<Array<Aiguille_DFV>> {
      let url = this.urlApi + "/updateAiguilleDFV";
      return this.http.post<Array<Aiguille_DFV>>(url, pList);
    }
    restituer(dfv : DFV): Observable<DFV> {
      let url = this.urlApi + "/restituer";
      return this.http.post<DFV>(url, dfv);
    }


}
