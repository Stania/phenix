import { Injectable } from "@angular/core";
import { Headers, RequestOptions, Response, Request, RequestMethod } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { UrlService } from "../../settings/url.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Zep } from "../../models/FicheZep/zep.models";
import { ContentType } from "@angular/http/src/enums";
import { Result } from "../../models/Commun/Result";

@Injectable()
export class ImportZepService {
    baseUrl: string = this.urlService.getUrlApi() + "api/ImportZep";

    constructor(private urlService: UrlService, private http: HttpClient) {
    }

    // **2e** méthode importZepFile :
    importZepFile(files: any): Observable<Result<number>> {
        const url = this.baseUrl + "/ImportZepFile";
        const formData = new FormData();
        formData.append(files[0].name, files[0]);
        return this.http.post<Result<number>>(url, formData);
    }

    private handleData(res: Response) {
        let data = res.json();
        alert('API succeeded');
        return data;
    }

    private handleError(error: Response | any) {
        return Observable.throw('API failed');
    }
}