import { Injectable } from "@angular/core";
import { Headers, RequestOptions, Response, Request } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { UrlService } from "../../settings/url.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Zep, ZepType, Poste } from "../../models/FicheZep/zep.models";
import { DFV } from "../../models/DFV/dfv.models";
import { ZepSynoptiqueVM } from "../../models/ViewModels/ZepSynoptiqueVM";

@Injectable()
export class ZepService {


    baseUrl: string = this.urlService.getUrlApi() + "api/Zep";

    constructor(private urlService: UrlService, private http: HttpClient) {
    }

      getAllZep(): Observable<Zep[]> {
        const url = this.baseUrl + "/GetAllZep";
        return this.http.get<Zep[]>(url);
      }

      getZepsAutorises(): Observable<Zep[]> {
        const url = this.baseUrl + "/GetZepsAutorises";
        return this.http.get<Zep[]>(url);
      }

      getZepById(id : number): Observable<Zep> {
        const url = this.baseUrl + "/GetZepById/" + id;
        return this.http.get<Zep>(url);
      }

      getNotDrawedZepList(): Observable<Array<Zep>> {
        const url = this.baseUrl + "/GetNotDrawedZepList";
        return this.http.get<Array<Zep>>(url);
      }
      getDrawedZepList(): Observable<Array<Zep>> {
        const url = this.baseUrl + "/GetDrawedZepList";
        return this.http.get<Array<Zep>>(url);
      }

      getEtatOpeationnel(): Observable<Array<ZepSynoptiqueVM>> {
        const url = this.baseUrl + "/GetEtatOpeationnel";
        return this.http.get<Array<ZepSynoptiqueVM>>(url);
      }

      linkDrawZep(zep : Zep): Observable<Zep> {
        const url = this.baseUrl + "/LinkDrawZep";
        return this.http.post<Zep>(url,zep,{
          headers: new HttpHeaders({'Content-Type':'application/json'})
       })
    }

    getAllZepTypes():Observable<Array<ZepType>> {
      const url = this.baseUrl + "/GetAllZepTypes";
      return this.http.get<Array<ZepType>>(url);
    };

    getAllPostes(): Observable<Poste[]> {
      const url = this.baseUrl + "/GetAllPostes";
      return this.http.get<Array<Poste>>(url);
    }
}
