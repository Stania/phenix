import { Injectable } from "@angular/core";
import { Headers, RequestOptions, Response, Request } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { UrlService } from "../../settings/url.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Manoeuvre, ManoeuvreType } from "../../models/DFV/dfv.models";
import { EffectueManoeuvreViewModel } from "../../models/ViewModels/EffectueManoeuvreViewModel.model";
import { AuthenticationService } from "../administration/authentication.service";
import { Constantes_Phenix } from "../../models/Commun/constantes.model";

@Injectable()
export class ManoeuvreService {
    baseUrl: string = this.urlService.getUrlApi() + "api/Manoeuvre/";

    constructor(private urlService: UrlService, private http: HttpClient, private authService :AuthenticationService) {
    }

    updateManoeuvre(manoeuvre : Manoeuvre):Observable<Manoeuvre> {
      const url = this.baseUrl + "update/";
      return this.http.post<Manoeuvre>(url,manoeuvre,{
        headers: new HttpHeaders({'Content-Type':'application/json'})
     });
    }

    createManoeuvre(manoeuvre : Manoeuvre):Observable<Manoeuvre>  {
      const url = this.baseUrl + "create/";
      return this.http.post<Manoeuvre>(url,manoeuvre,{
        headers: new HttpHeaders({'Content-Type':'application/json'})
     });
    }

    getAllManoeuvreType(): Observable<ManoeuvreType[]> {
      const url = this.baseUrl + "GetAllManoeuvreType";
      return this.http.get<ManoeuvreType[]>(url);
    }

    effectueManoeuvre(manoeuvre : EffectueManoeuvreViewModel):Observable<Manoeuvre>{
      const url = this.baseUrl + "EffectueManoeuvre/";
      return this.http.post<Manoeuvre>(url,manoeuvre,{
        headers: new HttpHeaders({'Content-Type':'application/json'})
     });
    }
    demandeDegagement(manoeuvre : Manoeuvre):Observable<Manoeuvre>{
      const url = this.baseUrl + "DemandeDegagement/";
      return this.http.post<Manoeuvre>(url,manoeuvre,{
        headers: new HttpHeaders({'Content-Type':'application/json'})
     });
    }
    public deleteManoeuvre(id: Number) :Observable<any> {
      let url = this.baseUrl +"DeleteManoeuvre/" + id;
      return this.http.post<any>(url,null,{
        headers: new HttpHeaders({'Content-Type':'application/json'})
     })
    }

    filtrerManoeuvre(typeManoeuvre:string,pManoeuvreList:Array<Manoeuvre>,isNotification = false,posteName : string = "") :Array<Manoeuvre>{
          let pListTypeManoeuvre = [Constantes_Phenix.ManoeuvreTypes.Engagement,Constantes_Phenix.ManoeuvreTypes.Franchissement,Constantes_Phenix.ManoeuvreTypes.Degagement]
          let typeManoeuvreObject = pListTypeManoeuvre.find(x => x == typeManoeuvre);
          if(!pListTypeManoeuvre || !typeManoeuvreObject)
            return new Array<Manoeuvre>();
          else{
            var manoeuvres = pManoeuvreList.filter(t => t.ManoeuvreType.Nom == typeManoeuvreObject);
            if(isNotification){
              //filtrer par poste=
              let profil = this.authService.getLoggedUser().Profil;
              if (profil === Constantes_Phenix.Profils.AgentCirculation)
                manoeuvres = manoeuvres.filter(t => t.PointRemarquable_Zep.PosteConcerne.Nom != posteName);
              else
                manoeuvres = manoeuvres.filter(t => t.PointRemarquable_Zep.PosteConcerne.Nom == posteName);

              return manoeuvres;
            }else{
              return manoeuvres;
            }
          }
    }
}
