import { Injectable } from "@angular/core";
import { Headers, RequestOptions, Response, Request } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { UrlService } from "../../settings/url.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Occupation, OccupationType } from "../../models/DFV/dfv.models";

@Injectable()
export class OccupationService {
    baseUrl: string = this.urlService.getUrlApi() + "api/Occupation/";

    constructor(private urlService: UrlService, private http: HttpClient) {
    }

    updateOccupation(occupation : Occupation):Observable<Occupation> {
      const url = this.baseUrl + "update/";
      return this.http.post<Occupation>(url,occupation,{
        headers: new HttpHeaders({'Content-Type':'application/json'})
     });
    }

    createOccupation(occupation : Occupation):Observable<Occupation>  {
      const url = this.baseUrl + "create/";
      return this.http.post<Occupation>(url,occupation,{
        headers: new HttpHeaders({'Content-Type':'application/json'})
     });
    }

    getAllOccupationType(): Observable<OccupationType[]> {
      const url = this.baseUrl + "GetAllOccupationType";
      return this.http.get<OccupationType[]>(url);
    }


    public deleteOccupation(id: Number) :Observable<any> {
      let url = this.baseUrl +"DeleteOccupation/" + id;
      return this.http.post<any>(url,null,{
        headers: new HttpHeaders({'Content-Type':'application/json'})
     })
    }
    filtrerOccupations(typeOccupation:string, pOccupationList : Array<Occupation>):Array<Occupation>{
      return pOccupationList.filter(t => t.OccupationType == null ? -1 : t.OccupationType.Nom == typeOccupation);
    }
}
