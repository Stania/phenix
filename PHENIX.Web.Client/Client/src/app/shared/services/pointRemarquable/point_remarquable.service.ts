import { Injectable } from "@angular/core";
import { Headers, RequestOptions, Response, Request } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { UrlService } from "../../settings/url.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { PointRemarquable_Zep } from "../../models/FicheZep/zep.models";

@Injectable()
export class PointRemarquableService {
    baseUrl: string = this.urlService.getUrlApi() + "api/PointRemarquable/";

    constructor(private urlService: UrlService, private http: HttpClient) {
    }

    GetListPointRemarquableByIdZep(idZep : number): Observable<PointRemarquable_Zep[]> {
      const url = this.baseUrl + "GetListPointRemarquableByIdZep/"+idZep;
      return this.http.get<PointRemarquable_Zep[]>(url);
    }

    getPointRemarquableById(idPr : number, pListPoint):PointRemarquable_Zep{
      if(idPr != null){
          return pListPoint.find(x=> x.Id == idPr);
      }
    }


}
