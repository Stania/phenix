import { Component, NgModule, OnInit, ViewContainerRef, ViewChild, OnChanges, SimpleChanges } from "@angular/core";
import { Field, FieldArrayType, FormlyFormBuilder, FieldWrapper } from "@ngx-formly/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormlyBootstrapModule, FormlyFieldInput } from "@ngx-formly/bootstrap";
import { FormsModule, ReactiveFormsModule, AbstractControl, FormControl, Validators, FormGroup } from "@angular/forms";
import { BsDatepickerConfig, tr } from "ngx-bootstrap";
import { ConfigOption } from "@ngx-formly/core";
import { FieldType } from "@ngx-formly/core";
import { FormlyFieldConfig } from "@ngx-formly/core";
import { ValidationService } from "./validators.service";
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { locale } from "moment";


// ERROR Wrapper
@Component({
  selector: 'formly-wrapper-validation-messages',
  template: `
    <ng-template #fieldComponent></ng-template>
    <div *ngIf="showError">
      <small class="text-danger invalid-feedback" [style.display]="'inline-block'" role="alert" [id]="validationId">
        <formly-validation-message [fieldForm]="formControl" [field]="field"></formly-validation-message>
      </small>
    </div>
  `,
})
export class FormlyWrapperValidationMessages extends FieldWrapper {
  @ViewChild('fieldComponent', {read: ViewContainerRef}) fieldComponent: ViewContainerRef;

  get validationId() {
    return this.field.id + '-message';
  }
}

//BUTTON
@Component({
  selector: "formly-field-button",
  template: `
  <label *ngIf="to.withLabel" class="form-control-label">{{ to.label }}</label>
  <button type="{{type}}" [ngClass]="inputClass" (click)="btnClick($event);"  ><i class="{{to.iconClass}}" ></i>{{ to.titre }}</button>
  `
})
export class FormlyFieldButton extends FieldType {
  btnClick(option: any): any {
    this.to.btnClick(option);
  }
  get type(): string {
    return this.to.type || 'button';
  }
  get withLabel(): boolean {
    return this.to.withLabel || true;
  }
  get inputClass(): string {
    return this.to.inputClass || "";
  }
  get titre(): string {
    return this.to.titre || "";
  }
  static createControl(model: any, field: FormlyFieldConfig): AbstractControl {
    return new FormGroup(
      field.validators ? field.validators.validation : undefined,
      field.asyncValidators ? field.asyncValidators.validation : undefined
    );
  }
}
//DATEPICKER
@Component({
  selector: "app-form-datepicker-type",
  template: `
  <div class="col-xs-12 col-12 input-group">
      <div class="input-group-addon input-sm">
        <i class="fa fa-calendar" style="margin-top: 2.5px;"></i>
      </div>
    <input type="text"
           class="form-control  input-sm"
           #dp="bsDatepicker"
           [bsConfig]="bsConfig"
           [minDate]="minDate"
           [maxDate]="maxDate"
           [formControl]="formControl"
           [formlyAttributes]="field"
           value="{{ bsValue | date: 'dd/MM/yyyy' }}"
           bsDatepicker >
  </div>

  `
})
export class DatepickerTypeComponent extends FormlyFieldInput
  implements OnInit {
  bsValue: Date = new Date();
  colorTheme = "theme-dark-blue";

  bsConfig: Partial<BsDatepickerConfig>;
  // Exemple ajout d'une propriété : {
  //   className: "col-md-6",
  //   key: "DateArrivee",
  //   type: "datepicker",
  //   templateOptions: {
  //     required: true,
  //     type: "text",
  //     label: "Date d'arrivée",
  //     minDate: new Date()
  //   }
  // }

  get minDate(): any {
    return this.to["minDate"] || "";
  }
  get maxDate(): any {
    return this.to["maxDate"] || "";
  }

  ngOnInit() {
    if (this.field.defaultValue != null && Date.parse(this.field.defaultValue) > 0){
      this.bsValue = new Date(this.field.defaultValue);
      this.model[this.field.key] =  new Date(this.field.defaultValue);
    }else{
        this.form.get(this.field.key).setValue(new Date());
        this.model[this.field.key] =  new Date();
    }
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.bsConfig.locale = "fr";
    this.bsConfig.dateInputFormat = "DD/MM/YYYY";
  }
}

//DATETIMEPICKER
@Component({
  selector: "app-form-datetimepicker-type",
  template: `
  <label class="row col-xs-12 col-12">{{ to.label }}</label>
  <div [class.error]="showError">
    <div class="row col-xs-8 col-8 ">
        <div class="input-group" >
            <div class="input-group-addon ">
                <i class="fa fa-calendar" style="margin-top: 2.5px;"></i>
            </div>
            <input style="padding:0.2em" type="text"
            class="form-control"
            #dp="bsDatepicker"
            [bsConfig]="bsConfig"
            [minDate]="minDate"
            [maxDate]="maxDate"
            [formControl]="formControl"
            [formlyAttributes]="field"
            [placement]="datePosition"
            value="{{ bsValue | date: 'dd/MM/yyyy' }}"
            [(ngModel)]="model[field.key]"
            bsDatepicker >
        </div>
    </div>
    <div *ngIf="to.type != 'dateonly'" class=" col-xs-4 col-4" style="text-align: -webkit-center;">
        <timepicker 
            [showSpinners]="false"  
            [formControl]="formControl"
            [formlyAttributes]="field"
            [(ngModel)]="model[field.key]" 
            [readonlyInput]="to.disabled" 
            [showMeridian]="false">
        </timepicker>
    </div>
    <br><br><br>
</div>

  `
})

export class DateTimePickerTypeComponent extends FieldType implements OnInit {
  bsValue: Date = new Date();
  colorTheme = "theme-dark-blue";
  bsConfig: Partial<BsDatepickerConfig>;

  get minDate(): any {
    return this.to["minDate"] || "";
  }

  get maxDate(): any {
    return this.to["maxDate"] || "";
  }

  get datePosition(): any {
    return this.to["datePosition"] || "bottom";
  }

  ngOnInit() {

    if (this.field.defaultValue != null && Date.parse(this.field.defaultValue) > 0){
      this.bsValue = new Date(this.field.defaultValue);
      this.model[this.field.key] =  new Date(this.field.defaultValue);
    }else{
      this.model[this.field.key] =  new Date();
        this.form.get(this.field.key)  == null ? ""  : this.form.get(this.field.key).setValue(new Date());
    }
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme,locale : "fr" ,dateInputFormat :"DD/MM/YYYY"});
  }
}





//MultiCheckBox
@Component({
  selector: 'formly-field-multicheckbox',
  template: `
    <div *ngFor="let option of to.options; let i = index;" class="checkbox">
      <label class="custom-control custom-checkbox">
        <input type="checkbox" (click)="checkBoxClick($event,option);"
          [value]="option.value"
          [id]="id + '_' + i"
          [formControl]="formControl.get(option.key)"
          [formlyAttributes]="field" class="custom-control-input">
        <span class="custom-control-label">{{ option.value }}</span>
        <span class="custom-control-indicator"></span>
      </label>
    </div>
  `,
})
export class FormlyFieldMultiCheckbox extends FieldType {
  checkBoxClick($event: any, option: any): any {
    this.to.checkClick(option, $event);
  }
  static createControl(model: any, field: FormlyFieldConfig): AbstractControl {
    if (!(field.templateOptions.options instanceof Observable)) {
      let controlGroupConfig = field.templateOptions.options.reduce((previous, option) => {
        previous[option.key] = new FormControl(model ? model[option.key] : undefined);
        return previous;
      }, {});

      return new FormGroup(
        controlGroupConfig,
        field.validators ? field.validators.validation : undefined,
        field.asyncValidators ? field.asyncValidators.validation : undefined,
      );
    } else {
      throw new Error(`[Formly Error] You cannot pass an Observable to a multicheckbox yet.`);
    }
  }
}
//CHECKBOX
@Component({
  selector: "formly-field-checkbox",
  template: `
    <label class="col-xs-12 col-12" *ngIf="align">&nbsp;</label>
    <label class="custom-control custom-checkbox">
      <input [id]="for" type="checkbox" [formControl]="formControl"
        *ngIf="!to.hidden" value="on"
        [formlyAttributes]="field" class="custom-control-input"/>
        <span class="custom-control-indicator"></span>
    </label>
    <label [for]="for" class="custom-checkBox-label" >{{ to.label }}</label>
  `
})

export class FormlyFieldCheckbox extends FieldType {
  get for(): any {
    return  this.model[this.to["for"]] || this.to.label;
  }
  get align() {
    return  this.to["align"]|| false;
  }
  static createControl(model: any, field: FormlyFieldConfig): AbstractControl {
    return new FormControl(
      {
        value: model ? "on" : undefined,
        disabled: field.templateOptions.disabled
      },
      field.validators ? field.validators.validation : undefined,
      field.asyncValidators ? field.asyncValidators.validation : undefined
    );
  }
}

@Component({
  selector: 'formly-repeat-section',
  template: `
    <div class="{{field.className}}" *ngFor="let field of field.fieldGroup; let i = index;">
      <div class="{{to.buttonFatherClass}}" *ngIf="to.showRemoveButton" class="">
        <label>&nbsp;</label>
        <button class="{{to.buttonClass}}" type="button" (click)="remove(i)"><i class="{{to.buttonIconClass}}"></i>{{to.buttonText}} </button>
      </div>
      <formly-group
        [model]="model[i]"
        [field]="field"
        [options]="options"
        [form]="formControl">
      </formly-group>
    </div>
  `,
})
export class RepeatTypeComponent extends FieldArrayType {
  get showRemoveButton(): boolean {
    return this.to.showRemoveButton || false;
  }
  get buttonClass(): string {
    return this.to.buttonClass || '';
  }
  get buttonIconClass(): string {
    return this.to.buttonIconClass || '';
  }
  get buttonText(): string {
    return this.to.buttonText || '';
  }
  get buttonFatherClass(): string {
    return this.to.buttonFatherClass || '';
  }

  constructor(builder: FormlyFormBuilder) {
    super(builder);
  }
}

//RADIO
@Component({
  selector: "formly-field-radio",
  template: `
    <div [formGroup]="form">
      <div *ngFor="let option of to.options" class="radio" [ngClass]="inputClass">
        <label class="custom-control custom-radio">
          <input [name]="id" type="radio" [value]="option.key" [formControl]="formControl"
          [formlyAttributes]="field" class="custom-control-input"  >
          {{ option.value }}
          <span class="custom-control-indicator"></span>
        </label>
      </div>
    </div>
  `
})
export class FormlyFieldRadio extends FieldType {
  get inputClass(): string {
    return this.to.inputClass || "";
  }
}

//HORIZONTAL WRAPPER
@Component({
  selector: "formly-horizontal-input-type",
  template: `<div class="row">
      <label attr.for="{{key}}" class="col-md-3 form-control-label">{{ to.label }}</label>
      <div class="col-md-9">
        <ng-template #fieldComponent></ng-template>
      </div>
    </div>`
})
export class FormlyHorizontalWrapper extends FieldWrapper {
  @ViewChild("fieldComponent", { read: ViewContainerRef })
  fieldComponent: ViewContainerRef;
}

//FIELDSET WRAPPER
@Component({
  selector: "formly-wrapper-fieldset",
  template: `
    <div class="form-group-sm" [class.has-error]="showError">
      <ng-template #fieldComponent></ng-template>
    </div>
  `
})
export class FormlyWrapperFieldset extends FieldWrapper {
  @ViewChild("fieldComponent", { read: ViewContainerRef })
  fieldComponent: ViewContainerRef;
}

export const NgFormlyConfig: ConfigOption = {
  wrappers: [
    { name: 'horizontalWrapper', component: FormlyHorizontalWrapper },
    { name: 'fieldset', component: FormlyWrapperFieldset },
    { name: 'validationDateTme',component : FormlyWrapperValidationMessages}
  ],
  types:
  [
    { name: 'datepicker',component: DatepickerTypeComponent},
    { name: 'datetimepicker',component: DateTimePickerTypeComponent},
    { name: 'checkbox', component: FormlyFieldCheckbox },
    { name: 'button', component: FormlyFieldButton },
    { name: 'repeat', component: RepeatTypeComponent },
    { name: 'radio', component: FormlyFieldRadio },
    { name: 'horizontalRadio', extends: 'radio', wrappers: ['fieldset', 'horizontalWrapper'] },
    { name: 'multicheckbox', component: FormlyFieldMultiCheckbox },

  ],

  validators: [
    // { name: 'required', validation: Validators.required},
    // { name: 'invalidEmailAddress', validation: ValidationService.emailValidator},
    // { name: 'invalidPassword', validation: ValidationService.passwordValidator},
    // { name: 'invalidPeriod', validation: ValidationService.periodValidator},
  ],
  validationMessages: [
    { name: 'required', message:  'Ce champ est obligatoire'},
    { name: 'invalidEmailAddress', message:'Adresse mail invalide'},
    { name: 'invalidPassword', message:  'Le mot de passe doit contenir au minimum 8 caractères avec au moins une majuscule, une miniscule et un chiffre.'},
    { name: 'invalidDebutPeriod', message:'La date de début doit être inférieur à la date de fin'},
    { name: 'invalidFinPeriod', message: 'La date de fin doit être supérieur à la date de début'},
  ]
 };
