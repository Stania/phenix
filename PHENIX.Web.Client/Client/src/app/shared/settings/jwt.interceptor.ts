import { Injectable, Inject, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/do';
import { AuthenticationService } from '../services/administration/authentication.service';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  headerName: string;
  authScheme: string;
  whitelistedDomains: Array<string | RegExp>;
  throwNoTokenError: boolean;
  auth : AuthenticationService;
  constructor(
    public inj: Injector
  ) {
    this.headerName =  'authorization';
    this.authScheme = 'bearer ';
    this.throwNoTokenError = false;

  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.auth = this.inj.get(AuthenticationService);
    // enrichir la requete avec le token d'authetification
    const token = this.auth.getToken();
    const requestWhithAddedHeader = this.handleInterception(token, request, next);
    // envoyer la requete et attendre le retour pour gérer la non authorization
    return requestWhithAddedHeader.do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          // faire quelque chose
          // afficher une modale par exemple
        }
      }
    });
  }

  handleInterception(
    token: string,
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!token && this.throwNoTokenError) {
      throw new Error('Impossible de récupérer le token.');
    }
    if (!token) {
      request = request.clone();
    }  else if (token) {
      request = request.clone({
        setHeaders: {
          [this.headerName]: `${this.authScheme}${token}`
        }
      });
    }
    return next.handle(request);
  }
}
