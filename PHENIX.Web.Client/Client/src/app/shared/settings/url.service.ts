import { Injectable } from '@angular/core';

@Injectable()
export class UrlService {
    public urlAthentification: string;
    public urlChoixProfil: string;
    private urlApi: string;
    constructor() {
        this.urlApi = 'http://localhost:52112/';
       //this.urlApi = 'http://localhost:5001/';
       
        //url du serveur
        //phenix_local http://serveurnextapp:9999/
        // this.urlApi = 'http://serveurclic:9998/';

        //phenix_local2 http://serveurnextapp:9989/
        //this.urlApi = 'http://serveurclic:9988/';

        //phenix_local3 http://serveurnextapp:9979/
        //this.urlApi = 'http://serveurclic:9978/';

        this.urlAthentification = 'api/identity';
        this.urlChoixProfil = 'api/identity/ChoixProfil'
    }
    public getUrlApi(): string {
        return this.urlApi;
    }
    public getAuthenticationUrl(): string {
        return this.urlAthentification;
    }
    public getUrlChoixProfil(): string {
      return this.urlChoixProfil;
  }
}
