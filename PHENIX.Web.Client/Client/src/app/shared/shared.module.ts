import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ToasterModule } from 'angular2-toaster/angular2-toaster';

import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { RatingModule } from 'ngx-bootstrap/rating';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { DatepickerModule } from 'ngx-bootstrap/datepicker';

import { FlotDirective } from './directives/flot/flot.directive';
import { SparklineDirective } from './directives/sparkline/sparkline.directive';
import { EasypiechartDirective } from './directives/easypiechart/easypiechart.directive';
import { ColorsService } from './colors/colors.service';
import { CheckallDirective } from './directives/checkall/checkall.directive';
import { VectormapDirective } from './directives/vectormap/vectormap.directive';
import { NowDirective } from './directives/now/now.directive';
import { ScrollableDirective } from './directives/scrollable/scrollable.directive';
import { JqcloudDirective } from './directives/jqcloud/jqcloud.directive';
import { InMemoryZepsService } from '../routes/synoptique/services/InMemoryDbService';
import {  SynoptiqueService } from '../routes/synoptique/services/synoptique.service';

import {FormlyModule} from '@ngx-formly/core';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import { UtilisateurService } from './services/administration/utilisateur.service';
import { ZepService } from '../shared/services/ficheZep/zep.service';
import { UrlService } from './settings/url.service';
import { DatepickerTypeComponent, NgFormlyConfig, DateTimePickerTypeComponent, FormlyFieldCheckbox,FormlyFieldMultiCheckbox, FormlyFieldButton, RepeatTypeComponent, FormlyHorizontalWrapper, FormlyWrapperFieldset, FormlyFieldRadio, FormlyWrapperValidationMessages } from './settings/formly-fields';
import { BsDatepickerModule, defineLocale} from 'ngx-bootstrap';
import { fr } from 'ngx-bootstrap/locale';
import { ValidationService } from './settings/validators.service';
defineLocale('fr',fr);
import * as moment from 'moment';
import * as locales from 'moment/min/locales';

moment.locale('fr-fr');

import { PopoverModule } from 'ngx-bootstrap/popover';


import { ImportZepService } from './services/ficheZep/importZep.service';
import { Ng4FilesModule } from './directives/ng4-files';

import { DfvListService } from './services/ficheDfv/dfv-list.service';
import { OccupationService } from './services/occupation/occupation.service';
import { ManoeuvreService } from './services/manoeuvre/manoeuvre.service';
import { PointRemarquableService } from './services/pointRemarquable/point_remarquable.service';
import { PhenixToasterService } from './settings/phenix-toaster.service';
import { MomentModule } from 'angular2-moment';
import { DepechesService } from './services/ficheDfv/depeches.service';
import { MesuresService } from './services/ficheDfv/mesures.service';
import { AutreDepecheService } from './services/autreDepeche/autre_depeche.service';
import { RestitutionService } from './services/ficheDfv/restitution.service';
import { VarDirective } from './directives/variables/variable.directive';
import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';
import { FilterDfvPipe } from './directives/pipes/filterDfv.pipe';
import { OrderByStatutPipe } from './directives/pipes/sortDfvState.pipe';
import { FicheZepProcedesDfvPositionAiguillesComponent } from '../routes/fiche-zep/fiche-zep-procede-dfv/fiche-zep-procedes-dfv-position-aiguilles/fiche-zep-procedes-dfv-position-aiguilles.component';
import { FicheZepProcedesDfvMesureProtectionComponent } from '../routes/fiche-zep/fiche-zep-procede-dfv/fiche-zep-procedes-dfv-mesure-protection/fiche-zep-procedes-dfv-mesure-protection.component';

// https://angular.io/styleguide#!#04-10
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        AccordionModule.forRoot(),
        AlertModule.forRoot(),
        ButtonsModule.forRoot(),
        CarouselModule.forRoot(),
        CollapseModule.forRoot(),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        PaginationModule.forRoot(),
        ProgressbarModule.forRoot(),
        RatingModule.forRoot(),
        TabsModule.forRoot(),
        TimepickerModule.forRoot(),
        TooltipModule.forRoot(),
        PopoverModule.forRoot(),
        TypeaheadModule.forRoot(),
        DatepickerModule.forRoot(),
        BsDatepickerModule.forRoot(),
        FormlyModule.forRoot(NgFormlyConfig),
        FormlyBootstrapModule,
        MomentModule,
        Ng4FilesModule,
        LoadingModule.forRoot({
            animationType: ANIMATION_TYPES.rectangleBounce,
            backdropBackgroundColour: 'rgba(0,0,0,0.1)',
            primaryColour: '#30bbe7',
            secondaryColour: '#ffffff',
            tertiaryColour: '#ffffff',
            fullScreenBackdrop : true
          }),
    ],
    providers: [
        ColorsService,
        SynoptiqueService,
        ZepService,
        UrlService,
        UtilisateurService,
        ValidationService,
        ImportZepService,
        DfvListService,
        OccupationService,
        ManoeuvreService,
        PointRemarquableService,
        PhenixToasterService,
        DepechesService,
        MesuresService,
        AutreDepecheService,
        RestitutionService
    ],
    declarations: [
        FlotDirective,
        SparklineDirective,
        EasypiechartDirective,
        CheckallDirective,
        VectormapDirective,
        NowDirective,
        ScrollableDirective,
        JqcloudDirective,
        VarDirective,
        DatepickerTypeComponent,
        DateTimePickerTypeComponent,
        FormlyFieldCheckbox,
        FormlyFieldButton,
        RepeatTypeComponent,
        FormlyHorizontalWrapper,
        FormlyWrapperFieldset,
        FormlyFieldRadio,
        FormlyFieldMultiCheckbox,
        FormlyWrapperValidationMessages,
        FilterDfvPipe,
        OrderByStatutPipe,
        FicheZepProcedesDfvPositionAiguillesComponent,
        FicheZepProcedesDfvMesureProtectionComponent,
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        RouterModule,
        AccordionModule,
        AlertModule,
        ButtonsModule,
        CarouselModule,
        CollapseModule,
        BsDropdownModule,
        ModalModule,
        PaginationModule,
        ProgressbarModule,
        PopoverModule,
        RatingModule,
        TabsModule,
        TimepickerModule,
        TooltipModule,
        TypeaheadModule,
        FlotDirective,
        SparklineDirective,
        EasypiechartDirective,
        CheckallDirective,
        VectormapDirective,
        VarDirective,
        NowDirective,
        ScrollableDirective,
        JqcloudDirective,
        DatepickerModule,
        FormlyModule,
        FormlyBootstrapModule,
        BsDatepickerModule,
        Ng4FilesModule,
        MomentModule,
        LoadingModule,
        FilterDfvPipe,
        OrderByStatutPipe,
        LoadingModule,
        FicheZepProcedesDfvPositionAiguillesComponent,
        FicheZepProcedesDfvMesureProtectionComponent,
    ],
    entryComponents:[]
})

// https://github.com/ocombe/ng2-translate/issues/209
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule
        };
    }
}
